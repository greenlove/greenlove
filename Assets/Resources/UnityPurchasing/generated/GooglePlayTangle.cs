#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("vv5IcxyJLqu9kEtHFo/BYKFeHMtXLj92Thml1bhmY4Nun4y2ZZsUvL8rvQtfMoyqRVjOLjPgEfRoguaVnIn1iWJuNh82cSRVcyqsCsLkRF6bwI1xuUUIh5sFBLIcwWgMeNfFjL3buGxiMfv5XMiSUhoKSX99q6Ksuzg2OQm7ODM7uzg4OYdL6+mG7FVoH6FaXUAZF+/p5VgtpF50q7/dWgm7OBsJND8wE79xv840ODg4PDk6HvQ6JNMda/1uO+jQj42qCPK2WucPDE3Bb+Bn++/d8EpLQgBx6PzBmZAGTYbrtJv+iwCBJHa0Wka499ifNl04FLURbkksrZP+iDy2NVeZaP0muOjVpIHImIUOMCADe1745ObmefQ2flqpRd3Y0Ds6ODk4");
        private static int[] order = new int[] { 2,8,9,6,6,5,8,12,9,10,11,12,13,13,14 };
        private static int key = 57;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
