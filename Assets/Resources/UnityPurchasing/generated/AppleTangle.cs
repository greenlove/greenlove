#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("ziahHYleYgWFicbZH5aomSUe6maJyMfNicrM293Az8DKyN3AxseJ2d7eh8jZ2cXMh8rGxIbI2dnFzMrIj5mNr6r8raK6tOjZ2cXMierM292J6uiZK6iLmaSvoIMv4S9epKioqJybmJ2Zmp/zvqSanJmbmZCbmJ2Zy8XMidrdyMfNyNvNid3M28TaicjdwcbbwN3QmL+Zva+q/K2quqTo2fAOrKDVvun/uLfdeh4iipLuCnzG3cDPwMrI3cyJy9CJyMfQidnI292DL+EvXqSoqKysqZnLmKKZoK+q/KypqiuopqmZK6ijqyuoqKlNOACgofeZK6i4r6r8tImtK6ihmSuorZkXXdoyR3vNpmLQ5p1xC5dQ0VbCYc2cirzivPC0Gj1eXzU3ZvkTaPH52cXMifvGxt2J6uiZt76kmZ+ZnZuvmaavqvy0uqioVq2smaqoqFaZtJm4r6r8raO6o+jZ2cXMieDHyoeY28jK3cDKzIna3cjdzMTMx93ah5nHzYnKxsfNwN3AxsfaicbPidzazNCJyNra3MTM2onIysrM2d3Ix8rMjUtCeB7ZdqbsSI5jWMTRRE4cvr7s17blwvk/6CBt3cuiuSroLpojKAIK2Dvu+vxoBoboGlFSStlkTwrlpjSUWoLggbNhV2ccEKdw97V/YpQckwRdpqepO6IYiL+H3XyVpHLLv58w5YTRHkQlMnVa3jJb33vemeZowM/AysjdwMbHiejc3cHG28Dd0JjTmSuo35mnr6r8tKaoqFatraqrqJkrrRKZK6oKCaqrqKurqKuZpK+gHrIUOuuNu4NuprQf5DX3ymHiKb6GmShqr6GCr6isrK6rq5koH7MoGonGz4ndwcyJ3cHMx4nI2dnFwMrIIrAgd1DixVyuAouZq0Gxl1H5oHqkr6CDL+EvXqSoqKysqaorqKip9dboATFQeGPPNY3CuHkKEk2yg2q2oYKvqKysrquov7fB3d3Z2pOGht4YmfFF862bJcEaJrR3zNpWzvfMFa2vuqv8+pi6mbivqvyto7qj6NnZKb2CecDuPd+gV13CJIfpD17u5NaUj86JI5rDXqQrZndCCoZQ+sPyzSbaKMlvsvKghjsbUe3hWcmRN7xcmp/zmcuYopmgr6r8ra+6q/z6mLoBddeLnGOMfHCmf8J9C42KuF4IBa+q/LSnrb+tvYJ5wO4936BXXcIkYLDbXPSnfNb2MluMqhP8JuT0pFjgcd82mr3MCN49YISrqqipqAorqLY4crfu+UKsRPfQLYRCnwv+5fxFK6ipr6CDL+EvXsrNrKiZKFuZg6/ZxcyJ6szb3cDPwMrI3cDGx4no3IWJyszb3cDPwMrI3cyJ2cbFwMrQPDfTpQ3uIvJ9v56aYm2m5Ge9wHiuRdSQKiL6iXqRbRgWM+ajwlaCVWnKmt5ek66F/0JzpoincxPasOYcxcyJ4MfKh5iPmY2vqvytorq06Nm/mb2vqvytqrqk6NnZxcyJ+8bG3XCf1mgu/HAOMBCb61JxfNg31wj7h+kPXu7k1qH3mbavqvy0iq2xmb/7zMXAyMfKzInGx4ndwcDaicrM27YsKiyyMJTunlsAMuknhX0YObtx+QMjfHNNVXmgrp4Z3NyI");
        private static int[] order = new int[] { 21,45,36,10,13,7,42,7,52,39,45,14,33,50,20,42,29,39,46,43,40,53,25,24,25,25,48,40,28,53,59,37,41,53,47,39,39,48,39,41,56,55,52,54,59,54,56,52,52,50,59,56,56,57,54,57,58,58,58,59,60 };
        private static int key = 169;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
