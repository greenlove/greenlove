﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Ads;
using BuildingBlocks;
using BuildingBlocks.Backend;
using Gameplay;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class Main : SerializedMonoBehaviour {
    [SerializeField] private List<Controller> controllers;
    private Dictionary<Type, Controller> controllersHash;
    protected bool isInitialized = false;

    protected virtual void Awake ()
    {
    }

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
        float dt = Time.deltaTime;

        foreach (KeyValuePair<Type, Controller> item in controllersHash)
        {
            item.Value.preupdate(dt);
        }

        foreach (KeyValuePair<Type, Controller> item in controllersHash)
        {
            item.Value.update(dt);
        }

        foreach (KeyValuePair<Type, Controller> item in controllersHash)
        {
            item.Value.postupdate(dt);
        }
    }

    public void initHash()
    {
        controllersHash = new Dictionary<Type, Controller>();
        foreach (Controller item in controllers)
        {
            controllersHash.Add(item.GetType(), item);
        }
    }

    protected virtual void init()
    {
        foreach (KeyValuePair<Type, Controller> item in controllersHash)
        {
            item.Value.init(this);
        }
    }

    public virtual void postinit()
    {
        foreach (KeyValuePair<Type, Controller> item in controllersHash)
        {
            item.Value.postinit();
        }
    }

    public T getController<T>() where T : class
    {
#if UNITY_EDITOR
        if (controllersHash == null) initHash();
#endif
        if(!controllersHash.ContainsKey(typeof(T))) return default;

        return controllersHash[typeof(T)] as T;
    }

    internal void stop()
    {
        foreach (Controller item in controllers)
        {
            item.stop();
        }
    }
}
