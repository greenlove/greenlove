﻿using BuildingBlocks.Backend;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CityMain : Main
{
    public DoubleVariable currentCityMode;
    public DoubleVariable lastCityMode;
    public DoubleVariable maxCityModes;

    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 60;
        initHash();
    }

    protected override void Start()
    {
        base.Start();

        init();
        isInitialized = true;

        getController<SavefileController>().load(null);
        postinit();

        getController<ColorizableController>().postinit();

    }

    private void OnApplicationFocus(bool focus)
    {
        if (!isInitialized) return;

        if(focus) getController<ServerController>().recoverFromApplicationPause();
        else getController<ServerController>().goToApplicationPause();
    }

    private void OnApplicationPause(bool pause)
    {
        if (!isInitialized) return;
        //Dunno
    }

    protected override void init()
    {
        base.init();

        currentCityMode.value = 0;
        lastCityMode.value = 0;
    }

    public void changeCityMode()
    {
        lastCityMode.value = currentCityMode.value;

    }

    private int getNextCityIndex()
    {
        return (int)(currentCityMode.value + 1 >= maxCityModes.value ? 0 : currentCityMode.value + 1);
    }
}
