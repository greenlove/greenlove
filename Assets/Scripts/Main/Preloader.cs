﻿using Boomlagoon.JSON;
using BuildingBlocks.Backend;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Preloader : Main
{
    [SerializeField]
    private int scene;
    AsyncOperation sceneAsync;

    [SerializeField]
    private int cutSceneIndex;

    [SerializeField]
    private Text loadingText;

    [SerializeField]
    private GameObject loadingFront;

    private bool isLoaded;

    protected override void Start()
    {
        initHash();
        init();

        JSONValue hasBeenCutsceneLoaded = getController<SavefileController>().getSingleEntry("hasLoadedCutscene");
        loadingText.text = getController<LocalizationController>().getTextByKey("PRELOADER_LOADING");
        isLoaded = false;

        if (hasBeenCutsceneLoaded == null)
        {
            StartCoroutine(loadCutscene());
        }
        else if(hasBeenCutsceneLoaded.Boolean)
        {
            StartCoroutine(loadMainScene(true));
        }
    }

    protected override void Update()
    {
        if(!isLoaded) loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
    }


    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator loadCutscene()
    {
        //Wait a bit
        yield return new WaitForSeconds(3);
        
        AsyncOperation cutsceneAsync = SceneManager.LoadSceneAsync(cutSceneIndex, LoadSceneMode.Additive);
        cutsceneAsync.completed += cutsceneAsyncCompleted;
    }


    private void cutsceneAsyncCompleted(AsyncOperation obj)
    {
        isLoaded = true;

        Destroy(loadingFront);
        StartCoroutine(loadMainScene());

        GameObject anim = Array.Find(SceneManager.GetSceneAt(1).GetRootGameObjects(), gameObject => gameObject.name == "Timeline");
        anim.GetComponent<PlayableDirector>().stopped += onCutsceneAnimFinished;
    }

    private void onCutsceneAnimFinished(PlayableDirector obj)
    {
        sceneAsync.allowSceneActivation = true;
    }

    private IEnumerator loadMainScene(bool allowActivation = false)
    {
        sceneAsync = SceneManager.LoadSceneAsync(scene);
        sceneAsync.allowSceneActivation = allowActivation;

        while(!sceneAsync.isDone)
        {
            yield return null;
        }
    }
}
