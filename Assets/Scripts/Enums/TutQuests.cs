public enum TutQuests
{
	NONE = -1,
	RECRUIT_DOGS_01,
	HIRE_ALPHA_02,
	RECRUIT_DOGS_03,
	SEND_TO_SCOUT_04,
	RECRUIT_DOGS_05,
	PICK_GIFTS_06,
	ENLARGE_HOME_07,
	PICK_GIFTS_08,
	UPGRADE_BUFFS_09,
	SEND_TO_SLEEP_10,
	RECRUIT_DOGS_11,
	ENLARGE_HOME_12,
	TAP_BALLOONS_12,
	RECRUIT_ANIMALS_13,
	UPGRADE_BUFF_14,
	RECRUIT_ANIMALS_15,
	IMPROVE_BUILDING_16,
	PICK_GIFTS_17,
	SEND_TO_EAT_18,
	TAP_BALLOONS_19,
	PICK_GIFTS_20,
	IMPROVE_BUILDING_21,
	RECRUIT_ANIMALS_22,
	SEND_TO_PLAY_23,
	EVOLVE_BUILDING_24,
	BUILD_BATHS_25,
	SEND_TO_EAT_26,
	IMPROVE_BUILDING_27,
	TAP_BALLOONS_28,
	RECRUIT_ANIMALS_29,
	BUILD_BATHS_30,
	RECRUIT_ANIMALS_31,
	SEND_TO_BATH_32,
	IMPROVE_BUILDING_33,
	IMPROVE_BUILDING_34,
	RECRUIT_ANIMALS_35,
	SEND_TO_SCOUT_36,
	RECRUIT_ANIMALS_37,
	OBTAIN_INVENTORY_ITEMS_38,
	BUILD_HOSPITAL_39,
	EVOLVE_BUILDING_40,
	BUILD_RESTAURANT_41,
	TAP_VEHICLES_42,
	FILL_STOCK_43,
	SEND_TO_EAT_44,
	IMPROVE_BUILDING_45,
	IMPROVE_BUILDING_46,
	SEND_TO_SCOUT_47,
	RECRUIT_ANIMALS_48,
	SEND_TO_SCOUT_49,
	SEND_TO_SCOUT_50,
	RECRUIT_ANIMALS_51,
	RECRUIT_ANIMALS_52,
	SEND_TO_SCOUT_53,
	IMPROVE_BUILDING_54,
	SEND_TO_BATH_55,
	SEND_TO_EAT_56,
	SEND_TO_SCOUT_57,
	RECRUIT_ANIMALS_58,
	UPGRADE_BUFF_59,
	SEND_TO_SCOUT_60,
	SEND_TO_PLAY_61,
	FILL_STOCK_62,
	BUILD_BATHS_63,
	RECRUIT_ANIMALS_64,
	SEND_TO_PLAY_65,
	FILL_STOCK_66,
	BUILD_PLAYGROUND_67,
	EVOLVE_BUILDING_68,
	RECRUIT_ANIMALS_69,
	IMPROVE_BUILDING_70,
	OBTAIN_INVENTORY_ITEMS_71,
	BUILD_PLAYGROUND_72,
	PICK_GIFTS_73,
	EVOLVE_BUILDING_74,
	EVOLVE_BUILDING_75,
	RECRUIT_ANIMALS_76,
	TAP_BALLOONS_77,
	PICK_GIFTS_78,
	RECRUIT_ANIMALS_79,
	UPGRADE_BUFF_80,
	SEND_TO_EAT_81,
	SEND_TO_PLAY_82,
	PICK_GIFTS_83,
	RECRUIT_ANIMALS_84,
	TAP_BALLOONS_85,
	SEND_TO_PLAY_86,
	SEND_TO_PLAY_87,
	PICK_GIFTS_88,
	UPGRADE_BUFF_89,
	TAP_BALLOONS_90,
	IMPROVE_BUILDING_91,
	TAP_VEHICLES_92,
	TAP_VEHICLES_93,
	TAP_VEHICLES_94,
	RECRUIT_ANIMALS_95,
	IMPROVE_BUILDING_96,
	IMPROVE_BUILDING_97,
	RECRUIT_ANIMALS_98,
	RECRUIT_ANIMALS_99,
	UPGRADE_BUFF_100,
	FILL_STOCK_101,
	UPGRADE_BUFF_102,
	PICK_GIFTS_103,
	RECRUIT_ANIMALS_104,
	FILL_STOCK_105,
	IMPROVE_BUILDING_106,
	SEND_TO_SCOUT_107,
	UPGRADE_BUFF_108,
	IMPROVE_BUILDING_109,
	PICK_GIFTS_110,
	BUILD_PLAYGROUND_111,
	LENGTH = 112,
}
