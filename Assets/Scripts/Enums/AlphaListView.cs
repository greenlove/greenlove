public enum AlphaListView
{
	NONE = -1,
	ALPHAS_LIST,
	ALPHA_LIST_ITEM,
	PET_VIEW,
	ALPHA_LIST_ITEM_ACTION_BUTTON,
	LENGTH = 4,
}
