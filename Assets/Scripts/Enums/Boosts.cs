public enum Boosts
{
	NONE = -1,
	MATHILDE,
	ARCHANGEL,
	DANZON,
	LENGTH = 3,
}
