public enum AlphaPickerView
{
	NONE = -1,
	ALPHA_PICKER,
	ALPHA_PICKER_ITEM,
	LENGTH = 2,
}
