public enum TownHallViewGroup
{
	NONE = -1,
	TOWN_HALL_POP_UP,
	BUFF_ITEM,
	CRAFT_ITEM,
	LOCKED_BUFF,
	LENGTH = 4,
}
