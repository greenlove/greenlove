public enum TutorialGroup
{
	NONE = -1,
	CONVERSATION_PANEL,
	CONV_ITEM,
	ARROW,
	TALK_ITEM,
	LENGTH = 4,
}
