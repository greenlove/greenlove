public enum BottomButtons
{
	NONE = -1,
	CALL_BUTTON,
	TUTORIAL_QUEST_BUTTON,
	SETTINGS_BUTTON,
	LENGTH = 3,
}
