public enum AlphasUI
{
	NONE = -1,
	PET_VIEW,
	ALPHAS_LIST,
	ALPHAS_LIST_ITEM,
	LENGTH = 3,
}
