﻿using Boomlagoon.JSON;
using FlurrySDK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : IStorable
{
    private string apiKey;
    private Flurry.Builder flurry;
    public bool isOptOut;

    public AnalyticsManager()
    {
#if UNITY_EDITOR
        return;
#endif

#if UNITY_ANDROID
        apiKey = "DQQCDV3SM2Y7WKWF9J93";
#elif UNITY_IOS
        apiKey = "66V3FRCFCFWZTKNKWH7R";
#endif
        flurry = new Flurry.Builder().WithCrashReporting(true).WithLogEnabled(true).WithLogLevel(Flurry.LogLevel.VERBOSE).WithMessaging(true);

#if UNITY_IOS
        flurry = flurry.WithAppVersion(Application.version);
        Flurry.SetIAPReportingEnabled(true);
#elif UNITY_ANDROID
        Flurry.SetVersionName(Application.version);
#endif
        flurry.Build(apiKey);
    }

    public bool sendEvent(string id, params KeyValuePair<string, string>[] parameters)
    {
#if UNITY_EDITOR
        return false;
#endif
        Dictionary<string, string> eventParams = new Dictionary<string, string>();

        foreach (KeyValuePair<string, string> pair in parameters)
        {
            eventParams.Add(pair.Key, pair.Value);
        }

        Flurry.EventRecordStatus result = Flurry.LogEvent(id, eventParams);

        return result == Flurry.EventRecordStatus.FlurryEventRecorded;
    }

    internal void openFlurryPrivacyWindow()
    {
        Flurry.OpenPrivacyDashboard();
    }

    internal void setOptOut(bool optOut)
    {
        isOptOut = optOut;
        Flurry.SetDataSaleOptOut(optOut);
    }

    internal void deleteAll()
    {
        Flurry.DeleteData();
    }

    public void load(JSONObject jsonFile)
    {
        setOptOut(jsonFile.GetBoolean("opt_out"));
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("opt_out", isOptOut);

        return obj;
    }
}
