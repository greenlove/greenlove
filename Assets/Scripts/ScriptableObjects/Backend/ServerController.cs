﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Boomlagoon.JSON;
using BuildingBlocks.Backend;
using Sirenix.OdinInspector;
using UnityEngine.Analytics;

[CreateAssetMenu(menuName = "Building Blocks/Backend/Server Controller")]
public class ServerController : Controller
{
    private double lastServerTimestamp;
    [ReadOnly] public double serverTimestamp;

    //Hi Sneaky one (: We're a small company and we pay for the API calls, please be nice
    private string timeAPIPass = "HPBERA5EXTIV";

    [HideInInspector] public GreenLoveEvent<double> onElapsedTimeCalculated;
    [HideInInspector] public GreenLoveEvent<KeyValuePair<string, double>> onServerTime;

    [Header("Analytics")]
    private AnalyticsManager analytics;

    public override void init(Main parent)
    {
        base.init(parent);

        onElapsedTimeCalculated = new GreenLoveEvent<double>();

        onServerTime = new GreenLoveEvent<KeyValuePair<string, double>>();
        onServerTime.AddListener((KeyValuePair<string, double> cipheredTime) => 
        {
            if(cipheredTime.Key == "pause")
            {
                lastServerTimestamp = cipheredTime.Value;
            }
            else if (cipheredTime.Key == "restore")
            {
                serverTimestamp = cipheredTime.Value;
                onElapsedTimeCalculated.Invoke(timeDifferenceBetweenLastLoads());
            }
        });

        analytics = new AnalyticsManager();
    }

    internal void recoverFromApplicationPause()
    {
        getTimestamp("restore");
    }

    internal void goToApplicationPause()
    {
        getTimestamp("pause");
    }

    private UnityWebRequest CreateTimestampRequest()
    {
        return UnityWebRequest.Get(string.Format("http://api.timezonedb.com/v2.1/get-time-zone?key={0}&format=json&by=zone&zone=Africa/Conakry", timeAPIPass));
    }

    public void getTimestamp(string key)
    {
        context.StartCoroutine(requestTimestamp(key));
    }

    private IEnumerator requestTimestamp(string key)
    {
        UnityWebRequest www = CreateTimestampRequest();
        yield return www.SendWebRequest();

        if (!www.isNetworkError && !www.isHttpError)
        {
            onServerTime.Invoke(new KeyValuePair<string, double>(key, JSONObject.Parse(www.downloadHandler.text).GetNumber("timestamp")));
        }
        else
        {
            onServerTime.Invoke(new KeyValuePair<string, double>(string.Format("{0}_failure", key), 0));
        }
    }

    public double timeDifferenceBetweenLastLoads()
    {
        return timeDifference(serverTimestamp, lastServerTimestamp);
    }

    public double timeDifference(double nowStamp, double beforeStamp)
    {
        DateTime now = timeStampToDateTime(nowStamp);
        DateTime before = timeStampToDateTime(beforeStamp);

        return now.Subtract(before).TotalSeconds;
    }

    private DateTime timeStampToDateTime(double timestamp)
    {
        // Unix timestamp is seconds past epoch
        DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(timestamp).ToLocalTime();
        return dtDateTime;
    }

    public void sendAnalyticWithParams(string id, params KeyValuePair<string, string>[] parameters)
    {
        analytics.sendEvent(id, parameters);
    }

    internal void openFlurryPrivacyWindow()
    {
        analytics.openFlurryPrivacyWindow();
    }

    internal void openUnityPrivacyWindow()
    {
        DataPrivacy.FetchPrivacyUrl((string url) => { Application.OpenURL(url); }, null);
    }

    internal void toggleFlurryOpting()
    {
        analytics.setOptOut(!analytics.isOptOut);
    }

    internal void deleteFlurryData()
    {
        analytics.deleteAll();
    }

    internal bool isUnityIn()
    {
        return !Analytics.playerOptedOut;
    }

    internal bool flurryIn()
    {
        return !analytics.isOptOut;
    }

    public override void load(JSONObject jsonFile)
    {
        base.load(jsonFile);
        analytics.load(jsonFile.GetObject("analytics"));
    }

    public override JSONObject save()
    {
        JSONObject obj = base.save();

        obj.Add("analytics", analytics.save());

        return obj;
    }
}
