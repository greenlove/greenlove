﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BuildingBlocks
{
    [CreateAssetMenu(menuName = "Building Blocks/FrontEnd/TouchController")]
    public class TouchController : Controller {

        private Camera cam;

        public float updateLapse;
        private float updateBuffer;
        private Vector3 lastTouchedCoordinate;
        public Vector2 lastTouchCoordinateInScreen;
        public float upGestureThreshold;

        private GameObject onBeginTouchGameObject;

        [HideInInspector] public GreenLoveEvent<Dictionary<string, object>> onTouched;
        [HideInInspector] public GreenLoveEvent<Dictionary<string, object>> onReleased;
        [HideInInspector] public GreenLoveEvent<Dictionary<string, object>> onHold;

        public override void init(Main parent)
        {
            base.init(parent);

            updateBuffer = 0;
            cam = Camera.main;
            onBeginTouchGameObject = null;
            lastTouchedCoordinate = Vector3.negativeInfinity;
            Input.multiTouchEnabled = false;

            onTouched = new GreenLoveEvent<Dictionary<string, object>>();
            onReleased = new GreenLoveEvent<Dictionary<string, object>>();
            onHold = new GreenLoveEvent<Dictionary<string, object>>();
        }


        public override void update(float dt)
        {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
            if (Input.GetMouseButtonUp(0) && EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject())
            {
                RaycastHit hit = createRaycast(Input.mousePosition);
                GameObject touched = touchedGameObject(hit);
                
                if (touched != null) notifyGameObjectTouchReleased(touched, hit.point, Input.mousePosition);
            }

            else if (Input.GetMouseButtonDown(0) && EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject())
            {
                RaycastHit hit = createRaycast(Input.mousePosition);
                GameObject touched = touchedGameObject(hit);
                lastTouchedCoordinate = hit.point;
                if (touched != null) notifyGameObjectTouchBegan(touched, hit.point, getHit(Input.mousePosition));
            }

            else if (Input.GetMouseButton(0) && EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject())
            {
                lastTouchCoordinateInScreen = Input.mousePosition;
                notifyGameObjectHold(getHit(Input.mousePosition));
            }
#else
            if(Input.touchCount > 0)
            {
                Touch touch = Input.touches[0]; //We allow single touch only
                if (touch.phase == TouchPhase.Began)
                {
                    // Check if finger is over a UI element
                    if( EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        lastTouchCoordinateInScreen = touch.position;

                        RaycastHit hit = createRaycast(touch.position);
                        GameObject touched = touchedGameObject(hit);
                        lastTouchedCoordinate = hit.point;
                        if (touched != null) notifyGameObjectTouchBegan(touched, hit.point, getHit(touch.position));
                    }
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    // Check if finger is over a UI element
                    if ( EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        RaycastHit hit = createRaycast(touch.position);
                        GameObject touched = touchedGameObject(hit);
                        if (touched != null) notifyGameObjectTouchReleased(touched, hit.point, touch.position);
                    }
                }

                else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                {
                    // Check if finger is over a UI element
                    if( EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        lastTouchCoordinateInScreen = touch.position;
                        notifyGameObjectHold(getHit(touch.position));
                    }
                }
            }
#endif
        }

        private void notifyGameObjectTouchBegan(GameObject obj, Vector3 point, Vector3 pos)
        {
            onBeginTouchGameObject = obj;
            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "gameobject", obj },
                { "hitpoint", point },
                { "position", pos }
            };

            onTouched.Invoke(parameters);
        }

        private RaycastHit createRaycast(Vector3 position)
        {
            Ray ray = cam.ScreenPointToRay(position);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, float.PositiveInfinity);
            return hit;
        }

        private Vector3 getHit(Vector3 position)
        {
            Ray ray = cam.ScreenPointToRay(position);
            return ray.GetPoint(findController<CameraController>().distanceFromCameraToRigPoint);
        }

        private GameObject touchedGameObject(RaycastHit hit)
        {
            return hit.transform?.gameObject;
        }

        private void notifyGameObjectTouchReleased(GameObject obj, Vector3 point, Vector3 touch)
        {
            if (obj == onBeginTouchGameObject)
            {
                float holdDistance = (lastTouchedCoordinate - point).magnitude;

                Dictionary<string, object> parameters = new Dictionary<string, object>()
                {
                    { "hitpoint", point },
                    { "gameobject", obj },
                    { "is_holding", holdDistance > upGestureThreshold },
                    { "touch", touch }
                };

                onReleased.Invoke(parameters);

                onBeginTouchGameObject = null;
            }

        }

        private void notifyGameObjectHold(Vector3 point)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                { "hitpoint", point }
            };

            onHold.Invoke(parameters);
        }
    }
}