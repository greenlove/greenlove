﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColormodeChanger : MonoBehaviour
{
    public ColorizableController colors;

    [Button("Change color mode")]
    public void changeColorMode()
    {
        if (colors == null) return;

        colors.changeColorMode();
    }
}
