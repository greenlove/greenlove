﻿using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Colors/Colorizable Controller")]
public class ColorizableController : Controller
{
    public Material environmentMaterial;
    public List<Material> materials;
    public List<UIPalette> uIPalletes;

    private Dictionary<IColorListener, string> receivers;

    public DoubleVariable currentColorMode;
    [SerializeField] private DoubleVariable previousColorMode;
    public DoubleVariable maxColorModes;

    public override void init(Main main)
    {
        base.init(main);

        currentColorMode.value = 0;

        if (receivers == null) receivers = new Dictionary<IColorListener, string>();
    }

    public Color getColor(string color)
    {
        return uIPalletes[(int)currentColorMode.value].colors[color];
    }

    public Color getColor(string color, float alpha)
    {
        Color normalColor = getColor(color);
        Color newColor = new Color(normalColor.r, normalColor.g, normalColor.b, alpha);

        return newColor;
    }

    public Color getNextColor(string color)
    {
        return uIPalletes[getNextPaletteIndex()].colors[color];
    }

    public void subscribeToColorChanges(IColorListener receiver, string color)
    {
        if (receivers == null) receivers = new Dictionary<IColorListener, string>();
        if(!receivers.ContainsKey(receiver)) receivers.Add(receiver, color);
    }

    public void pullOutFromColorChanges(IColorListener receiver)
    {
        if (receivers == null) return;

        receivers.Remove(receiver);
    }

    public void setupColors()
    {
        foreach (Material material in materials)
        {
            material.DisableKeyword(string.Format("PALETTE_P0{0}", (int)previousColorMode.value + 1));
            material.EnableKeyword(string.Format("PALETTE_P0{0}", (int)currentColorMode.value + 1));
            material.SetInt("PALETTE", (int)currentColorMode.value);
        }

        talk();
    }

    private void talk()
    {
        if (receivers == null) return;

        foreach (KeyValuePair<IColorListener, string> receiver in receivers)
        {
            receiver.Key.onColorChanged(getColor(receiver.Value));
        }
    }

    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("current", currentColorMode.value);

        return obj;
    }

    public override void load(JSONObject jsonFile)
    {
        if (jsonFile == null) return;

        currentColorMode.value = jsonFile.GetNumber("current", 0);
    }

    public override void postinit()
    {
        base.postinit();
        setupColors();//Feed yummy color to the static ui
    }

    private int getNextPaletteIndex()
    {
        return (int)(currentColorMode.value + 1 >= maxColorModes.value ? 0 : currentColorMode.value + 1);
    }

    public void changeColorMode()
    {
        previousColorMode.value = currentColorMode.value;
        currentColorMode.value = getNextPaletteIndex();

        setupColors();
    }

    public bool exists(string color)
    {
        return uIPalletes[(int)currentColorMode.value].colors.ContainsKey(color);
    }
}
