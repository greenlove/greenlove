﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Colors/UI Palette")]
public class UIPalette : SerializedScriptableObject
{
    [NonSerialized, OdinSerialize, ShowInInspector]
    public Dictionary<string, Color> colors;
}
