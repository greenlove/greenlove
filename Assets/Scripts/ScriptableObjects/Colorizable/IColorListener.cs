﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColorListener
{
    Color getColor(string type);
    void setColor(Color color, float alpha);
    void subscribeToColor();
    void unsubscribeToColor();
    void onColorChanged(Color color);
}
