﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIColorReceiver : UIBehaviour, IColorListener
{
    [SerializeField]
    private MaskableGraphic graphic;

    public string desiredColorType;

    public bool overridesAlpha;

    [ShowIf("overridesAlpha"), Range(0, 1)]
    public float alpha;

    [SerializeField]
    private ColorizableController colorController;

    protected override void Start()
    {
        if(string.IsNullOrEmpty(desiredColorType) || new Regex("^[0-9]+$").Match(desiredColorType).Success)
        {
            Debug.LogWarning(string.Format("The listener on the element {0} has a wrong key [{1}], we skip subscription", name, desiredColorType));
            return;
        }
        subscribeToColor();
        setColor(getColor(desiredColorType), alpha);
    }

    public void onColorChanged(Color color)
    {
        setColor(color, alpha);
    }

    protected override void OnDestroy()
    {
        unsubscribeToColor();
    }

    public Color getColor(string type)
    {
        return colorController.getColor(type);
    }

    public void setColor(Color color, float alpha)
    {
        Color c = new Color(color.r, color.g, color.b, overridesAlpha? alpha : color.a);
        graphic.color = c;
    }

    public void subscribeToColor()
    {
        colorController.subscribeToColorChanges(this, desiredColorType);
    }

    public void unsubscribeToColor()
    {
        colorController.pullOutFromColorChanges(this);
    }
}
