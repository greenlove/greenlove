﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Ratings/Rating Controller")]
public class RatingsController : Controller, IBankListener
{
    private float lastTimeWeAskedTimestamp;
    
    [SerializeField] private int maxTimesAskingForRating;
    [SerializeField] private float minPeriodBetweenRatingsInMinutes;
    [SerializeField] private Currency[] currenciesToListen;
    [SerializeField] private int howManyHaveWeAskedAlready;

    public bool hasRatedYet;


    public override void init(Main main)
    {
        base.init(main);

        hasRatedYet = false;
        howManyHaveWeAskedAlready = 0;
        lastTimeWeAskedTimestamp = 0;

        subscribeToBankChanges();
    }


    public void onListeningToBank(Currency currency, double delta, double total, string formattedTotal)
    {
        if (isStopped) return;

        if (!hasRatedYet)
        {
            if (total > 0 && howManyHaveWeAskedAlready < maxTimesAskingForRating)
            {
                float now = Time.time;
                if (now - lastTimeWeAskedTimestamp > minPeriodBetweenRatingsInMinutes * 60)
                {
                    lastTimeWeAskedTimestamp = now;
                    howManyHaveWeAskedAlready++;

                    context.StartCoroutine(findController<GameplayController>().invokeRatingsPopup(this, 15));
                }
            }
        }
        else
        {
            unsubscribeToBankChanges();
        }
    }

    public void subscribeToBankChanges(bool skipTalk = false)
    {
        foreach (Currency currency in currenciesToListen)
        {
            findController<GameplayController>().gameplayManager.bank.onSubscribe(this, currency, skipTalk);
        }
    }

    public void unsubscribeToBankChanges()
    {
        foreach (Currency currency in currenciesToListen)
        {
            findController<GameplayController>().gameplayManager.bank.onUnsubscribe(this, currency);
        }
    }

    public override void load(JSONObject jsonFile)
    {
        lastTimeWeAskedTimestamp = (float)jsonFile.GetNumber("timestamp");
    }

    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("timestamp", lastTimeWeAskedTimestamp);

        return obj;
    }
}
