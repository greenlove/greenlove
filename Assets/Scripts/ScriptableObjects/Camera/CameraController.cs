﻿using BuildingBlocks;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Camera Controller")]
public class CameraController : Controller
{
    public Camera cam;

    [HideInInspector]
    public Transform rig;
    Vector3 beginTouch;
    Vector3 initPosition;

    private float horizontalLimit;
    private float verticalLimit;
    private float screenWidth;
    bool dragHasBegun;

    private GameObject bounds;
    public Bounds plane;

    public float distanceFromCameraToRigPoint;

    [SerializeField]
    private bool useSmoothSwipping;

    [SerializeField, ShowIf("useSmoothSwipping"), PropertyRange(0.0f, 1.0f)]
    private float currentBlend;

    [SerializeField, ShowIf("useSmoothSwipping"), PropertyRange(0.0f, 1.0f)]
    private float nextBlend;

    [SerializeField, ShowIf("useSmoothSwipping"), PropertyRange(0.0f, 1.0f)]
    private float dragTolerance;

    private bool isFollowing;
    private GameObject followedTarget;
    [HideInInspector] public GreenLoveEvent<GameObject> onFollowing;

    public override void init(Main ctx)
    {
        base.init(ctx);

        cam = Camera.main;

        rig = cam.transform.parent;

        bounds = GameObject.Find("CameraBoundaries");
        if (bounds != null) plane = bounds.GetComponent<MeshRenderer>().bounds;

        initPosition = rig.position;
        dragHasBegun = false;

        distanceFromCameraToRigPoint = Vector3.Distance(cam.transform.position, rig.position);

        horizontalLimit = bounds != null ? Math.Abs(plane.center.x - plane.extents.x) / 2 : float.MaxValue;
        verticalLimit = bounds != null ? Math.Abs(plane.center.z - plane.extents.z) / 2 : float.MaxValue;

        Ray ray1 = cam.ScreenPointToRay(new Vector3(0, 0, 0));
        RaycastHit hit1;
        Physics.Raycast(ray1, out hit1, float.PositiveInfinity);

        Ray ray2 = cam.ScreenPointToRay(new Vector3(Screen.width / 2, 0, 0));
        RaycastHit hit2;
        Physics.Raycast(ray2, out hit2, float.PositiveInfinity);

        //Cojo como referencia el ancho de la pantalla, ya que al ser vista isométrica con 45º va a ser siempre más grande el ancho que el alto del área que tomemos como referencia
        screenWidth = Mathf.Abs(hit1.point.x - hit2.point.x);

        isFollowing = false;
        onFollowing = new GreenLoveEvent<GameObject>();

        findController<TouchController>().onReleased.AddListener((Dictionary<string, object> parameters) =>
        {
            finishDrag();
        });

        findController<TouchController>().onTouched.AddListener((Dictionary<string, object> parameters) =>
        {
            beginDrag((Vector3)parameters["position"]);
        });

        findController<TouchController>().onHold.AddListener((Dictionary<string, object> parameters) =>
        {
            drag((Vector3)parameters["hitpoint"]);
        });
    }

    internal void drag(Vector3 coordinate)
    {
        if (isStopped) return;

        float distance = Vector3.Distance(beginTouch, coordinate);

        if (distance > 0 && dragHasBegun)
        {
            Vector3 relative = (beginTouch - coordinate).normalized;
            Vector3 pos = rig.position;

            //easing
            //x = (0.9f * x) + (0.1f * target);

            //Original method
            float xVal = pos.x + ((relative.x + relative.y) * distance);
            float zVal = pos.z + ((relative.z + relative.y) * distance);

            if (useSmoothSwipping)
            {
                xVal = pos.x * currentBlend + xVal * nextBlend;
                zVal = pos.z * currentBlend + zVal * nextBlend;
            }

            pos.x = Mathf.Clamp(xVal, initPosition.x - horizontalLimit + screenWidth, initPosition.x + horizontalLimit - screenWidth);
            pos.z = Mathf.Clamp(zVal, initPosition.z - verticalLimit + screenWidth, initPosition.z + verticalLimit - screenWidth);

            rig.position = pos;
        }
    }

    internal void beginDrag(Vector3 coordinate)
    {
        dragHasBegun = true;
        beginTouch = coordinate;
    }

    internal void finishDrag()
    {
        dragHasBegun = false;
    }

    internal void follow(GameObject star)
    {
        isFollowing = true;
        followedTarget = star;
    }

    internal void stopFollowing()
    {
        isFollowing = false;
        followedTarget = null;
        onFollowing.RemoveAllListeners();
    }

    public override void update(float dt)
    {
        if (isStopped) return;

        if (isFollowing)
        {
            rig.position = new Vector3(followedTarget.transform.position.x, rig.transform.position.y, followedTarget.transform.position.z);
            onFollowing.Invoke(followedTarget);
        }
    }

    public Sequence focusOn(GameObject star, TweenCallback updateCallback = null, TweenCallback completeCallback = null)
    {
        if (star == null) return null;

        return focusOn(star.transform.position, updateCallback, completeCallback);
    }

    public Sequence focusOn(Vector3 star, TweenCallback updateCallback = null, TweenCallback completeCallback = null)
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(findController<CameraController>().rig.DOMove(new Vector3(star.x, rig.transform.position.y, star.z), 0.5f));

        if (updateCallback != null) sequence.OnUpdate(updateCallback);
        if (completeCallback != null) sequence.OnComplete(completeCallback);

        sequence.Play();

        return sequence;
    }

    public bool isInFrustum(MeshFilter meshFilter)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

        foreach (Plane plane in planes)
        {
            foreach (Vector3 vertice in meshFilter.mesh.vertices)
            {
                if (plane.GetDistanceToPoint(meshFilter.gameObject.transform.TransformPoint(vertice)) < 0)
                {
                    return false;
                }
            }
        }

        return true;
    }
}
