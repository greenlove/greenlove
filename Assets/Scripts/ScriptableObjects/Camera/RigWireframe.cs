﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RigWireframe : MonoBehaviour
{
#if UNITY_EDITOR
    [CustomEditor(typeof(RigWireframe))]
    public class RigWireframeEditor : Editor
    {
        void OnSceneGUI()
        {
            RigWireframe t = target as RigWireframe;

            if (t == null) return;

            Vector3 rigPos = t.transform.position;

            Handles.DrawWireDisc(rigPos, new Vector3(0, 1, 0), 7.5f);

            for (int i = 0; i < t.transform.childCount; i++)
            {
                if (t.transform.GetChild(i) != null) Handles.DrawWireCube(t.transform.GetChild(i).position, Vector3.one);
            }
        }
    }
#endif
}
