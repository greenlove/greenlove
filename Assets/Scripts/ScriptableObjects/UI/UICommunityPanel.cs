﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UICommunityPanel : UIPanel
{
    public UIItem redditButton;
    public UIItem discordButton;

#if UNITY_EDITOR

    [CustomEditor(typeof(UICommunityPanel)), CanEditMultipleObjects]
    public class UICommunityPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UICommunityPanel item = Item as UICommunityPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.redditButton = (UIItem)EditorGUILayout.ObjectField("Reddit Button", item.redditButton, typeof(UIItem), true);
            item.discordButton = (UIItem)EditorGUILayout.ObjectField("Discord Button", item.discordButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
