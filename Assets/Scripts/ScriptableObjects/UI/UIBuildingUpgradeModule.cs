﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIBuildingUpgradeModule : UIItem
{
    public Image buildingIcon;
    public Text stockTitle;
    public Text serviceRangeText;
    public UIItem purchaseButton;
    public Text purchasePriceText;
    public GameObject purchasedTick;


#if UNITY_EDITOR

    [CustomEditor(typeof(UIBuildingUpgradeModule)), CanEditMultipleObjects]
    public class UIBuildingUpgradeModuleEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIBuildingUpgradeModule item = Item as UIBuildingUpgradeModule;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.buildingIcon = (Image)EditorGUILayout.ObjectField("Building Icon", item.buildingIcon, typeof(Image), true);
            item.stockTitle = (Text)EditorGUILayout.ObjectField("Stock Title", item.stockTitle, typeof(Text), true);
            item.serviceRangeText = (Text)EditorGUILayout.ObjectField("Service Range Text", item.serviceRangeText, typeof(Text), true);
            item.purchaseButton = (UIItem)EditorGUILayout.ObjectField("Purchase Button", item.purchaseButton, typeof(UIItem), true);
            item.purchasePriceText = (Text)EditorGUILayout.ObjectField("Purchase Price Text", item.purchasePriceText, typeof(Text), true);
            item.purchasedTick = (GameObject)EditorGUILayout.ObjectField("Purchase Tick", item.purchasedTick, typeof(GameObject), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
