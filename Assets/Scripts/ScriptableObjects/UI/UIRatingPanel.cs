﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIRatingPanel : UIPanel
{
    public Image icon;
    public UIItem rateButton;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIRatingPanel)), CanEditMultipleObjects]
    public class UIRatingPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIRatingPanel item = Item as UIRatingPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.icon = (Image)EditorGUILayout.ObjectField("Dog Icon", item.icon, typeof(Image), true);
            item.rateButton = (UIItem)EditorGUILayout.ObjectField("Rate Button", item.rateButton, typeof(UIItem), true);
            
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
