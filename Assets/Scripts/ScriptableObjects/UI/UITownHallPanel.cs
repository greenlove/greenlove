﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UITownHallPanel : UIPanel
{
    public Image headerIcon;
    public Text headerTitle;
    public Text headerDescription;
    public RectTransform scrollView;


#if UNITY_EDITOR
    [CustomEditor(typeof(UITownHallPanel)), CanEditMultipleObjects]
    public class UITownHallPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UITownHallPanel item = Item as UITownHallPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.headerIcon = (Image)EditorGUILayout.ObjectField("Header Icon", item.headerIcon, typeof(Image), true);
            item.headerTitle = (Text)EditorGUILayout.ObjectField("Header Title", item.headerTitle, typeof(Text), true);
            item.headerDescription = (Text)EditorGUILayout.ObjectField("Header Description", item.headerDescription, typeof(Text), true);
            item.scrollView = (RectTransform)EditorGUILayout.ObjectField("Scroll View", item.scrollView, typeof(RectTransform), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif

}
