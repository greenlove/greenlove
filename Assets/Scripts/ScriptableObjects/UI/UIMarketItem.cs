﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIMarketItem : UIItem
{
    public Text title;
    public Text stockText;
    public Image bg;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIMarketItem)), CanEditMultipleObjects]
    public class UIMarketItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIMarketItem item = Item as UIMarketItem;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Animatable on invocation: ");

            item.title = (Text)EditorGUILayout.ObjectField("Boost name: ", item.title, typeof(Text), true);
            item.stockText = (Text)EditorGUILayout.ObjectField("Stock Text: ", item.stockText, typeof(Text), true);
            item.bg = (Image)EditorGUILayout.ObjectField("Background: ", item.bg, typeof(Image), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
