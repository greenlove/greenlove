﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIMarketPanel : UIPanel
{
    public UIMarketItem[] items;
    public Image buildingImage;
    public Text boostText;
    public UIItem adButton;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIMarketPanel)), CanEditMultipleObjects]
    public class UIMarketPanelEditor : UIPanelEditor
    {
        SerializedProperty items;

        protected override void OnEnable()
        {
            base.OnEnable();
            items = serializedObject.FindProperty("items");
        }


        protected override void selfModule<T>(T Item)
        {
            UIMarketPanel item = Item as UIMarketPanel;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self Module: ");

            item.buildingImage = (Image)EditorGUILayout.ObjectField("Building Image: ", item.buildingImage, typeof(Image), true);
            item.boostText = (Text)EditorGUILayout.ObjectField("Boost Text", item.boostText, typeof(Text), true);
            item.adButton = (UIItem)EditorGUILayout.ObjectField("Ad button", item.adButton, typeof(UIItem), true);

            EditorGUILayout.PropertyField(items, new GUIContent("Market Items"), true);

            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
