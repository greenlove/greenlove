﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UIBuildingUpgradePanel : UIPanel
{
    public List<UIBuildingUpgradeModule> modules;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIBuildingUpgradePanel)), CanEditMultipleObjects]
    public class UIBuildingUpgradePanelEditor : UIPanelEditor { }
#endif
}
