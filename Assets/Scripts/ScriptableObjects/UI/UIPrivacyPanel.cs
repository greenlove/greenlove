﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIPrivacyPanel : UIPanel
{
    public UIItem unityButton;
    public UIItem flurryButton;
    public UIItem flurryOptingButton;
    public UIItem flurryDeleteDataButton;

    public Text unityText;
    public Text flurryText;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIPrivacyPanel)), CanEditMultipleObjects]
    public class UIPrivacyPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIPrivacyPanel item = Item as UIPrivacyPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.unityButton = (UIItem)EditorGUILayout.ObjectField("Unity Button", item.unityButton, typeof(UIItem), true);
            item.flurryButton = (UIItem)EditorGUILayout.ObjectField("Flurry Button", item.flurryButton, typeof(UIItem), true);
            item.flurryOptingButton = (UIItem)EditorGUILayout.ObjectField("Flurry Opt In Btn", item.flurryOptingButton, typeof(UIItem), true);
            item.flurryDeleteDataButton = (UIItem)EditorGUILayout.ObjectField("Flurry Delete Data Btn", item.flurryDeleteDataButton, typeof(UIItem), true);

            item.unityText = (Text)EditorGUILayout.ObjectField("Unity Text", item.unityText, typeof(Text), true);
            item.flurryText = (Text)EditorGUILayout.ObjectField("Flurry Text", item.flurryText, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
