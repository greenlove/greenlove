﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UICheatsPanel : UIPanel
{
    public GameObject view;
    public UIItem panelToggle;
    public Text fpsText;
    public Text logText;
    public UIItem skipTutsButton;
    public UIItem unlockAllButton;
    public UIItem claimRandomGiftButton;
    public UIItem viewAdButton;
    public UIItem adDebuggerButton;
    public UIItem logInGooglePlayButton;
    public UIItem allFreeButton;
    public UIItem limitlessRecruitButton;
    public UIItem goToBuildingSceneButton;
    public UIItem changeColorPaletteButton;
    public UIItem shortMissionTimesButton;
    public UIItem uploadSaveToDevsButton;
    public UIItem reloadWithBorruptedSaveButton;

    public InputField currencyInput;
    public Dropdown currenciesDropdown;
    public UIItem addCurrencyButton;

    public Dropdown tutorialsDropdown;
    public UIItem launchTutorialsButton;

#if UNITY_EDITOR

    [CustomEditor(typeof(UICheatsPanel)), CanEditMultipleObjects]
    public class UICheatsPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UICheatsPanel item = Item as UICheatsPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.view = (GameObject)EditorGUILayout.ObjectField("View", item.view, typeof(GameObject), true);
            item.panelToggle = (UIItem)EditorGUILayout.ObjectField("Panel Toggle", item.panelToggle, typeof(UIItem), true);
            item.fpsText = (Text)EditorGUILayout.ObjectField("FPS Text", item.fpsText, typeof(Text), true);
            item.logText = (Text)EditorGUILayout.ObjectField("Log Text", item.logText, typeof(Text), true);
            item.skipTutsButton = (UIItem)EditorGUILayout.ObjectField("Skip tuts Button", item.skipTutsButton, typeof(UIItem), true);
            item.unlockAllButton = (UIItem)EditorGUILayout.ObjectField("Unlock All Button", item.unlockAllButton, typeof(UIItem), true);
            item.claimRandomGiftButton = (UIItem)EditorGUILayout.ObjectField("Claim Gift button", item.claimRandomGiftButton, typeof(UIItem), true);
            item.viewAdButton = (UIItem)EditorGUILayout.ObjectField("View Ad Button", item.viewAdButton, typeof(UIItem), true);
            item.adDebuggerButton = (UIItem)EditorGUILayout.ObjectField("Ad Debugger Button", item.adDebuggerButton, typeof(UIItem), true);
            item.logInGooglePlayButton = (UIItem)EditorGUILayout.ObjectField("Log in to google play Button", item.logInGooglePlayButton, typeof(UIItem), true);
            item.allFreeButton = (UIItem)EditorGUILayout.ObjectField("All Free Button", item.allFreeButton, typeof(UIItem), true);
            item.limitlessRecruitButton = (UIItem)EditorGUILayout.ObjectField("Limitless Recruit Button", item.limitlessRecruitButton, typeof(UIItem), true);
            item.goToBuildingSceneButton = (UIItem)EditorGUILayout.ObjectField("Buildings Scene Button", item.goToBuildingSceneButton, typeof(UIItem), true);
            item.changeColorPaletteButton = (UIItem)EditorGUILayout.ObjectField("Change Palette Button", item.changeColorPaletteButton, typeof(UIItem), true);
            item.shortMissionTimesButton = (UIItem)EditorGUILayout.ObjectField("Short Mission Times Button", item.shortMissionTimesButton, typeof(UIItem), true);
            item.uploadSaveToDevsButton = (UIItem)EditorGUILayout.ObjectField("Upload save Button", item.uploadSaveToDevsButton, typeof(UIItem), true);
            item.reloadWithBorruptedSaveButton = (UIItem)EditorGUILayout.ObjectField("Corrupted save Button", item.reloadWithBorruptedSaveButton, typeof(UIItem), true);

            EditorGUILayout.Space();

            item.currenciesDropdown = (Dropdown)EditorGUILayout.ObjectField("Currencies Dropdown", item.currenciesDropdown, typeof(Dropdown), true);
            item.currencyInput = (InputField)EditorGUILayout.ObjectField("Currency Input", item.currencyInput, typeof(InputField), true);
            item.addCurrencyButton = (UIItem)EditorGUILayout.ObjectField("Add Currency Button", item.addCurrencyButton, typeof(UIItem), true);

            EditorGUILayout.Space();

            item.tutorialsDropdown = (Dropdown)EditorGUILayout.ObjectField("Tutorials Dropdown", item.tutorialsDropdown, typeof(Dropdown), true);
            item.launchTutorialsButton = (UIItem)EditorGUILayout.ObjectField("Add Currency Button", item.launchTutorialsButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
