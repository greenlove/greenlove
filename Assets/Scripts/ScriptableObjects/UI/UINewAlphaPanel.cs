﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UINewAlphaPanel : UIPanel
{
    public GameObject portraitObj;
    public Image portraitImg;
    public GameObject newAlphaTextFrame;
    public Text newAlphaText;
    public GameObject stars;
    public GameObject beams;

#if UNITY_EDITOR
    [CustomEditor(typeof(UINewAlphaPanel)), CanEditMultipleObjects]
    public class UINewAlphaPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UINewAlphaPanel item = Item as UINewAlphaPanel;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self Module: ");

            item.portraitObj = (GameObject)EditorGUILayout.ObjectField("Portrait Object", item.portraitObj, typeof(GameObject), true);
            item.portraitImg = (Image)EditorGUILayout.ObjectField("Portrait Image", item.portraitImg, typeof(Image), true); 
            item.newAlphaTextFrame = (GameObject)EditorGUILayout.ObjectField("New Alpha Text Frame", item.newAlphaTextFrame, typeof(GameObject), true);
            item.newAlphaText = (Text)EditorGUILayout.ObjectField("New Alpha Text", item.newAlphaText, typeof(Text), true);
            item.stars = (GameObject)EditorGUILayout.ObjectField("Stars Pattern", item.stars, typeof(GameObject), true);
            item.beams = (GameObject)EditorGUILayout.ObjectField("Beams Pattern", item.beams, typeof(GameObject), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
