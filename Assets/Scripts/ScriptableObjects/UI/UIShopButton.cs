﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIShopButton : UIItem
{
    public GameObject closeIcon;
    public Image background;
    public GameObject shopIcon;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIShopButton)), CanEditMultipleObjects]
    public class UIShopButtonEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIShopButton item = Item as UIShopButton;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.closeIcon = (GameObject)EditorGUILayout.ObjectField("Close Icon", item.closeIcon, typeof(GameObject), true);
            item.shopIcon = (GameObject)EditorGUILayout.ObjectField("Shop Icon", item.shopIcon, typeof(GameObject), true);
            item.background = (Image)EditorGUILayout.ObjectField("Background", item.background, typeof(Image), true);
            
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
