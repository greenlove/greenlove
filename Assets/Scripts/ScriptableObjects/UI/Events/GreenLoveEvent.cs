﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GreenLoveEvent<T0> : UnityEvent<T0>
{
    public int eventsCount { get { return actions == null ? 0 : actions.Count; } }
    private List<UnityAction<T0>> actions;
    private List<UnityAction<T0>> frozenActions;

    public GreenLoveEvent()
    {
        actions = new List<UnityAction<T0>>();
        frozenActions = null;
    }

    new public void AddListener(UnityAction<T0> call)
    {
        actions.Add(call);
        base.AddListener(call);
    }

    new public void RemoveListener(UnityAction<T0> call)
    {
        actions.Remove(call);
        base.RemoveListener(call);
    }

    new public void RemoveAllListeners()
    {
        actions.Clear();
        base.RemoveAllListeners();
    }

    public bool containsAction(UnityAction<T0> call)
    {
        return actions.Contains(call);
    }

    public void freezeListenersState()
    {
        if(frozenActions == null)
        {
            frozenActions = new List<UnityAction<T0>>();
            foreach (UnityAction<T0> action in actions)
            {
                frozenActions.Add(action);
            }
        }
    }

    public void restoreFromFreeze()
    {
        if (frozenActions == null) return;

        RemoveAllListeners();

        foreach (UnityAction<T0> action in frozenActions)
        {
            AddListener(action);
        }

        frozenActions.Clear();
        frozenActions = null;
    }
}
