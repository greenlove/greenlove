﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIPetsGauge : UIItem
{
    public Text amountText;
    public Text AlphaAmountText;
    public Image alphaPortrait;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIPetsGauge)), CanEditMultipleObjects]
    public class UIPetsGaugeEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIPetsGauge item = Item as UIPetsGauge;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.alphaPortrait = (Image)EditorGUILayout.ObjectField("Alpha Portrait: ", item.alphaPortrait, typeof(Image), true);

            item.amountText = (Text)EditorGUILayout.ObjectField("Amount Text", item.amountText, typeof(Text), true);
            item.AlphaAmountText = (Text)EditorGUILayout.ObjectField("Alpha Amount Text", item.AlphaAmountText, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
