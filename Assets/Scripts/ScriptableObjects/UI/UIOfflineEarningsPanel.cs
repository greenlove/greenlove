﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIOfflineEarningsPanel : UIPanel
{
    public Text goldText;
    public Text timeText;
    public UIItem PickButton;    

#if UNITY_EDITOR

    [CustomEditor(typeof(UIOfflineEarningsPanel)), CanEditMultipleObjects]
    public class UIOfflineEarningsPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIOfflineEarningsPanel item = Item as UIOfflineEarningsPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.goldText = (Text)EditorGUILayout.ObjectField("Gold Text", item.goldText, typeof(Text), true);
            item.timeText = (Text)EditorGUILayout.ObjectField("Time Text", item.timeText, typeof(Text), true);
            item.PickButton = (UIItem)EditorGUILayout.ObjectField("Pick Button", item.PickButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
