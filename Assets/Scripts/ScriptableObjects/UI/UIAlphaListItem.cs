﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIAlphaListItem : UIItem
{
    public int index;
    public bool isSelected;

    //Locked & Empty
    public GameObject emptyObject;
    public Image emptyBackground;
    public Image emptyPortrait;
    public GameObject emptyLockIcon;

    //Assigned:
    public GameObject assignedObject;
    public Image assignedBackground;
    public RawImage renderItem;
    public GameObject stateTextContainer;
    public Text stateText;
    public GameObject gift;
    public GameObject waves;

    //State
    public AlphaListViewState state;

    public float scaleWhenSelected;

    public void updateState(AlphaSlot s)
    {
        this.state = s.state;

        switch (state)
        {
            case AlphaListViewState.LOCKED:
                disableAssigned();
                enableLocked();
                break;
            case AlphaListViewState.EMPTY:
                disableAssigned();
                enableEmpty();
                break;
            case AlphaListViewState.ASSIGNED:
                disableLockedOrEmpty();
                enableAssigned();
                break;
        }

        //gifts
        if (s?.alpha?.gifts > 0) gift.SetActive(true);
        else gift.SetActive(false);
    }

    private void enableLocked()
    {
        emptyObject.SetActive(true);
        emptyBackground.color = Color.gray;
        emptyLockIcon.SetActive(true);
    }

    private void disableLockedOrEmpty()
    {
        emptyObject.SetActive(false);
    }

    private void enableEmpty()
    {
        emptyObject.SetActive(true);
        emptyLockIcon.SetActive(false);
    }

    private void enableAssigned()
    {
        assignedObject.SetActive(true);
        //stateTextContainer.SetActive(false);
    }

    private void disableAssigned()
    {
        assignedObject.SetActive(false);
    }

    public void select(AlphaSlot slot)
    {
        isSelected = true;
        stateTextContainer.gameObject.SetActive(true);

        transform.SetAsLastSibling();

        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(scaleWhenSelected, 0.25f));
        seq.Join(transform.DOLocalMove(initPosition + new Vector2(-180, 0), 0.25f));
        seq.Play();
    }

    public void unselect(AlphaSlot slot)
    {
        if (slot.uiAction != null) Destroy(slot.uiAction.gameObject);
        stateTextContainer.gameObject.SetActive(false);

        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(1, 0.25f));
        seq.Join(transform.DOLocalMove(initPosition, 0.25f));
        seq.OnComplete(() => {
            isSelected = false;
        });
        seq.Play();

    }

    public void updateAssigned(AlphaSlot slot)
    {
        if (state == AlphaListViewState.ASSIGNED)
        {
            if (slot.alpha.pack?.mission != null)
            {
                assignedBackground.enabled = true;
                assignedBackground.fillAmount = slot.alpha.pack.getNormalizedRemainingMissionTime();
            }
            else
            {
                assignedBackground.enabled = false;
            }
    
            stateText.text = loc.getTextByKey(slot.alpha.pack.moodKey);
            stateTextContainer.GetComponent<Image>().color = slot.alpha.pack.mood == PackMood.HAPPY || slot.alpha.isDoingSomething()  ? new Color(0.48f, 0.76f, 0.403f) : new Color(0.83f, 0.32f, 0.26f);

            if (slot.alpha.pack.mission != null)
            {
                slot.alphaAnimator.Play(slot.alpha.pack.mission.what.ToString(), 0, 0);
            }
            else
            {
                slot.alphaAnimator.Play(slot.alpha.pack.mood != PackMood.HAPPY ? "SAD" : "HAPPY");
            }
            
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(UIAlphaListItem)), CanEditMultipleObjects]
    public class UIAlphaListItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIAlphaListItem item = Item as UIAlphaListItem;

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self: ");

            item.emptyObject = (GameObject)EditorGUILayout.ObjectField("Empty Item: ", item.emptyObject, typeof(GameObject), true);
            item.emptyBackground = (Image)EditorGUILayout.ObjectField("Empty Bg: ", item.emptyBackground, typeof(Image), true);
            item.emptyPortrait = (Image)EditorGUILayout.ObjectField("Empty portrait: ", item.emptyPortrait, typeof(Image), true);
            item.emptyLockIcon = (GameObject)EditorGUILayout.ObjectField("Empty Lock Icon: ", item.emptyLockIcon, typeof(GameObject), true);

            item.assignedObject = (GameObject)EditorGUILayout.ObjectField("Assigned Item: ", item.assignedObject, typeof(GameObject), true);
            item.assignedBackground = (Image)EditorGUILayout.ObjectField("Assigned Bg: ", item.assignedBackground, typeof(Image), true);
            item.renderItem = (RawImage)EditorGUILayout.ObjectField("Assigned Render Image: ", item.renderItem, typeof(RawImage), true);
            item.stateTextContainer = (GameObject)EditorGUILayout.ObjectField("State container: ", item.stateTextContainer, typeof(GameObject), true);
            item.stateText = (Text)EditorGUILayout.ObjectField("State text: ", item.stateText, typeof(Text), true);
            item.gift = (GameObject)EditorGUILayout.ObjectField("Gift Image: ", item.gift, typeof(GameObject), true);
            item.waves = (GameObject)EditorGUILayout.ObjectField("Waves: ", item.waves, typeof(GameObject), true);
            item.scaleWhenSelected = EditorGUILayout.FloatField("Scale when selected: ", item.scaleWhenSelected);
            
            EditorGUILayout.EndVertical();
        }
    }
#endif
}

public enum AlphaListViewState
{
    LOCKED,
    EMPTY,
    ASSIGNED,
    COUNT
}