﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIPrestigePanel : UIPanel
{
    public Text goldMultiplier;
    public Text giftMultiplier;
    public Text currentMultiplierGold;
    public Text currentMultiplierGift;

    public Text petsAmount;
    public Slider petsAmountSlider;

    public UIItem prestigeButton;

    public UIItem infoButton;
    public UIPanel infoPanel;

    public bool isInfoPanelOpen;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIPrestigePanel)), CanEditMultipleObjects]
    public class UIPrestigePanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIPrestigePanel item = Item as UIPrestigePanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            item.goldMultiplier = (Text)EditorGUILayout.ObjectField("Gold Reward Text", item.goldMultiplier, typeof(Text), true);
            item.giftMultiplier = (Text)EditorGUILayout.ObjectField("Gift Reward Text", item.giftMultiplier, typeof(Text), true);
            item.currentMultiplierGold = (Text)EditorGUILayout.ObjectField("Current Gold Multiplier Text", item.currentMultiplierGold, typeof(Text), true);
            item.currentMultiplierGift = (Text)EditorGUILayout.ObjectField("Current Gold Multiplier Text", item.currentMultiplierGift, typeof(Text), true);

            item.petsAmount = (Text)EditorGUILayout.ObjectField("Pets Amount Text", item.petsAmount, typeof(Text), true);
            item.petsAmountSlider = (Slider)EditorGUILayout.ObjectField("Pets Amount Slider", item.petsAmountSlider, typeof(Slider), true);

            item.prestigeButton = (UIItem)EditorGUILayout.ObjectField("Prestige Button", item.prestigeButton, typeof(UIItem), true);

            item.infoButton = (UIItem)EditorGUILayout.ObjectField("Info Button", item.infoButton, typeof(UIItem), true);
            item.infoPanel = (UIPanel)EditorGUILayout.ObjectField("Info Panel", item.infoPanel, typeof(UIPanel), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
