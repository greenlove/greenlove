﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

[CreateAssetMenu(menuName = "Building Blocks/UI/UIGroup")]
public class UIGroup : ScriptableObject {

    
    public List<UIBehaviour> items;



#if UNITY_EDITOR
    [CustomEditor(typeof(UIGroup)), CanEditMultipleObjects]
    public class UIGroupEditor : Editor
    {

        UIGroup group;
        string enumName;

        public override void OnInspectorGUI()
        {
            group = (UIGroup)target;

            //serializedObject.Update();
            //showList(serializedObject.FindProperty("items"));
            //serializedObject.ApplyModifiedProperties();

            DrawDefaultInspector();

            EditorGUILayout.Space();

            enumName = EditorGUILayout.TextField("Enum name: ", enumName);

            if(GUILayout.Button("Create Enum"))
            {
                createEnum();
            }

        }
        /*
        private void showList(SerializedProperty list)
        {
            EditorGUILayout.PropertyField(list);
            EditorGUI.indentLevel += 1;
            if (list.isExpanded)
            {
                EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                for (int i = 0; i < list.arraySize; i++)
                {
                    EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
                    
                }
            }
            EditorGUI.indentLevel -= 1;

        }
        */

        private void createEnum()
        {
            string[] enumEntries = new string[group.items.Count];
            int j = 0;
            foreach (UIBehaviour item in group.items)
            {
                enumEntries[j] = item.name.ToUpper();
                j++;
            }
            string filePathAndName = "Assets/Scripts/Enums/" + enumName + ".cs"; //The folder Scripts/Enums/ is expected to exist

            using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
            {
                streamWriter.WriteLine("public enum " + enumName);
                streamWriter.WriteLine("{");
                streamWriter.WriteLine("\tNONE = -1,");
                for (int i = 0; i < enumEntries.Length; i++)
                {
                    Debug.Log(string.Format("enum entry = {0}", enumEntries[i]));
                    streamWriter.WriteLine("\t" + enumEntries[i] + ",");
                }
                streamWriter.WriteLine(string.Format("{0}LENGTH = {1}{2}", "\t", enumEntries.Length, ","));
                streamWriter.WriteLine("}");
            }
            AssetDatabase.Refresh();
        }
    }

#endif
}
