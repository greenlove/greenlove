﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UITutorialQuestModule : UIItem
{
    public Slider slider;
    public Image rewardTypeIcon;
    public Image questTypeIcon;
    public Text rewardAmount;
    public Text description;

#if UNITY_EDITOR

    [CustomEditor(typeof(UITutorialQuestModule)), CanEditMultipleObjects]
    public class UITutorialQuestModuleEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UITutorialQuestModule item = Item as UITutorialQuestModule;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.slider = (Slider)EditorGUILayout.ObjectField("Progress bar:", item.slider, typeof(Slider), true);
            item.rewardTypeIcon = (Image)EditorGUILayout.ObjectField("Reward Image:", item.rewardTypeIcon, typeof(Image), true);
            item.questTypeIcon = (Image)EditorGUILayout.ObjectField("Quest Image:", item.questTypeIcon, typeof(Image), true);
            item.rewardAmount = (Text)EditorGUILayout.ObjectField("Reward Amount:", item.rewardAmount, typeof(Text), true);
            item.description = (Text)EditorGUILayout.ObjectField("Quest Description:", item.description, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
