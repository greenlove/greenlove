﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIBoostConfirmationPopup : UIPanel
{
    public Text itemName;
    public Text itemStock;
    public Image itemPortrait;
    public Text descriptionText;
    public UIItem watchAdButton;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIBoostConfirmationPopup)), CanEditMultipleObjects]
    public class UIBoostConfirmationPopupEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIBoostConfirmationPopup item = Item as UIBoostConfirmationPopup;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self Module: ");

            item.itemName = (Text)EditorGUILayout.ObjectField("Name Text", item.itemName, typeof(Text), true);
            item.itemStock = (Text)EditorGUILayout.ObjectField("Stock Text", item.itemStock, typeof(Text), true);
            item.itemPortrait = (Image)EditorGUILayout.ObjectField("Desc Text", item.itemPortrait, typeof(Image), true);
            item.descriptionText = (Text)EditorGUILayout.ObjectField("Desc Text", item.descriptionText, typeof(Text), true);
            item.watchAdButton = (UIItem)EditorGUILayout.ObjectField("Watch Button", item.watchAdButton, typeof(UIItem), true);
            
            EditorGUILayout.EndVertical();

        }
    }
#endif
}
