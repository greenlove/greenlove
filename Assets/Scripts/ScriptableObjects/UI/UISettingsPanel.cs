﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UISettingsPanel : UIPanel
{
    public Slider sfxToggle;
    public UIItem sfxHandle;

    public Slider musicToggle;
    public UIItem musicHandle;

    public Text langText;
    public UIItem langLeftArrow;
    public UIItem langRightArrow;

    public UIItem ratingButton;
    public Text versionText;
    public Text idText;
    public UIItem idTextButton;

    public UIItem privacyButton;


#if UNITY_EDITOR

    [CustomEditor(typeof(UISettingsPanel)), CanEditMultipleObjects]
    public class UISettingsPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UISettingsPanel item = Item as UISettingsPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            item.sfxToggle = (Slider)EditorGUILayout.ObjectField("SFX Toggle", item.sfxToggle, typeof(Slider), true);
            item.sfxHandle = (UIItem)EditorGUILayout.ObjectField("SFX Handle", item.sfxHandle, typeof(UIItem), true);

            item.musicToggle = (Slider) EditorGUILayout.ObjectField("Music Toggle", item.musicToggle, typeof(Slider), true);
            item.musicHandle = (UIItem)EditorGUILayout.ObjectField("Music Handle", item.musicHandle, typeof(UIItem), true);

            item.langText = (Text) EditorGUILayout.ObjectField("Lang Text", item.langText, typeof(Text), true);

            item.langLeftArrow = (UIItem)EditorGUILayout.ObjectField("Left Arrow", item.langLeftArrow, typeof(UIItem), true);
            item.langRightArrow = (UIItem)EditorGUILayout.ObjectField("Right Arrow", item.langRightArrow, typeof(UIItem), true);

            item.ratingButton = (UIItem) EditorGUILayout.ObjectField("Rating Button", item.ratingButton, typeof(UIItem), true);
            item.versionText = (Text) EditorGUILayout.ObjectField("Version Text", item.versionText, typeof(Text), true);

            item.idText = (Text)EditorGUILayout.ObjectField("ID Text", item.idText, typeof(Text), true);
            item.idTextButton = (UIItem)EditorGUILayout.ObjectField("ID Text Button", item.idTextButton, typeof(UIItem), true);

            item.privacyButton = (UIItem)EditorGUILayout.ObjectField("Privacy Button", item.privacyButton, typeof(UIItem), true);
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
