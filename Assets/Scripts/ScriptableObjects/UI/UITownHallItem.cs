﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UITownHallItem : UIItem
{
    public Image buildingIcon;
    public Text title;
    public Text desc;
    public GameObject purchasedSignal;
    public UIItem purchaseButton;
    public Text purchasePrice;
    public GameObject lockIcon;


#if UNITY_EDITOR

    [CustomEditor(typeof(UITownHallItem)), CanEditMultipleObjects]
    public class UITownHallItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UITownHallItem item = Item as UITownHallItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.buildingIcon = (Image)EditorGUILayout.ObjectField("Building Icon", item.buildingIcon, typeof(Image), true);
            item.title = (Text)EditorGUILayout.ObjectField("Title", item.title, typeof(Text), true);
            item.desc = (Text)EditorGUILayout.ObjectField("Description", item.desc, typeof(Text), true);
            item.purchasedSignal = (GameObject)EditorGUILayout.ObjectField("Purchased Signal", item.purchasedSignal, typeof(GameObject), true);
            item.purchaseButton = (UIItem)EditorGUILayout.ObjectField("Purchased Button", item.purchaseButton, typeof(UIItem), true);
            item.purchasePrice = (Text)EditorGUILayout.ObjectField("Purchase Price", item.purchasePrice, typeof(Text), true);
            item.lockIcon = (GameObject)EditorGUILayout.ObjectField("Lock Icon", item.lockIcon, typeof(GameObject), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
