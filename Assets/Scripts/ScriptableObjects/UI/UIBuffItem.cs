﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIBuffItem : UIItem
{
    public Image buffIcon;
    public Text buffTitle;
    public Text buffDescription;
    public Slider levelSlider;

    public GameObject craftItemsContainer;
    public List<UIBuffCraftItem> craftItems;

    public Text buffLevel;
    public UIItem upgradeButton;
    public Text priceText;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIBuffItem)), CanEditMultipleObjects]
    public class UIBuffItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIBuffItem item = Item as UIBuffItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.buffIcon = (Image)EditorGUILayout.ObjectField("Buff Icon", item.buffIcon, typeof(Image), true);
            item.buffTitle = (Text)EditorGUILayout.ObjectField("Buff Title", item.buffTitle, typeof(Text), true);
            item.buffDescription = (Text)EditorGUILayout.ObjectField("Buff Description", item.buffDescription, typeof(Text), true);

            item.craftItemsContainer = (GameObject)EditorGUILayout.ObjectField("Craft Items Container", item.craftItemsContainer, typeof(GameObject), true);

            item.buffLevel = (Text)EditorGUILayout.ObjectField("Buff Level", item.buffLevel, typeof(Text), true);
            item.levelSlider = (Slider)EditorGUILayout.ObjectField("Level Slider", item.levelSlider, typeof(Slider), true);

            item.upgradeButton = (UIItem)EditorGUILayout.ObjectField("Upgrade Button", item.upgradeButton, typeof(UIItem), true);
            item.priceText = (Text)EditorGUILayout.ObjectField("Price Text", item.priceText, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
