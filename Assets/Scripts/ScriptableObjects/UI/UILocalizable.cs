﻿using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class UILocalizable {

    [SerializeField]
    public StringReference stringRef;

    [SerializeField]
    public Text txt;


    public UILocalizable()
    {
        stringRef = new StringReference(StringReferenceValue.LOC);
    }
}
