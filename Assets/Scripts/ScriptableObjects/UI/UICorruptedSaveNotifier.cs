﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UICorruptedSaveNotifier : UIPanel
{
    public Text message;
    public UIItem mailButton;

#if UNITY_EDITOR

    [CustomEditor(typeof(UICorruptedSaveNotifier)), CanEditMultipleObjects]
    public class UICorruptedSaveNotifierEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UICorruptedSaveNotifier item = Item as UICorruptedSaveNotifier;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.message = (Text)EditorGUILayout.ObjectField("Message", item.message, typeof(Text), true);
            item.mailButton = (UIItem)EditorGUILayout.ObjectField("Mail Button", item.mailButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
