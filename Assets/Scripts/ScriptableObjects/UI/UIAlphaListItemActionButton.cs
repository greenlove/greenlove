﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIAlphaListItemActionButton : UIItem
{
    public Image icon;
    public Text action;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIAlphaListItemActionButton)), CanEditMultipleObjects]
    public class UIAlphaListItemActionbuttonEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIAlphaListItemActionButton item = Item as UIAlphaListItemActionButton;

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self: ");

            item.icon = (Image)EditorGUILayout.ObjectField("Action Icon: ", item.icon, typeof(Image), true);
            item.action = (Text)EditorGUILayout.ObjectField("Action Text: ", item.action, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
