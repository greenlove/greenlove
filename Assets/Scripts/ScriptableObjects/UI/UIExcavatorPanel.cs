﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIExcavatorPanel : UIPanel
{
    public Text title;
    public Text description;
    public Image icon;
    public ScrollRect scroll;

    [HideInInspector]
    public List<UITownHallItem> items;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIExcavatorPanel)), CanEditMultipleObjects]
    public class UIExcavatorPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIExcavatorPanel item = Item as UIExcavatorPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.title = (Text)EditorGUILayout.ObjectField("Title text", item.title, typeof(Text), true);
            item.description = (Text)EditorGUILayout.ObjectField("Description text", item.description, typeof(Text), true);
            item.icon = (Image)EditorGUILayout.ObjectField("Building Icon", item.icon, typeof(Image), true);
            item.scroll = (ScrollRect)EditorGUILayout.ObjectField("Scroll Rect", item.scroll, typeof(ScrollRect), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
