﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICutscene : Main
{
    public Text[] texts;

    protected override void Awake()
    {
        initHash();
        init();

        for (int i = 0; i < texts.Length; i++)
        {
            string str = getController<LocalizationController>().getTextByKey(string.Format("CUTSCENE_0{0}", i + 1));
            texts[i].text = str;
        }

        getController<AudioController>().playSound("cutscene");
    }
}
