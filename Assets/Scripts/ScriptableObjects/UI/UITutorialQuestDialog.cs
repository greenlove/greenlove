﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UITutorialQuestDialog : UIPanel
{
    public UIItem claimButton;
    public Image questTypeIcon;
    public Text description;
    public Text priceDescription;

#if UNITY_EDITOR

    [CustomEditor(typeof(UITutorialQuestDialog)), CanEditMultipleObjects]
    public class UITutorialQuestDialogEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UITutorialQuestDialog item = Item as UITutorialQuestDialog;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.claimButton = (UIItem)EditorGUILayout.ObjectField("Claim Button:", item.claimButton, typeof(UIItem), true);
            item.questTypeIcon = (Image)EditorGUILayout.ObjectField("Quest Image:", item.questTypeIcon, typeof(Image), true);
            item.description = (Text)EditorGUILayout.ObjectField("Quest Description:", item.description, typeof(Text), true);
            item.priceDescription = (Text)EditorGUILayout.ObjectField("Price Description:", item.priceDescription, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
