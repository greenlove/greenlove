﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIPostPrestigePopop : UIPanel
{
    public Image currentBg;
    public Image nextBg;

    public Text rescueTitle;
    public Text petsAmountText;
    public Text multiplierText;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIPostPrestigePopop)), CanEditMultipleObjects]
    public class UIPostPrestigePopupEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIPostPrestigePopop item = Item as UIPostPrestigePopop;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.currentBg = (Image)EditorGUILayout.ObjectField("Current background", item.currentBg, typeof(Image), true);
            item.nextBg = (Image)EditorGUILayout.ObjectField("Next background", item.nextBg, typeof(Image), true);

            item.rescueTitle = (Text)EditorGUILayout.ObjectField("Rescue Title", item.rescueTitle, typeof(Text), true);
            item.petsAmountText = (Text)EditorGUILayout.ObjectField("Pets Amount", item.petsAmountText, typeof(Text), true);
            item.multiplierText = (Text)EditorGUILayout.ObjectField("Multiplier", item.multiplierText, typeof(Text), true);



            EditorGUILayout.EndVertical();
        }
    }
#endif
}
