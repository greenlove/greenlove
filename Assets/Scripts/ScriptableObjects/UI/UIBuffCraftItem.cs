﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIBuffCraftItem : UIItem
{
    public Image itemBackground;
    public Image itemIcon;
    public Text itemText;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIBuffCraftItem)), CanEditMultipleObjects]
    public class UIBuffCraftItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIBuffCraftItem item = Item as UIBuffCraftItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            
            item.itemIcon = (Image)EditorGUILayout.ObjectField("Item Icon", item.itemIcon, typeof(Image), true);
            item.itemBackground = (Image)EditorGUILayout.ObjectField("Item Background", item.itemBackground, typeof(Image), true);
            item.itemText = (Text)EditorGUILayout.ObjectField("Item Text", item.itemText, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
