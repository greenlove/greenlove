﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIBuildingsSceneButton : UIItem
{
    // Start is called before the first frame update
    void Start()
    {
        base.Start();

        onTouched.AddListener((bool isLocked) => 
        {
            AsyncOperation ops = SceneManager.LoadSceneAsync("CityScene");
            ops.allowSceneActivation = true;
        });
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(UIBuildingsSceneButton)), CanEditMultipleObjects]
    public class UIBuildingsSceneButtonEditor : UIItemEditor { }
#endif
}
