﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIShopPanel : UIPanel
{
    public GameObject invItemsContainer;
    public Text invItemsTimestampText;

    public GameObject packsContainer;

    public Text bundleTitle;
    public Text bundleTimestamp;
    public UIItem purchaseBundleButton;
    public GameObject bundleContainer;
    public Text purchasedBundleText;
    public Text bundlePriceText;

    public ScrollRect scroll;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIShopPanel)), CanEditMultipleObjects]
    public class UIShopPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIShopPanel item = Item as UIShopPanel;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self Module: ");

            item.invItemsContainer = (GameObject)EditorGUILayout.ObjectField("Inventory Items Container: ", item.invItemsContainer, typeof(GameObject), true);
            item.invItemsTimestampText = (Text)EditorGUILayout.ObjectField("Inventory Items Timestamp: ", item.invItemsTimestampText, typeof(Text), true);
            item.packsContainer = (GameObject)EditorGUILayout.ObjectField("Packs Container: ", item.packsContainer, typeof(GameObject), true);

            item.bundleTitle = (Text)EditorGUILayout.ObjectField("Bundle Title: ", item.bundleTitle, typeof(Text), true);
            item.purchaseBundleButton = (UIItem)EditorGUILayout.ObjectField("Purchase Bundle button: ", item.purchaseBundleButton, typeof(UIItem), true);
            item.purchasedBundleText = (Text)EditorGUILayout.ObjectField("Purchased Bundle Text: ", item.purchasedBundleText, typeof(Text), true);
            item.bundleContainer = (GameObject)EditorGUILayout.ObjectField("Bundle Container: ", item.bundleContainer, typeof(GameObject), true);
            item.bundleTimestamp = (Text)EditorGUILayout.ObjectField("Bundle Timestamp: ", item.bundleTimestamp, typeof(Text), true);
            item.bundlePriceText = (Text)EditorGUILayout.ObjectField("Bundle Price: ", item.bundlePriceText, typeof(Text), true);
            item.scroll = (ScrollRect)EditorGUILayout.ObjectField("Scroll: ", item.scroll, typeof(ScrollRect), true);


            EditorGUILayout.EndVertical();

        }
    }
#endif
}
