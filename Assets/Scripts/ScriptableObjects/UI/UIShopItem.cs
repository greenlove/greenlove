﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIShopItem : UIItem
{
    public Text packAmount;
    public Text packPrice;
    public Image icon;
    public GameObject discountTagContainer;
    public Text discountTag;
    public UIItem purchaseButton;


#if UNITY_EDITOR
    [CustomEditor(typeof(UIShopItem)), CanEditMultipleObjects]
    public class UIShopItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIShopItem item = Item as UIShopItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.packAmount = (Text)EditorGUILayout.ObjectField("Amount Text", item.packAmount, typeof(Text), true);
            item.packPrice = (Text)EditorGUILayout.ObjectField("Price Text", item.packPrice, typeof(Text), true);
            item.icon = (Image)EditorGUILayout.ObjectField("Icon", item.icon, typeof(Image), true);
            item.discountTag = (Text)EditorGUILayout.ObjectField("Discount Text", item.discountTag, typeof(Text), true);
            item.discountTagContainer = (GameObject)EditorGUILayout.ObjectField("Discount Tag Container", item.discountTagContainer, typeof(GameObject), true);
            item.purchaseButton = (UIItem)EditorGUILayout.ObjectField("Purchase Button", item.purchaseButton, typeof(UIItem), true);
            
            EditorGUILayout.EndVertical();
        }
    }
#endif
}
