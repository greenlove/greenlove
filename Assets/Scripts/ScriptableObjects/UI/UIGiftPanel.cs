﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIGiftPanel : UIPanel
{
    public Text alphaMessage;
    public UIItem okButton;
    public GameObject currenciesContainer;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIGiftPanel)), CanEditMultipleObjects]
    public class UIGiftPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIGiftPanel item = Item as UIGiftPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.alphaMessage = (Text)EditorGUILayout.ObjectField("Alpha Message", item.alphaMessage, typeof(Text), true);
            item.okButton = (UIItem)EditorGUILayout.ObjectField("Ok Button", item.okButton, typeof(UIItem), true);
            item.currenciesContainer = (GameObject)EditorGUILayout.ObjectField("Currency container", item.currenciesContainer, typeof(GameObject), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
