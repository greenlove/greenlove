﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UIDumpPanel : UIPanel
{
    public UIItem supportButton;
    public UIItem aboutButton;
    public UIItem communityButton;
    public UIItem settingsButton;
    public UIItem prestigeButton;
    public UIItem inventoryButton;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIDumpPanel)), CanEditMultipleObjects]
    public class UIDumpPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIDumpPanel item = Item as UIDumpPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                
            item.supportButton = (UIItem)EditorGUILayout.ObjectField("Support Button", item.supportButton, typeof(UIItem), true);
            item.aboutButton = (UIItem) EditorGUILayout.ObjectField("About Button", item.aboutButton, typeof(UIItem), true);
            item.communityButton = (UIItem) EditorGUILayout.ObjectField("Community Button", item.communityButton, typeof(UIItem), true);
            item.settingsButton = (UIItem) EditorGUILayout.ObjectField("Settings Button", item.settingsButton, typeof(UIItem), true);
            item.prestigeButton = (UIItem) EditorGUILayout.ObjectField("Prestige Button", item.prestigeButton, typeof(UIItem), true);
            item.inventoryButton = (UIItem) EditorGUILayout.ObjectField("Inventory Button", item.inventoryButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();

        }
}
#endif
}
