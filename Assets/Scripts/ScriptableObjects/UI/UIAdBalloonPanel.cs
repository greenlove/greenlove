﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIAdBalloonPanel : UIPanel
{
    public Image rewardIcon;
    public Text amountText;
    public Text descriptionText;
    public UIItem adButton;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIAdBalloonPanel)), CanEditMultipleObjects]
    public class UIAdBalloonPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIAdBalloonPanel item = Item as UIAdBalloonPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.rewardIcon = (Image)EditorGUILayout.ObjectField("Reward Icon", item.rewardIcon, typeof(Image), true);
            item.amountText = (Text)EditorGUILayout.ObjectField("Amount text", item.amountText, typeof(Text), true);
            item.descriptionText = (Text)EditorGUILayout.ObjectField("Description Text", item.descriptionText, typeof(Text), true);
            item.adButton = (UIItem)EditorGUILayout.ObjectField("Settings Button", item.adButton, typeof(UIItem), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
