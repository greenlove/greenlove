﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif


[Serializable]
public class UIItem : UIBehaviour, IUIRealTimeUpdateable, IPointerEnterHandler, IPointerExitHandler,
    IPointerUpHandler, IPointerDownHandler, IDragHandler, ILocalizable, IBankListener, IColorListener
{
    public GreenLoveEvent<bool> onHoverIn { get; private set; }
    public GreenLoveEvent<bool> onHoverOut { get; private set; }
    public GreenLoveEvent<bool> onReleased { get; private set; }
    public GreenLoveEvent<Vector2> onDragged { get; private set; }
    public GreenLoveEvent<bool> onTouched { get; private set; }
    public GreenLoveEvent<bool> onTouchedAnimFinished { get; private set; }
    public GreenLoveEvent<bool> onHolded { get; private set; }
    public GreenLoveEvent<Dictionary<string, object>> onUpdate { get; private set; }
    public GreenLoveEvent<Dictionary<string, object>> onBankChange { get; private set; }

    private float holdBuffer;
    private const float holdTreshold = 0.8f;
    private bool isTouchDown;

    [SerializeField]
    protected Image mainImage;

    [SerializeField]
    protected bool lookComponentsInChildren;

    [SerializeField]
    protected GameObject componentChild;

    [SerializeField]
    public bool isInteractable;
    protected bool isLocked;

    [SerializeField]
    protected GameObject buttonIcon;
    protected Sprite initIconSprite;

    [SerializeField]
    protected bool isDraggable;

    [SerializeField]
    protected bool lockX;

    [SerializeField]
    protected bool lockY;

    [SerializeField]
    protected float rangeLeftX, rangeRightX;
    protected Vector2 rangeX;


    [SerializeField]
    protected float rangeLeftY, rangeRightY;
    protected Vector2 rangeY;

    [HideInInspector]
    public Vector2 initPosition;

    [SerializeField]
    protected bool isAnimatable;

    [SerializeField]
    protected float punchSizeIncrement;

    [SerializeField]
    protected float punchTimeLength;

    [SerializeField]
    protected UIAnims anim;

    private Vector3 initScale;
    private Vector3 childScale;


    [SerializeField]
    protected bool isOutlinable;

    [SerializeField]
    protected Image outlineImage;

    [SerializeField]
    protected float outlineTime;


    [SerializeField]
    protected bool isTintable;

    [SerializeField]
    protected float tintDuration;
    [SerializeField]
    protected bool useAnimatableTimeScale;

    [SerializeField]
    protected bool isHoldable;


    [SerializeField]
    protected bool hasTouchSound;

    [SerializeField, Stem.SoundID]
    protected int sound;

    [SerializeField]
    protected AudioController audioController;

    [SerializeField]
    protected bool isRealTimeUpdateable;

    [SerializeField]
    protected bool usesBadge;
    [SerializeField]
    protected GameObject badge;

    [SerializeField]
    protected UIController uiController;


    [SerializeField]
    protected bool isHighlightable;

    [SerializeField]
    protected bool highlightOnUnlocking;

    [SerializeField]
    protected float unlockingHighlightTime;

    [SerializeField]
    protected int highlightedTimes;

    [SerializeField]
    protected int tapsToStopHighlight;

    [SerializeField]
    protected bool hasStrongHighlight;

    [SerializeField]
    protected GameObject strongHighlight;

    private float timeSinceNotClicked;
    public int taps;
    private List<float> tapTimes;
    public float tapsPerSecond { get; private set; }

    private bool isHighLighted;
    private Sequence highlightSequence;
    

    [SerializeField]
    protected bool isLocalizable;

    [SerializeField]
    protected LocalizationController loc;

    [SerializeField]
    protected List<UILocalizable> uiLocalizables;


    [SerializeField]
    protected bool isBankListener;

    [SerializeField]
    protected Bank bank;

    [SerializeField]
    protected Currency[] currenciesToListen;


    [SerializeField]
    private bool hasWarnings;

    public Dictionary<string, object> onTouchParams;
    public Dictionary<string, object> onUpdateParams;
    public Dictionary<string, object> onBankChangeParams;

    [SerializeField]
    protected bool usesColorModule;

    [SerializeField]
    protected Color mainColor;

    [SerializeField]
    protected Color touchedColor;

    [SerializeField]
    protected Color highlightColor;

    [SerializeField]
    protected Color lockedColor;

    [SerializeField]
    protected ColorizableController colorizableController;

    [SerializeField]
    protected bool doesOverridePalette;

    public new virtual void Awake()
    {
        //Initialize all event handlers
        onHoverIn = new GreenLoveEvent<bool>();
        onHoverOut = new GreenLoveEvent<bool>();
        onReleased = new GreenLoveEvent<bool>();
        onDragged = new GreenLoveEvent<Vector2>();
        onTouched = new GreenLoveEvent<bool>();
        onTouchedAnimFinished = new GreenLoveEvent<bool>();
        onHolded = new GreenLoveEvent<bool>();
        onUpdate = new GreenLoveEvent<Dictionary<string, object>>();
        onBankChange = new GreenLoveEvent<Dictionary<string, object>>();

        uiController = GameObject.FindGameObjectWithTag("Main")?.GetComponent<Main>()?.getController<UIController>();

        base.Awake();
    }


    // Use this for initialization
    public new virtual void Start ()
    {
        init();
        base.Start();
    }


    //destroy
    protected new virtual void OnDestroy()
    {
        realTimeUnsubscribe();

        cleanupDelegates();

        unsubscribeToLangChanges();

        unsubscribeToBankChanges();

        unsubscribeToColor();
    }

    public void cleanupDelegates()
    {
        onHoverIn.RemoveAllListeners();
        onHoverOut.RemoveAllListeners();
        onReleased.RemoveAllListeners();
        onDragged.RemoveAllListeners();
        onTouched.RemoveAllListeners();
        onTouchedAnimFinished.RemoveAllListeners();
        onUpdate.RemoveAllListeners();
    }

    public virtual void init()
    {
        if (lookComponentsInChildren)
        {
            mainImage = componentChild.GetComponent<Image>();
            childScale = new Vector3(mainImage.transform.localScale.x, mainImage.transform.localScale.y, mainImage.transform.localScale.z);
        }
        else
        {
            mainImage = GetComponent<Image>();
        }
        

        //subscribe to realtime if you can (already handled within the method)
        realTimeSubscribe();

        //if has badge, hide it
        hideBadge();

        subscribeToLangChanges();
        onLangChanged();

        isHighLighted = false;

        initScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);

        //subscribe to bank
        onBankChangeParams = new Dictionary<string, object>();
        subscribeToBankChanges();

        //Hide outline image
        if (outlineImage != null) outlineImage.color = new Color(outlineImage.color.r, outlineImage.color.g, outlineImage.color.b, 0);

        initPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);
        rangeX = new Vector2(initPosition.x - rangeLeftX, initPosition.x + rangeRightX);
        rangeY = new Vector2(initPosition.y - rangeLeftY, initPosition.y + rangeRightY);


        //Lock icon
        if(buttonIcon != null)
        {
            initIconSprite = buttonIcon.GetComponent<Image>().sprite;
        }

        subscribeToColor();
        setupColors();

        holdBuffer = 0.0f;
        isTouchDown = false;

        taps = 0;
        tapsPerSecond = 0;
        tapTimes = new List<float>();
    }

    //Method to check if everything has been injected as promised, if not it will throw a merciless exception
    private void checkStatus()
    {
        //lol someday
    }


    /***Realtime update***/
    public virtual void realTimeSubscribe(UIController controller = null)
    {
        if(isRealTimeUpdateable && !hasWarnings)
        {
            if(controller != null)
            {
                controller.requestRealTimeUpdate(this);
            }
            else if(uiController != null)
            {
                uiController.requestRealTimeUpdate(this);
            }
            
        }
        
    }

    public virtual void realTimeUpdate(Dictionary<string, object> parameters)
    {
        if(isRealTimeUpdateable && !hasWarnings)
        {
            calculateTapsPerSecond();

            if(isHoldable) checkStartHolding();

            if (parameters == null) parameters = onUpdateParams;
            simulateOnUpdate(parameters);

            timeSinceNotClicked += Time.deltaTime;
            
            checkHighlightCondition();
        }
    }

    private void calculateTapsPerSecond()
    {
        if (!isInteractable || !isRealTimeUpdateable) return;

        for (int i = 0; i < tapTimes.Count; i++)
        {
            if (tapTimes[i] <= Time.timeSinceLevelLoad - 1)
            {
                tapTimes.RemoveAt(i);
            }
        }
        tapsPerSecond = tapTimes.Count;
    }

    public virtual void realTimeUnsubscribe(UIController controller = null)
    {
        if(isRealTimeUpdateable)
        {
            if (controller != null)
            {
                controller.pullOutFromRealTimeUpdate(this);
            }
            else if (uiController != null)
            {
                uiController.pullOutFromRealTimeUpdate(this);
            }
        }
    }

    public void addToBankParams(string key, object v)
    {
        if(onBankChangeParams.ContainsKey(key))
        {
            onBankChangeParams[key] = v;
        }
        else
        {
            onBankChangeParams.Add(key, v);
        }
    }

    public void displayBadge()
    {
        if(usesBadge && badge != null)
        {
            badge.SetActive(true);
            punchBadge();
        }
    }


    public void hideBadge()
    {
        if(usesBadge && badge != null)
        {
            punchBadge();
            badge.SetActive(false);
        }
    }

    private void punchBadge()
    {
        if(usesBadge && badge != null)
        {
            badge.transform.DOPunchScale(Vector3.one * 2f, 0.2f);
        }
    }


    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (uiController != null && !uiController.registerUITouch(this)) return;

        isTouchDown = true;

        if (!isInteractable) return;

        if (onTouched != null)
        {
            simulateOnTouched();
        }

        if (isLocked) return;

        timeSinceNotClicked = 0;
        taps++;

        tapTimes.Add(Time.timeSinceLevelLoad);

        if (isHighLighted) downlight();

        animateOnTouch(anim);
        playInteractableSound();
    }

    public bool hasOnTouchedEvents()
    {
        return onTouched != null && onTouched.eventsCount > 0;
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        resetHold();

        if (isInteractable)
        {
            //Debug.Log("Pointer up");
            simulateOnReleased();
        }
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (isInteractable)
        {
            //Debug.Log("Pointer exit");
            simulateOnHoverOut();
        }
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        if (isInteractable)
        {
            //Debug.Log("Pointer enter");
            simulateOnHoverIn();
        }
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (isInteractable)
        {
            //Debug.Log("Drag");
            simulateOnDragged(eventData.delta);
        }
    }


    /**event encapsulations**/
    public virtual void simulateOnHoverIn()
    {
        onHoverIn?.Invoke(true);
    }

    public virtual void simulateOnHoverOut()
    {
        onHoverOut?.Invoke(true);
    }

    public virtual void simulateOnReleased()
    {
        onReleased?.Invoke(true);
    }

    public virtual void simulateOnDragged(Vector2 delta)
    {
        onDragged.Invoke(delta);

        if (isDraggable)
        {
            Vector2 newVec = new Vector2(transform.localPosition.x, transform.localPosition.y);
               
            
            if(!lockX)
            {
                float posX = newVec.x + delta.x;

                if (posX >= rangeX.x && posX <= rangeX.y) newVec.Set(posX, newVec.y);
            }

            if (!lockY)
            {
                float posY = newVec.y + delta.y;

                if (posY >= rangeY.x && posY <= rangeY.y) newVec.Set(newVec.x, posY);
            }

            transform.localPosition = newVec;
        }
    }

    public virtual void simulateOnTouched()
    {
        onTouched?.Invoke(isLocked);
    }

    private void checkStartHolding()
    {
        if(isTouchDown)
        {
            holdBuffer += Time.deltaTime;
            if(holdBuffer > holdTreshold)
            {
                simulateOnHolded(holdBuffer);
            }
        }
    }

    public virtual void simulateOnHolded(float elapsedTime)
    {
        onHolded?.Invoke(isLocked);
    }

    private void resetHold()
    {
        isTouchDown = false;
        holdBuffer = 0.0f;
    }

    public virtual void simulateOnUpdate(Dictionary<string, object> parameters)
    {
        onUpdate?.Invoke(parameters);
    }

    public virtual void simulateOnBankChange(Dictionary<string, object> parameters)
    {
        onBankChange?.Invoke(parameters);
    }

    private void simulateOnTouchedAnimFinished()
    {
        onTouchedAnimFinished?.Invoke(isLocked);
    }

    public virtual void lockInteraction(bool isLock)
    {
        if (isLock) lockInteraction();
        else unlockInteraction();
    }

    public virtual void lockInteraction()
    {
        if (mainImage == null) mainImage = GetComponent<Image>();

        if (mainImage != null)
        {
            mainImage.color = lockedColor;

            if(usesBadge) badge.SetActive(false);
        }

        if(buttonIcon != null)
        {
            Texture2D tex = Resources.Load<Texture2D>("UI/Sprites/lock");
            if (tex != null)
            {
                Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 50);
                buttonIcon.GetComponent<Image>().sprite = sprite;
            }
        }

        isLocked = true;
    }

    public virtual void unlockInteraction()
    {
        if(mainImage != null)
        {
            mainImage.color = mainColor;
        }

        if (buttonIcon != null)
        {
            buttonIcon.GetComponent<Image>().sprite = initIconSprite;
        }

        isLocked = false;
    }


    /***Highlightable***/

    public void toggleHighLight()
    {
        if(isHighlightable && !hasWarnings)
        {
            if(isHighLighted) downlight();
            else highlight();
            
        }
    }

    public void highlight(bool isOn)
    {
        if (!isHighlightable) return;

        if (isOn)
        {
            Sequence seq = highlight();
        }
        else
        {
            downlight();
        }
    }

    public Sequence highlight()
    {
        if (mainImage == null) init();

        if (!isHighLighted)
        {
            isHighLighted = true;

            if (highlightSequence != null) highlightSequence.Kill();

            if (!isLocked)
            {
                Color lightHighlightColor = Color.Lerp(mainColor, highlightColor, 0.5f);

                highlightSequence = DOTween.Sequence();
                highlightSequence.Append(mainImage.DOColor(lightHighlightColor, 1f));
                highlightSequence.Join(transform.DOScale(transform.localScale * 1.1f, 1f));
            }

            if (hasStrongHighlight && strongHighlight != null)
            {
                strongHighlight.SetActive(true);
                strongHighlight.transform.localScale = Vector3.one;
                Image strongHighlightImage = strongHighlight.GetComponent<Image>();
                strongHighlightImage.color = Color.white;


                Sequence strongHighlightSequence = DOTween.Sequence();
                strongHighlightSequence.Append(strongHighlight.transform.DOScale(1.4f, 0.3f));
                strongHighlightSequence.Join(strongHighlightImage.DOColor(new Color(1, 1, 1, 0), 0.3f));
                strongHighlightSequence.SetLoops(-1, LoopType.Restart);

                highlightSequence.Join(strongHighlightSequence);
            }

            highlightSequence.SetLoops(-1, LoopType.Yoyo);
        }
        return highlightSequence;
    }

    public void downlight()
    {
        isHighLighted = false;

        if (highlightSequence != null) highlightSequence.Kill();

        if(hasStrongHighlight) strongHighlight.SetActive(false);

        highlightSequence = DOTween.Sequence();
        highlightSequence.Append(mainImage.DOColor(mainColor, 0.1f));
        highlightSequence.Join(transform.DOScale(initScale, 0.1f));

        highlightSequence.Play();
    }


    private void checkHighlightCondition()
    {
        if(highlightOnUnlocking)
        {

            if (timeSinceNotClicked >= unlockingHighlightTime)
            {

                if (highlightedTimes > 0 && !isHighLighted && !isLocked && tapsToStopHighlight > taps)
                {
                    highlightedTimes--;

                    highlight();
                }
            }
        }
    }


    /**Outlinable**/
    public void showOutline()
    {
        if(isOutlinable && outlineImage != null)
        {
            outlineImage.DOFade(255, outlineTime);
        }
    }

    public void hideOutline()
    {
        if (isOutlinable && outlineImage != null)
        {
            outlineImage.DOFade(0, outlineTime);
        }
    }

    /**Animatable**/
    protected void animateOnTouch(UIAnims a)
    {
        Sequence seq = DOTween.Sequence();

        if (isAnimatable)
        {
            switch (a)
            {
                case UIAnims.SCALE_PUNCH:

                    seq.Append(transform.DOPunchScale(Vector3.one * punchSizeIncrement, punchTimeLength));

                    break;
            }
        }

        if (isTintable)
        {
            seq.Insert(0, mainImage.DOBlendableColor(touchedColor, useAnimatableTimeScale? punchTimeLength/2 : tintDuration/2));
            seq.Append(mainImage.DOColor(mainColor, useAnimatableTimeScale ? punchTimeLength / 2 : tintDuration / 2));
        }

        seq.AppendCallback(() =>
        {
            if (this == null || !isAnimatable) return;

            //Failsafe do the button doesn't keep growing on clicking
            if (transform.localScale.x != initScale.x || transform.localScale.y != initScale.y)
            {
                transform.DOScale(initScale, 0.1f);
                if(lookComponentsInChildren)
                {
                    mainImage.transform.localScale = childScale;
                }
            }

            simulateOnTouchedAnimFinished();
        });

        seq.Play();
    }

    //Sound on interacted
    protected void playInteractableSound()
    {
        if(audioController != null)
        {
            audioController.playSound(sound);
        }
    }

    /***ILocalizable***/
    public void onLangChanged(LocalizationController locController = null)
    {
        if (isLocalizable)
        {
            foreach (UILocalizable item in uiLocalizables)
            {
                item.txt.text = loc.getTextByKey(item.stringRef.key);
            }
        }
    }

    public void subscribeToLangChanges(LocalizationController locController = null)
    {
        if (isLocalizable)
        {
            loc.subscribeToLocListening(this);
        }
    }

    public void unsubscribeToLangChanges()
    {
        if (isLocalizable)
        {
            loc.unsubscribeToLocListening(this);
        }
    }


    //Bank methods
    public void subscribeToBankChanges(bool skipTalk = false)
    {
        if (isBankListener && bank != null && !hasWarnings)
        {
            foreach (Currency currency in currenciesToListen)
            {
                bank.onSubscribe(this, currency, skipTalk);
            }
            
        }
    }

    public void unsubscribeToBankChanges()
    {
        if (isBankListener && bank != null && !hasWarnings)
        {
            foreach (Currency currency in currenciesToListen)
            {
                bank.onUnsubscribe(this, currency);
            }

        }
    }

    public void onListeningToBank(Currency currency, double delta, double total, string formattedTotal)
    {
        addToBankParams("ui_item", this);
        addToBankParams("currency", currency);
        addToBankParams("delta", delta);
        addToBankParams("total", total);
        addToBankParams("formatted_total", formattedTotal);
        

        simulateOnBankChange(onBankChangeParams);
    }

    public Color getColor(string type)
    {
        Color returnable = mainColor;
        if(!doesOverridePalette)
        {
            returnable = colorizableController.getColor(type);
        }

        return returnable;
    }

    public void setColor(Color color, float alpha)
    {
    }

    public void subscribeToColor()
    {
        if (usesColorModule && !doesOverridePalette) colorizableController.subscribeToColorChanges(this, "main");
    }

    public void unsubscribeToColor()
    {
        if (usesColorModule &&!doesOverridePalette) colorizableController.pullOutFromColorChanges(this);
    }

    public virtual void onColorChanged(Color color)
    {
        setupColors();
    }

    private void setupColors()
    {
        if (usesColorModule)
        {
            if (!doesOverridePalette)
            {
                mainColor = getColor("main");
                highlightColor = getColor("highlighted");
                touchedColor = getColor("main_dark");
                lockedColor = getColor("locked");
            }

            if (isLocked) mainImage.color = lockedColor;
            else mainImage.color = mainColor;
        }
        else
        {
            mainColor = mainImage == null? Color.white : mainImage.color;
            highlightColor = mainImage == null ? Color.white : mainImage.color;
            touchedColor = mainImage == null ? Color.white : mainImage.color;
            lockedColor = mainImage == null ? Color.grey : mainImage.color;
        }
    }

    public void addOnHoverInListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onHoverIn.eventsCount > 0) return;

        if (onHoverIn == null) onHoverIn = new GreenLoveEvent<bool>();
        onHoverIn.AddListener(action);
    }

    public void addOnHoverOutListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onHoverOut.eventsCount > 0) return;

        if (onHoverOut == null) onHoverOut = new GreenLoveEvent<bool>();
        onHoverOut.AddListener(action);
    }

    public void addOnReleasedListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onReleased.eventsCount > 0) return;

        if (onReleased == null) onReleased = new GreenLoveEvent<bool>();
        onReleased.AddListener(action);
    }

    public void addOnDraggedListener(UnityAction<Vector2> action, bool allowMany = true)
    {
        if (!allowMany && onDragged.eventsCount > 0) return;

        if (onDragged == null) onDragged = new GreenLoveEvent<Vector2>();
        onDragged.AddListener(action);
    }

    public virtual void addOnTouchedListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onTouched.eventsCount > 0) return;

        if (onTouched == null) onTouched = new GreenLoveEvent<bool>();
        onTouched.AddListener(action);
    }

    public virtual void addOnHoldedListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onTouched.eventsCount > 0) return;

        if (onHolded == null) onHolded = new GreenLoveEvent<bool>();
        onHolded.AddListener(action);
    }

    public void addOnTouchedAnimFinishedListener(UnityAction<bool> action, bool allowMany = true)
    {
        if (!allowMany && onTouchedAnimFinished.eventsCount > 0) return;

        if (onTouchedAnimFinished == null) onTouchedAnimFinished = new GreenLoveEvent<bool>();
        onTouchedAnimFinished.AddListener(action);
    }

    public void addOnUpdateListener(UnityAction<Dictionary<string, object>> action, bool allowMany = true)
    {
        if (!allowMany && onUpdate.eventsCount > 0) return;

        if (onUpdate == null) onUpdate = new GreenLoveEvent<Dictionary<string, object>>();
        onUpdate.AddListener(action);
    }

    public void addOnBankChangeListener(UnityAction<Dictionary<string, object>> action, bool allowMany = true)
    {
        if (!allowMany && onBankChange.eventsCount > 0) return;

        if (onBankChange == null) onBankChange = new GreenLoveEvent<Dictionary<string, object>>();
        onBankChange.AddListener(action);
    }


#if UNITY_EDITOR

    [CustomEditor(typeof(UIItem)), CanEditMultipleObjects]
    public class UIItemEditor : Editor
    {
        protected bool toggleGroup = true;

        SerializedProperty uiLoc, currenciesToListen, sound;

        protected virtual void OnEnable()
        {
            uiLoc = serializedObject.FindProperty("uiLocalizables");
            currenciesToListen = serializedObject.FindProperty("currenciesToListen");
            sound = serializedObject.FindProperty("sound");
        }

        public override void OnInspectorGUI()
        {
            UIItem item = (UIItem)target;

            toggleModule(item);

            selfModule(item);

            colorModule(item);

            realTimeUpdateModule(item);

            bankUpdateModule(item);

            interactableModule(item);

            highlightableModule(item);

            outlinableModule(item);

            lookComponentsInChildren(item);

            localizableModule(item);

            EditorGUILayout.Space();

            warningsModule(item, true);

            saveChangesModule(item);
        }

        protected virtual void colorModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Color Module: ");

            item.usesColorModule = EditorGUILayout.Toggle("Uses Color Module", item.usesColorModule);

            if (item.usesColorModule)
            {
                item.doesOverridePalette = EditorGUILayout.Toggle("Overrides central palettes?", item.doesOverridePalette);

                if (item.doesOverridePalette)
                {
                    item.mainColor = EditorGUILayout.ColorField("Main Color", item.mainColor);
                    item.touchedColor = EditorGUILayout.ColorField("Touched Color", item.touchedColor);
                    item.highlightColor = EditorGUILayout.ColorField("Highlight Color", item.highlightColor);
                    item.lockedColor = EditorGUILayout.ColorField("Locked Color", item.lockedColor);
                }
                else
                {
                    item.colorizableController = (ColorizableController)EditorGUILayout.ObjectField("Color Controller", item.colorizableController, typeof(ColorizableController), true);
                }
            }

            EditorGUILayout.EndVertical();
        }

        protected virtual void toggleModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            toggleGroup = EditorGUILayout.BeginToggleGroup("Enable all? ", toggleGroup);
            item.isRealTimeUpdateable = EditorGUILayout.Toggle("Subscribe to realtime updates?", item.isRealTimeUpdateable);
            item.isBankListener = EditorGUILayout.Toggle("Subscribe to bank updates?", item.isBankListener);
            item.isInteractable = EditorGUILayout.Toggle("Is interactable?", item.isInteractable);
            item.isHighlightable = EditorGUILayout.Toggle("Is Highlightable? (required for tutos)", item.isHighlightable);
            item.isOutlinable = EditorGUILayout.Toggle("Is outlinable?", item.isOutlinable);
            item.lookComponentsInChildren = EditorGUILayout.Toggle("Look components in children?", item.lookComponentsInChildren);
            item.isLocalizable = EditorGUILayout.Toggle("Is Localizable?", item.isLocalizable);

            EditorGUILayout.EndToggleGroup();
        }

        protected virtual void realTimeUpdateModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.isRealTimeUpdateable)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Real time updates: ");

                if (item.uiController == null)
                {
                    EditorGUILayout.HelpBox("Please add an UI controller to request realtime updates", MessageType.Warning);
                    item.uiController = (UIController)EditorGUILayout.ObjectField("UIController", item.uiController, typeof(UIController), true);

                }

                else
                {
                    if (item.hasWarnings)
                    {
                        EditorGUILayout.HelpBox("please fix your warnings, otherwise this item won't be subscribed to realtime updates", MessageType.Warning);
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("This item will be hooked to realtime updates on play mode, please fill your update method according to your ui item", MessageType.None);
                        item.usesBadge = EditorGUILayout.Toggle("Uses badge?", item.usesBadge);

                        if (item.usesBadge)
                        {
                            if (item.badge == null)
                            {
                                item.badge = (GameObject)GameObject.Instantiate(Resources.Load("UI/Badge"));
                                item.badge.transform.parent = item.transform;
                            }
                        }

                    }
                }

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual void bankUpdateModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.isBankListener)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Listen to updates on the bank: ");

                if (item.bank == null)
                {
                    EditorGUILayout.HelpBox("Please add Bank to request updates on a currency", MessageType.Warning);
                    item.bank = (Bank)EditorGUILayout.ObjectField("Bank", item.bank, typeof(Bank), true);
                }

                else
                {
                    if (item.hasWarnings)
                    {
                        EditorGUILayout.HelpBox("please fix your warnings, otherwise this item won't be subscribed to bank updates", MessageType.Warning);
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("This item will be hooked to bank updates on play mode, please fill your bank listener method according to your ui item", MessageType.None);


                        EditorGUILayout.PropertyField(currenciesToListen, new GUIContent("Currencies to listen"), true);
                        serializedObject.ApplyModifiedProperties();

                        item.usesBadge = EditorGUILayout.Toggle("Uses badge?", item.usesBadge);

                        if (item.usesBadge)
                        {
                            if (item.badge == null)
                            {
                                item.badge = (GameObject)GameObject.Instantiate(Resources.Load("UI/Badge"));
                                item.badge.transform.parent = item.transform;
                            }
                        }

                    }
                }

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual void interactableModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.isInteractable)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Interactable: ");

                EditorGUILayout.BeginFadeGroup(1);

                EditorGUILayout.Space();

                item.isAnimatable = EditorGUILayout.Toggle("Is Animatable?", item.isAnimatable);

                if (item.isAnimatable)
                {
                    item.anim = (UIAnims)EditorGUILayout.EnumPopup("Animation type: ", item.anim);
                    if(item.anim == UIAnims.SCALE_PUNCH)
                    {
                        item.punchSizeIncrement = EditorGUILayout.Slider("Size increment:", item.punchSizeIncrement, 0f, 1f);
                        item.punchTimeLength = EditorGUILayout.Slider("Punch time: ", item.punchTimeLength, 0f, 1f);
                    }
                }

                EditorGUILayout.Space();

                item.isTintable = EditorGUILayout.Toggle("IsTintable?", item.isTintable);
                if (item.isTintable)
                {
                    if(item.isAnimatable) item.useAnimatableTimeScale = EditorGUILayout.Toggle("Use animatable time?", item.useAnimatableTimeScale);
                    if(!item.useAnimatableTimeScale) item.tintDuration = EditorGUILayout.Slider("Tint time: ", item.tintDuration, 0f, 1f);
                }
                EditorGUILayout.Space();

                item.hasTouchSound = EditorGUILayout.Toggle("Has sound on touched?", item.hasTouchSound);
                if (item.hasTouchSound)
                {
                    EditorGUILayout.PropertyField(sound, new GUIContent("Touched Sound"));
                    serializedObject.ApplyModifiedProperties();

                    item.audioController = (AudioController)EditorGUILayout.ObjectField("Audio Controller", item.audioController, typeof(AudioController), true);
                }
                EditorGUILayout.Space();

                item.isDraggable = EditorGUILayout.Toggle("Is Draggable?", item.isDraggable);
                if(item.isDraggable)
                {
                    EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

                    EditorGUILayout.BeginVertical();
                    item.lockX = EditorGUILayout.Toggle("Lock X?", item.lockX);

                    if (!item.lockX)
                    {
                        EditorGUILayout.BeginHorizontal();

                        item.rangeLeftX = EditorGUILayout.FloatField(item.rangeLeftX);
                        item.rangeRightX = EditorGUILayout.FloatField(item.rangeRightX);

                        EditorGUILayout.EndHorizontal();

                    }

                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical();
                    item.lockY = EditorGUILayout.Toggle("Lock Y?", item.lockY);

                    if (!item.lockY)
                    {
                        EditorGUILayout.BeginHorizontal();

                        item.rangeLeftY = EditorGUILayout.FloatField(item.rangeLeftY);
                        item.rangeRightY = EditorGUILayout.FloatField(item.rangeRightY);

                        EditorGUILayout.EndHorizontal();

                    }

                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
                item.isHoldable = EditorGUILayout.Toggle("Is Holdable?", item.isHoldable);

                EditorGUILayout.Space();
                item.buttonIcon = (GameObject)EditorGUILayout.ObjectField("Button icon: ", item.buttonIcon, typeof(GameObject), true);

                EditorGUILayout.EndFadeGroup();

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual void highlightableModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.isHighlightable)
            {
                EditorGUILayout.Space();
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Highlightable: ");

                item.highlightOnUnlocking = EditorGUILayout.Toggle("Highlight on unlocking?", item.highlightOnUnlocking);
                if(item.highlightOnUnlocking)
                {
                    item.unlockingHighlightTime = EditorGUILayout.Slider("How long not clicking for it to be highlighted?", item.unlockingHighlightTime, 0, 10);
                    item.highlightedTimes = EditorGUILayout.IntSlider("How many times should it be highlighted?", item.highlightedTimes, 1, 10);
                    item.tapsToStopHighlight = EditorGUILayout.IntSlider("How many taps to stop highlighting?", item.tapsToStopHighlight, 1, 500);

                }

                item.hasStrongHighlight = EditorGUILayout.Toggle("Uses strong highlight?", item.hasStrongHighlight);
                if(item.hasStrongHighlight)
                {
                    item.strongHighlight = (GameObject)EditorGUILayout.ObjectField("Strong highlight Object: ", item.strongHighlight, typeof(GameObject), true);
                }

                EditorGUILayout.EndVertical();
            }
        }


        protected virtual void outlinableModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.isOutlinable)
            {
                EditorGUILayout.Space();
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Outlinable: ");

                item.outlineImage = (Image)EditorGUILayout.ObjectField("Outline Image: ", item.outlineImage, typeof(Image), true);
                item.outlineTime = EditorGUILayout.Slider("Outline fade time: ", item.outlineTime, 0, 5);

                EditorGUILayout.EndVertical();
            }
        }
        protected virtual void lookComponentsInChildren<T>(T Item)
        {
            UIItem item = Item as UIItem;

            if (item.lookComponentsInChildren)
            {
                EditorGUILayout.Space();
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Look component in children: ");

                item.componentChild = (GameObject)EditorGUILayout.ObjectField("Child: ", item.componentChild, typeof(GameObject), true);

                //Check if is children
                if (item.componentChild != null && item.componentChild.transform.parent != item.transform)
                {
                    EditorGUILayout.HelpBox("The provided object is not child of this object, please provide the right object", MessageType.Warning);
                }

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual void localizableModule<T>(T Item)
        {
            UIItem item = Item as UIItem;
            
            if(item.isLocalizable)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Localizable: ");

                item.loc = (LocalizationController)EditorGUILayout.ObjectField("Localization Controller", item.loc, typeof(LocalizationController), true);

                EditorGUILayout.PropertyField(uiLoc, new GUIContent("UI Localizables"), true);
                serializedObject.ApplyModifiedProperties();

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual bool warningsModule<T>(T Item, bool shouldRender = false)
        {
            UIItem item = Item as UIItem;
            int initChecks = 0;
            bool hasWarnings = false;

            //Initial checks
            item.hasWarnings = false;

            if(shouldRender) EditorGUILayout.BeginVertical(EditorStyles.helpBox);


            if (shouldRender) EditorGUILayout.LabelField("Warnings: ");

            //Are we in a canvas?
            if (!(item.transform is RectTransform))
            {
                if (shouldRender) EditorGUILayout.HelpBox("This element is probably not on a canvas, UIItems are for canvas only elements", MessageType.Warning);
                initChecks++;
            }

            //Do we have an image
            if (item.GetComponent<Image>() == null)
            {
                if (item.lookComponentsInChildren && item.componentChild != null && item.componentChild.GetComponent<Image>() == null)
                {
                    if (shouldRender)
                    {
                        EditorGUILayout.HelpBox("This GameObject has no Image component, please add one", MessageType.Warning);
                        if (GUILayout.Button("Add image component"))
                        {
                            if (!item.lookComponentsInChildren)
                            {
                                item.gameObject.AddComponent<Image>();
                                moveToBotton(item);
                            }
                            else
                            {
                                item.componentChild.AddComponent<Image>();
                            }
                        }
                    }
                    initChecks++;
                }
            }

            //Highlight on unlocking
            if(item.highlightOnUnlocking)
            {
                if(!item.isRealTimeUpdateable)
                {
                    EditorGUILayout.HelpBox("This item is not realtime updateable, it won't highlight on unlocking NEVER, please subscribe to realtime updates", MessageType.Warning);

                    initChecks++;
                }
            }

            //Hold module
            if(item.isHoldable)
            {
                if(!item.isRealTimeUpdateable)
                {
                    EditorGUILayout.HelpBox("This item is not realtime updateable, it won't respond to hold events EVER. To fix this, please subscribe to realtime updates", MessageType.Warning);
                    initChecks++;
                }
            }

            if (initChecks <= 0)
            {
                if (shouldRender) EditorGUILayout.LabelField("None. Everything seems fine!");
                hasWarnings = false;
            }
            else
            {
                if (shouldRender) EditorGUILayout.LabelField(string.Format("Warnings to fix: {0}", initChecks));
                item.hasWarnings = true;
                hasWarnings = true;
            }
            if (shouldRender) EditorGUILayout.EndVertical();

            return hasWarnings;
        }

        protected virtual void saveChangesModule<T>(T Item)
        {
            UIItem item = Item as UIItem;

            //save the changes
            if (GUI.changed)
            {
                EditorUtility.SetDirty(item);
                EditorSceneManager.MarkSceneDirty(item.gameObject.scene);
            }
        }

        protected void moveToBotton(UIItem item)
        {
            while (moveDown(item));
        }

        protected bool moveDown(UIItem item)
        {
            
            return UnityEditorInternal.ComponentUtility.MoveComponentDown(item);
        }

        protected virtual void selfModule<T>(T Item) { }

    }
#endif
}

[Serializable]
public enum UIAnims
{
    NONE,
    SCALE_PUNCH,
    ERROR
}
