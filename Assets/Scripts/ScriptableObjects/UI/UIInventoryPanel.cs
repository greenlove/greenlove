﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryPanel : UIPanel
{
    public GameObject itemsContainer;

    public List<UIBigInventoryItem> items;

    public override void Start()
    {
        base.Start();

        items = new List<UIBigInventoryItem>();
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(UIInventoryPanel)), CanEditMultipleObjects]
    public class UIInventoryPanelEditor : UIPanelEditor
    {
    }
#endif
}
