﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryItem : UIItem
{
    public Image icon;
    public Image bgCircle;
    public Text amountText;


#if UNITY_EDITOR
    [CustomEditor(typeof(UIInventoryItem)), CanEditMultipleObjects]
    public class UIInventoryItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            UIInventoryItem item = Item as UIInventoryItem;
            
            item.icon = (Image)EditorGUILayout.ObjectField("Icon", item.icon, typeof(Image), true);
            item.bgCircle = (Image)EditorGUILayout.ObjectField("Circle Background", item.bgCircle, typeof(Image), true);
            item.amountText = (Text)EditorGUILayout.ObjectField("Amount Text", item.amountText, typeof(Text), true);            

            EditorGUILayout.EndVertical();
        }
        
    }
#endif
}
