﻿using Ads;
using BuildingBlocks;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.U2D;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Building Blocks/UI/UI Controller")]
public class UIController : Controller
{
    private Canvas canvas;
    public GameObject fullArea;
    public GameObject panelArea;
    public GameObject hudArea;
    private RectTransform canvasRect;

    public Stack<UIPanel> panels;

    private List<IUIRealTimeUpdateable> updateables;

    [Header("Main UI")]
    public Library bottomButtons;
    public Library TopButtons;

    [Header("Tutorials UI")]
    public Library tutorialPanel;

    [Header("Invokable popups")]
    public Library tutQuestGroup;
    public Library alphaListGroup;
    public Library alphaPopupGroup;
    public Library speechPopupGroup;
    public Library dumpPopupGroup;
    public Library inventoryGroup;
    public Library townHallGroup;
    public Library buildingModeGroup;
    public Library shopGroup;
    public Library giftGroup;
    public Library menuGroup;
    public Library buildingUpgradeGroup;
    public Library excavatorGroup;
    public Library buildingGroup;

    [Header("Overlays")]
    public Library overlaysGroup;

    [Header("Boosts")]
    private GameObject boostsBox;

    [Header("Atlases")]
    public Dictionary<string, SpriteAtlas> atlases;

    private UIItem lockingItem;

    private Dictionary<Type, int> openedTimes;
    private int hintStreak = 2;

    public override void init(Main main)
    {
        base.init(main);

        findCanvas();

        if (updateables == null) updateables = new List<IUIRealTimeUpdateable>();

        panels = new Stack<UIPanel>();
        boostsBox = hudArea.transform.Find("BoostsBox").gameObject;

        findController<TouchController>().onReleased.AddListener((Dictionary<string, object> parameters) =>
        {
            resetAlphaListSelection();
            dispelSimpleFeedback();
        });

        lockingItem = null;

        openedTimes = new Dictionary<Type, int>();
    }

    public override void update(float dt)
    {
        if (isStopped) return;

        if (updateables != null)
        {
            for (int i = updateables.Count - 1; i >= 0; i--)
            {
                updateables[i].realTimeUpdate(null);
            }
        }
    }


    public void requestRealTimeUpdate(IUIRealTimeUpdateable updateable)
    {
        if (updateables == null) updateables = new List<IUIRealTimeUpdateable>();
        if (!updateables.Contains(updateable)) updateables.Add(updateable);
    }


    public void pullOutFromRealTimeUpdate(IUIRealTimeUpdateable updateable)
    {
        updateables.Remove(updateable);
    }


    public void findCanvas()
    {
        canvas = context.transform.Find("Canvas").GetComponent<Canvas>();
        fullArea = canvas.transform.Find("FullArea").gameObject;
        panelArea = canvas.transform.Find("PanelArea").gameObject;
        hudArea = canvas.transform.Find("HudArea").gameObject;

        canvasRect = canvas.GetComponent<RectTransform>();
    }

    internal void invokeSettingsButton()
    {
        GameObject settingsButton = (GameObject)Instantiate(bottomButtons[(int)BottomButtons.SETTINGS_BUTTON]);
        settingsButton.transform.SetParent(hudArea.transform, false);
        settingsButton.GetComponent<UIItem>().addOnTouchedListener((bool isLocked) =>
        {
            findController<GameplayController>().invokeDumpView(null);
        });
        cleanUpName(settingsButton);
    }

    internal void invokeShopButton()
    {
        GameObject btn = (GameObject)Instantiate(shopGroup[(int)UIShopGroup.SHOP_BUTTON]);
        btn.transform.SetParent(hudArea.transform, false);
        UIShopButton shopButton = btn.GetComponent<UIShopButton>();
        ColorizableController colors = findController<ColorizableController>();

        shopButton.addOnTouchedListener((bool isLocked) =>
        {
            UIShopPanel panel = (UIShopPanel)isPanelInvoked("ShopPopup");

            if (panel == null)
            {
                panel = findController<GameplayController>().invokeShop();
                shopButton.closeIcon.SetActive(true);
                shopButton.shopIcon.SetActive(false);

                panel.onClosed.AddListener((bool parameter) => {
                    shopButton.closeIcon.SetActive(false);
                    shopButton.shopIcon.SetActive(true);

                    findController<PurchaseController>().scrollFlag = false;
                });
            }
            else
            {
                panel.close();
            }
        });
        cleanUpName(btn);
    }

    internal void invokePetsGauge()
    {
        GameObject animalsNum = (GameObject)Instantiate(TopButtons[(int)TopUI.PETS_GAUGE]);
        animalsNum.transform.SetParent(hudArea.transform, false);
        UIPetsGauge updateable = animalsNum.GetComponent<UIPetsGauge>();

        updateable.addOnUpdateListener((Dictionary<string, object> dictionary) =>
        {
            updateable.amountText.text = context.getController<GameplayController>().gameplayManager.bank.getFormatted(Currency.DOGS);
        });

        cleanUpName(animalsNum);
    }

    internal void invokeGoldGauge()
    {
        GameObject cashNum = (GameObject)Instantiate(TopButtons[(int)TopUI.GOLD_GAUGE]);
        cleanUpName(cashNum);
        cashNum.transform.SetParent(hudArea.transform, false);
        UIItem cashUpdateable = cashNum.GetComponent<UIItem>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;
        Text goldText = cashNum.transform.Find("Text 1").GetComponent<Text>();

        cashUpdateable.addOnUpdateListener((Dictionary<string, object> dictionary) =>
        {
            goldText.text = bank.getFormatted(Currency.GOLD);
        });
    }

    internal void invokeQuestsButton()
    {
        GameObject tutBtn = (GameObject)Instantiate(bottomButtons[(int)BottomButtons.TUTORIAL_QUEST_BUTTON]);
        tutBtn.transform.SetParent(hudArea.transform, false);
        UIItem tutQuestButton = tutBtn.GetComponent<UIItem>();

        tutQuestButton.addOnTouchedListener((bool isLocked) =>
        {
            findController<GameplayController>().invokeTutQuestView(null);
        });

        tutQuestButton.addOnUpdateListener((Dictionary<string, object> dictionary) =>
        {
            tutQuestButton.highlight(findController<GameplayController>().tutorialManager.isAnyActiveQuestCompleted());
        });
        cleanUpName(tutBtn);

    }

    internal void invokeCreditsGauge()
    {
        GameObject gemsNum = (GameObject)Instantiate(TopButtons[(int)TopUI.CREDIT_GAUGE]);
        gemsNum.transform.SetParent(hudArea.transform, false);
        UIItem gemsUpdateable = gemsNum.GetComponent<UIItem>();
        Text cash = gemsNum.transform.Find("Text 1").GetComponent<Text>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;

        gemsUpdateable.addOnUpdateListener((Dictionary<string, object> dictionary) =>
        {
            cash.text = bank.getFormatted(Currency.CREDIT);
        });
        
        cleanUpName(gemsNum);
    }

    internal void invokePrestigeButton()
    {
        GameObject prestigeButton = (GameObject)Instantiate(TopButtons[(int)TopUI.PRESTIGE_BUTTON]);
        prestigeButton.transform.SetParent(hudArea.transform, false);
        UIItem button = prestigeButton.GetComponent<UIItem>();
        GameplayController gameplay = findController<GameplayController>();
        PrestigeManager manager = gameplay.prestigeManager;
        GameObject img = button.transform.Find("Image").gameObject;

        button.addOnTouchedListener((bool isLocked) => 
        {
            gameplay.invokePrestigeView();
            button.highlight(false);
        }, false);

        button.addOnUpdateListener((Dictionary<string, object> dictionary) =>
        {
            if (manager.canPrestige())
            {
                img.SetActive(true);
                button.unlockInteraction();
                button.highlight(true);
                button.realTimeUnsubscribe(this);
            }
            else
            {
                img.SetActive(false);
                button.lockInteraction();
            }
        });

        cleanUpName(prestigeButton);
    }

    internal void invokeCallButton()
    {
        GameplayController gameplay = findController<GameplayController>();

        Bank bank = findController<GameplayController>().gameplayManager.bank;
        GameObject callBtn = (GameObject)Instantiate(bottomButtons[(int)BottomButtons.CALL_BUTTON]);
        callBtn.transform.SetParent(hudArea.transform, false);
        setImg(callBtn.transform.Find("Icon").GetComponent<Image>(), "others", Breed.DOGS.ToString());

        int taps = 0;
        int tapsMax = 200;
        float tapsBuffer = 0;
        float tapsTimeout = 2.0f;

        float redStringBuffer = 0.0f;

        UIItem item = callBtn.GetComponent<UIItem>();

        item.addOnTouchedListener((bool isLocked) =>
        {
            taps++;
            if(taps < tapsMax) gameplay.summonAnimal(false);
        });

        //Redstring hold
        item.addOnHoldedListener((bool isLocked) =>
        {
            BuffRedString redString = (BuffRedString)gameplay.gameplayManager.buffs[Buffs.RED_STRING];
            redStringBuffer += Time.deltaTime;
            float timeBetweenCalls = 1.0f / redString.amountOfAnimals;

            if (redStringBuffer >= timeBetweenCalls && redString.canTapAndHold())
            {
                redStringBuffer = 0.0f;
                gameplay.summonAnimal(false);
            }
        });

        item.addOnUpdateListener((Dictionary<string, object> parameters) => 
        {
            tapsBuffer += Time.deltaTime;
            if(tapsBuffer >= tapsTimeout)
            {
                taps = 0;
                tapsBuffer = 0;
            }
            
            if (taps > 0)
            {
                bank.update(Currency.MAX_TAPS_PER_SEC_CALL_BUTTON, item.tapsPerSecond, BankUpdateStrategy.MAX);
            }
        });

        cleanUpName(callBtn);
    }

    private void injectBuildingViewBase(Building building, GameObject objPanel, Dictionary<string, string> texts, UnityAction<Dictionary<string, object>> updateEvent)
    {
        UIBuildingPanel panel = objPanel.GetComponent<UIBuildingPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;

        setBuildingImage(panel.buildingIcon, building.id, building.activeSegment);

        building.dispelWarningParticle();

        panel.title.text = texts["title"];

        panel.upgradeBuildingText.text = texts["upgrade_building"];

        panel.maxCapacityTitle.text = findController<LocalizationController>().getTextByKey("MAX_CAPACITY_TITLE");
        panel.maxCapacity.text = bank.toFormattedCurrency(building.getActiveSegment().capacity);

        panel.increaseServiceTitle.text = texts["increase_service"];

        panel.itemStars.minValue = 0;
        panel.itemStars.maxValue = 5;

        panel.increaseServiceSlider.minValue = 0;
        panel.increaseServiceSlider.maxValue = 99;

        bool canDisplayEvolveButton = findController<GameplayController>().tutorialManager.hasAllBeenDisplayed(Tutorials.FULL_HOME_TUTORIAL);
        panel.upgradeBuildingButton.gameObject.SetActive(building.activeSegment < building.segments.Length && canDisplayEvolveButton);

        panel.upgradeBuildingButton.addOnTouchedListener((bool isLocked) =>
        {
            if (isLocked) return;
            findController<GameplayController>().invokeUpgradeBuildingPanel(building);
        }, false);

        panel.increaseServiceButton.addOnTouchedListener((bool isLocked) =>
        {
            upgradeBuildingService(panel, building, !isLocked, true);
        }, false);

        panel.increaseServiceButton.onHolded.AddListener((bool isLocked) => 
        {
            upgradeBuildingService(panel, building, !isLocked);
        });

        panel.addOnUpdateListener(updateEvent);
    }

    private void upgradeBuildingService(UIBuildingPanel panel, Building building, bool canAfford, bool withEvent = false)
    {
        building.upgradeServiceTime(findController<GameplayController>().gameplayManager.bank, canAfford, withEvent: withEvent);

        if (!canAfford) return;

        panel.serviceTimeFrame.highlight(true);
        Sequence sequence = DOTween.Sequence();
        sequence.Append(panel.serviceTimeFrame.transform.DOScale(1.15f, 0.25f));
        sequence.Append(panel.serviceTimeFrame.transform.DOScale(1.0f, 0.25f));

        sequence.OnComplete(() => 
        {
            panel.serviceTimeFrame.highlight(false);
            panel.serviceItemImage.DOFade(1.0f, 0.1f);
        });
        sequence.Play();
    }

    private void injectBuildingView(Building building, GameObject objPanel, Dictionary<string, string> texts)
    {
        UIBuildingPanel panel = objPanel.GetComponent<UIBuildingPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;
        ColorizableController colors = findController<ColorizableController>();
        panel.upgradeStockButton.gameObject.SetActive(!building.id.ToString().Contains("HOME"));
        panel.upgradeStockPrice.text = bank.toFormattedCurrency(building.getStockPrice(), true);

        panel.increaseStockTitle.text = texts["increase_stock"];

        for (int i = 0; i < building.getActiveSegment().stockConstant; i++)
        {
            GameObject stockGO = GameObject.Instantiate((GameObject)buildingGroup[(int)BuildingPopupsGroup.STOCK_ITEM]);
            stockGO.transform.SetParent(panel.stockContainer.transform, false);

            stockGO.GetComponent<Image>().color = i >= building.getActiveSegment().stockAmount ? colors.getColor("second", 0.25f) : colors.getColor("second");
        } 

        panel.addOnInvokeAnimFinishedListener((Dictionary<string, object> parameters) => 
        {
            if (building.hasNoStockFlag)
            {
                panel.upgradeStockButton.highlight(true);
                building.hasNoStockFlag = false;
            }
            else if(building.hasNoCapacityFlag)
            {
                panel.upgradeBuildingButton.highlight(true);
                building.hasNoCapacityFlag = false;
            }
        });
        
        panel.upgradeStockButton.addOnTouchedListener((bool isLocked) =>
        {
            if (!isLocked)
            {
                panel.isFillingStock = true;
                Sequence seq = DOTween.Sequence();
                for (int i = (int)building.getActiveSegment().stockAmount; i < panel.stockContainer.transform.childCount; i++)
                {
                    GameObject child = panel.stockContainer.transform.GetChild(i).gameObject;
                    seq.Insert((i + 1) * 0.15f, child.transform.DOPunchScale(Vector3.one * 1.01f, 0.2f, 2, 0.3f));
                    seq.Join(child.GetComponent<Image>().DOColor(colors.getColor("second"), 0.3f).SetEase(Ease.OutBounce));
                }
                seq.OnComplete(() => { panel.isFillingStock = false; });
            }

            building.getActiveSegment().refillStock(!isLocked);
        });

        UnityAction<Dictionary<string, object>> updateAction = new UnityAction<Dictionary<string, object>>((Dictionary<string, object> parameters) =>
        {
            panel.serviceTimeAmount.text = TimeSpan.FromSeconds(building.currentServiceTime()).ToString("mm':'ss");

            if (bank.canAfford(Currency.GOLD, building.getStockPrice()) && !building.getActiveSegment().isStockReplenished())
            {
                panel.upgradeStockButton.unlockInteraction();
            }
            else
            {
                panel.upgradeStockButton.lockInteraction();
            }

            updateBuildingViewBase(building, objPanel, texts);
        });

        injectBuildingViewBase(building, objPanel, texts, updateAction);
    }

    private void updateBuildingViewBase(Building building, GameObject objPanel, Dictionary<string, string> texts)
    {
        UIBuildingPanel panel = objPanel.GetComponent<UIBuildingPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;

        //Capacity
        double buildingPrice = building.getNextLevelPrice();
        string buildingFormattedPrice = bank.toFormattedCurrency(buildingPrice, true);
        float normalizedStep = building.getActiveSegment().getStepNormalized();

        panel.serviceTimeAmount.text = TimeSpan.FromSeconds(building.currentServiceTime()).ToString("mm':'ss");

        int evoIndex = building.activeSegment * 2;
        if (normalizedStep >= 1.0f) evoIndex += 1;

        setBuildingServiceImage(panel.itemIcon, building.textsID, evoIndex);
        panel.itemTitle.text = building.getActiveSegment().upgradeTitles[normalizedStep >= 1.0f ? 1 : 0];
        panel.itemDescription.text = building.getActiveSegment().upgradeDescriptions[normalizedStep >= 1.0f ? 1 : 0];
        panel.itemStars.value = (float)0.5f + (evoIndex / 2.0f);

        panel.increaseServiceTitle.text = texts["increase_service"];
        panel.increaseServiceSlider.value = building.getActiveSegment().currentStep;

        panel.upgradeServicePrice.text = bank.toFormattedCurrency(building.getNextLevelPrice(), true);
        if (!bank.canAfford(Currency.GOLD, building.getNextLevelPrice()) || normalizedStep >= 1.0f)
        {
            panel.increaseServiceButton.lockInteraction();
        }
        else
        {
            panel.increaseServiceButton.unlockInteraction();
        }
    }

    public void injectHomeView(Home home, GameObject homePanel)
    {
        Dictionary<string, string> texts = new Dictionary<string, string> {
            { "title", findController<LocalizationController>().getTextByKey("HOME_TITLE") },
            { "upgrade_building", string.Format(findController<LocalizationController>().getTextByKey("UI_BUFFS_IMPROVE_BUFF_TEXT"), findController<LocalizationController>().getTextByKey("HOME_TITLE")) },
            { "increase_stock", findController<LocalizationController>().getTextByKey("HOME_INCREASE_STOCK_TITLE") },
            { "increase_service", findController<LocalizationController>().getTextByKey("HOME_INCREASE_SERVICE_TITLE") },
            { "current_stock", findController<LocalizationController>().getTextByKey("HOME_CURRENT_CAPACITY") }
        };

        UIHomePanel panel = homePanel.GetComponent<UIHomePanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;

        panel.capacitySlider.minValue = 0;
        panel.capacitySlider.maxValue = (float) home.getActiveSegment().capacity;

        UnityAction<Dictionary<string, object>> updateAction = new UnityAction<Dictionary<string, object>>((Dictionary<string, object> parameters) =>
        {
            setAnimalImage(panel.alphaPortrait, home.alphaSlot.alpha, home.alphaSlot.alpha.pack.mood);
            panel.capacitySlider.value = (float)home.alphaSlot.alpha.pack.amount;

            updateBuildingViewBase(home, homePanel, texts);
        });

        injectBuildingViewBase(home, homePanel, texts, updateAction);
    }

    internal void injectBuildingUpgradePanel(Building building, GameObject upgradePanel)
    {
        UIBuildingUpgradePanel panel = upgradePanel.GetComponent<UIBuildingUpgradePanel>();
        panel.modules = new List<UIBuildingUpgradeModule>();

        for (int i = 0; i < building.segments.Length; i++)
        {
            BuildingSegment segment = building.segments[i];
            GameObject upgradeModule = (GameObject)Instantiate(buildingUpgradeGroup[(int)BuildingUpgradeGroup.BUILDING_ITEM]);
            upgradeModule.transform.SetParent(panel.container.transform, false);
            upgradeModule.name = string.Format("BuildingItem0{0}", i);

            UIBuildingUpgradeModule module = upgradeModule.GetComponent<UIBuildingUpgradeModule>();
            setBuildingImage(module.buildingIcon, building.id, i);
            module.stockTitle.text = findController<LocalizationController>().getTextByKey(building.name.Contains("HOME") ? "UI_UPGRADE_SPACE_TITLE" : "UI_UPGRADE_STOCK_TITLE");
            module.serviceRangeText.text = findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(segment.capacity);

            if(segment.isPurchased)
            {
                module.purchaseButton.gameObject.SetActive(false);
                module.purchasedTick.SetActive(true);
            }
            else
            {
                module.purchaseButton.gameObject.SetActive(true);
                module.purchasedTick.SetActive(false);
            }

            module.purchasePriceText.text = findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(segment.price);
            module.purchaseButton.addOnTouchedListener((bool isLocked) => 
            {
                closeAllOpenPanels();
                findController<GameplayController>().evolveBuilding(building, segment, !isLocked, true);
            }, false);
            panel.modules.Add(module);
        }

        panel.addOnUpdateListener((Dictionary<string, object> parameters) => 
        {
            for (int i = 0; i < building.segments.Length; i++)
            {
                UIBuildingUpgradeModule module = panel.modules[i];
                BuildingSegment segment = building.segments[i];

                if(!segment.isPurchased && findController<GameplayController>().gameplayManager.bank.canAfford(Currency.GOLD, segment.price))
                {
                    module.purchaseButton.unlockInteraction();
                }
                else
                {
                    module.purchaseButton.lockInteraction();
                }
            }
        }, false);
    }


    public void injectBathHousePanel(BathHouse bathHouse, GameObject popup)
    {
        Dictionary<string, string> texts = new Dictionary<string, string> {
            { "title", findController<LocalizationController>().getTextByKey("BATH_HOUSE_TITLE") },
            { "upgrade_building", string.Format(context.getController<LocalizationController>().getTextByKey("UI_BUFFS_IMPROVE_BUFF_TEXT"), findController<LocalizationController>().getTextByKey("BATH_HOUSE_TITLE")) },
            { "increase_stock", findController<LocalizationController>().getTextByKey("BATH_HOUSE_INCREASE_STOCK_TITLE") },
            { "increase_service", findController<LocalizationController>().getTextByKey("BATH_HOUSE_INCREASE_SERVICE_TITLE") },
            { "current_stock", findController<LocalizationController>().getTextByKey("BATH_HOUSE_CURRENT_STOCK") }
        };

        injectBuildingView(bathHouse, popup, texts);
    }


    public void injectTavernPanel(Tavern tavern, GameObject popup)
    {
        Dictionary<string, string> texts = new Dictionary<string, string> {
            { "title", findController<LocalizationController>().getTextByKey("TAVERN_TITLE") },
            { "upgrade_building", string.Format(findController<LocalizationController>().getTextByKey("UI_BUFFS_IMPROVE_BUFF_TEXT"), findController<LocalizationController>().getTextByKey("TAVERN_TITLE")) },
            { "increase_stock", findController<LocalizationController>().getTextByKey("TAVERN_INCREASE_STOCK_TITLE") },
            { "increase_service", findController<LocalizationController>().getTextByKey("TAVERN_INCREASE_SERVICE_TITLE") },
            { "current_stock", findController<LocalizationController>().getTextByKey("TAVERN_CURRENT_STOCK") }
        };

        injectBuildingView(tavern, popup, texts);
    }


    public void injectPlaygroundPanel(Building playground, GameObject popup)
    {
        Dictionary<string, string> texts = new Dictionary<string, string> {
            { "title", findController<LocalizationController>().getTextByKey("PLAYGROUND_TITLE") },
            { "upgrade_building", string.Format(findController<LocalizationController>().getTextByKey("UI_BUFFS_IMPROVE_BUFF_TEXT"), findController<LocalizationController>().getTextByKey("PLAYGROUND_TITLE")) },
            { "increase_stock", findController<LocalizationController>().getTextByKey("PLAYGROUND_INCREASE_STOCK_TITLE") },
            { "increase_service", findController<LocalizationController>().getTextByKey("PLAYGROUND_INCREASE_SERVICE_TITLE") },
            { "current_stock", findController<LocalizationController>().getTextByKey("PLAYGROUND_CURRENT_STOCK") }
        };

        injectBuildingView(playground, popup, texts);
    }


    public void injectHospitalPanel(Building hospital, GameObject popup)
    {
        Dictionary<string, string> texts = new Dictionary<string, string> {
            { "title", findController<LocalizationController>().getTextByKey("HOSPITAL_TITLE") },
            { "upgrade_building", string.Format(findController<LocalizationController>().getTextByKey("UI_BUFFS_IMPROVE_BUFF_TEXT"), findController<LocalizationController>().getTextByKey("HOSPITAL_TITLE")) },
            { "increase_stock", findController<LocalizationController>().getTextByKey("HOSPITAL_INCREASE_STOCK_TITLE") },
            { "increase_service", findController<LocalizationController>().getTextByKey("HOSPITAL_INCREASE_SERVICE_TITLE") },
            { "current_stock", findController<LocalizationController>().getTextByKey("HOSPITAL_CURRENT_STOCK") }
        };

        injectBuildingView(hospital, popup, texts);
    }


    public void injectTownHallPanel(BuffsLibrary buffs, GameObject townHallPanel, TownHall townHall)
    {
        UITownHallPanel panel = townHallPanel.GetComponent<UITownHallPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;
        GameplayController gameplay = findController<GameplayController>();
        
        foreach (Buff buff in buffs)
        {
            GameObject buffObj = (GameObject)Instantiate(townHallGroup[(int)TownHallViewGroup.BUFF_ITEM]);
            buffObj.transform.SetParent(panel.container.transform, false);
            UIBuffItem buffItem = buffObj.GetComponent<UIBuffItem>();

            buffItem.buffTitle.text = buff.localizedName.value;
            setImg(buffItem.buffIcon, "others", buff.name);
            buffItem.craftItems = new List<UIBuffCraftItem>();

            //Recipe
            for (int i = 0; i < buff.recipeComponents.Length; i++)
            {
                UIBuffCraftItem craftItem = buffItem.craftItemsContainer.transform.GetChild(i).GetComponent<UIBuffCraftItem>();
                setImg(craftItem.itemIcon, "others", buff.recipeComponents[i].name);
                craftItem.itemBackground.color = buff.recipeComponents[i].bgColor;
            }

            for (int i = buff.recipeComponents.Length; i < buffItem.craftItemsContainer.transform.childCount; i++)
            {
                buffItem.craftItemsContainer.transform.GetChild(i).gameObject.SetActive(false);
            }

            buffItem.addOnUpdateListener((Dictionary<string, object> parameters) => 
            {
                if (!((RectTransform)panel.scrollView.transform).Overlaps((RectTransform)buffItem.transform)) return;

                buffItem.buffDescription.text = buff.getFormattedDescription();
                buffItem.buffLevel.text = gameplay.gameplayManager.bank.toFormattedCurrency(buff.level);

                //Recipe
                for (int i = 0; i < buff.recipeComponents.Length; i++)
                {
                    UIBuffCraftItem craftItem = buffItem.craftItemsContainer.transform.GetChild(i).GetComponent<UIBuffCraftItem>();
                    InventoryItem recipeComponent = buff.recipeComponents[i];

                    craftItem.itemText.text = string.Format("{0:0}/{1:0}", recipeComponent.amount.value, buff.recipeRequired[i]);
                    if (recipeComponent.amount.value < buff.recipeRequired[i]) craftItem.itemText.color = new Color(0.83f, 0.32f, 0.26f);
                }

                //Upgrade button
                buffItem.priceText.text = gameplay.gameplayManager.bank.toFormattedCurrency(buff.getUpgradePriceByLevel());
                if (buff.canUpgradeRecipe() && gameplay.gameplayManager.bank.canAfford(Currency.GOLD, buff.getUpgradePriceByLevel()))
                {
                    buffItem.upgradeButton.unlockInteraction();
                }
                else
                {
                    buffItem.upgradeButton.lockInteraction();
                }

                //Level Slider
                buffItem.levelSlider.value = buff.normalizedLevel;
            });

            buffItem.upgradeButton.addOnTouchedListener((bool isLocked) => 
            {
                if (!isLocked && buff.level == 0) panel.close();
                buff.levelUp(gameplay, !isLocked);
            });

            buffItem.upgradeButton.onHolded.AddListener((bool isLocked) => 
            {
                if (!isLocked && buff.level == 0) panel.close();
                buff.levelUp(gameplay, !isLocked);
            });
        }
        
    }


    internal void injectExcavatorView(GameObject popup, City city)
    {
        LocalizationController loc = findController<LocalizationController>();
        GameplayController gameplay = findController<GameplayController>();

        UIExcavatorPanel panel = popup.GetComponent<UIExcavatorPanel>();
        Excavator excavator = (Excavator)city[CityGroup.EXCAVATOR];
        excavator.dispelWarningParticle();
        
        if(excavator.neededBuildingFlag != null)
        {
            excavator.lastUnlockedBuilding = excavator.neededBuildingFlag;
        }


        panel.title.text = loc.getTextByKey("BUILDINGS_TITLE");
        panel.description.text = loc.getTextByKey("EXCAVATOR_DESC");

        panel.items = new List<UITownHallItem>();

        UITownHallItem homeItem = null;
        int flagged = -1;
        int count = 0;

        foreach (Building building in city)
        {
            if (!building.isBuildable) continue;
            
            bool isAnotherHome = new CityGroup[] { CityGroup.HOME01, CityGroup.HOME02, CityGroup.HOME03 }.Contains(building.id);

            GameObject obj = null;
            UITownHallItem item = null;            

            bool isLockedByMood = excavator.lastUnlockedBuilding.id < building.id;

            string name = building.getCleanIDName();

            if (!isAnotherHome)
            {
                obj = (GameObject)Instantiate(excavatorGroup[(int)ExcavatorGroup.TOWN_HALL_ITEM]);
                item = obj.GetComponent<UITownHallItem>();

                obj.transform.SetParent(panel.container.transform, false);
                obj.name = name;

                setBuildingImage(item.buildingIcon, building.id, 0);
                if(isLockedByMood)
                {
                    item.buildingIcon.color = new Color(1, 1, 1, 0.25f);
                    item.lockIcon.SetActive(true);
                }
                else
                {
                    item.lockIcon.SetActive(false);
                }

                item.title.text = loc.getTextByKey(string.Format("{0}_TITLE", name));
                item.desc.text = isLockedByMood ? loc.getTextByKey("BUILDING_LOCKED_MSG") : loc.getTextByKey(string.Format("{0}_DESC", name));
            }
            else
            {
                obj = panel.container.transform.Find(name).gameObject;
                item = obj.GetComponent<UITownHallItem>();
            }

            if (building.isPurchased())
            {
                item.purchasedSignal.gameObject.SetActive(true);
                item.purchaseButton.gameObject.SetActive(false);
            }
            else
            {
                item.purchasedSignal.gameObject.SetActive(false);
                item.purchaseButton.gameObject.SetActive(!isLockedByMood);

                if (!isLockedByMood || building.id == CityGroup.TOWN_HALL)
                {
                    item.purchasePrice.text = findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(building.segments[0].price, true);

                    item.purchaseButton.addOnTouchedListener((bool isLocked) =>
                    {
                        findController<GameplayController>().startBuildingMode(building, !isLocked);

                        if (isLocked) return;

                        closeAllOpenPanels();
                        displayBuildingMode(isAnotherHome ? city.getNextHomeToPurchase() : building);
                    }, false);
                }
            }

            if (isAnotherHome)
            {
                homeItem = item;
            }
            else if (building.id == excavator.neededBuildingFlag?.id)
            {
                flagged = count;
            }

            panel.items.Add(item);
            count++;
        }


        //Lock the home item if needed
        Alpha alpha = (Alpha)findController<GameplayController>().gameplayManager.alphas.items.Find(e => ((Alpha)e).state == AlphaState.UNLOCKED);
        if (alpha != null)
        {
            homeItem.purchasedSignal.gameObject.SetActive(false);
            homeItem.purchaseButton.gameObject.SetActive(true);
        }
        else
        {
            homeItem.purchasedSignal.gameObject.SetActive(true);
            homeItem.purchaseButton.gameObject.SetActive(false);
        }

        panel.addOnUpdateListener((Dictionary<string, object> parameters) =>
        { 
            int i = 0;
            foreach (Building building in city)
            {
                if (!building.isBuildable) continue;

                if (findController<GameplayController>().gameplayManager.bank.canAfford(Currency.GOLD, building.segments[0].price))
                {
                    panel.items[i].purchaseButton.unlockInteraction();
                }
                else
                {
                    panel.items[i].purchaseButton.lockInteraction();
                }
                i += 1;
            }
            
        }, false);

        if(flagged > -1)
        {
            float scrollPos = panel.scroll.preferredHeight / Math.Max(1, flagged - 1);
            panel.scroll.DOVerticalNormalizedPos(scrollPos / panel.scroll.preferredHeight, 0.75f).OnComplete(() =>
            {
                panel.items[flagged].purchaseButton.highlight(true);
            });

            excavator.neededBuildingFlag = null;
        }
    }

    internal void notifyPurchaseSuccess(IAPItem item)
    {
        UIShopPanel panel = (UIShopPanel)panels.Where(e => e.name == "ShopPopup").First();
        GameObject shopItem = panel.packsContainer.transform.Find(item.name).gameObject;
        currencyParticlesToTarget(shopItem.transform.position, panelArea.transform.Find("PremiumCurrency").gameObject, "others", "credit_coin", item.rewardAmount);
    }

    public void injectTutQuestView(TutorialManager manager, GameObject tutQuestPanel)
    {
        UITutorialQuestPanel panel = tutQuestPanel.GetComponent<UITutorialQuestPanel>();
        panel.modules = new UITutorialQuestModule[manager.maxAmountOfAvailableQuests];
        panel.name = "TutorialQuestPanel";
        panel.questsContainer.name = "TutorialQuestsContainer";

        panel.addOnUpdateListener((Dictionary<string, object> parameters) =>
        {
            TutorialQuest[] quests = manager.getActiveTutQuests();

            for (int i = 0; i < manager.maxAmountOfAvailableQuests; i++)
            {
                TutorialQuest quest = quests[i];
                if (quest == null) continue;

                if (panel.modules[i] == null)
                {
                    GameObject moduleGO = (GameObject)Instantiate(tutQuestGroup[(int)TutorialQuestGroup.TUTORIAL_QUEST_MODULE]);
                    moduleGO.transform.SetParent(panel.questsContainer.transform, false);
                    panel.modules[i] = moduleGO.GetComponent<UITutorialQuestModule>();
                    panel.modules[i].name = string.Format("TutorialQuestModule{0}", i);
                }

                UITutorialQuestModule module = panel.modules[i];

                setQuestTypeIcon(quest, module.questTypeIcon);
                module.description.text = string.Format(quest.description.value, quest.tickUntil);
                module.slider.value = quest.normalized;

                module.rewardAmount.text = 
                    quest.reward == TutorialQuestRewards.ALPHA? findController<LocalizationController>().getTextByKey("QUEST_REWARD_NEW_ALPHA") : 
                                                                findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(quest.getRewardAmount());

                setQuestRewardIcon(quest, module.rewardTypeIcon);
            }
        });
    }

    private void setQuestRewardIcon(TutorialQuest quest, Image icon)
    {
        switch (quest.reward)
        {
            case TutorialQuestRewards.ALPHA:
                setImg(icon, "portraits", string.Format("{0}_HAPPY", quest.alphaReward.ToString()));
                break;
            case TutorialQuestRewards.INVENTORY_ITEM:
                setImg(icon, "others", quest.inventoryItemReward.name);
                break;
            case TutorialQuestRewards.FEAT_MARKET:
                setBuildingImage(icon, CityGroup.MARKET, 0);
                break;
            case TutorialQuestRewards.CREDIT:
                setImg(icon, "others", "credit_coin");
                break;
            case TutorialQuestRewards.GOLD:
                setImg(icon, "others", "gold_coin");
                break;
        }
    }


    private void setQuestTypeIcon(TutorialQuest quest, Image icon)
    {
        setImg(icon, "others", quest.type.ToString());
    }

    internal void injectTutQuestDialog(TutorialQuest quest)
    {
        if (quest == null) return;

        if (quest.dialog != null)
        {
            quest.dialog.SetActive(true);
            return;
        }

        resetAlphaListSelection();
        
        GameObject popup = firstOrCreateByTag("QuestDialog", tutQuestGroup, (int)TutorialQuestGroup.TUTORIAL_QUEST_DIALOG);
        cleanUpName(popup);
        quest.dialog = popup;

        popup.transform.localScale = new Vector3(0, 1, 0);
        Sequence sequence = DOTween.Sequence();
        sequence.Append(popup.transform.DOScaleX(1.0f, 0.15f).SetEase(Ease.OutBounce));
        sequence.Append(popup.transform.DOScaleX(1.0f, 1.75f)); //Do nothing for 2.5s

        UITutorialQuestDialog dialog = popup.GetComponent<UITutorialQuestDialog>();
        GameObject button = GameObject.Find(tutQuestGroup[(int)TutorialQuestGroup.TUTORIAL_QUEST_BUTTON].name);
        popup.transform.SetParent(panelArea.transform, false);

        setQuestTypeIcon(quest, dialog.questTypeIcon);
        dialog.description.text = string.Format(quest.description.value, quest.tickUntil);

        dialog.claimButton.addOnTouchedListener((bool isLocked) => 
        {
            claimTutorialQuest(quest, popup);
        });

        if (quest.reward != TutorialQuestRewards.ALPHA || quest.isFtue)
        {
            dialog.claimButton.gameObject.SetActive(false);
            dialog.description.rectTransform.SetHeight(((RectTransform)dialog.description.rectTransform.parent).GetHeight());
            dialog.description.rectTransform.localPosition = new Vector3(dialog.description.rectTransform.localPosition.x, -dialog.description.rectTransform.GetHeight() / 2, 0);

            sequence.InsertCallback(2.5f, () =>
            {
                claimTutorialQuest(quest, popup, skipClose: true);
            });

            sequence.OnComplete(() => 
            {
                quest.dialog = null;
                Destroy(popup);
            });
        }

        sequence.Play();
    }

    private void claimTutorialQuest(TutorialQuest quest, GameObject popup, bool skipClose = false)
    {
        if (!popup.activeInHierarchy) return;

        UITutorialQuestDialog dialog = popup.GetComponent<UITutorialQuestDialog>();

        if (quest.reward != TutorialQuestRewards.INVENTORY_ITEM)
        {
            Sequence seq = currencyParticlesToTarget(
                ((RectTransform)dialog.questTypeIcon.transform).position,
                getTargetByReward(quest.reward),
                "others",
                getIconNameByReward(quest),
                quest.rewardAmount);
        }
        else
        {
            launchInventoryParticlesToTarget(
                ((RectTransform)dialog.questTypeIcon.transform).position,
                getTargetByReward(quest.reward),
                getIconNameByReward(quest),
                (int)quest.rewardAmount);
        }
        findController<GameplayController>().claimTutorialQuest(quest);

        if (skipClose) return;

        quest.dialog = null;
        Destroy(popup);
    }

    public GameObject getTargetByReward(TutorialQuestRewards reward)
    {
        GameObject target = null;

        switch (reward)
        {
            case TutorialQuestRewards.GOLD:
                target = GameObject.Find(TopButtons[(int)TopUI.GOLD_GAUGE].name);
                break;
            case TutorialQuestRewards.INVENTORY_ITEM:
                target = GameObject.Find(bottomButtons[(int)BottomButtons.SETTINGS_BUTTON].name);
                break;
            case TutorialQuestRewards.CREDIT:
                target = GameObject.Find(TopButtons[(int)TopUI.CREDIT_GAUGE].name);
                break;
        }

        return target;
    }

    private string getIconNameByReward(TutorialQuest quest)
    {
        string particleName = "";

        switch (quest.reward)
        {
            case TutorialQuestRewards.GOLD:
                particleName = "gold_coin";
                break;
            case TutorialQuestRewards.INVENTORY_ITEM:
                particleName = quest.inventoryItemReward.name;
                break;
            case TutorialQuestRewards.CREDIT:
                particleName = "credit_coin";
                break;
        }

        return particleName;
    }

    public void injectAlphasListView(AlphaSlots slots, GameObject alphaList)
    {
        Bank bank = findController<GameplayController>().gameplayManager.bank;
        for (int i = 0; i < slots.Count; i++)
        {
            AlphaSlot slot = slots[i];

            GameObject alphaListItem = (GameObject)Instantiate(alphaListGroup[(int)AlphaListView.ALPHA_LIST_ITEM]);
            alphaListItem.transform.SetParent(alphaList.transform, false);
            alphaListItem.name = string.Format("AlphaSlot0{0}", i);

            UIAlphaListItem item = alphaListItem.GetComponent<UIAlphaListItem>();
            slot.ui = item;
            item.state = AlphaListViewState.LOCKED;

            float height = ((RectTransform)item.transform).GetHeight();
            float listHeight = ((RectTransform)alphaList.transform).GetHeight();
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, ((height / 2) + 10.0f) + ((slots.Count - 1 - i) * height * 1.1f));

            item.init();
            item.unselect(slot);

            item.renderItem.texture = slot.renderTexture;
            slot.alpha.requestRender();

            item.addOnUpdateListener((Dictionary<string, object> parameters) => 
            {
                item.updateState(slot);
                if (slot.state == AlphaListViewState.EMPTY)
                {
                    setAnimalImage(item.emptyPortrait, slot, PackMood.HAPPY);
                    item.emptyPortrait.color = new Color(1, 1, 1, 0.45f);
                }

                item.updateAssigned(slot);

                if (item.isSelected) slot.alpha.requestRender();
            });

            item.addOnTouchedListener((bool isLocked) => 
            {
                if(slot.alpha.waveSequence != null) slot.alpha.waveSequence.Kill();

                if (slot.state == AlphaListViewState.EMPTY)
                {
                    if (slots.current != null) return;

                    string feedbackMsg = "";
                    if(slot.alpha.state == AlphaState.UNLOCKED)
                    {
                        feedbackMsg = findController<LocalizationController>().getTextByKey("FEEDBACK_ALPHA_SLOT_PURCHASE_HOME");
                        findController<AudioController>().playSound("pitch_bark");
                    }
                    else
                    {
                        TutorialQuest target = (TutorialQuest) findController<GameplayController>().tutorialManager.tutorialQuests.items.Find(element => ((TutorialQuest)element).alphaReward == slot.alpha.alphaID);

                        int missionCount = findController<GameplayController>().tutorialManager.getNumberOfQuestsUpTo(target);
                        string key = missionCount <= 1 ? "FEEDBACK_ALPHA_SLOT_UNLOCK_ALPHA_CLOSE" : "FEEDBACK_ALPHA_SLOT_UNLOCK_ALPHA";
                        feedbackMsg = string.Format(findController<LocalizationController>().getTextByKey(key), missionCount);

                        findController<AudioController>().playSound("locked_action");
                    }

                    showSimpleFeedback(feedbackMsg);
                }
                else if(slot.state == AlphaListViewState.ASSIGNED)
                {
                    dispelSimpleFeedback();
                    findController<GameplayController>().selectAlpha(slot.id);

                    if (slot.alpha?.gifts > 0)
                    {
                        string[] data = findController<GameplayController>().pickGift(slot.id);
                        displayGiftClaimed(data);
                        item.gift.SetActive(false);
                        item.updateState(slot);
                        displayWavesOnAlpha(slot.alpha);

                        findController<AudioController>().playSound("pet_gift");
                    }
                    else if (slots.selectedSlot != -1)
                    {
                        for (int j = 0; j < slots.Count; j++)
                        {
                            if(slots[j].ui != item) slots[j].ui.unselect(slots[j]);
                        }
                        
                        if (!slots.current.ui.isSelected)
                        {
                            slots.current.ui.select(slots.current);
                            if (findController<GameplayController>().tutorialManager.hasAllBeenDisplayed(Tutorials.SCOUTING_TUTORIAL))
                            {
                                if(slots.current.uiAction == null) slots.current.uiAction = invokeAlphaActionButton(slots.current);
                            }
                        }
                        else
                        {
                            slots.current.ui.unselect(slots.current);
                            slots.selectedSlot = -1;
                        }

                        findController<AudioController>().playSound("pet_view");
                    }
                }
            });
        }
    }

    private UIAlphaListItemActionButton invokeAlphaActionButton(AlphaSlot current)
    {
        GameObject go = null;
        UIAlphaListItemActionButton button = null;

        if (current.alpha.pack.mission == null)
        {
            go = (GameObject)Instantiate(alphaListGroup[(int)AlphaListView.ALPHA_LIST_ITEM_ACTION_BUTTON]);
            button = go.GetComponent<UIAlphaListItemActionButton>();
            cleanUpName(go);

            go.transform.SetParent(current.ui.transform, false);
            go.transform.localScale = Vector3.zero;

            button.addOnUpdateListener((Dictionary<string, object> parameters) =>
            {
                setActionImage(button.icon, current.alpha.pack.mood);
                button.action.text = findController<LocalizationController>().getTextByKey(current.alpha.pack.getActionVerb());
            });

            button.addOnTouchedListener((bool isLocked) => 
            {
                FlockState state = current.alpha.pack.getMissionType();
                Building building = findController<GameplayController>().findBuildingByState(state, current.alpha.alphaID);
                if (building.isPurchased())
                {
                    bool canBeSentToMission = true;
                    if (state != FlockState.SCOUTING && state != FlockState.SLEEPING)
                    {
                        bool sendByVacancy = findController<GameplayController>().canSendToMissionByVacancy(current.alpha, state);
                        if (!sendByVacancy)
                        {
                            current.ui.unselect(current);
                            showSimpleFeedback(
                                string.Format(findController<LocalizationController>().getTextByKey("ERROR_CANT_SEND_TO_MISSION_BY_VACANCY"),
                                findController<LocalizationController>().getTextByKey(string.Format("{0}_TITLE", building.getCleanIDName()))
                                ));

                            canBeSentToMission = false;
                        }
                        else
                        {
                            bool sendByCapacity = findController<GameplayController>().canSendToMissionByCapacity(current.alpha, state);
                            if (!sendByCapacity)
                            {
                                current.ui.unselect(current);
                                showSimpleFeedback(
                                    string.Format(findController<LocalizationController>().getTextByKey("ERROR_CANT_SEND_TO_MISSION_BY_CAPACITY"),
                                    findController<LocalizationController>().getTextByKey(string.Format("{0}_TITLE", building.getCleanIDName()))
                                    ));
                                findController<GameplayController>().findBuildingByState(state, current.alpha.alphaID).displayWarningParticle();
                                findController<CameraController>().focusOn(building.position);
                                building.hasNoCapacityFlag = true;

                                canBeSentToMission = false;
                            }
                            else
                            {
                                bool sendByStock = findController<GameplayController>().canSendToMissionByStock(current.alpha);
                                if (!sendByStock)
                                {
                                    current.ui.unselect(current);
                                    showSimpleFeedback(findController<LocalizationController>().getTextByKey("ERROR_CANT_SEND_TO_MISSION"));
                                    findController<GameplayController>().findBuildingByState(state, current.alpha.alphaID).displayWarningParticle();
                                    findController<CameraController>().focusOn(building.position);
                                    building.hasNoStockFlag = true;

                                    canBeSentToMission = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        canBeSentToMission = true;
                    }

                    if (canBeSentToMission)
                    {
                        current.alpha.pack.sendToMission();
                        findController<GameplayController>().sendToScout(current.alpha.pack.mission);

                        Sequence offSequence = DOTween.Sequence();
                        offSequence.Append(go.transform.DOScale(Vector3.zero, 0.25f));
                        offSequence.OnComplete(() => { Destroy(go); });
                        offSequence.Play();

                        findController<AudioController>().playSound("send_dogs");
                    }
                    else
                    {
                        findController<AudioController>().playSound("locked_action");
                    }
                }
                else
                {
                    showSimpleFeedback(string.Format(findController<LocalizationController>().getTextByKey("ERROR_BUILDING_MISSING"), findController<LocalizationController>().getTextByKey(string.Format("{0}_TITLE", building.id))));

                    Excavator excavator = (Excavator)findController<GameplayController>().gameplayManager.city[CityGroup.EXCAVATOR];
                    excavator.displayWarningParticle();
                    excavator.neededBuildingFlag = building;

                    findController<CameraController>().focusOn(excavator.position);

                    current.ui.unselect(current);

                    findController<AudioController>().playSound("locked_action");
                }
            });

            Sequence seq = DOTween.Sequence();
            seq.Append(go.transform.DOScale(0.4f, 0.15f));
            seq.Play();
        }
        return button;
    }

    internal void displayWavesOnAlpha(Alpha alpha)
    {
        if(alpha.waveSequence != null) alpha.waveSequence.Kill();

        GameObject wave = findController<GameplayController>().gameplayManager.alphaSlots[alpha.slotIndex].ui.waves;
        wave.SetActive(true);

        Color warningColor = findController<ColorizableController>().getColor("warning", 0.0f);

        Sequence wavesSequence = DOTween.Sequence();
        alpha.waveSequence = wavesSequence;

        Image waveImg = wave.GetComponent<Image>();
        waveImg.color = Color.white;

        wavesSequence.Append(waveImg.DOFade(1, 0.3f));
        wavesSequence.Join(wave.transform.DOScale(2.5f, 1.2f));
        wavesSequence.Insert(0.7f, waveImg.DOFade(0, 0.5f));
        wavesSequence.SetEase(Ease.InBack);

        wavesSequence.SetLoops(25, LoopType.Restart);

        wavesSequence.OnStepComplete(() => 
        {
            waveImg.color = new Color(1, 1, 1, 0);
        });

        wavesSequence.OnComplete(() => 
        {
            alpha.waveSequence = null;
            wave.SetActive(false);
            wave.transform.localScale = Vector3.one;
        });

        wavesSequence.OnKill(() =>
        {
            alpha.waveSequence = null;
            wave.SetActive(false);
            wave.transform.localScale = Vector3.one;
        });

        wavesSequence.Play();
    }


    internal UIPanel injectNewAlphaPanel(AlphaSlot slot, GameObject obj)
    {
        UINewAlphaPanel panel = obj.GetComponent<UINewAlphaPanel>();

        panel.beams.transform.localScale = Vector3.zero;

        panel.portraitObj.transform.localScale = Vector3.zero;
        setAnimalImage(panel.portraitImg, slot.alpha, PackMood.HAPPY);

        Vector3 textFrameOriginalPosition = panel.newAlphaTextFrame.transform.position;
        panel.newAlphaTextFrame.transform.Translate(0, 1000, 0);
        panel.newAlphaText.text = findController<LocalizationController>().getTextByKey("QUEST_REWARD_NEW_ALPHA");

        RawImage stars = panel.stars.GetComponent<RawImage>();

        Sequence In = DOTween.Sequence();
        Sequence Out = DOTween.Sequence();

        In.Append(stars.DOFade(0.5f, 1.0f));
        In.Join(panel.beams.transform.DOScale(1, 1.0f));
        In.Insert(0.5f, panel.portraitObj.transform.DOScale(1, 1.0f).SetEase(Ease.OutBounce));
        In.Append(panel.newAlphaTextFrame.transform.DOMoveY(textFrameOriginalPosition.y, 1.5f).SetEase(Ease.OutBounce));
        
        In.Append(panel.beams.transform.DOScale(1, 4.0f));

        Out.Append(panel.beams.transform.DOScale(0.0f, 0.5f));
        Out.Join(stars.DOFade(0.0f, 0.75f));
        Out.Insert(4.5f, panel.portraitObj.transform.DOMoveY(-1000.0f, 1.0f).SetEase(Ease.InBack));
        Out.Insert(4.5f, panel.newAlphaTextFrame.transform.DOMoveY(-500.0f, 0.75f).SetEase(Ease.InBack));

        Out.OnComplete(() => 
        {
            panel.close();
        });

        In.OnComplete(() =>
        {
            Out.Play();
        });

        In.Play();

        panel.transform.Find("ClickableLayer").GetComponent<UIItem>().addOnTouchedListener((bool isLocked) =>
        {
            In.Complete();
            In.Kill();

            Out.timeScale = 5.5f;
            Out.Play();
        });

        return panel;
    }


    public void showSimpleFeedback(string message)
    {
        GameObject feedback = firstOrCreateByName(overlaysGroup[(int)Overlays.FEEDBACK_PROVIDER].name, overlaysGroup, (int)Overlays.FEEDBACK_PROVIDER);
        UISimpleFeedbackProvider item = feedback.GetComponent<UISimpleFeedbackProvider>();

        feedback.transform.SetParent(panelArea.transform, false);

        item.feedbackText.text = message;
        Image img = item.GetComponent<Image>();

        item.addOnTouchedListener((bool isLocked) => { Destroy(feedback); });

        Sequence sequence = DOTween.Sequence();
        sequence.Append(item.transform.DOMove(item.transform.position, 5.0f));
        sequence.Append(feedback.GetComponent<Image>().DOFade(0, 0.25f));
        sequence.Join(item.feedbackText.DOFade(0, 0.25f));
        sequence.OnComplete(() => { Destroy(feedback); });

        sequence.Play();

        findController<AudioController>().playSound("feedback_in");
    }

    private void dispelSimpleFeedback()
    {
        Transform simpleFeedback = panelArea.transform.Find(overlaysGroup[(int)Overlays.FEEDBACK_PROVIDER].name);
        if(simpleFeedback != null) Destroy(simpleFeedback.gameObject);
    }

    public void displayGiftClaimed(string[] uiData)
    {
        closeAllOpenPanels();

        UIGiftPanel panel = invokeUIPanel((GameObject)giftGroup[(int)GiftPopupGroup.GIFT_POP_UP]).GetComponent<UIGiftPanel>();
        GameplayController gameplayController = findController<GameplayController>();
        Bank bank = gameplayController.gameplayManager.bank;

        foreach (string item in uiData)
        {
            string[] data = item.Split('#');

            if (data[0] != "total")
            {
                if (data[0] != "premium")
                {
                    InventoryItem model = (InventoryItem)findController<GameplayController>().gameplayManager.inventory.items.Find(e => e.name == data[0]);
                    GameObject invItem = Instantiate((GameObject)giftGroup[(int)GiftPopupGroup.INVENTORY_ITEM]);
                    UIBigInventoryItem invItemScript = invItem.GetComponent<UIBigInventoryItem>();

                    setImg(invItemScript.icon, "others", data[0]);
                    invItemScript.itemName.text = findController<LocalizationController>().getTextByKey(string.Format("{0}_NAME", data[0].ToMayusUnderscored()));
                    invItemScript.amountText.text = string.Format("x{0}", data[1]);
                    invItemScript.bgCircle.color = model.bgColor;

                    invItem.transform.SetParent(panel.container.transform, false);
                }
                else
                {
                    GameObject currencyItem = Instantiate((GameObject)giftGroup[(int)GiftPopupGroup.GIFT_CURRENCY_ITEM]);
                    UIGiftCurrencyItem script = currencyItem.GetComponent<UIGiftCurrencyItem>();

                    setImg(script.currencyIcon, "others", "credit_coin");
                    script.amount.text = data[1];

                    currencyItem.transform.SetParent(panel.currenciesContainer.transform, false);
                }
            }

            panel.okButton.addOnTouchedListener((bool isLocked) => 
            {
                //Claim the displayed gifts
                gameplayController.claimGifts();
                panel.close();
            });
        }

        //In case there were no gifts reduce popup size THIS CODE MAYBE USELESS. CHECK AND DELETE
        if (panel.container.transform.childCount <= 0)
        {
            RectTransform itemsContainerTransform = (RectTransform)panel.container.transform;
            float containerHeight = itemsContainerTransform.GetHeight();

            RectTransform panelRectTransform = (RectTransform)panel.transform;
            float panelHeight = panelRectTransform.GetHeight();

            itemsContainerTransform.SetHeight(0);
            panelRectTransform.SetHeight(panelHeight - containerHeight / 2);
        }

        panel.alphaMessage.text = findController<LocalizationController>().getTextByKey(string.Format("GIFT_RANDOM_SPEECH_0{0}", UnityEngine.Random.Range(1, 5)));
    }

    
    public void injectDumpPanel(GameObject dump)
    {
        UIDumpPanel panel = dump.GetComponent<UIDumpPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;

        panel.supportButton.addOnTouchedListener((bool isLocked) => 
        {
            EmailHelper.sendEmail(
                "help@flockingpets.com",
                "[Write your issue here]",
                "Write your issue here\n" +
                "If you have screenshotsm videos or detailed steps of the issue, they will be greatly appreciated"
                );
        });

        panel.aboutButton.addOnTouchedListener((bool isLocked) =>
        {
            findController<GameplayController>().invokeCreditsView();
        });

        panel.communityButton.addOnTouchedListener((bool isLocked) =>
        {
            findController<GameplayController>().invokeCommunityView();
        });

        panel.settingsButton.addOnTouchedListener((bool isLocked) =>
        {
            findController<GameplayController>().invokeSettingsView();
        });

        bool canShowPrestigeButton = findController<GameplayController>().tutorialManager.hasAllBeenDisplayed(Tutorials.PRESTIGE_TUTORIAL);
        panel.prestigeButton.gameObject.SetActive(canShowPrestigeButton);
        if (canShowPrestigeButton)
        {
            panel.prestigeButton.addOnTouchedListener((bool isLocked) =>
            {
                findController<GameplayController>().invokePrestigeView();
            });
        }

        bool canShowInventoryButton = findController<GameplayController>().gameplayManager.bank[Currency.GIFTS_PICKED].value > 0;
        panel.inventoryButton.gameObject.SetActive(canShowInventoryButton);
        if (canShowInventoryButton)
        {
            panel.inventoryButton.addOnTouchedListener((bool isLocked) =>
            {
                findController<GameplayController>().invokeInventoryView();
            });
        }
    }


    public void injectPrestigeView(GameObject popup)
    {
        UIPrestigePanel panel = popup.GetComponent<UIPrestigePanel>();
        GameplayController gameplay = findController<GameplayController>();
        Bank bank = gameplay.gameplayManager.bank;

        double prestigePriceValue = gameplay.prestigeManager.getPriceByAmountOfPrestiges();
        string prestigePrice = bank.toFormattedCurrency(prestigePriceValue, true);
        panel.petsAmountSlider.minValue = 0;
        panel.petsAmountSlider.maxValue = 1.0f;

        panel.isInfoPanelOpen = false;
        RectTransform infoPanelTransform = (RectTransform)panel.infoPanel.transform;
        float infoPanelHeight = infoPanelTransform.GetHeight();

        string current = gameplay.prestigeManager.getCurrentEarningsMultiplierFormatted();
        panel.currentMultiplierGold.text = current;
        panel.currentMultiplierGift.text = current;

        panel.addOnUpdateListener((Dictionary<string, object> parameters) =>
        {
            string prestigeReward = gameplay.prestigeManager.getNextEarningsMultiplierFormatted();
            panel.goldMultiplier.text = prestigeReward;
            panel.giftMultiplier.text = prestigeReward;

            panel.petsAmount.text = string.Format("{0}/{1}", gameplay.gameplayManager.bank.getFormatted(Currency.DOGS), prestigePrice);
            panel.petsAmountSlider.value = (float)(gameplay.gameplayManager.bank[Currency.DOGS].value / prestigePriceValue);

            if (gameplay.prestigeManager.canPrestige())
            {
                panel.prestigeButton.unlockInteraction();
            }
            else
            {
                panel.prestigeButton.lockInteraction();
            }
        });

        panel.prestigeButton.addOnTouchedListener((bool isLocked) =>
        {
            if (isLocked) return;

            closeAllOpenPanels();
            findController<CameraController>().focusOn(findController<GameplayController>().gameplayManager.city[CityGroup.HOME00].position);

            PlayableDirector director = GameObject.FindWithTag("PrestigeCutscene").GetComponent<PlayableDirector>();
            director.enabled = true;
            director.Play();
        });

        panel.infoButton.addOnTouchedListener((bool isLocked) => 
        {
            panel.infoButton.lockInteraction();
            panel.infoPanel.gameObject.SetActive(true);

            infoPanelTransform.SetHeight(panel.isInfoPanelOpen? infoPanelHeight : 0);

            infoPanelTransform.DOSizeDelta(new Vector2(0, panel.isInfoPanelOpen ? -infoPanelHeight : infoPanelHeight), 0.5f).SetEase(Ease.OutBack).OnComplete(() => 
            {
                panel.infoButton.unlockInteraction();
                panel.isInfoPanelOpen = !panel.isInfoPanelOpen;
                if(!panel.isInfoPanelOpen)
                {
                    panel.infoPanel.gameObject.SetActive(false);
                }
            });

            Image buttonImg = panel.infoButton.transform.GetChild(0).GetComponent<Image>();
            setImg(buttonImg, "others", panel.isInfoPanelOpen ?"Info2" : "CloseButton");
            buttonImg.color = findController<ColorizableController>().getColor("main");
        });
    }


    public void injectInventoryView(Inventory inventory, GameObject inv)
    {
        UIInventoryPanel panel = inv.GetComponent<UIInventoryPanel>();
        Bank bank = findController<GameplayController>().gameplayManager.bank;
        
        for (int i = 0; i < inventory.unlockedSlots; i++)
        {
            InventoryItem item = inventory.findBySlot(i);

            if (i >= panel.items.Count)
            {
                GameObject invItem = Instantiate((GameObject)inventoryGroup[item == null ? (int)InventoryGroup.INVENTORY_EMPTY_ITEM : (int)InventoryGroup.INVENTORY_ITEM]);
                invItem.transform.SetParent(panel.container.transform, false);

                invItem.name = item == null ? string.Format("empty_{0}", i) : item.name;

                panel.items.Add(invItem.GetComponent<UIBigInventoryItem>());
            }

            if (item != null)
            {
                setImg(panel.items[i].icon, "others", panel.items[i].name);
                panel.items[i].amountText.text = string.Format("x{0}", bank.toFormattedCurrency(item.amount.value));
                panel.items[i].bgCircle.color = item.bgColor;
                panel.items[i].itemName.text = item.itemName.value;
            }
        }
        
        //Add last slot locked
        if (inventory.hasRemainingLockedSlots())
        {
            GameObject lockedLast = Instantiate((GameObject)inventoryGroup[(int)InventoryGroup.INVENTORY_LOCKED_ITEM]);
            lockedLast.transform.SetParent(panel.container.transform, false);
            lockedLast.GetComponent<UIInventoryItem>().amountText.text = bank.toFormattedCurrency(inventory.nextUnlockedSlotPrice(), true);

            lockedLast.GetComponent<UIInventoryItem>().name = "locked";

            panel.items.Add(lockedLast.GetComponent<UIBigInventoryItem>());

            lockedLast.GetComponent<UIInventoryItem>().addOnTouchedListener((bool isLocked) => 
            {
                if (!bank.canAfford(Currency.CREDIT, inventory.nextUnlockedSlotPrice())) return;
                
                inventory.purchaseNextSlot();
                inventory.selectedSlot = inventory.unlockedSlots - 1;

                panel.items.Remove(lockedLast.GetComponent<UIBigInventoryItem>());
                Destroy(lockedLast);
            });
        }
    }


    public void injectCommunityPanel(GameObject dump)
    {
        UICommunityPanel panel = dump.GetComponent<UICommunityPanel>();

        panel.redditButton.addOnTouchedListener((bool isLocked) => { Application.OpenURL("www.reddit.com"); });
        panel.discordButton.addOnTouchedListener((bool isLocked) => { Application.OpenURL("www.discordapp.com"); });
    }


    public void injectSettingsPanel(GameObject dump)
    {
        UISettingsPanel panel = dump.GetComponent<UISettingsPanel>();
        AudioController audio = findController<AudioController>();

        panel.sfxToggle.minValue = 0;
        panel.sfxToggle.maxValue = 1;

        panel.musicToggle.minValue = 0;
        panel.musicToggle.maxValue = 1;

        panel.sfxHandle.addOnTouchedListener((bool isLocked) => { audio.toggleFX(); });

        panel.musicHandle.addOnTouchedListener((bool isLocked) => { audio.toggleMusic(); });

        panel.langLeftArrow.addOnTouchedListener((bool isLocked) => { onChangeLangFromSettings(true); });
        panel.langRightArrow.addOnTouchedListener((bool isLocked) => { onChangeLangFromSettings(false); });

        panel.idTextButton.addOnTouchedListener((bool isLocked) => { });

        panel.privacyButton.addOnTouchedListener((bool isLocked) => 
        {
            findController<GameplayController>().invokePrivacyPanel();
        });

        panel.ratingButton.addOnTouchedListener((bool isLocked) => { Application.OpenURL("market://details?id=com.greenlove.flockingpets"); });

        panel.versionText.text = Application.version;

        panel.addOnUpdateListener((Dictionary<string, object> parameters) => 
        {
            panel.sfxToggle.value = audio.isFXMuted() ? 0 : 1;
            panel.musicToggle.value = audio.isMusicMuted() ? 0 : 1;

            panel.langText.text = findController<LocalizationController>().getCurrentLangName();
            panel.idText.text = "";
        });
    }

    internal void injectPrivacyPanel(GameObject popup)
    {
        UIPrivacyPanel panel = popup.GetComponent<UIPrivacyPanel>();
        LocalizationController loc = findController<LocalizationController>();
        ServerController server = findController<ServerController>();

        panel.unityButton.addOnTouchedListener((bool isLocked) => 
        {
            server.openUnityPrivacyWindow();
        });

        panel.flurryButton.addOnTouchedListener((bool isLocked) =>
        {
            server.openFlurryPrivacyWindow();
        });

        panel.flurryOptingButton.addOnTouchedListener((bool isLocked) =>
        {
            server.toggleFlurryOpting();
        });

        panel.flurryDeleteDataButton.addOnTouchedListener((bool isLocked) =>
        {
            server.deleteFlurryData();
        });

        panel.addOnUpdateListener((Dictionary<string, object> parameters) =>
        {
            bool unityIn = server.isUnityIn();
            bool flurryIn = server.flurryIn();

            panel.unityText.text = string.Format(loc.getTextByKey("UI_PRIVACY_UNITY"), unityIn ? loc.getTextByKey("UI_PRIVACY_ON") : loc.getTextByKey("UI_PRIVACY_OFF"));

            panel.flurryText.text = string.Format(loc.getTextByKey("UI_PRIVACY_FLURRY"), flurryIn ? loc.getTextByKey("UI_PRIVACY_ON") : loc.getTextByKey("UI_PRIVACY_OFF"));

            panel.flurryOptingButton.transform.Find("Text").GetComponent<Text>().text = loc.getTextByKey(flurryIn ? "UI_PRIVACY_OPT_OUT" : "UI_PRIVACY_OPT_IN");
        });
    }


    private void onChangeLangFromSettings(bool isLeft)
    {
        Lang l = findController<LocalizationController>().currentLang;
        int nextLang = (int)l;

        if (isLeft) nextLang -= 1;
        else nextLang += 1;

        //bounds control
        if (nextLang < 0) nextLang = ((int)Lang.MAX_LANGS) - 1;
        if (nextLang >= (int)Lang.MAX_LANGS) nextLang = 0;

        l = (Lang)nextLang;

        findController<LocalizationController>().switchLanguage(l);
    }

    public void injectMarketView(Market market, GameObject objPanel, BoostLibrary boosts)
    {
        LocalizationController loc = findController<LocalizationController>();
        UIMarketPanel panel = objPanel.GetComponent<UIMarketPanel>();
        boosts.selectedBoost = 0;

        for (int i = 0; i < panel.items.Length; i++)
        {
            Boost boost = (Boost)boosts[i];
            UIMarketItem item = panel.items[i];
            Boosts id = (Boosts)i;

            item.title.text = boost.getFormattedName(loc);
            item.addOnTouchedListener((bool isLocked) => { boosts.selectedBoost = (int)id; });
        }

        panel.adButton.addOnTouchedListener((bool isLocked) =>
        {
            if (isLocked) return;

            closeAllOpenPanels();
            findController<GameplayController>().obtainBoost(boosts.selected);
        });

        panel.addOnUpdateListener((Dictionary<string, object> parameters) =>
        {
            for (int i = 0; i < panel.items.Length; i++)
            {
                UIMarketItem item = panel.items[i];
                Boost boost = (Boost)boosts[i];

                item.stockText.text = string.Format("x{0}", boost.remainingStock);

                item.bg.color = new Color(item.bg.color.r, item.bg.color.g, item.bg.color.b, boosts.selectedBoost == i ? 1.0f : 0.3f);
            }

            if (boosts.selected != null)
            {
                panel.boostText.text = boosts.selected.getFormattedDescription(loc);
                panel.adButton.gameObject.SetActive(true);
            }
            else
            {
                panel.boostText.text = string.Empty;
                panel.adButton.gameObject.SetActive(false);
            }
        });
    }


    public void purchaseBoost(Boost boost)
    {
        string flagName = string.Format("{0}_flag", boost.name);

        GameObject boostFlag = boostsBox.transform.Find(flagName)?.gameObject;

        if (boostFlag == null)
        {
            boostFlag = (GameObject)Instantiate(Resources.Load("UI/Market/BoostTimer"));
            boostFlag.name = flagName;
            boostFlag.transform.SetParent(boostsBox.transform, false);
        }

        UIItem flag = boostFlag.GetComponent<UIItem>();

        Image img = flag.transform.Find("Icon").GetComponent<Image>();
        setImg(img, "others", boost.name);

        flag.addOnUpdateListener((Dictionary<string, object> parameters) =>
        {
            Text t = flag.transform.Find("Text 1").GetComponent<Text>();
            t.text = TimeSpan.FromSeconds(boost.boostRemainingTime).ToString(@"mm\:ss");


            if (boost.boostRemainingTime <= 0) Destroy(flag.gameObject);
        });
    }


    public UIShopPanel injectShopPanel(Library products, GameObject shopPopup)
    {
        UIShopPanel panel = shopPopup.GetComponent<UIShopPanel>();
        GameObject shopButton = GameObject.Find(shopGroup[(int)UIShopGroup.SHOP_BUTTON].name);
        GameplayController gameplay = findController<GameplayController>();
        LocalizationController loc = findController<LocalizationController>();
        PurchaseController purchases = findController<PurchaseController>();

        //Products
        foreach (IAPItem item in products)
        {
            GameObject uiIAPItem = (GameObject)Instantiate(shopGroup[(int)UIShopGroup.SHOP_ITEM]);
            uiIAPItem.transform.SetParent(panel.packsContainer.transform, false);

            UIShopItem shopItem = uiIAPItem.GetComponent<UIShopItem>();
            shopItem.name = item.name;

            shopItem.packAmount.text = item.rewardAmount.ToString();
            shopItem.packPrice.text = purchases.getPrice(item.name);
            setImg(shopItem.icon, "others", item.index.ToString());

            shopItem.discountTag.text = string.Format(loc.getTextByKey("DISCOUNT_TAG"), item.discountTag);
            shopItem.discountTagContainer.SetActive(item.hasDiscountTag);

            shopItem.purchaseButton.addOnTouchedListener((bool isLocked) => 
            {
                purchases.buyProduct(item.name);
            });
        }

        //Offers
        for (int i = 0; i < gameplay.gameplayManager.currentOffer.subject.Count; i++)
        {
            int purchaseIndex = i;

            InventoryItemOffer offer = gameplay.gameplayManager.currentOffer;
            InventoryItem item = offer.subject[i];

            GameObject invOffer = (GameObject)Instantiate(shopGroup[(int)UIShopGroup.SHOP_INV_ITEM]);
            UIShopInvItem ui = invOffer.GetComponent<UIShopInvItem>();

            invOffer.transform.SetParent(panel.invItemsContainer.transform, false);

            ui.itemNameText.text = item.itemName.value;
            ui.itemAmountText.text = string.Format("x{0:0}", offer.amount);
            setImg(ui.icon, "others", item.name);
            ui.bg.color = item.bgColor;

            ui.purchaseButton.addOnTouchedListener((bool isLocked) =>
            {
                if (!gameplay.purchaseShopOffer(gameplay.gameplayManager.currentOffer, purchaseIndex)) return;

                currencyParticlesToTarget(ui.purchaseButton.transform.position, shopButton, "others", item.name, gameplay.gameplayManager.currentOffer.amount);
            });

            ui.purchasePriceText.text = gameplay.gameplayManager.bank.toFormattedCurrency(gameplay.gameplayManager.currentOffer.price);
        }

        //Bundles
        BuffBundleOffer bundleOffer = gameplay.gameplayManager.bundleOffer;
        InventoryItem[] bundleItems = bundleOffer.offeredBuff.recipeComponents;

        panel.bundleTitle.text = string.Format(loc.getTextByKey("SHOP_BUNDLE OFFER"), bundleOffer.offeredBuff.localizedName.value);

        foreach (InventoryItem item in bundleItems)
        {
            GameObject invOffer = (GameObject)Instantiate(shopGroup[(int)UIShopGroup.BUNDLE_SHOP_INV_ITEM]);
            UIShopInvItem ui = invOffer.GetComponent<UIShopInvItem>();

            invOffer.transform.SetParent(panel.bundleContainer.transform, false);

            ui.itemNameText.text = item.itemName.value;
            ui.itemAmountText.text = string.Format("x{0:0}", gameplay.gameplayManager.bank.toFormattedCurrency(bundleOffer.amount));
            setImg(ui.icon, "others", item.name);
            ui.bg.color = item.bgColor;

            ui.purchaseButton.gameObject.SetActive(false);
        }

        panel.purchaseBundleButton.addOnTouchedListener((bool isLocked) => 
        {
            if (!gameplay.purchaseBundleOffer(gameplay.gameplayManager.bundleOffer)) return;
            
            foreach (InventoryItem item in bundleItems)
            {
                currencyParticlesToTarget(panel.purchaseBundleButton.transform.position, shopButton, "others", item.name, bundleOffer.amount / 3);
            }
        });

        panel.purchasedBundleText.text = loc.getTextByKey("UI_PURCHASED");
        panel.bundlePriceText.text = gameplay.gameplayManager.bank.toFormattedCurrency(bundleOffer.price);

        panel.addOnUpdateListener((Dictionary<string, object> parameters) => 
        {
            Bank bank = gameplay.gameplayManager.bank;
            InventoryItemOffer offer = gameplay.gameplayManager.currentOffer;

            for (int i = 0; i < panel.invItemsContainer.transform.childCount; i++)
            {
                UIShopInvItem item = panel.invItemsContainer.transform.GetChild(i).GetComponent<UIShopInvItem>();

                if (!offer.isPurchased(i))
                {
                    item.purchaseButton.gameObject.SetActive(true);
                    item.purchasedTag.gameObject.SetActive(false);

                    if (bank.canAfford(Currency.CREDIT, offer.price))
                    {
                        item.purchaseButton.unlockInteraction();
                    }
                    else
                    {
                        item.purchaseButton.lockInteraction();
                    }
                }
                else
                {
                    item.purchaseButton.gameObject.SetActive(false);
                    item.purchasedTag.gameObject.SetActive(true);
                }
            }

            TimeSpan offerTime = TimeSpan.FromSeconds(gameplay.gameplayManager.currentOffer.remainingTime);
            panel.invItemsTimestampText.text = string.Format(loc.getTextByKey("INVENTORY_SHOP_TIMESTAMP"), offerTime.Hours, offerTime.Minutes, offerTime.Seconds);

            //Bundles
            if (bundleOffer.isPurchased || bundleOffer.remainingTime <= 0.0f)
            {
                panel.purchaseBundleButton.gameObject.SetActive(false);
                panel.purchasedBundleText.gameObject.SetActive(true);
            }
            else
            {
                panel.purchasedBundleText.gameObject.SetActive(false);
                panel.purchaseBundleButton.gameObject.SetActive(true);

                if (bank.canAfford(Currency.CREDIT, bundleOffer.price))
                {
                    panel.purchaseBundleButton.unlockInteraction();
                }
                else
                {
                    panel.purchaseBundleButton.lockInteraction();
                }
            }

            TimeSpan bundleTime = TimeSpan.FromSeconds(gameplay.gameplayManager.bundleOffer.remainingTime);
            panel.bundleTimestamp.text = string.Format(loc.getTextByKey("INVENTORY_SHOP_TIMESTAMP"), bundleTime.Hours, bundleTime.Minutes, bundleTime.Seconds);
        });

        if(purchases.scrollFlag)
        {
            Sequence sequence = DOTween.Sequence();

            sequence.Append(panel.scroll.DOVerticalNormalizedPos(1.0f, 0.75f));
            sequence.Append(panel.scroll.DOVerticalNormalizedPos(0.0f, 2.5f));
            sequence.Play();
        }

        return panel;
    }


    public void injectOfflineEarningsPanel(double offlineGold, double diffInSeconds, GameObject popup)
    {
        UIOfflineEarningsPanel panel = popup.GetComponent<UIOfflineEarningsPanel>();
        panel.goldText.text = findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(offlineGold);

        TimeSpan time = TimeSpan.FromSeconds(diffInSeconds);
        panel.timeText.text = string.Format(findController<LocalizationController>().getTextByKey("UI_OFFLINE_REWARD_TITLE"), time.Hours, time.Minutes);

        panel.PickButton.addOnTouchedListener((bool isLocked) => 
        {
            findController<GameplayController>().gameplayManager.bank.update(Currency.GOLD, offlineGold, BankUpdateStrategy.ADD);
            launchUICurrencyParticles(Vector3.one, Currency.GOLD, offlineGold);
            panel.close();
        });
    }


    public void injectRatingsPopup(RatingsController ratings, GameObject popup)
    {
        //closeAllOpenPanels();

        UIRatingPanel panel = popup.GetComponent<UIRatingPanel>();

        setImg(panel.icon, "portraits", string.Format("rating_pet_0{0}", UnityEngine.Random.value < 0.5? '1':'2'));

        panel.rateButton.addOnTouchedListener((bool isLocked) =>
        {
            ratings.hasRatedYet = true;
            Application.OpenURL("market://details?id=com.greenlove.flockingpets");
            panel.close();
        });
    }


    internal void injectAdBalloonPanel(KeyValuePair<TutorialQuestRewards, DoubleVariable> pair, GameObject popup)
    {
        UIAdBalloonPanel panel = popup.GetComponent<UIAdBalloonPanel>();

        panel.amountText.text = findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(pair.Value.value);
        switch (pair.Key)
        {
            case TutorialQuestRewards.GOLD:
                setImg(panel.rewardIcon, "others", "gold_coin");
                break;
            
            case TutorialQuestRewards.INVENTORY_ITEM:
                setImg(panel.rewardIcon, "others", pair.Value.name);
                break;
            
            case TutorialQuestRewards.CREDIT:
                setImg(panel.rewardIcon, "others", "credit_coin");
                break;
        }

        panel.adButton.addOnTouchedListener((bool isLocked) => 
        {
            if (isLocked) return;
#if UNITY_EDITOR || GREENLOVE_DEBUG
        findController<GameplayController>().gameplayManager.claimBalloonReward(pair);
#else
            findController<AdsController>().playAdById("Balloon").onSuccess.AddListener(() => 
            {
                findController<GameplayController>().gameplayManager.claimBalloonReward(pair);

                if (pair.Key == TutorialQuestRewards.INVENTORY_ITEM)
                {
                    launchInventoryParticlesToTarget(panel.adButton.transform.position, getTargetByReward(pair.Key), pair.Value.name, (int)pair.Value.value);
                }
                else
                {
                    currencyParticlesToTarget(panel.adButton.transform.position, getTargetByReward(pair.Key), "others", string.Format("{0}_coin", pair.Value.name), pair.Value.value);
                }
                panel.close();
            });
#endif
        });

        panel.onClosed.AddListener((bool unUsed) => { findController<GameplayController>().gameplayManager.sendBalloonAway(); });
    }


    public GameObject bringBlackCurtain(Color color)
    {
        GameObject blackCurtain = null;
        string name = overlaysGroup[(int)Overlays.BLACK_OVERLAY].name;

        //allow only one overlay
        blackCurtain = firstOrCreateByTag(Tutorial.overlayTag, overlaysGroup, (int)Overlays.BLACK_OVERLAY);
        blackCurtain.transform.SetParent(canvas.transform, false);
        blackCurtain.name = name;

        Sequence curtainOn = DOTween.Sequence();
        curtainOn.Append(blackCurtain.GetComponent<Image>().DOColor(color, 0.3f));
        curtainOn.SetDelay(0.1f);
        curtainOn.Play();

        return blackCurtain;
    }


    private void bringBlueCurtain(UIPanel summoner, bool makeTransparent, bool skipClosing)
    {
        string name = overlaysGroup[(int)Overlays.BLUE_OVERLAY].name;

        GameObject blue = Instantiate((GameObject)overlaysGroup[(int)Overlays.BLUE_OVERLAY]);
        blue.transform.SetParent(fullArea.transform, false);
        blue.name = name;

        summoner.blueCurtainReference = blue;

        if(!summoner.skipGaugesOnTop) moveGaugesToArea(panelArea.transform, summoner.gameObject.name.ToLower().Contains("shoppopup"));

        findController<CheatsController>().putOnTop();

        //Outline top ui
        for (int i = 0; i < (int)TopUI.LENGTH; i++)
        {
            //Exclude the tutorialquests button
            GameObject obj = GameObject.Find(TopButtons[i].name);
            if (obj != null) obj.GetComponent<UIItem>().showOutline();
        }

        ColorizableController colors = findController<ColorizableController>();
        string colorTag = summoner.overrideOverlayColor && colors.exists(summoner.overlayOverrideColorKey) ? summoner.overlayOverrideColorKey : "main"; 
        Color curtainColor = findController<ColorizableController>().getColor(colorTag);
        curtainColor.a = 0;

        var patternImage = blue.transform.Find("Pattern").GetComponent<RawImage>();

        Sequence curtainOn = DOTween.Sequence();
        curtainOn.OnStart(() => { blue.GetComponent<Image>().color = curtainColor; });

        if (summoner.blueOverlayUsesPattern)
        {
            curtainOn.Append(blue.GetComponent<Image>().DOFade(summoner.makeOverlayTransparent ? summoner.overlayAlpha : 1, 0.5f));
            curtainOn.Join(patternImage.DOFade(0.3f, 0.5f));
        }

        if (!skipClosing)
        {
          curtainOn.AppendCallback(() =>
          {
              blue.GetComponent<UIItem>().addOnTouchedListener((bool isLocked) => { summoner.close(); });
          });
        }
        curtainOn.SetDelay(0.1f);
        curtainOn.Play();

    }


    public void dispelBlackCurtain(bool forceDispelling = false)
    {
        string name = overlaysGroup[(int)Overlays.BLACK_OVERLAY].name;
        Transform curtain = canvas.transform.Find(name);
        if (curtain == null) return;

        GameObject blackCurtain = curtain.gameObject;
            
        findController<CheatsController>().putOnTop();

        Sequence curtainOff = DOTween.Sequence();
        curtainOff.Append(blackCurtain.GetComponent<Image>().DOColor(new Color(0, 0, 0, 0.0f), 0.3f));
        curtainOff.SetDelay(0.1f);
        curtainOff.OnComplete(() => { Destroy(blackCurtain); });
        curtainOff.Play();
    }


    public void dispelBlueCurtain(UIPanel panel, GameObject curtain)
    {
        findController<CheatsController>().putOnTop();

        if (panels.Count <= 0)
        {
            //Hide Outline top ui
            for (int i = 0; i < (int)TopUI.LENGTH; i++)
            {
                //Exclude the tutorialquests button
                GameObject obj = GameObject.Find(TopButtons[i].name);
                if (obj != null) obj.GetComponent<UIItem>().hideOutline();
            }
        }


        Sequence curtainOff = DOTween.Sequence();
        curtainOff.Append(curtain.GetComponent<Image>().DOColor(Color.clear, 0.3f));
        curtainOff.SetDelay(0.1f);
        curtainOff.OnComplete(() => { Destroy(curtain); });

        if (panel.blueOverlayUsesPattern)
        {
            var patternImage = curtain.transform.Find("Pattern").GetComponent<RawImage>();
            curtainOff.Join(patternImage.DOFade(0, 0.3f));
        }

        curtainOff.Play();
    }

    private void moveGaugesToArea(Transform area, bool isShop = false)
    {
        GameObject.Find(TopButtons[(int)TopUI.GOLD_GAUGE].name)?.transform.SetParent(area, false);
        GameObject.Find(TopButtons[(int)TopUI.CREDIT_GAUGE].name)?.transform.SetParent(area, false);
        GameObject.Find(TopButtons[(int)TopUI.PETS_GAUGE].name)?.transform.SetParent(area, false);

        if (!isShop) return;

        GameObject.Find(TopButtons[(int)TopUI.SHOP_BUTTON].name)?.transform.SetParent(area, false);
    }


    /***Tutorial management***/

    public void launchTutorial()
    {
        resetAlphaListSelection();
        findController<GameplayController>().requestNextTutorial();
    }


    public void tutorialNext()
    {
        findController<GameplayController>().requestNextTutorial();
    }

    public void showNextTutorial(ConversationItem item, int index)
    {
        dispelSimpleFeedback();

        if (item == null)
        {
            finishTutorial();
            return;
        }

        //Remove any tag from previous step
        removeHighlightTags();

        //Check if any panel is open and close it
        if (index == 0) closeAllOpenPanels();

        //Unlock if missing in scene
        if (item.instantiateDueMissingInScene)
        {
            findController<UIUnlocksController>().unlock(item.unlockableContent);
            closeAllOpenPanels();
        }


        GameObject curtain = null;
        GameObject star = findInHierarchy(item.pathInHierarchy);

        //Highlight
        if (item.featuresSomething)
        {
            if (star != null)
            {
                if (!item.isSomethingIn3D)
                {
                    Canvas highlightCanvas = star.AddComponent<Canvas>();
                    star.AddComponent<GraphicRaycaster>();
                    highlightCanvas.overrideSorting = true;
                    highlightCanvas.sortingOrder = 1;
                    star.tag = Tutorial.toBeHighlighted2DTag;

                    UIPanel panel = star.GetComponent<UIPanel>();
                    if (star.GetComponent<UIPanel>() != null)
                    {
                        if (panel.usesCloseButton) panel.closeButton.SetActive(false);
                        star.transform.Find("ClickableLayer")?.gameObject.SetActive(true);
                    }

                    UIItem starUI = star.GetComponent<UIItem>();
                    starUI.onTouched.freezeListenersState();
                    starUI.highlight(true);

                    if(!item.canSkipByTouchingTheBackground) lockAllTouchesExceptFor(starUI);

                    starUI.addOnTouchedListener((bool isLocked) => 
                    {
                        starUI.highlight(false);
                        starUI.onTouched.restoreFromFreeze();
                        if (item.flowNextMethod == FlowMethodType.TAP)
                        {
                            tutorialNext();
                        }
                    });
                }
                else
                {
                    closeAllOpenPanels(); //If it's 3d try clearing panels so it can be clicked
                }
            }
        }

        //Overlay
        if (star != null && item.isSomethingIn3D)
        {
            dispelBlackCurtain(true);
            curtain = spotlightForTutorial(item);
        }
        else
        {
            Destroy(GameObject.FindGameObjectWithTag(Tutorial.spotlightTag));
            curtain = bringBlackCurtain(item.getOverlayColor());
        }
        curtain.transform.SetAsLastSibling();

        GameObject textBox = null;
        if (item.usesText)
        {
            //Create textbox and setup
            textBox = firstOrCreateByTag(Tutorial.textBoxTag, tutorialPanel, (int)TutorialGroup.TALK_ITEM);
            textBox.transform.SetParent(canvas.transform, false);
            setuptextBox(textBox, item);
            textBox.transform.SetAsLastSibling();

            UIItem textBoxButton = textBox.GetComponent<UIItem>();
            textBoxButton.addOnTouchedListener((bool isLocked) => { tutorialNext(); }, false);
        }
        else
        {
            Destroy(firstOrCreateByTag(Tutorial.textBoxTag, tutorialPanel, (int)TutorialGroup.TALK_ITEM, true));
        }

        if (item.canSkipByTouchingTheBackground)
        {
            curtain.GetComponent<UIItem>()?.addOnTouchedListener((bool isLocked) => { tutorialNext(); }, false);
        }
        else
        {
            if(item.usesText) textBox.GetComponent<UIItem>().cleanupDelegates();
            curtain.GetComponent<UIItem>()?.cleanupDelegates();
        }

        //Create arrow and setup
        GameObject arrow = null;
        if (item.usesArrow)
        {
            arrow = firstOrCreateByTag(Tutorial.arrowTag, tutorialPanel, (int)TutorialGroup.ARROW);
            arrow.transform.SetParent(canvas.transform, false);
            arrow.transform.SetAsLastSibling();
            setupArrow(item, arrow, (RectTransform)star.transform);
        }
        else Destroy(firstOrCreateByTag(Tutorial.arrowTag, tutorialPanel, (int)TutorialGroup.ARROW, true));
        
        findController<CheatsController>().putOnTop();
    }

    private void setupArrow(ConversationItem item, GameObject arrow, RectTransform star)
    {
        arrow.transform.SetParent(star.transform, false);

        Vector3 starScale = star.transform.localScale;

        if (starScale.x != 0 && starScale.y != 0 && starScale.z != 0)
        {
            Vector3 arrowScale = new Vector3(1 / starScale.x, 1 / starScale.y, 1 / starScale.z);
            arrow.transform.localScale = Vector3.Max(Vector3.one, arrowScale);
        }

        Vector2 origin = ((RectTransform)star.transform).rect.center;
        arrow.transform.localPosition = new Vector2(
            origin.x + item.distanceFromParent * Mathf.Cos(item.angleFromParent * Mathf.Deg2Rad),
            origin.y + item.distanceFromParent * Mathf.Sin(item.angleFromParent * Mathf.Deg2Rad)
            );

        arrow.transform.eulerAngles = new Vector3(0, 0, item.angleFromParent + 180);

        
    }

    private void setuptextBox(GameObject textBox, ConversationItem item)
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(textBox.transform.DOLocalMove(new Vector3(0, hudArea.GetComponent<RectTransform>().GetHeight() * item.textPosY / 2), 0.3f));
        seq.Play();

        textBox.transform.Find("Text").GetComponent<Text>().text = item.text.value;
    }

    private GameObject spotlightForTutorial(ConversationItem item)
    {
        GameObject star = findInHierarchy(item.pathInHierarchy);

        GameObject spotlight = null;
        
        if (item.isSomethingIn3D)
        {
            // Calculate the screen offset
            Vector2 uiOffset = new Vector2(canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);

            spotlight = firstOrCreateByName(overlaysGroup[(int)Overlays.SPOTLIGHT].name, overlaysGroup, (int)Overlays.SPOTLIGHT);
            Bounds aabb = star.GetComponent<MeshRenderer>().bounds;
            spotlight.transform.localScale = Vector3.one * aabb.size.maxValue();
            spotlightFollow(spotlight, aabb, uiOffset);

            if (item.cameraFollow)
            {
                findController<CameraController>().follow(star);
                findController<CameraController>().onFollowing.AddListener((GameObject followed) => 
                {
                    spotlightFollow(spotlight, followed.GetComponent<MeshRenderer>().bounds, uiOffset);
                });
            }
            else
            {
                Sequence sequence = findController<CameraController>().focusOn(star, () => { spotlightFollow(spotlight, aabb, uiOffset); });
            }

            spotlight.transform.Find("spot").GetComponent<UIItem>().onTouched.RemoveAllListeners();

            if (item.flowNextMethod == FlowMethodType.TAP)
            {
                spotlight.transform.Find("spot").GetComponent<UIItem>().addOnTouchedListener((bool isLocked) =>
                {
                    simulateTouchOn3DWorld(findNameInHierarchy(item.pathInHierarchy));
                    tutorialNext();
                    if (item.cameraFollow) findController<CameraController>().stopFollowing();
                });
            }
        }

        spotlight.name = "Spotlight";
        spotlight.transform.SetParent(canvas.transform, false);
        spotlight.tag = Tutorial.spotlightTag;

        Sequence seq = DOTween.Sequence();
        seq.Append(spotlight.transform.DOScale(spotlight.transform.localScale * 1.2f, 2f));
        seq.Append(spotlight.transform.DOScale(spotlight.transform.localScale, 2f));
        seq.SetLoops(-1, LoopType.Yoyo);
        seq.Play();

        return spotlight;
    }

    private void spotlightFollow(GameObject spotlight, Bounds aabb, Vector2 uiOffset)
    {
        Vector2 ViewportPosition = findController<CameraController>().cam.WorldToViewportPoint(aabb.center);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);
        spotlight.transform.localPosition = proportionalPosition - uiOffset;
    }

    private GameObject findInHierarchy(string nameInHierarchy)
    {
        GameObject original = null;
        try
        {
            string[] path = nameInHierarchy.Split('/');

            original = GameObject.Find(path[0]);
            if (original != null) for (int i = 1; i < path.Length; i++) original = original.transform.Find(path[i]).gameObject;
        }
        catch(NullReferenceException)
        {
            //Debug.Log("[Find Name in Hierarchy]: " + nameInHierarchy);
        }
        return original;
    }

    private string findNameInHierarchy(string nameInHierarchy)
    {
        string[] path = nameInHierarchy.Split('/');
        return path[path.Length - 1];
    }

    public void closeAllOpenPanels()
    {
        int count = panels.Count;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                panels.Peek().close();
            }
        }

        dispelSimpleFeedback();
    }

    public UIPanel isPanelInvoked(string panelName)
    {
        foreach(UIPanel panel in panels)
        {
            if (panel.name == panelName) return panel;
        }
        return null;
    }

    private void simulateTouchOn3DWorld(string touched)
    {
        findController<GameplayController>().touchReleasedReceived(touched, Vector3.zero, false, true);
    }

    public void finishTutorial()
    {
        dispelBlackCurtain(true);

        foreach (string tutorialTag in new string[] { Tutorial.arrowTag, Tutorial.textBoxTag, Tutorial.overlayTag, Tutorial.spotlightTag })
        {
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag(tutorialTag))
            {
                Destroy(obj);
            }
        }

        removeHighlightTags(); 
    }

    private void removeHighlightTags()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag(Tutorial.toBeHighlighted2DTag))
        {
            obj.tag = "Untagged";
            DestroyImmediate(obj.GetComponent<GraphicRaycaster>());
            DestroyImmediate(obj.GetComponent<Canvas>());
            obj.GetComponent<UIItem>().highlight(false);
        }
    }

    public void unlockAllButtons()
    {
        //Bottom buttons
        foreach (GameObject button in bottomButtons)
        {
            GameObject.Find(button.name).GetComponent<UIItem>().unlockInteraction();
        }
    }


    public GameObject invokeUIPanel(GameObject obj, bool alreadyInScene = false, bool skipCurtain = false)
    {
        GameObject panel = alreadyInScene? obj : Instantiate(obj);
        UIPanel uiPanel = panel.GetComponent<UIPanel>();

        if(!skipCurtain) bringBlueCurtain(uiPanel, uiPanel.makeOverlayTransparent, uiPanel.skipClosingFromOverlay);

        panel.transform.SetParent(panelArea.transform, false);

        cleanUpName(panel);
        findController<CheatsController>().putOnTop();

        if(panels.Count > 0)
        {
            panels.Peek().gameObject.SetActive(false);
        }

        panels.Push(uiPanel);

        if(openedTimes.ContainsKey(uiPanel.GetType()))
        {
            openedTimes[uiPanel.GetType()] += 1;
        }
        else
        {
            openedTimes.Add(uiPanel.GetType(), 0);
        }

        GameObject hint = uiPanel.transform.Find("Hint")?.gameObject;

        if (hint != null)
        {
            if (openedTimes[uiPanel.GetType()] % 5 == 0 && uiPanel.usesHint)
            {
                hint.SetActive(true);
                hint.transform.Find("Text").GetComponent<Text>().text = findController<LocalizationController>().getTextByKey(uiPanel.hintKey);
            }
            else
            {
                hint.SetActive(false);
            }
        }

        dispelSimpleFeedback();

        return panel;
    }

    public GameObject invokeUIPanel(string v)
    {
        GameObject panel = (GameObject)Instantiate(Resources.Load(v));
        return invokeUIPanel(panel, true);
    }


    public void notifyPanelFinishedInvocation()
    {
        resetAlphaListSelection();
    }

    public void resetAlphaListSelection()
    {
        GameObject alphaList = GameObject.Find("AlphasList");
        if (alphaList == null) return;

        AlphaSlots slots = findController<GameplayController>().gameplayManager.alphaSlots;
        for (int i = 0; i < alphaList.transform.childCount; i++)
        {
            UIAlphaListItem item = alphaList.transform.GetChild(i).GetComponent<UIAlphaListItem>();
            item.unselect(slots[i]);
        }

        slots.selectedSlot = -1;
    }


    public void notifyPanelClosed(UIPanel uiPanel)
    {
        if (panels.Count > 0)
        {
            if (panels.Peek() == uiPanel) panels.Pop();
            else
            {
                throw new ArgumentException(string.Format("The popup {0} is not the last called one, check again sunshine", uiPanel.name));
            }

            if(panels.Count > 0)
            {
                panels.Peek().gameObject.SetActive(true);
            }

            if(!uiPanel.skipGaugesOnTop && panels.Count <= 0)
            {
                moveGaugesToArea(hudArea.transform, uiPanel.gameObject.name.ToLower().Contains("shoppopup"));
            }
        }
    }


    public GameObject invokeUI(string v)
    {
        GameObject ui = (GameObject)Instantiate(Resources.Load(v));
        ui.transform.SetParent(hudArea.transform, false);

        cleanUpName(ui);

        return ui;
    }


    private Sprite setAnimalImage(Image img, Alphas index, PackMood mood, FlockState state)
    {
        bool isDoingSomething = ((Alpha)findController<GameplayController>().gameplayManager.alphas[(int)index]).isDoingSomething();
        string finalMood = findController<GameplayController>().gameplayManager.getGoodOrBadMood(mood);

        string spriteName = string.Format("{0}_{1}", index.ToString(), isDoingSomething ? state.ToString().ToUpper() : finalMood);

        if (img?.sprite?.name == spriteName) return img.sprite;

        img.sprite = atlases["portraits"].GetSprite(spriteName);
        return img.sprite;
    }


    private void setAnimalBg(Image img, Alpha alpha)
    {
        string spriteName = alpha.pack.mood.ToString();
        if (img?.sprite?.name == alpha.pack.mood.ToString()) return;

        img.sprite = atlases["portraits"].GetSprite(spriteName);
    }

    private void setAnimalImage(Image img, AlphaSlot slot, PackMood overrideMood = PackMood.NONE)
    {
        if (slot.state == AlphaListViewState.EMPTY && img.sprite.name.ToLower().Contains("happy")) return;

        setAnimalImage(img, slot.alpha, overrideMood);
    }

    private void setAnimalImage(Image img, Alpha alpha, PackMood overrideMood = PackMood.NONE)
    {
        if (alpha != null && alpha.alphaID > Alphas.NONE && alpha.alphaID < Alphas.LENGTH)
        {
            FlockState state = alpha.pack.mission == null ? FlockState.NONE : alpha.pack.mission.what;
            setAnimalImage(img, alpha.alphaID, overrideMood == PackMood.NONE? alpha.pack.mood : overrideMood, state);
            if (img.color != Color.white) img.color = Color.white;
        }
    }


    private void setBgMood(Image bg, PackMood mood)
    {
        bg.color = mood == PackMood.HAPPY ? Color.green : Color.red;
    }

    private void setActionImage(Image img, PackMood mood)
    {
        string spriteName = string.Format("{0}Icon", mood == PackMood.HAPPY ? "Scouting" : mood.ToString().Capitalize());
        if (img?.sprite?.name == spriteName) return;

        img.sprite = atlases["others"].GetSprite(spriteName);
    }

    private void setBuildingImage(Image img, CityGroup index, int which)
    {
        string indexStr = index.ToString();

        if (index > CityGroup.NONE && index < CityGroup.LENGTH)
        {
            string sprite = string.Format("{0}0{1}_0{2}", indexStr, which + 1, findController<ColorizableController>().currentColorMode.value);

            img.sprite = atlases["buildings"].GetSprite(sprite);
        }
    }

    private void setBuildingServiceImage(Image img, string name, int level)
    {
        string spriteName = string.Format("{0}_0{1}", name, level);
        if (img?.sprite?.name == spriteName) return;

        img.sprite = atlases["building_items"].GetSprite(spriteName);
    }


    private void setImg(Image img, string sheet, string name)
    {
        if (img?.sprite?.name == name) return;

        img.sprite = atlases[sheet].GetSprite(name);
    }


    private void cleanUpName(GameObject obj)
    {
        obj.name = obj.name.Replace("(Clone)", "");
    }

    public void launchUICurrencyParticles(Vector3 pos, Currency currency, double value)
    {
        int particles = value <= 5 ? (int)value : value <= 500 ? 10 : 15;

        for (int i = 0; i < particles; i++)
        {
            GameObject part = (GameObject)Instantiate(Resources.Load(string.Format("UI/Particles/{0}", currency.ToString().ToLower())));
            GameObject star = null;

            float starChance = UnityEngine.Random.value;
            if(starChance <0.5f) star = (GameObject)Instantiate(Resources.Load("UI/Particles/star"));

            part.transform.SetParent(panelArea.transform, false);
            part.transform.localPosition = pos;

            if (star != null)
            {
                star.transform.SetParent(panelArea.transform, false);
                star.transform.localPosition = pos;
            }

            Sequence sequence = DOTween.Sequence();

            Vector3 endPos = new Vector3(pos.x + UnityEngine.Random.Range(-500, 500), UnityEngine.Random.Range(pos.y, pos.y - 100), 0);
            Vector3 starEndPos = new Vector3(pos.x + UnityEngine.Random.Range(-500, 500), UnityEngine.Random.Range(pos.y, pos.y - 100), 0);

            float tweenDuration = UnityEngine.Random.Range(1f, 2.5f);

            sequence.Append(part.transform.DOLocalJump(endPos, UnityEngine.Random.Range(150f, 400f), UnityEngine.Random.Range(2, 3), tweenDuration));

            if (star != null)
            {
                sequence.Join(star.transform.DOLocalJump(starEndPos, UnityEngine.Random.Range(150f, 400f), UnityEngine.Random.Range(2, 3), tweenDuration));
                sequence.Join(star.transform.DOScale(Vector3.one * UnityEngine.Random.Range(0.4f, 1.5f), tweenDuration));
                sequence.Join(star.transform.DOShakeRotation(tweenDuration));
                sequence.Insert(tweenDuration * 0.8f, star.GetComponent<Image>().DOFade(0, tweenDuration * 0.2f));
            }

            sequence.Insert(tweenDuration * 0.8f, part.GetComponent<Image>().DOFade(0, tweenDuration * 0.2f));
            
            sequence.AppendCallback(() =>
            {
                Destroy(star);
                Destroy(part);
            });
            sequence.SetDelay(i * 0.05f);
            sequence.Play();
        }

        findController<AudioController>().playSound("coin_rain");
    }

    public Sequence currencyParticlesToTarget(Vector3 origin, GameObject target, string sheet, string sprite, double value)
    {
        if (target == null) return null;

        int particles = value <= 5 ? (int)value : value <= 500 ? 10 : 15;
        Sequence seq = null;

        float tickTime = 1.0f;

        for (int i = 0; i < particles; i++)
        {
            GameObject part = (GameObject)Instantiate(Resources.Load(string.Format("UI/Particles/gold")));
            part.transform.SetParent(panelArea.transform, false);
            part.transform.SetAsLastSibling();

            part.transform.position = origin;
            setImg(part.GetComponent<Image>(), sheet, sprite);

            seq = DOTween.Sequence();

            seq.Append(part.transform.DOJump(origin + new Vector3(UnityEngine.Random.Range(-100, 100), UnityEngine.Random.Range(-20, -80), 0), 2, 2, tickTime/2));
            seq.AppendCallback(() =>
            {
                Sequence secondPart = DOTween.Sequence();
                secondPart.Append(part.transform.DOJump(target.transform.position, 50, 1, tickTime/2));
                secondPart.AppendCallback(() => 
                {
                    if (target.transform.DOPause() <= 0)
                    {
                        Sequence jumpy = DOTween.Sequence();
                        jumpy.Append(target.transform.DOScale(1.02f, 0.05f));
                        jumpy.Append(target.transform.DOScale(1f, 0.05f));
                        jumpy.Play();
                    }
                    Destroy(part);
                });
                secondPart.SetDelay((particles - i) * tickTime);
                secondPart.Play();
                
            });
        }

        seq.OnStart(() => { findController<AudioController>().playSound("coin_rain"); });
        seq.Play();
        return seq;
    }

    public void launchInventoryParticlesToTarget(Vector2 origin, GameObject target, string rewardName, int amount = 0)
    {
        if (target == null) return;

        float tickTime = 1.0f;

        GameObject particleObject = (GameObject) GameObject.Instantiate(Resources.Load("UI/Particles/InventoryParticle"));
        UIInventoryItem particle = particleObject.GetComponent<UIInventoryItem>();
        particle.transform.SetParent(panelArea.transform, false);

        Color endColor = particle.bgCircle.color;
        particle.bgCircle.color = new Color(1, 1, 1, 0);
        particle.transform.localScale = Vector3.one * 0.4f;

        setImg(particle.icon, "others", rewardName);
        if (amount > 0) particle.amountText.text = string.Format("x{0}", findController<GameplayController>().gameplayManager.bank.toFormattedCurrency(amount));

        particle.transform.position = origin;

        Sequence sequence = DOTween.Sequence();
        sequence.Append(particle.transform.DOLocalJump(Vector3.zero, 25.0f, 1, tickTime/2));
        sequence.Join(particle.bgCircle.DOColor(endColor, tickTime));
        sequence.Join(particle.transform.DOScale(1, tickTime));

        sequence.Join(particle.transform.DOLocalMove(Vector3.zero, tickTime * 1.5f)); //Wait

        sequence.Append(particle.transform.DOJump(target.transform.position, 10.0f, 1, tickTime * 0.75f));
        sequence.Join(particle.bgCircle.DOFade(0, tickTime / 2));
        sequence.Join(particle.icon.DOFade(0, tickTime / 2));
        sequence.Join(particle.amountText.DOFade(0, tickTime / 2));
        sequence.Join(particle.transform.DOScale(0.1f, tickTime / 2));

        sequence.OnComplete(() => { Destroy(particleObject); });

        sequence.Play();
    }


    private Sequence bumpItemsInARow(float bumpPerItem, float timePerItem, params GameObject[] list)
    {
        Sequence sequence = DOTween.Sequence();

        foreach (GameObject obj in list)
        {
            sequence.Append(obj.transform.DOScale(Vector3.one * bumpPerItem, timePerItem / 2));
            sequence.Append(obj.transform.DOScale(Vector3.one, timePerItem / 2));
        }

        return sequence;
    }


    private Sequence bumpItemsInARow(float bumpPerItem, float timePerItem, params Transform[] list)
    {
        Sequence sequence = DOTween.Sequence();

        foreach (Transform obj in list)
        {
            sequence.Append(obj.transform.DOScale(Vector3.one * bumpPerItem, timePerItem / 2));
            sequence.Append(obj.transform.DOScale(Vector3.one, timePerItem / 2));
        }

        return sequence;
    }


    public void displayBuildingMode(Building building)
    {
        //Close any open panel
        closeAllOpenPanels();

        GameObject buildingModeToolTip = (GameObject)Instantiate(buildingModeGroup[(int)BuildingModeViewGroup.BUILDING_MODE_TOOLTIP]);
        cleanUpName(buildingModeToolTip);
        buildingModeToolTip.transform.SetParent(panelArea.transform.transform, false);

        GameObject buildingModeCloseButton = null;
        if (findController<GameplayController>().tutorialManager.hasAllBeenDisplayed(Tutorials.STOCK_RESTAURANT_TUTORIAL))
        {
            buildingModeCloseButton = (GameObject)Instantiate(buildingModeGroup[(int)BuildingModeViewGroup.CLOSE_MODE_BUTTON]);
            cleanUpName(buildingModeCloseButton);
            buildingModeCloseButton.transform.SetParent(panelArea.transform.transform, false);

            UIItem closeButton = buildingModeCloseButton.GetComponent<UIItem>();
            closeButton.addOnTouchedAnimFinishedListener((bool isLocked) =>
            {
                dispelBuildingMode(buildingModeCloseButton, buildingModeToolTip);
                findController<GameplayController>().dispelBuildingMode();
                closeButton.onTouchedAnimFinished.RemoveAllListeners();
            });
        }

        Sequence sequence = hideHud();

        sequence.Insert(0.5f, buildingModeToolTip.transform.DOBlendableLocalMoveBy(new Vector3(0, -500), 0.25f));

        if (findController<GameplayController>().tutorialManager.hasAllBeenDisplayed(Tutorials.STOCK_RESTAURANT_TUTORIAL))
            sequence.Insert(0.55f, buildingModeCloseButton.transform.DOBlendableLocalMoveBy(new Vector3(0, 400), 0.25f));

        sequence.Play();
    }


    public void dispelBuildingMode(GameObject buildingModeCloseButton, GameObject buildingModeToolTip)
    {
        Sequence sequence = showHud();

        sequence.Insert(0.0f, buildingModeToolTip.transform.DOBlendableLocalMoveBy(new Vector3(0, 500), 0.25f));
        if (buildingModeCloseButton != null) sequence.Insert(0.1f, buildingModeCloseButton.transform.DOBlendableLocalMoveBy(new Vector3(0, -400), 0.25f));

        sequence.AppendCallback(()=> {
            Destroy(buildingModeToolTip);
            Destroy(buildingModeCloseButton);
        });

        sequence.Play();
    }


    public void dispelBuildingMode()
    {
        GameObject buildingModeCloseButton = GameObject.Find(buildingModeGroup[(int)BuildingModeViewGroup.CLOSE_MODE_BUTTON].name);
        GameObject buildingModeToolTip = GameObject.Find(buildingModeGroup[(int)BuildingModeViewGroup.BUILDING_MODE_TOOLTIP].name);

        dispelBuildingMode(buildingModeCloseButton, buildingModeToolTip);
    }

    public Sequence hideHud()
    {
        GameObject callBtn = GameObject.Find(bottomButtons[(int)BottomButtons.CALL_BUTTON].name);
        GameObject shopButton = GameObject.Find(shopGroup[(int)UIShopGroup.SHOP_BUTTON].name);
        GameObject tutBtn = GameObject.Find(bottomButtons[(int)BottomButtons.TUTORIAL_QUEST_BUTTON].name);
        GameObject settingsButton = GameObject.Find(bottomButtons[(int)BottomButtons.SETTINGS_BUTTON].name);
        GameObject cashNum = GameObject.Find(TopButtons[(int)TopUI.GOLD_GAUGE].name);
        GameObject animalsNum = GameObject.Find(TopButtons[(int)TopUI.PETS_GAUGE].name);
        GameObject gemsNum = GameObject.Find(TopButtons[(int)TopUI.CREDIT_GAUGE].name);
        GameObject alphaList = GameObject.Find(alphaListGroup[(int)AlphaListView.ALPHAS_LIST].name);
        GameObject prestigeButton = GameObject.Find(TopButtons[(int)TopUI.PRESTIGE_BUTTON].name);

        bool isAnyAlphaHired = findController<GameplayController>().gameplayManager.alphaSlots.isAnyAssigned();

        Sequence sequence = DOTween.Sequence();
        sequence.Append(callBtn.transform.DOBlendableLocalMoveBy(new Vector3(0, -500), 0.25f));
        if (shopButton != null) sequence.Insert(0.1f, shopButton.transform.DOBlendableLocalMoveBy(new Vector3(550, 0), 0.25f));
        sequence.Insert(0.15f, settingsButton.transform.DOBlendableLocalMoveBy(new Vector3(-550, 0), 0.25f));
        sequence.Insert(0.2f, tutBtn.transform.DOBlendableLocalMoveBy(new Vector3(-550, 0), 0.25f));
        sequence.Insert(0.25f, prestigeButton.transform.DOBlendableLocalMoveBy(new Vector3(-550, 0), 0.25f));
        sequence.Insert(0.3f, boostsBox.transform.DOBlendableLocalMoveBy(new Vector3(-550, 0), 0.25f));

        if (animalsNum != null) sequence.Insert(0.35f, animalsNum.transform.DOBlendableLocalMoveBy(new Vector3(0, 550), 0.25f));
        if (cashNum != null) sequence.Insert(0.4f, cashNum.transform.DOBlendableLocalMoveBy(new Vector3(0, 550), 0.25f));
        if (gemsNum != null) sequence.Insert(0.45f, gemsNum.transform.DOBlendableLocalMoveBy(new Vector3(0, 550), 0.25f));
        if (isAnyAlphaHired) sequence.Insert(0.5f, alphaList.transform.DOBlendableLocalMoveBy(new Vector3(500, 0), 0.25f));

        return sequence;
    }

    public Sequence showHud()
    {
        GameObject callBtn = GameObject.Find(bottomButtons[(int)BottomButtons.CALL_BUTTON].name);
        GameObject shopButton = GameObject.Find(shopGroup[(int)UIShopGroup.SHOP_BUTTON].name);
        GameObject tutBtn = GameObject.Find(bottomButtons[(int)BottomButtons.TUTORIAL_QUEST_BUTTON].name);
        GameObject settingsButton = GameObject.Find(bottomButtons[(int)BottomButtons.SETTINGS_BUTTON].name);
        GameObject cashNum = GameObject.Find(TopButtons[(int)TopUI.GOLD_GAUGE].name);
        GameObject animalsNum = GameObject.Find(TopButtons[(int)TopUI.PETS_GAUGE].name);
        GameObject gemsNum = GameObject.Find(TopButtons[(int)TopUI.CREDIT_GAUGE].name);
        GameObject alphaList = GameObject.Find(alphaListGroup[(int)AlphaListView.ALPHAS_LIST].name);
        GameObject prestigeButton = GameObject.Find(TopButtons[(int)TopUI.PRESTIGE_BUTTON].name);


        bool isAnyAlphaHired = findController<GameplayController>().gameplayManager.alphaSlots.isAnyAssigned();

        Sequence sequence = DOTween.Sequence();

        if (isAnyAlphaHired) sequence.Insert(0.15f, alphaList.transform.DOBlendableLocalMoveBy(new Vector3(-500, 0), 0.25f));
        if (gemsNum != null) sequence.Insert(0.2f, gemsNum.transform.DOBlendableLocalMoveBy(new Vector3(0, -550), 0.25f));
        if (cashNum != null) sequence.Insert(0.25f, cashNum.transform.DOBlendableLocalMoveBy(new Vector3(0, -550), 0.25f));
        if (animalsNum != null) sequence.Insert(0.3f, animalsNum.transform.DOBlendableLocalMoveBy(new Vector3(0, -550), 0.25f));

        sequence.Insert(0.35f, boostsBox.transform.DOBlendableLocalMoveBy(new Vector3(550, 0), 0.25f));
        sequence.Insert(0.4f, tutBtn.transform.DOBlendableLocalMoveBy(new Vector3(550, 0), 0.25f));
        sequence.Insert(0.45f, prestigeButton.transform.DOBlendableLocalMoveBy(new Vector3(550, 0), 0.25f));
        sequence.Insert(0.5f, settingsButton.transform.DOBlendableLocalMoveBy(new Vector3(550, 0), 0.25f));
        if (shopButton != null) sequence.Insert(0.55f, shopButton.transform.DOBlendableLocalMoveBy(new Vector3(-550, 0), 0.25f));
        sequence.Insert(0.6f, callBtn.transform.DOBlendableLocalMoveBy(new Vector3(0, 500), 0.25f));

        return sequence;
    }

    public GameObject firstOrCreateByTag(string tag, Library lib, int index, bool searchForDestroy = false)
    {
        //Find the text box
        GameObject obj = GameObject.FindGameObjectWithTag(tag);

        if (!searchForDestroy)
        {
            if (obj == null) obj = (GameObject)Instantiate(lib[index]);
            obj.tag = tag;
        }

        return obj;
    }

    public GameObject firstOrCreateByName(string name, Library lib, int index)
    {
        //Find the text box
        GameObject obj = GameObject.Find(name);
        if (obj == null) obj = (GameObject)Instantiate(lib[index]);
        obj.name = name;

        return obj;
    }

    //Test in many ways, not sure it works
    public Vector2 getTouchPositionInCanvas()
    {
        Vector2 touch = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvasRect,
            findController<TouchController>().lastTouchCoordinateInScreen,
            findController<CameraController>().cam,
            out touch
            );

        return touch;
    }

    public Vector2 getTouchPositionInCanvas(Vector2 touchPos)
    {
        Vector2 touch = Vector2.zero;
        var screenPoint = findController<CameraController>().cam.WorldToScreenPoint(touchPos);

        Vector2 screenCenter = new Vector2(Screen.currentResolution.width / 2, Screen.currentResolution.height / 2);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvasRect,
            touchPos,
            findController<CameraController>().cam,
            out touch
            );

        return touch;
    }

    public Vector2 WorldToCanvasPosition(Vector3 position)
    {
        // Calculate the screen offset
        Vector2 uiOffset = new Vector2((float)canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);
        
        // Get the position on the canvas
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(position);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);

        // Set the position and remove the screen offset
        return proportionalPosition - uiOffset;
    }

    internal bool registerUITouch(UIItem uIItem)
    {
        if (isStopped) return false;

        if (lockingItem == null) return true;

        bool canAcceptTouches = uIItem == lockingItem;
        if (canAcceptTouches) lockingItem = null;

        return canAcceptTouches;
    }

    private void lockAllTouchesExceptFor(UIItem starUI)
    {
        lockingItem = starUI;
    }
}
