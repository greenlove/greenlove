﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILocalizable {


    void onLangChanged(LocalizationController locController= null);

    void subscribeToLangChanges(LocalizationController locController = null);
    void unsubscribeToLangChanges();
}
