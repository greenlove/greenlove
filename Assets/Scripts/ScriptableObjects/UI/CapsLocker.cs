﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapsLocker : MonoBehaviour
{
    [SerializeField, ValueDropdown("capsMode")]
    private string mode;

    // The selectable values for the dropdown.
    private string[] capsMode = { "Upcase", "Downcase", "Capitalize" };

    [BoxGroup("Preview"), Button("To Upper Case")]
    private void upcaseText()
    {
        Text text = GetComponent<Text>();
        if (text == null) return;

        text.text = text.text.ToUpper();
    }

    [BoxGroup("Preview"), Button("To Lower Case")]
    private void downcaseText()
    {
        Text text = GetComponent<Text>();
        if (text == null) return;

        text.text = text.text.ToLower();
    }

    [BoxGroup("Preview"), Button("Capitalize")]
    private void capitalizeText()
    {
        Text text = GetComponent<Text>();
        if (text == null) return;

        text.text = text.text.Capitalize();
    }


    // Start is called before the first frame update
    void Start()
    {
        call();
    }

    private void Update()
    {
        call();
    }

    private void call()
    {
        switch (mode)
        {
            case "Upcase":
                upcaseText();
                break;

            case "Downcase":
                downcaseText();
                break;

            case "Capitalize":
                capitalizeText();
                break;
        }
    }
}
