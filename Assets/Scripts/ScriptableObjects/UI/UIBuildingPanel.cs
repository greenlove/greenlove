﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UIBuildingPanel : UIPanel
{
    public Text title;

    public Image buildingIcon;
    public Text upgradeBuildingText;
    public UIItem upgradeBuildingButton;

    public Image itemIcon;
    public Text itemTitle;
    public Text itemDescription;
    public Slider itemStars;

    public UIItem serviceTimeFrame;
    public Text serviceTimeAmount;
    public Image serviceItemImage;

    public Text increaseStockTitle;
    public GameObject stockContainer;
    public Text upgradeStockPrice;
    public UIItem upgradeStockButton;

    public Text increaseServiceTitle;
    public Slider increaseServiceSlider;
    public Text upgradeServicePrice;
    public UIItem increaseServiceButton;

    public Text maxCapacity;
    public Text maxCapacityTitle;

    public bool isFillingStock = false;

    public override void Start()
    {
        base.Start();
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(UIBuildingPanel)), CanEditMultipleObjects]
    public class UIBuildingPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIBuildingPanel item = Item as UIBuildingPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.title = (Text)EditorGUILayout.ObjectField("Title", item.title, typeof(Text), true);

            EditorGUILayout.Space();

            item.buildingIcon = (Image) EditorGUILayout.ObjectField("Buildign icon", item.buildingIcon, typeof(Image), true);
            item.upgradeBuildingText = (Text)EditorGUILayout.ObjectField("Upgrade Building Text", item.upgradeBuildingText, typeof(Text), true);
            item.upgradeBuildingButton = (UIItem)EditorGUILayout.ObjectField("Upgrade Building Button", item.upgradeBuildingButton, typeof(UIItem), true);

            EditorGUILayout.Space();

            item.itemIcon = (Image)EditorGUILayout.ObjectField("Item icon", item.itemIcon, typeof(Image), true);
            item.itemTitle = (Text)EditorGUILayout.ObjectField("Item title", item.itemTitle, typeof(Text), true);
            item.itemDescription = (Text)EditorGUILayout.ObjectField("Item description", item.itemDescription, typeof(Text), true);
            item.itemStars = (Slider)EditorGUILayout.ObjectField("Item stars", item.itemStars, typeof(Slider), true);

            EditorGUILayout.Space();

            item.serviceTimeAmount = (Text)EditorGUILayout.ObjectField("Service Time Amount", item.serviceTimeAmount, typeof(Text), true);
            item.serviceTimeFrame = (UIItem)EditorGUILayout.ObjectField("Service Time Frame", item.serviceTimeFrame, typeof(UIItem), true);
            item.serviceItemImage = (Image)EditorGUILayout.ObjectField("Service Time Image", item.serviceItemImage, typeof(Image), true);

            EditorGUILayout.Space();

            item.increaseStockTitle = (Text)EditorGUILayout.ObjectField("Stock slider title", item.increaseStockTitle, typeof(Text), true);
            item.stockContainer = (GameObject)EditorGUILayout.ObjectField("Stock Container", item.stockContainer, typeof(GameObject), true);
            item.upgradeStockButton = (UIItem)EditorGUILayout.ObjectField("Stock Button", item.upgradeStockButton, typeof(UIItem), true);
            item.upgradeStockPrice = (Text)EditorGUILayout.ObjectField("Stock price", item.upgradeStockPrice, typeof(Text), true);

            EditorGUILayout.Space();

            item.increaseServiceTitle = (Text)EditorGUILayout.ObjectField("Service title", item.increaseServiceTitle, typeof(Text), true);
            item.increaseServiceSlider = (Slider)EditorGUILayout.ObjectField("Service slider", item.increaseServiceSlider, typeof(Slider), true);
            item.increaseServiceButton = (UIItem)EditorGUILayout.ObjectField("Service Button", item.increaseServiceButton, typeof(UIItem), true);
            item.upgradeServicePrice = (Text)EditorGUILayout.ObjectField("Service price", item.upgradeServicePrice, typeof(Text), true);

            EditorGUILayout.Space();

            item.maxCapacityTitle = (Text)EditorGUILayout.ObjectField("Max Capacity Title", item.maxCapacityTitle, typeof(Text), true);
            item.maxCapacity = (Text)EditorGUILayout.ObjectField("Max Capacity Text", item.maxCapacity, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
