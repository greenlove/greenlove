﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIShopInvItem : UIItem
{
    public Text itemNameText;
    public Text itemAmountText;
    public Image icon;
    public Image bg;

    public Text ctaTag;

    public UIItem purchaseButton;
    public Text purchasePriceText;
    public Text purchasedTag;


#if UNITY_EDITOR
    [CustomEditor(typeof(UIShopInvItem)), CanEditMultipleObjects]
    public class UIShopInvItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIShopInvItem item = Item as UIShopInvItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.itemNameText = (Text)EditorGUILayout.ObjectField("Item name Text", item.itemNameText, typeof(Text), true);
            item.itemAmountText = (Text)EditorGUILayout.ObjectField("Amount Text", item.itemAmountText, typeof(Text), true);
            item.icon = (Image)EditorGUILayout.ObjectField("Icon", item.icon, typeof(Image), true);
            item.bg = (Image)EditorGUILayout.ObjectField("Background", item.bg, typeof(Image), true);
            item.purchaseButton = (UIItem)EditorGUILayout.ObjectField("Purchase Button", item.purchaseButton, typeof(UIItem), true);
            item.purchasePriceText = (Text)EditorGUILayout.ObjectField("Price Text", item.purchasePriceText, typeof(Text), true);
            item.purchasedTag = (Text)EditorGUILayout.ObjectField("Purchased Text", item.purchasedTag, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
