﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using System.IO;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

/// <summary>
/// A UIPanel is as stated, a panel which holds N UIItems that require attention, be it realtime updates or interaction injection
/// The injection will be limited to the controller related functions, frontend functions such as closing or animating won't be injected at all.
/// </summary>
[Serializable]
public class UIPanel : UIItem
{
    [SerializeField]
    public bool usesCloseButton;
    private bool isClosingBlocked;

    [SerializeField]
    public GameObject closeButton;

    [SerializeField]
    protected UIItem clickableLayer;

    [SerializeField]
    public bool makeOverlayTransparent;

    [SerializeField]
    public bool overrideOverlayColor;

    [SerializeField]
    public string overlayOverrideColorKey;

    [SerializeField]
    public float overlayAlpha;

    [SerializeField]
    public bool skipClosingFromOverlay;

    [SerializeField]
    public bool blueOverlayUsesPattern = true;

    [SerializeField]
    public bool skipGaugesOnTop;

    [SerializeField]
    public GameObject container;

    [SerializeField]
    protected bool animatableOnInvoke;

    [SerializeField]
    protected PopUpAnimType animType;

    [SerializeField]
    protected float invokeAnimTime;

    [HideInInspector]
    public Dictionary<string, object> onInvokeAnimFinishedParams;
    public GreenLoveEvent<Dictionary<string, object>> onInvokeAnimFinished;

    [SerializeField]
    protected bool displaySoundOnInkove;

    [SerializeField, Stem.SoundID]
    protected int invokableSound;

    private bool invokeNotified;

    [SerializeField]
    protected PopupSize popupSize;

    protected GameObject background;
    public GameObject blueCurtainReference;
    public GreenLoveEvent<bool> onClosed;

    public bool usesHint;
    public string hintKey;

    public override void Awake()
    {
        //Start events
        onInvokeAnimFinished = new GreenLoveEvent<Dictionary<string, object>>();
        onClosed = new GreenLoveEvent<bool>();

        base.Awake();
    }

    // Use this for initialization
    public override void Start ()
    {
        invokeNotified = false;
        isClosingBlocked = false;

        base.Start();

        if(usesCloseButton && closeButton != null)
        {
            closeButton.GetComponent<UIItem>().onTouchedAnimFinished.AddListener((bool isLocked) => 
            {
                close();
            });
        }

        fitToBackground();
        animateOnInvoke();
        playSoundOnInvoke();
    }


    public virtual void close(float delay = 0.0f)
    {
        if (isClosingBlocked || this == null) return;       
        
        if(uiController != null)
        {
            uiController.notifyPanelClosed(this);
            if(blueCurtainReference != null) uiController.dispelBlueCurtain(this, blueCurtainReference);
        }
        onClosed.Invoke(true);

        Destroy(gameObject, delay);
        invokeNotified = false;
    }

    internal void blockClosing()
    {
        isClosingBlocked = true;
    }

    internal void allowClosing()
    {
        isClosingBlocked = false;
    }


    public virtual void animateOnInvoke()
    {
        if (!animatableOnInvoke)
        {
            notifyInvocationFinished();
            return;
        }

        Sequence sq = DOTween.Sequence();

        switch (animType)
        {
            case PopUpAnimType.FADE_IN:
                //Doesn't seem to exist a reasonable way to do it...
                break;

            case PopUpAnimType.JUMP_FROM_BELOW:
                Vector3 originalPos = transform.localPosition;

                transform.localPosition = new Vector3(transform.localPosition.x, -2000);
                sq.Append(transform.DOLocalMove(originalPos, invokeAnimTime));
                sq.SetEase(Ease.OutBack);

                sq.AppendCallback(() =>
                {
                    notifyInvocationFinished();
                });

                sq.OnComplete(() =>
                {
                    onInvokeAnimFinished?.Invoke(onInvokeAnimFinishedParams);
                });

                sq.Play();
                break;
        }
    }


    private void playSoundOnInvoke()
    {
        if(displaySoundOnInkove && audioController != null)
        {
            audioController.playSound(invokableSound);
        }
    }

    
    protected void notifyInvocationFinished()
    {
        if (!invokeNotified)
        {
            invokeNotified = true;
            uiController?.notifyPanelFinishedInvocation();
        }
    }

    //TODO Check if not used and remove
    private void fitToBackground()
    {
        if (background == null)
        {
            Transform bg = transform.Find("Background");
            if (bg != null)
            {
                background = bg.gameObject;
            }
            else
            {
                return;
            }
            
        }
    
        Vector2 size=((RectTransform)background.transform).GetSize();
        Vector2 parentSize =((RectTransform)transform).GetSize();
        Vector2 parentSizeCopy = new Vector2(parentSize.x, parentSize.y);
        Vector2 pos = ((RectTransform)transform).localPosition;
        

        ((RectTransform)transform).SetSize(size);
        ((RectTransform)transform).localPosition=new Vector2(pos.x,pos.y-((size.y-parentSizeCopy.y)/2));
    }

    public void addOnInvokeAnimFinishedListener(UnityAction<Dictionary<string, object>> action)
    {
        if (onInvokeAnimFinished == null) onInvokeAnimFinished = new GreenLoveEvent<Dictionary<string, object>>();
        onInvokeAnimFinished.AddListener(action);
    }

    public void setupClickableLayer(bool setttings)
    {
        clickableLayer = transform.Find("ClickableLayer")?.GetComponent<UIItem>();
        if (clickableLayer != null)
        {
            clickableLayer.GetComponent<Image>().raycastTarget = setttings;
            clickableLayer.isInteractable = setttings;
        }
    }

    public override void addOnTouchedListener(UnityAction<bool> action, bool allowMany = true)
    {
        setupClickableLayer(true);
        clickableLayer?.addOnTouchedListener(action, allowMany);
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(UIPanel)), CanEditMultipleObjects]
    public class UIPanelEditor : UIItemEditor
    {
        SerializedProperty invokableSound;

        protected override void OnEnable()
        {
            base.OnEnable();
            invokableSound = serializedObject.FindProperty("invokableSound");
        }

        public override void OnInspectorGUI()
        {
            UIPanel item = (UIPanel)target;

            toggleModule(item);

            uiGroupModule(item);

            selfModule(item);

            realTimeUpdateModule(item);

            overlayModule(item);

            bankUpdateModule(item);

            animatableOnInvokeModuke(item);

            soundOnInvokeModule(item);

            closeButtonModule(item);

            hintingModule(item);

            localizableModule(item);

           // fitToBackgroundModule(item);

            EditorGUILayout.Space();

            warningsModule(item, true);

            saveChangesModule(item);

        }

        protected override void toggleModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            toggleGroup = EditorGUILayout.BeginToggleGroup("Enable all? ", toggleGroup);
            item.usesCloseButton = EditorGUILayout.Toggle("Uses close button?", item.usesCloseButton);
            item.isRealTimeUpdateable = EditorGUILayout.Toggle("Subscribe to realtime updates?", item.isRealTimeUpdateable);
            item.isBankListener = EditorGUILayout.Toggle("Subscribe to bank updates?", item.isBankListener);
            item.animatableOnInvoke = EditorGUILayout.Toggle("Is Animatable On Invocation?", item.animatableOnInvoke);
            item.displaySoundOnInkove = EditorGUILayout.Toggle("Displays sounds on invocation?", item.displaySoundOnInkove);
            item.isLocalizable = EditorGUILayout.Toggle("Is Localizable?", item.isLocalizable);
            EditorGUILayout.EndToggleGroup();
        }

        protected override bool warningsModule<T>(T Item, bool shouldRender = true)
        {
            UIPanel item = Item as UIPanel;

            bool hasWarnings = true;
            bool parentWarnings = base.warningsModule(item, false);
            int warnings = 0;

            if (shouldRender) EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            EditorGUILayout.LabelField("Warnings ");
            EditorGUILayout.Space();


            if(item.usesCloseButton && item.closeButton == null)
            {
                if (shouldRender) EditorGUILayout.LabelField("- You've enabled closebutton support but you don't have any closed buttons");
                warnings++;
            }
            if(item.displaySoundOnInkove && item.audioController == null)
            {
                if (shouldRender) EditorGUILayout.LabelField("- You've enabled sound on invokation but haven't assigned any sound controller, careful");
                warnings++;
            }

            if (parentWarnings)
            {
                if (shouldRender) EditorGUILayout.LabelField("- You're probably not on a canvas or don't have an image where you should");
                warnings++;
            }

            EditorGUILayout.Space();

            if (shouldRender) EditorGUILayout.LabelField(string.Format("Amount of warnings: {0}", warnings));
            
            
            if (shouldRender) EditorGUILayout.EndVertical();

            return hasWarnings && parentWarnings;
        }

        protected override void saveChangesModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            //save the changes
            if (GUI.changed)
            {
                EditorUtility.SetDirty(item);
                EditorSceneManager.MarkSceneDirty(item.gameObject.scene);
            }
        }

        protected void uiGroupModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Library: ");

            item.uiController = (UIController)EditorGUILayout.ObjectField("UI Controller", item.uiController, typeof(UIController), true);
            item.container = (GameObject)EditorGUILayout.ObjectField("Container: ", item.container, typeof(GameObject), true);

            EditorGUILayout.EndVertical();
        }

        protected void closeButtonModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            if (item.usesCloseButton)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Closing button: ");

                if (GUILayout.Button("Add Close Button"))
                {
                    if (item.closeButton == null)
                    {
                        //C:\repo\greenlove\greenlove\Assets\Resources\UI\Labels
                        item.closeButton = (GameObject) PrefabUtility.InstantiatePrefab(Resources.Load("UI/Buttons/Others/CloseButton"));
                        item.closeButton.transform.SetParent(item.transform, true);

                        item.closeButton.transform.localScale = Vector3.one;

                        ((RectTransform)item.closeButton.transform).anchoredPosition =
                            new Vector2(((RectTransform)item.closeButton.transform).rect.width/2, ((RectTransform)item.closeButton.transform).rect.height / 2) * -1;

                        //Debug.Log(new Vector2(((RectTransform)item.closeButton.transform).rect.width / 2, ((RectTransform)item.closeButton.transform).rect.height / 2));
                    }
                }

                item.closeButton = (GameObject)EditorGUILayout.ObjectField("Close button: ", item.closeButton, typeof(GameObject), true);

                EditorGUILayout.EndVertical();
            }
        }

        protected virtual void animatableOnInvokeModuke<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            if(item.animatableOnInvoke)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Animatable on invocation: ");

                item.animType = (PopUpAnimType) EditorGUILayout.EnumPopup("Animation Type: ", item.animType);
                item.invokeAnimTime = EditorGUILayout.FloatField("Animation Time: ", item.invokeAnimTime);

                EditorGUILayout.EndVertical();
            }
        }

        protected void soundOnInvokeModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;

            if (item.displaySoundOnInkove)
            {
                EditorGUILayout.Space();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField("Sound on invokation: ");

                EditorGUILayout.PropertyField(invokableSound);
                serializedObject.ApplyModifiedProperties();

                item.audioController = (AudioController)EditorGUILayout.ObjectField("Audio Controller", item.audioController, typeof(AudioController), true);

                EditorGUILayout.EndVertical();
            }
        }


        private void overlayModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Overlay Module: ");

            item.makeOverlayTransparent = EditorGUILayout.Toggle("Make the overlay transparent", item.makeOverlayTransparent);
            if(item.makeOverlayTransparent)
            {
                item.overlayAlpha = EditorGUILayout.Slider("Overlay alpha", item.overlayAlpha, 0, 1);
            }

            item.overrideOverlayColor = EditorGUILayout.Toggle("Override Overlay Color", item.overrideOverlayColor);
            if(item.overrideOverlayColor)
            {
                item.overlayOverrideColorKey = EditorGUILayout.TextField("Override Color key", item.overlayOverrideColorKey);
            }

            item.skipClosingFromOverlay = EditorGUILayout.Toggle("Skip closing from overlay", item.skipClosingFromOverlay);
            item.blueOverlayUsesPattern = EditorGUILayout.Toggle("Uses background pattern?", item.blueOverlayUsesPattern);
            item.skipGaugesOnTop = EditorGUILayout.Toggle("Skip Gauges on top", item.skipGaugesOnTop);

            EditorGUILayout.EndVertical();
        }

        private void hintingModule<T>(T Item)
        {
            UIPanel item = Item as UIPanel;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Hinting Module: ");

            item.usesHint = EditorGUILayout.Toggle("Uses hints", item.usesHint);

            if(item.usesHint)
            {
                item.hintKey = EditorGUILayout.TextField("Hint Key", item.hintKey);
            }

            EditorGUILayout.EndVertical();
        }
    }
#endif

}

public enum PopUpAnimType
{
    NONE,
    FADE_IN,
    JUMP_FROM_BELOW
}

public enum PopupSize
{
    NONE = -1,
    SMALL,
    MEDIUM,
    LARGE
}
