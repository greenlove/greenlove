﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UISimpleFeedbackProvider : UIItem
{
    public Text feedbackText;
    
#if UNITY_EDITOR

    [CustomEditor(typeof(UISimpleFeedbackProvider)), CanEditMultipleObjects]
    public class UISettingsPanelEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UISimpleFeedbackProvider item = Item as UISimpleFeedbackProvider;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.feedbackText = (Text)EditorGUILayout.ObjectField("Feedback Text", item.feedbackText, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
