﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIGiftCurrencyItem : UIItem
{
    public Text amount;
    public Image currencyIcon;

#if UNITY_EDITOR
    [CustomEditor(typeof(UIGiftCurrencyItem)), CanEditMultipleObjects]
    public class UIGiftCurrencyItemEditor : UIItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIGiftCurrencyItem item = Item as UIGiftCurrencyItem;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.amount = (Text)EditorGUILayout.ObjectField("Amoount Text", item.amount, typeof(Text), true);
            item.currencyIcon = (Image)EditorGUILayout.ObjectField("Currency Icon", item.currencyIcon, typeof(Image), true);

            EditorGUILayout.EndVertical();
        }

    }
#endif
}
