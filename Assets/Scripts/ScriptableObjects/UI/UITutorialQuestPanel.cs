﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UITutorialQuestPanel : UIPanel
{
    public GameObject questsContainer;

    [HideInInspector]
    public UITutorialQuestModule[] modules;

#if UNITY_EDITOR

    [CustomEditor(typeof(UITutorialQuestPanel)), CanEditMultipleObjects]
    public class UITutorialQuestPanelEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UITutorialQuestPanel item = Item as UITutorialQuestPanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.questsContainer = (GameObject)EditorGUILayout.ObjectField("Quests container:", item.questsContainer, typeof(GameObject), true);           

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
