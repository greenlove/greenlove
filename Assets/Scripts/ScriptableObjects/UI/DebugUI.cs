﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour {

    public Button purchaseBtn01;
    public Button purchaseBtn02;
    public Button purchaseBtn03;

    public Button loginBtn;

    public Button watchAdBtn;
    public Button adDebuggerBtn;

    public Slider summonAmount;
    public Button summonAnimalsBtn;

    public PurchaseController iapController;
    public Ads.AdsController adsController;
    public GameplayController gameplayController;

    void Start()
    {
        purchaseBtn01.onClick.AddListener(
            delegate
            {

            });

        purchaseBtn02.onClick.AddListener(
            delegate
            {

            });

        purchaseBtn03.onClick.AddListener(
            delegate
            {

            });


        loginBtn.onClick.AddListener(
            delegate
            {

            });

        watchAdBtn.onClick.AddListener(
            delegate
            {

            });

        adDebuggerBtn.onClick.AddListener(
            delegate
            {

            });

        summonAnimalsBtn.onClick.AddListener(
            delegate
            {

            });
    }
}
