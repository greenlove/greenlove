﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIRealTimeUpdateable {


    void realTimeSubscribe(UIController controller);

    void realTimeUpdate(Dictionary<string, object> paameters);

    void realTimeUnsubscribe(UIController controller = null);

}
