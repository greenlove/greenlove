﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIHomePanel : UIBuildingPanel
{
    public Image alphaPortrait;
    public Slider capacitySlider;

#if UNITY_EDITOR

    [CustomEditor(typeof(UIHomePanel)), CanEditMultipleObjects]
    public class UIHomePanelEditor : UIBuildingPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIHomePanel item = Item as UIHomePanel;

            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            item.title = (Text)EditorGUILayout.ObjectField("Title", item.title, typeof(Text), true);

            EditorGUILayout.Space();

            item.buildingIcon = (Image)EditorGUILayout.ObjectField("Building icon", item.buildingIcon, typeof(Image), true);
            item.upgradeBuildingText = (Text)EditorGUILayout.ObjectField("Upgrade Building Text", item.upgradeBuildingText, typeof(Text), true);
            item.upgradeBuildingButton = (UIItem)EditorGUILayout.ObjectField("Upgrade Building Button", item.upgradeBuildingButton, typeof(UIItem), true);
            item.alphaPortrait = (Image)EditorGUILayout.ObjectField("Alpha portrait", item.alphaPortrait, typeof(Image), true);

            EditorGUILayout.Space();

            item.maxCapacityTitle = (Text)EditorGUILayout.ObjectField("Max Capacity Title", item.maxCapacityTitle, typeof(Text), true);
            item.maxCapacity = (Text)EditorGUILayout.ObjectField("Max Capacity Text", item.maxCapacity, typeof(Text), true);
            item.capacitySlider = (Slider)EditorGUILayout.ObjectField("Capacity Slider", item.capacitySlider, typeof(Slider), true);

            EditorGUILayout.Space();

            item.itemIcon = (Image)EditorGUILayout.ObjectField("Item icon", item.itemIcon, typeof(Image), true);
            item.itemTitle = (Text)EditorGUILayout.ObjectField("Item title", item.itemTitle, typeof(Text), true);
            item.itemDescription = (Text)EditorGUILayout.ObjectField("Item description", item.itemDescription, typeof(Text), true);
            item.itemStars = (Slider)EditorGUILayout.ObjectField("Item stars", item.itemStars, typeof(Slider), true);

            EditorGUILayout.Space();

            item.serviceTimeAmount = (Text)EditorGUILayout.ObjectField("Service Time Amount", item.serviceTimeAmount, typeof(Text), true);
            item.serviceTimeFrame = (UIItem)EditorGUILayout.ObjectField("Service Time Frame", item.serviceTimeFrame, typeof(UIItem), true);
            item.serviceItemImage = (Image)EditorGUILayout.ObjectField("Service Time Image", item.serviceItemImage, typeof(Image), true);

            EditorGUILayout.Space();

            item.increaseServiceTitle = (Text)EditorGUILayout.ObjectField("Service title", item.increaseServiceTitle, typeof(Text), true);
            item.increaseServiceSlider = (Slider)EditorGUILayout.ObjectField("Service slider", item.increaseServiceSlider, typeof(Slider), true);
            item.increaseServiceButton = (UIItem)EditorGUILayout.ObjectField("Service Button", item.increaseServiceButton, typeof(UIItem), true);
            item.upgradeServicePrice = (Text)EditorGUILayout.ObjectField("Service price", item.upgradeServicePrice, typeof(Text), true);

            EditorGUILayout.EndVertical();
        }
    }
#endif
}
