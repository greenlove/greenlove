﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIBigInventoryItem : UIInventoryItem
{
    public Text itemName;


#if UNITY_EDITOR
    [CustomEditor(typeof(UIBigInventoryItem)), CanEditMultipleObjects]
    public class UIBigInventoryItemEditor : UIInventoryItemEditor
    {
        protected override void selfModule<T>(T Item)
        {
            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);

            UIBigInventoryItem item = Item as UIBigInventoryItem;
            
            item.icon = (Image)EditorGUILayout.ObjectField("Icon", item.icon, typeof(Image), true);
            item.bgCircle = (Image)EditorGUILayout.ObjectField("Circle Background", item.bgCircle, typeof(Image), true);
            item.amountText = (Text)EditorGUILayout.ObjectField("Amount Text", item.amountText, typeof(Text), true);
            item.itemName = (Text)EditorGUILayout.ObjectField("Item Name", item.itemName, typeof(Text), true);            

            EditorGUILayout.EndVertical();
        }
        
    }
#endif
}
