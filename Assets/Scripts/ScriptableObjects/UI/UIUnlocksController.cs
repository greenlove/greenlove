﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using BuildingBlocks.Backend;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/UI/UI Unlocks Controller")]
public class UIUnlocksController : Controller, IStorable
{
    private Dictionary<UIUnlocks, bool> unlocksState;
    public static string uiUnlocksSaveTag = "UI Unlocks";

    public override void init(Main parent)
    {
        base.init(parent);

        unlocksState = new Dictionary<UIUnlocks, bool>();
        for (int i = 0; i < (int)UIUnlocks.COUNT; i++)
        {
            unlocksState.Add((UIUnlocks)i, false);
        }
    }

    public override void load(JSONObject jsonFile)
    {
        JSONObject obj = jsonFile.GetObject(uiUnlocksSaveTag);
        for (UIUnlocks i = 0; i < UIUnlocks.COUNT; i++)
        {
            if (obj.ContainsKey(i.ToString()))
            {
                unlocksState[i] = obj.GetValue(i.ToString()).Boolean;
            }
        }
    }

    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj[uiUnlocksSaveTag] = new JSONObject();
        for (UIUnlocks i = 0; i < UIUnlocks.COUNT; i++)
        {
            obj[uiUnlocksSaveTag].Obj.Add(i.ToString(), unlocksState[i]);
        }

        return obj;
    }

    public void unlock(UIUnlocks unlock, bool forceUnlock = false)
    {
        if (!unlocksState[unlock] || forceUnlock)
        {
            unlocksState[unlock] = true;
            UIController ui = findController<UIController>();
            switch (unlock)
            {
                case UIUnlocks.CALL_BUTTON:
                    ui.invokeCallButton();
                    ui.invokeSettingsButton();
                    unlocksState[UIUnlocks.SETTINGS_BUTTON] = true;
                    break;
                case UIUnlocks.QUESTS_BUTTON:
                    ui.invokeQuestsButton();
                    break;
                case UIUnlocks.SHOP_BUTTON:
                    ui.invokeShopButton();
                    break;
                case UIUnlocks.ALPHA_LIST:
                    findController<GameplayController>().invokeAlphasListView();
                    break;
                case UIUnlocks.PETS_GAUGE:
                    ui.invokePetsGauge();
                    break;
                case UIUnlocks.GOLD_GAUGE:
                    ui.invokeGoldGauge();
                    break;
                case UIUnlocks.CREDITS_GAUGE:
                    ui.invokeCreditsGauge();
                    break;
                case UIUnlocks.PRESTIGE_BUTTON_HUD:
                    ui.invokePrestigeButton();
                    break;
                default:
                    break;
            }

            findController<SavefileController>().preSave();
        }
    }

    public override void postinit()
    {
        if (!findController<SavefileController>().saveLoadedCorrectly) return;

        for (UIUnlocks i = 0; i < UIUnlocks.COUNT; i++)
        {
            if (unlocksState[i]) unlock(i, true);
        }
    }
}

public enum UIUnlocks
{
    NONE = -1,
    //Start adding from here
    CALL_BUTTON,
    QUESTS_BUTTON,
    INVENTORY_BUTTON,
    SHOP_BUTTON,
    ALPHA_LIST,
    PETS_GAUGE,
    GOLD_GAUGE,
    CREDITS_GAUGE,
    PRESTIGE_BUTTON,
    SETTINGS_BUTTON,
    PRESTIGE_BUTTON_HUD,
    //Don't add anything from here on
    COUNT
}