﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BuildingBlocks
{
    namespace Backend
    {
        [CreateAssetMenu(menuName = "Building Blocks/Backend/Login Controller")]
        public class LoginController : Controller, ILoginNotifier
        {

            private LoginManager loginManager;
            
            [SerializeField]
            private StringReference cloudID;

            [SerializeField]
            private StringReference loginFlag;

            public override void postinit()
            {
                base.postinit();
                login();
            }

            public void login()
            {
                loginManager = new LoginManager(this);
                loginManager.login();
            }


            public void notifyLoginChange(string newID)
            {
                cloudID.value = newID;
                //Debug.Log(string.Format("Ready to send notification for changedID: {0}", cloudID));

                //loginEvent.Raise(loginFlag, cloudID.value);

            }

        }
    }
}
