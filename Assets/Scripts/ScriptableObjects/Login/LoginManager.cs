﻿using BuildingBlocks;
//using GooglePlayGames;
//using GooglePlayGames.BasicApi;
//using PlayFab;
//using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[Serializable]
public class LoginManager {


    private string ID;

    private ILoginNotifier notifier;


    public LoginManager(ILoginNotifier not)
    {
        notifier = not;
    }

    public void login()
    {
        string identifier = SystemInfo.deviceUniqueIdentifier;

#if UNITY_EDITOR
        if (EditorApplication.isPlayingOrWillChangePlaymode)
        {
            //LoginEditor(identifier);
        }

#elif UNITY_ANDROID
        //PlayFabLoginAndroid(identifier);
        //InitGooglePlay();

#elif UNITY_IOS
        //PlayFabLoginIOS(identifier);

#endif
    }

    public void loginWithUser()
    {
#if UNITY_EDITOR
        return;
#elif UNITY_ANDROID
        //LoginWithGooglePlay();
#endif
    }


#if UNITY_EDITOR
    /*
    private void LoginEditor(string identifier)
    {
        //Debug.Log("Intento loggear desde el editor");

        var request = new LoginWithCustomIDRequest
        {
            CustomId = identifier,
            CreateAccount = true
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }
    */
#elif UNITY_ANDROID

    /*private void InitGooglePlay()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .AddOauthScope("profile")
            .RequestServerAuthCode(false)
            .Build();
        PlayGamesPlatform.InitializeInstance(config);

        PlayGamesPlatform.DebugLogEnabled = true;

        PlayGamesPlatform.Activate();
    }*/
    /*
    private void LoginWithGooglePlay()
    {
        Social.localUser.Authenticate((bool success, string error) =>
        {
            if (success)
            {
                string serverAuthToken = PlayGamesPlatform.Instance.GetServerAuthCode();
                PlayFabLoginAndroid(serverAuthToken);

                Debug.Log(string.Format("GPGS Login Success"));
            }
            else
            {
                Debug.Log(string.Format("GPGS failure: {0}", error));
            }
        });
    }
    */
    /*
    private void PlayFabLoginAndroid(string ID)
    {
        var request = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = ID,
            CreateAccount = true
        };

        PlayFabClientAPI.LoginWithAndroidDeviceID(request, OnLoginSuccess, OnLoginFailure);
    }
    */

#elif UNITY_IOS
    /*
    private void PlayFabLoginIOS(string ID)
    {
        var request = new LoginWithIOSDeviceIDRequest
        {
            DeviceId = ID,
            CreateAccount = true
        };

        PlayFabClientAPI.LoginWithIOSDeviceID(request, OnLoginSuccess, OnLoginFailure);
    }
    */
#endif
    /*
    private void OnLoginFailure(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }
    */
    /*
    private void OnLoginSuccess(LoginResult result)
    {

        Debug.Log(string.Format("Login succesful, ID: {0}", result.PlayFabId));
        ID = result.PlayFabId;
        notifyLogInChange();
    }
    */
    /*
    public void notifyLogInChange()
    {
        notifier.notifyLoginChange(ID);
    }
    */
}
