﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILoginNotifier {

    void notifyLoginChange(string newID);
}
