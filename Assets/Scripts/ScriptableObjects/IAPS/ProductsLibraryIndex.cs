public enum ProductsLibraryIndex
{
    NONE = -1,
    SMALL_PACK,
    BASIC_PACK,
    MEDIUM_PACK,
    BIG_PACK,
    GREAT_PACK,
    HUGE_PACK,
    LENGTH = 6,
}
