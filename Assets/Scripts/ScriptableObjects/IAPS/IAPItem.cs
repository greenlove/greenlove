﻿using UnityEngine;
using Sirenix.OdinInspector;
using BuildingBlocks;
using EasyMobile;

[CreateAssetMenu(menuName = "Building Blocks/IAP/IAP Item")]
public class IAPItem : ScriptableObject
{
    public ProductsLibraryIndex index;
    public double rewardAmount;

    //Not used, wont delete since we may use it in the future
    public bool hasCTATag;

    [EnableIf("hasCTATag")]
    public StringReference ctaTag;

    public bool hasDiscountTag;

    [EnableIf("hasDiscountTag")]
    public string discountTag;

    [HideInInspector]
    public IAPProduct product;
}
