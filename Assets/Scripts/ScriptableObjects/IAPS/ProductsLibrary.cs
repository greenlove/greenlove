﻿using System.Collections;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace BuildingBlocks
{
    namespace ProductsArchive
    {


        /// <summary>
        /// Library of products meant to be purchased along
        /// </summary>
        [CreateAssetMenu(menuName = "Building Blocks/IAP/IAP's Library")]
        public class ProductsLibrary : Library, IEnumerable
        {

#if UNITY_EDITOR
            [CustomEditor(typeof(ProductsLibrary)), CanEditMultipleObjects]
            public class ProductsLibraryEditor : LibraryEditor
            {

                public override void OnInspectorGUI()
                {
                    base.OnInspectorGUI();
                }
            }
#endif
        }
    }
}