﻿using UnityEngine;
using BuildingBlocks;

namespace BuildingBlocks
{
    namespace ProductsArchive
    {
        /// <summary>
        /// Class containing the product ID plus all overrideables for each store
        /// The overrideables will keep a format as com.greenlove.[googleplay, appstore, ...].mainID
        /// where main ID will be provided externally
        /// </summary>
        [CreateAssetMenu(menuName = "Building Blocks/IAP/IAP ID")]
        public class ProductID : ScriptableObject
        {
            [SerializeField] private StringReference gamePrefix;
            [SerializeField] private StringReference googlePlayPrefix;
            [SerializeField] private StringReference appStorePrefix;

            [SerializeField] private StringReference mainID;

            public string googlePlayID { get { return gamePrefix.value + googlePlayPrefix.value + mainID.value; } }
            public string appStoreID { get { return gamePrefix.value + appStorePrefix.value + mainID.value; } }
            public string mainProductID { get { return mainID.value; } }

        }
    }
}