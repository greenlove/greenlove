﻿using BuildingBlocks.ProductsArchive;
using EasyMobile;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class PurchaseManager
{
    private PurchaseController controller;

    public PurchaseManager(PurchaseController parent, Library products)
    {
        controller = parent;

        InAppPurchasing.InitializePurchasing();

        foreach(IAPProduct product in InAppPurchasing.GetAllIAPProducts())
        {
            Debug.Log(product.Name);
            IAPItem item = (IAPItem) products.items.Find(e => e.name == product.Name);
            item.product = product;
        }

        InAppPurchasing.PurchaseCompleted += OnPurchaseSuccess;
        InAppPurchasing.PurchaseFailed += OnPurchaseFailure;
    }

    private void OnPurchaseFailure(IAPProduct obj)
    {
        //Dunno lol
    }

    private void OnPurchaseSuccess(IAPProduct obj)
    {
        IAPItem item = (IAPItem)controller.products.items.Find(e => ((IAPItem)e).product == obj);

        controller.notifyPurchaseSuccess(item);
    }

    internal string getPrice(string name)
    {
        ProductMetadata data = InAppPurchasing.GetProductLocalizedData(name);

        if (data == null) return "";

        return data.localizedPriceString;
    }

    internal void buyProduct(string name)
    {
        InAppPurchasing.Purchase(name);
    }
}
