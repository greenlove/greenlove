﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BuildingBlocks.ProductsArchive;
using BuildingBlocks;
using System;
using EasyMobile;

[CreateAssetMenu(menuName = "Building Blocks/IAP/IAP's Controller")]
public class PurchaseController : Controller {

    private PurchaseManager iapManager;

    [SerializeField]
    public Library products;

    public bool scrollFlag;

    // Use this for initialization
    public override void init(Main main)
    {
        base.init(main);
        iapManager = new PurchaseManager(this, products);
        scrollFlag = false;
    }

    public void buyProduct(string name)
    {
        iapManager.buyProduct(name);
    }

    public string getPrice(string name)
    {
        string price = iapManager.getPrice(name);
        return string.IsNullOrEmpty(price) ? findController<LocalizationController>().getTextByKey("IAP_NOT_AVAILABLE") : price;
    }

    public void notifyPurchaseSuccess(IAPItem product)
    {
        findController<GameplayController>().iapPurchaseSuccess(product);
        findController<ServerController>().sendAnalyticWithParams("PurchaseIAP", "IAPProduct".pair(product.index.ToString()));
    }
}
