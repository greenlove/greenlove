﻿using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ads
{
    [CreateAssetMenu(menuName = "Building Blocks/Ads/Ads Controller")]
    public class AdsController : Controller
    {
        public override void init(Main main)
        {
            base.init(main);
        }

        public AdEventor playAdById(string placement)
        {
            AdEventor eventor = new AdEventor().init(placement);

            findController<ServerController>().sendAnalyticWithParams("RewardedAd", "Placement".pair(placement));

            return eventor;
        }

        public void notifyAdFInished(string placement)
        {

        }

        public void notifyAdStarted(string placement)
        {

        }
    }
}