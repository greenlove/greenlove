﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AdEventor
{
    string placement;
    public UnityEvent onSuccess;
    public UnityEvent onClosed;
    public UnityEvent onShowed;

    public AdEventor init(string id)
    {
        placement = id;
        onSuccess = new UnityEvent();
        onClosed = new UnityEvent();
        onShowed = new UnityEvent();
        return this;
    }

    public AdEventor OnSuccess(UnityEvent ev)
    {
        onSuccess = ev;
        return this;
    }

    public AdEventor OnClosed(UnityEvent ev)
    {
        onClosed = ev;
        return this;
    }
}
