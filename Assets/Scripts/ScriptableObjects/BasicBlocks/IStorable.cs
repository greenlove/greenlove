﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boomlagoon.JSON;

public interface IStorable
{
    void load(JSONObject jsonFile);
    JSONObject save();
}
