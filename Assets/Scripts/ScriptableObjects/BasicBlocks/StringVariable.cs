﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BuildingBlocks
{
    [CreateAssetMenu(menuName = "Building Blocks/String Variable")]
    public class StringVariable : ScriptableObject
    {
        public string value;
    }
}