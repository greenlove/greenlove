﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Building Blocks/Double Variable")]
public class DoubleVariable : ScriptableObject
{
    public double value;


    public virtual double add(double v)
    {
        return value += v;
    }

    public virtual double sub(double v)
    {
        return value -= v;
    }

    public virtual double prod(double v)
    {
        return value *= v;
    }

    public virtual double div(double v)
    {
        return value /= v;
    }

    public virtual double setMax(double v)
    {
        return value = Math.Max(value, v);
    }
}
