﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BuildingBlocks
{
    [Serializable]
    public class FloatReference
    {
        [SerializeField]
        private FloatVariable floatVariable;

        [SerializeField]
        private float constant;

        public bool useConst;

        public float value
        {
            get
            {
                return useConst?
                    constant :
                    floatVariable == null?
                        0f :
                        floatVariable.value;
            }
        }
        
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(FloatReference)), CanEditMultipleObjects]
    public class FloatReferenceDrawer : PropertyDrawer
    {

        public SerializedProperty variable, constant, useConstant;

        private string[] options = new string[] { "Use constant", "Use variable" };
        int selected = 0;


        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            variable = property.FindPropertyRelative("floatVariable");
            constant = property.FindPropertyRelative("constant");
            useConstant = property.FindPropertyRelative("useConst");

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.LabelField(position, "Float Reference");
            useConstant.boolValue = EditorGUI.ToggleLeft(new Rect(position.x, position.y + 15, position.width, 15), "Use contant?", useConstant.boolValue);


            if (useConstant.boolValue)
            {
                //useConstant.boolValue = false;
                constant.floatValue = EditorGUI.FloatField(new Rect(position.x, position.y + 30, position.width, 15), constant.floatValue);

            }
            else
            {
                //useConstant.boolValue = true;
                EditorGUI.PropertyField(new Rect(position.x, position.y + 30, position.width, 15), variable);

            }

            property.serializedObject.ApplyModifiedProperties();

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + 35f;
        }
    }
#endif
}