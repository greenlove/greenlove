﻿using Boomlagoon.JSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

static class JSONObjectExtensions
{
    public static T GetEnum<T>(this JSONObject obj, string key)
    {
        try
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                // Cast ConvertFromString(string text) : object to (T)
                return (T)converter.ConvertFromString(obj.GetString(key));
            }
            return default(T);
        }
        catch (NotSupportedException)
        {
            Debug.Log("LOAD GET ENUM NOT SUPPORTED: " + obj.ToString() + " KEY: " + key);
            return default(T);
        }
    }
}
