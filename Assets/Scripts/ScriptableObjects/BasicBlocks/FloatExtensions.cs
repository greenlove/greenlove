﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FloatExtensions
{
    public static string ToPercentageString(this float integer)
    {
        return integer.ToPercentage().ToString();
    }

    public static float ToPercentage(this float integer)
    {
        return integer * 100;
    }
}
