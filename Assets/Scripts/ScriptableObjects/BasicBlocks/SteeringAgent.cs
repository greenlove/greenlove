﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Implementation of steering behaviors, will be implemented on demand
public class SteeringAgent
{
    private Vector3 position;
    public Vector3 velocity { get; private set; }
    public float maxVelocity;
    private Vector3 desiredVelocity;
    private Vector3 steering;
    private float mass;
    private bool isPaused;

    public void init(float maxVel, float m)
    {
        maxVelocity = maxVel;
        mass = m;

        position = Vector3.zero;
        velocity = Vector3.zero;
        desiredVelocity = Vector3.zero;
        steering = Vector3.zero;
        isPaused = false;
    }

    public void pause()
    {
        isPaused = true;
    }

    public void play()
    {
        isPaused = false;
    }

    public Vector3 seek(Vector3 pos, Vector3 target)
    {
        if (isPaused) return pos;

        position = pos;
        desiredVelocity = Vector3.Normalize(target - position) * maxVelocity;

        steering = desiredVelocity - velocity;
        steering = Vector3.ClampMagnitude(steering, maxVelocity);
        steering = steering / mass;

        velocity = Vector3.ClampMagnitude(velocity + steering, maxVelocity);

        position = position + velocity;

        return position;
    }

    public Vector3 flee(Vector3 pos, Vector3 target)
    {
        if (isPaused) return pos;

        position = pos;
        desiredVelocity = Vector3.Normalize(position - target) * maxVelocity;

        steering = desiredVelocity - velocity;
        steering = Vector3.ClampMagnitude(steering, maxVelocity);
        steering = steering / mass;

        velocity = Vector3.ClampMagnitude(velocity + steering, maxVelocity);

        position = position + velocity;

        return position;
    }

    public Vector3 arrive(Vector3 pos, Vector3 target, float slowRadius)
    {
        if (isPaused) return pos;

        position = pos;

        Vector3 distance = target - position;
        desiredVelocity = (distance.normalized * maxVelocity) * (distance.sqrMagnitude < slowRadius ? distance.sqrMagnitude / slowRadius : 1.0f);

        steering = desiredVelocity - velocity;
        steering = Vector3.ClampMagnitude(steering, maxVelocity);
        steering = steering / mass;

        velocity = Vector3.ClampMagnitude(velocity + steering, maxVelocity);

        position = position + velocity;

        return position;
    }
}
