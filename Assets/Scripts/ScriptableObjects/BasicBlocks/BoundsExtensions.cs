﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoundsExtensions
{
    public static JSONObject ToJSONObject(this Bounds bounds)
    {
        JSONObject obj = new JSONObject();
        obj.Add("center", bounds.center.ToJSONObject());
        obj.Add("size", bounds.size.ToJSONObject());
        return obj;
    }

    public static Bounds FromJSONObject(this Bounds bounds, JSONObject obj)
    {
        bounds.center = new Vector3().FromJSONObject(obj.GetObject("center"));
        bounds.size = new Vector3().FromJSONObject(obj.GetObject("size"));

        return bounds;
    }
}
