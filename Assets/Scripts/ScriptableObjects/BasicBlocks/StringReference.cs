﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;


namespace BuildingBlocks
{
    [Serializable]
    public class StringReference
    {
        [SerializeField]
        private StringVariable stringVariable;

        [SerializeField]
        private string constant;

        [SerializeField]
        private string locKey;

        [SerializeField]
        private string locValue;

        [SerializeField]
        public Lang lang;

        public StringReferenceValue usedValue;

        [SerializeField]
        public LocalizationController loc;

        public string value
        {
            get
            {
                string returned = null;
                switch(usedValue)
                {
                    case StringReferenceValue.STRINGVAR:

                        if (stringVariable == null)
                        {
                            returned = "";
                        }
                        else
                        {
                            returned = stringVariable.value;
                        }

                        break;

                    case StringReferenceValue.CONST:

                        returned = constant;

                        break;
                    case StringReferenceValue.LOC:

                        locValue = loc.getTextByKey(locKey);

                        if (string.IsNullOrEmpty(locValue))
                        {
                            returned = locKey;

                            Debug.LogWarning(string.Format("Warning: The key {0} has no set value for the language {1}", locKey, loc.currentLang.ToString()));
                        }
                        else
                        {
                            returned = locValue;
                        }

                        break;
                }

                return returned;
            }

            set {
                switch (usedValue)
                {
                    case StringReferenceValue.STRINGVAR:

                        stringVariable.value = value;

                        break;

                    case StringReferenceValue.CONST:

                        constant = value;

                        break;
                    case StringReferenceValue.LOC:

                        locValue = value;

                        break;
                }
            }
        }

        public string key
        {
            get
            {
                if (usedValue == StringReferenceValue.LOC) return locKey;
                else return null;
            }
            set
            {
                locKey = value;
            }
        }

        public StringReference(StringReferenceValue v)
        {
            usedValue = v;
        }

    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(StringReference)), CanEditMultipleObjects]
    public class StringReferenceDrawer : PropertyDrawer
    {
        public SerializedProperty variable, constant, locKey, locValue, usedValue, locController;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            variable = property.FindPropertyRelative("stringVariable");
            constant = property.FindPropertyRelative("constant");

            locKey = property.FindPropertyRelative("locKey");
            locValue = property.FindPropertyRelative("locValue");

            usedValue = property.FindPropertyRelative("usedValue");

            locController = property.FindPropertyRelative("loc");

            StringReference target = (StringReference)GetTargetObjectOfProperty(property);

            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var rect = new Rect(position.position, Vector2.one * 20);

            if (EditorGUI.DropdownButton(rect,
                                        new GUIContent(EditorGUIUtility.IconContent("animationdopesheetkeyframe")),
                                        FocusType.Keyboard,
                                        new GUIStyle()
                                        {
                                            fixedWidth = 20f, border = new RectOffset(1, 1, 1, 1)
                                        }))
            {
                GenericMenu genericMenu = new GenericMenu();
                genericMenu.AddItem(new GUIContent("Constant"), target.usedValue == StringReferenceValue.CONST, () => SetProperty(property, StringReferenceValue.CONST));
                genericMenu.AddItem(new GUIContent("Variable"), target.usedValue == StringReferenceValue.STRINGVAR, () => SetProperty(property, StringReferenceValue.STRINGVAR));
                genericMenu.AddItem(new GUIContent("Loc"), target.usedValue == StringReferenceValue.LOC, () => SetProperty(property, StringReferenceValue.LOC));

                genericMenu.ShowAsContext();
            }

            position.position += Vector2.right * 15;
            position.width = position.width * 0.95f;

            EditorGUIUtility.labelWidth =100;

            if (target != null)
            {

                if (target.usedValue == StringReferenceValue.CONST)
                {
                    string newValue = EditorGUI.TextField(position, constant.stringValue);
                    constant.stringValue = newValue;
                }
                else if (target.usedValue == StringReferenceValue.STRINGVAR)
                {
                    EditorGUI.ObjectField(position, variable);
                }
                else if (target.usedValue == StringReferenceValue.LOC)
                {
                    position.height = 16f;

                    if (target.loc == null)
                    {
                        EditorGUI.ObjectField(position, locController);
                        position.position = new Vector2(position.position.x, position.position.y + 18f);
                    }
                    
                    position.height = 16f;

                    string newKey = EditorGUI.TextField(position, locKey.stringValue);

                    position.position = new Vector2(position.position.x, position.position.y + 18f);
                    position.height = 16f;

                    string newVal = EditorGUI.TextField(position, locValue.stringValue);
                    locValue.stringValue = newVal;

                    position.position = new Vector2(position.position.x, position.position.y + 18f);
                    position.height = 16f;

                    position.width = position.width / 4;
                    if (GUI.Button(position, "Get Value"))
                    {
                        if (target.loc != null)
                        {
                            target.loc.init(null);
                            locValue.stringValue = target.loc.getTextByKey(locKey.stringValue);

                            property.serializedObject.ApplyModifiedProperties();
                            property.serializedObject.Update();
                        }
                    }

                    locKey.stringValue = newKey;
                }
            }
            EditorGUI.EndProperty();
        }

        private void SetProperty(SerializedProperty property, StringReferenceValue v)
        {
            StringReference target = (StringReference)GetTargetObjectOfProperty(property);
            target.usedValue = v;
        }

        public static object GetTargetObjectOfProperty(SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("["));
                    var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    obj = GetValue_Imp(obj, elementName, index);
                }
                else
                {
                    obj = GetValue_Imp(obj, element);
                }
            }
            return obj;
        }

        private static object GetValue_Imp(object source, string name, int index)
        {
            var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
            if (enumerable == null) return null;
            var enm = enumerable.GetEnumerator();
            //while (index-- >= 0)
            //    enm.MoveNext();
            //return enm.Current;

            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext()) return null;
            }
            return enm.Current;
        }

        private static object GetValue_Imp(object source, string name)
        {
            if (source == null)
                return null;
            var type = source.GetType();

            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                if (f != null)
                    return f.GetValue(source);

                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p != null)
                    return p.GetValue(source, null);

                type = type.BaseType;
            }
            return null;
        }


        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            StringReference target = (StringReference)GetTargetObjectOfProperty(property);
            float addAmount = 0f;

            if (target != null)
            {
                if (target.usedValue == StringReferenceValue.LOC)
                {
                    addAmount = 55f;

                    if(target.loc != null) { addAmount -= 18; }
                }
            }

            return base.GetPropertyHeight(property, label) + addAmount;
        }

        public static void RepaintInspector(SerializedObject BaseObject)
        {
            foreach (var item in ActiveEditorTracker.sharedTracker.activeEditors)
            {
                if (item.serializedObject == BaseObject)
                {
                    item.Repaint(); return;
                }
            }
        }

    }
#endif
}

public enum StringReferenceValue
{
    ERROR = -1,

    STRINGVAR,
    CONST,
    LOC
}
