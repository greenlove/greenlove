﻿using UnityEngine;

namespace BuildingBlocks
{
    [CreateAssetMenu(menuName ="Building Blocks/Float Variable")]
    public class FloatVariable : ScriptableObject
    {

        public float value;
    }
}
