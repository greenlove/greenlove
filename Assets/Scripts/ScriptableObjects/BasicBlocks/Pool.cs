﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool<T> {

    private List<T> inactives;
    public List<T> actives;

    public int Count
    {
        get
        {
            return (actives == null? 0 : actives.Count) + (inactives == null? 0 : inactives.Count);
        }
    }

    private System.Random random;

    /// <summary>
    /// Creates an "infinite" length pool
    /// </summary>
    public Pool()
    {
        initPool();
    }

    /// <summary>
    /// Creates a pool with a specific size
    /// </summary>
    /// <param name="poolSize">Size of the pool</param>
    public Pool(int poolSize)
    {
        initPool(poolSize);
    }


    private void initPool(int size = -1)
    {
        if (size == -1)
        {
            actives = new List<T>();
            this.inactives = new List<T>();
        }
        else
        {
            actives = new List<T>(size);
            inactives = new List<T>(size);
        }

        random = new System.Random();
    }

    public bool push(T item)
    {
        bool ok = false;
        if (actives != null && inactives != null)
        {
            actives.Remove(item);
            inactives.Add(item);

            ok = true;
        }

        return ok;
    }

    public T pop()
    {
        T t = default;

        if (inactives != null && inactives.Count > 0)
        {
            T temp = inactives[random.Next(inactives.Count)];
            actives.Add(temp);
            inactives.Remove(temp);

            t = temp;
        }

        return t;
    }

    public T peek()
    {
        T t = default;

        if (inactives != null && inactives.Count > 0)
        {
            T temp = inactives[random.Next(inactives.Count)];
            t = temp;
        }

        return t;
    }

    public void clear()
    {
        inactives.RemoveAll(element => element != null);
        inactives.RemoveAll(element => element == null);

        actives.RemoveAll(element => element != null);
        actives.RemoveAll(element => element == null);
    }


    public void printPool()
    {
        Debug.Log(string.Format("Inactives = {0}", inactives.Count));
        Debug.Log(string.Format("Actives = {0}", actives.Count));
    }
}
