﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public static class StringExtensions
{
    public static string ToMayusUnderscored(this string s)
    {
        string rgx = @"([A-Z])([A-Z][a-z])|([a-z0-9])([A-Z])";
        return Regex.Replace(s, rgx, "$1$3_$2$4").ToUpper();
    }

    public static string Capitalize(this string s)
    {
        string result = "";

        if(!string.IsNullOrEmpty(s))
        {
            result = char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }

        return result;
    }

    public static Stream ToStream(this string s)
    {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    public static KeyValuePair<string, string> pair(this string s, string obj)
    {
        return new KeyValuePair<string, string>(s, obj);
    }
}
