﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using UnityEngine;

public class Controller : SerializedScriptableObject, IStorable
{
    protected Main context;
    protected bool isStopped;
    protected bool isInitialized;

    public virtual void init(Main parent)
    {
        context = parent;
        isStopped = false;
        isInitialized = true;
    }

    public virtual void postinit()
    {
    }

    public virtual void preupdate(float dt)
    {
    }

    public virtual void update(float dt)
    {
    }

    public virtual void postupdate(float dt)
    {
    }

    public virtual void stop()
    {
        isStopped = true;
    }

    public virtual T findController<T>() where T: class
    {
        return context.getController<T>();
    }

    public virtual GameObject getRoot()
    {
        return context.gameObject;
    }

    public virtual void load(JSONObject jsonFile)
    {
    }

    public virtual JSONObject save()
    {
        return null;
    }
}
