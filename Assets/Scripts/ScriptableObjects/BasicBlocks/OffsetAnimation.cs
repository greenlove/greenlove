﻿using UnityEngine;
using UnityEngine.UI;

public class OffsetAnimation : MonoBehaviour
{
    void Update()
    {
        float x = (pattern.uvRect.x - Time.deltaTime * speed) % 1;
        float y = (pattern.uvRect.y + Time.deltaTime * speed) % 1;
        pattern.uvRect = new Rect(x, y, pattern.uvRect.width, pattern.uvRect.height);
    }

    [SerializeField] RawImage pattern;
    [SerializeField] float speed;
}
