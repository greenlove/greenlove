﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class EmailHelper
{
    public static void sendEmail(string email, string subject, string body) =>
      Application.OpenURL(string.Format("mailto:{0}?subject={1}&body={2}", email, subject.escapeURL(), body.escapeURL()));

    static string escapeURL(this string url) => UnityWebRequest.EscapeURL(url).Replace("+", "%20");
}
