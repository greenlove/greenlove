﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace BuildingBlocks
{
    [Serializable]
    public class DoubleReference
    {
        [SerializeField]
        private DoubleVariable doubleVariable;

        [SerializeField]
        private double constant;

        [SerializeField]
        private bool useConst;


        public double value
        {
            get
            {
                double returned = 0;

                if (useConst) returned = constant;
                else returned = doubleVariable.value;
                
                return returned;
            }

            set
            {
                if (useConst) constant = value;
                else doubleVariable.value = value;
            }
        }


#if UNITY_EDITOR
        [CustomPropertyDrawer(typeof(DoubleReference)), CanEditMultipleObjects]
        public class DoubleReferenceDrawer : PropertyDrawer
        {

            public SerializedProperty variable, constant, useConst;

            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {

                variable = property.FindPropertyRelative("doubleVariable");
                constant = property.FindPropertyRelative("constant");

                useConst = property.FindPropertyRelative("useConst");
                

                DoubleReference target = (DoubleReference)GetTargetObjectOfProperty(property);


                EditorGUI.BeginProperty(position, label, property);

                position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
                var rect = new Rect(position.position, Vector2.one * 20);

                if (EditorGUI.DropdownButton(rect,
                                            new GUIContent(EditorGUIUtility.IconContent("animationdopesheetkeyframe")),
                                            FocusType.Keyboard,
                                            new GUIStyle()
                                            {
                                                fixedWidth = 20f,
                                                border = new RectOffset(1, 1, 1, 1)
                                            }))
                {
                    GenericMenu genericMenu = new GenericMenu();
                    genericMenu.AddItem(new GUIContent("Constant"), target.useConst, () => SetProperty(property, true));
                    genericMenu.AddItem(new GUIContent("Variable"), !target.useConst, () => SetProperty(property, false));
                    
                    genericMenu.ShowAsContext();
                }

                position.position += Vector2.right * 15;
                //Debug.Log(position.position);

                EditorGUIUtility.labelWidth = 100;

                if (target != null)
                {

                    if (target.useConst)
                    {
                        double newValue = EditorGUI.DoubleField(position, constant.doubleValue);
                        constant.doubleValue = newValue;
                    }
                    else 
                    {
                        EditorGUI.ObjectField(position, variable);
                    }
                    
                }
                EditorGUI.EndProperty();
            }

            private void SetProperty(SerializedProperty property, bool v)
            {
                DoubleReference target = (DoubleReference)GetTargetObjectOfProperty(property);

                target.useConst = v;

            }

            public static object GetTargetObjectOfProperty(SerializedProperty prop)
            {
                var path = prop.propertyPath.Replace(".Array.data[", "[");
                object obj = prop.serializedObject.targetObject;
                var elements = path.Split('.');
                foreach (var element in elements)
                {
                    if (element.Contains("["))
                    {
                        var elementName = element.Substring(0, element.IndexOf("["));
                        var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                        obj = GetValue_Imp(obj, elementName, index);
                    }
                    else
                    {
                        obj = GetValue_Imp(obj, element);
                    }
                }
                return obj;
            }

            private static object GetValue_Imp(object source, string name, int index)
            {
                var enumerable = GetValue_Imp(source, name) as IEnumerable;
                if (enumerable == null) return null;
                var enm = enumerable.GetEnumerator();
                //while (index-- >= 0)
                //    enm.MoveNext();
                //return enm.Current;

                for (int i = 0; i <= index; i++)
                {
                    if (!enm.MoveNext()) return null;
                }
                return enm.Current;
            }

            private static object GetValue_Imp(object source, string name)
            {
                if (source == null)
                    return null;
                var type = source.GetType();

                while (type != null)
                {
                    var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                    if (f != null)
                        return f.GetValue(source);

                    var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    if (p != null)
                        return p.GetValue(source, null);

                    type = type.BaseType;
                }
                return null;
            }

        }
#endif
    }
}