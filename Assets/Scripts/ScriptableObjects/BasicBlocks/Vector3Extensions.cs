﻿using Boomlagoon.JSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static JSONObject ToJSONObject(this Vector3 vector)
    {
        JSONObject obj = new JSONObject();
        obj.Add("x", vector.x);
        obj.Add("y", vector.y);
        obj.Add("z", vector.z);
        return obj;
    }

    public static Vector3 FromJSONObject(this Vector3 vector, JSONObject obj)
    {
        vector.x = (float)obj.GetNumber("x", 0);
        vector.y = (float)obj.GetNumber("y", 0);
        vector.z = (float)obj.GetNumber("z", 0);
        return vector;
    }

    public static float maxValue(this Vector3 vector)
    {
        return Mathf.Max(vector.x, vector.y, vector.z);
    }

    public static float minValue(this Vector3 vector)
    {
        return Mathf.Min(vector.x, vector.y, vector.z);
    }
}
