﻿using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using System.Linq;

#if UNITY_EDITOR
using Sirenix.Utilities.Editor;
#endif

[CreateAssetMenu(menuName = "Building Blocks/Tutorials/New Tutorial")]
public class Tutorial : ScriptableObject, IStorable
{
    public static string textBoxTag = "TutorialText";
    public static string overlayTag = "TutorialOverlay";
    public static string spotlightTag = "TutorialOverlay3D";
    public static string arrowTag = "TutorialPointer";
    public static string highlightTag = "TutorialHighlighted";
    public static string toBeHighlighted3DTag = "HighlightMe3D";
    public static string toBeHighlighted2DTag = "HighlightMe2D";

    public string locPrefix;

    [ListDrawerSettings(OnBeginListElementGUI = "BeginDrawListElement", OnEndListElementGUI = "EndDrawListElement", CustomAddFunction = "createNewConversationItem")]
    public ConversationItem[] conversation;

    [Space(20)]

    public int index = 0;

    //[HideInInspector]
    public bool hasBeenDisplayed;
    public bool hasFinished;

    [HideInInspector]
    public TutorialManager parent;
    private Tutorials id;

    [HideInInspector]
    public GameObject star;

    [HideInInspector]
    internal ConversationItem currentConversation
    {
        get
        {
            if(parent.activeTutorial == (int)id)
            {
                if (index > 0 && index < conversation.Length)
                {
                    return conversation[index];
                }
            }

            return null;
        }
    }


    public void OnEnable()
    {
        foreach (ConversationItem item in conversation) item.parent = this;
    }

    public void init(TutorialManager tutorialManager, Tutorials selfId)
    {
        index = 0;
        hasBeenDisplayed = false;
        hasFinished = false;

        parent = tutorialManager;
        id = selfId;

        star = null;
    }

    public ConversationItem next()
    {
        ConversationItem item = null;
        if(index < conversation.Length)
        {
            item = conversation[index];
            index++;
        }


        return item;
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("displayed", hasBeenDisplayed);
        obj.Add("finished", hasFinished);

        return obj;
    }

    public void load(JSONObject jsonFile)
    {
        if (jsonFile == null) return;

        hasBeenDisplayed = jsonFile.GetBoolean("displayed");
        hasFinished = jsonFile.GetBoolean("finished");
    }

#if UNITY_EDITOR
    private int previewIndex = 0;

    [HorizontalGroup("preview")]
    [Button("Preview last")]
    private void previewLast()
    {
        previewIndex--;
        if (previewIndex < 0) previewIndex = conversation.Length - 1;

        conversation[previewIndex].preview();
    }

    [HorizontalGroup("preview")]
    [Button("Preview next")]
    private void previewNext()
    {
        previewIndex++;
        if (previewIndex >= conversation.Length) previewIndex = 0;

        conversation[previewIndex].preview();
    }

    [HorizontalGroup("preview")]
    [Button("Stop")]
    private void stopPreview()
    {
        previewIndex = 0;
        removePreview();
    }

    public void removePreview()
    {
        GameObject[] items = GameObject.FindGameObjectsWithTag(ConversationItem.tutorialPreviewTag);
        if (items == null || items.Length <= 0) return;

        for (int i = 0; i < items.Length; i++)
        {
            DestroyImmediate(items[i]);
        }
    }

    private void BeginDrawListElement(int index)
    {
        Color[] editorColors = new Color[]
        {
            new Color(0.98f, 0.89f, 0.6f),
            new Color(0.96f, 0.75f, 0.79f),
            new Color(0.98f, 0.53f, 0.36f),
            new Color(0.98f, 0.65f, 0.57f)
        };
        GUI.color = editorColors[index % 4];
        SirenixEditorGUI.BeginListItem(true);
    }

    private void EndDrawListElement(int index)
    {
        GUI.color = new Color(0.9f, 0.9f, 0.9f);
        SirenixEditorGUI.EndListItem();
    }

    private ConversationItem createNewConversationItem()
    {
        ConversationItem item = new ConversationItem();

        if (!string.IsNullOrEmpty(locPrefix))
        {
            item.init();
            item.text.key = string.Format("{0}_{1}", locPrefix, conversation.Length);
            LocalizationController loc = GameObject.FindGameObjectWithTag("Main").GetComponent<CityMain>().getController<LocalizationController>();
            item.text.loc = loc;

            loc.init(null);
            loc.saveKey(item.text.key);
        }

        return item;
    }
#endif
}


