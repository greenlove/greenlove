﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Tutorials/New Quest Weights")]
public class QuestsWeights : SerializedScriptableObject
{
    [HideInInspector]
    public Dictionary<QuestType, float> accumulatedWeights;

    [HideInInspector]
    public Dictionary<TutorialQuestRewards, float> rewardsAccumulatedWeights;

    public void init()
    {
        accumulatedWeights = new Dictionary<QuestType, float>
        {
            { QuestType.RECRUIT_ANIMALS, 1000 },
            { QuestType.PICK_GIFTS, 1890 },
            { QuestType.OBTAIN_INVENTORY_ITEMS, 2500 },
            { QuestType.TAP_BALLOONS, 3200 },
            { QuestType.TAP_VEHICLES, 3700 },
            { QuestType.UPGRADE_BUFF, 4350 },
            { QuestType.FILL_STOCK, 4800 },
            { QuestType.SEND_TO_EAT, 5100 },
            { QuestType.SEND_TO_SLEEP, 5400 },
            { QuestType.SEND_TO_BATH, 5700 },
            { QuestType.SEND_TO_PLAY, 6000 },
            { QuestType.SEND_TO_VET, 6300 },
            { QuestType.SEND_TO_SCOUT, 7000 },
            { QuestType.BUILD_RESTAURANT, 7200 },
            { QuestType.BUILD_BATHS, 7400 },
            { QuestType.BUILD_HOSPITAL, 7600 },
            { QuestType.BUILD_PLAYGROUND, 7800 },
            { QuestType.EVOLVE_BUILDING, 8100 },
            { QuestType.IMPROVE_BUILDING, 8800 }
        };

        rewardsAccumulatedWeights = new Dictionary<TutorialQuestRewards, float>
        {
            { TutorialQuestRewards.GOLD, 1000 },
            { TutorialQuestRewards.INVENTORY_ITEM, 1800 },
            { TutorialQuestRewards.CREDIT, 2200 }
        };
    }

    public QuestType findRandomWeighted()
    {
        if (accumulatedWeights == null || accumulatedWeights.Count <= 0) init();

        float max = accumulatedWeights.Last().Value;
        float randomWeight = UnityEngine.Random.Range(0, max);

        foreach (KeyValuePair<QuestType, float> pair in accumulatedWeights)
        {
            if (randomWeight <= pair.Value)
            {
                return pair.Key;
            }
        }

        return QuestType.NONE;
    }

    internal TutorialQuestRewards findRandomRewardAmountWeighted()
    {
        if (rewardsAccumulatedWeights == null || rewardsAccumulatedWeights.Count <= 0) init();

        float max = rewardsAccumulatedWeights.Last().Value;
        float randomWeight = UnityEngine.Random.Range(0, max);

        foreach (KeyValuePair<TutorialQuestRewards, float> pair in rewardsAccumulatedWeights)
        {
            if (randomWeight <= pair.Value)
            {
                return pair.Key;
            }
        }

        return TutorialQuestRewards.NONE;
    }
}
