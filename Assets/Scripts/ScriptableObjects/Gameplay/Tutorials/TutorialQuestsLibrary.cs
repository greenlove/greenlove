﻿
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Tutorials/New Quests Library")]
public class TutorialQuestsLibrary : Library
{
    public UnityEngine.Object this[TutQuests key]
    {
        get
        {
            return items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(TutorialQuestsLibrary)), CanEditMultipleObjects]
    public class TutorialQuestsLibraryEditor : LibraryEditor
    {
        

        protected override void populateEnumNames(string[] enumEntries)
        {
            base.populateEnumNames(enumEntries);
            int j = 0;
            foreach (UnityEngine.Object item in lib)
            {
                string theName = Regex.Replace(item.name, rgx, "$1$3_$2$4").ToUpper();
                string[] splitName = theName.Split('_');
                enumEntries[j] = theName.Substring(splitName[0].Length + 1) + "_" + splitName[0];

                j++;
            }
        }
    }

#endif
}
