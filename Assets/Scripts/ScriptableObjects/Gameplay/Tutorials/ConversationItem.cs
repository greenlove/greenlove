﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BuildingBlocks;
using System;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System.Linq;

[Serializable]
public class ConversationItem : IBankListener
{
    [FoldoutGroup("Text Message")]
    public bool usesText = true;

    [FoldoutGroup("Text Message"), ShowIf("usesText")]
    public StringReference text;

    [FoldoutGroup("Pos"), Range(-1, 1)]
    public float textPosY;

    [FoldoutGroup("Overlay")]
    public bool usesColorModule;

    [FoldoutGroup("Overlay"), HideIf("usesColorModule")]
    public Color overlayColor;

    [FoldoutGroup("Overlay"), ShowIf("usesColorModule")]
    public string colorTag;

    [FoldoutGroup("Overlay"), Range(0, 1)]
    public float overlayAlpha;

    [FoldoutGroup("Skip & Highlight")]
    public bool canSkipByTouchingTheBackground;

    [FoldoutGroup("Skip & Highlight")]
    public bool featuresSomething;

    [FoldoutGroup("Skip & Highlight"), ShowIf("featuresSomething")]
    public bool instantiateDueMissingInScene;

    [FoldoutGroup("Skip & Highlight"), ShowIf("featuresSomething"), ShowIf("instantiateDueMissingInScene")]
    public UIUnlocks unlockableContent;

    [FoldoutGroup("Skip & Highlight"), ShowIf("featuresSomething")]
    public bool isSomethingIn3D;

    [FoldoutGroup("Skip & Highlight"), ShowIf("featuresSomething"), ShowIf("isSomethingIn3D"), Tooltip("Enable if you want to hightlight a moving target")]
    public bool cameraFollow;

    [FoldoutGroup("Skip & Highlight"), ShowIf("featuresSomething")]
    public string pathInHierarchy;

    [FoldoutGroup("Arrow"), ShowIf("featuresSomething")]
    public bool usesArrow;

    [FoldoutGroup("Arrow"), ShowIf("featuresSomething"), ShowIf("usesArrow"), Range(100, 500)]
    public float distanceFromParent;

    [FoldoutGroup("Arrow"), ShowIf("featuresSomething"), ShowIf("usesArrow"), Range(0, 360)]
    public float angleFromParent;

    [FoldoutGroup("Control Flow"), ReadOnly]
    bool canMoveToNext;

    [FoldoutGroup("Control Flow")]
    public FlowMethodType flowNextMethod;

    [FoldoutGroup("Control Flow"), ShowIf("flowNextMethod", FlowMethodType.BANK)]
    public Currency listenedCurrency;

    [FoldoutGroup("Control Flow"), ShowIf("flowNextMethod", FlowMethodType.BANK)]
    public Bank bank;

    [FoldoutGroup("Control Flow"), ShowIf("flowNextMethod", FlowMethodType.BANK)]
    public double targetValue;

    [ShowIf("parent", null)]
    public Tutorial parent;

    internal Color getOverlayColor()
    {
        Color color = new Color();
        
        if (usesColorModule)
        {
            Color standard = parent.parent.parent.findController<ColorizableController>().getColor(colorTag);
            standard.a = overlayAlpha;
            color = standard;
        }
        else
        {
            color = new Color(overlayColor.r, overlayColor.g, overlayColor.b, overlayAlpha);
        }
        return color;
    }

    public void onSent()
    {
        if(flowNextMethod == FlowMethodType.BANK)
        {
            subscribeToBankChanges();
            canMoveToNext = false;
        }
        else
        {
            canMoveToNext = true;
        }
    }

    public bool canMoveForward(GameObject star)
    {
        if (star != null)
        {
            string featured = pathInHierarchy.Split('/').Last();
            return star.name == featured;
        }

        return canMoveToNext;
    }

    public void moveForward()
    {
        unsubscribeToBankChanges();
        if (cameraFollow) parent.parent.parent.findController<CameraController>().stopFollowing();
        parent.parent.sendNextTutorial();
    }

    public void init()
    {
        text = new StringReference(StringReferenceValue.LOC);
    }

    public void subscribeToBankChanges(bool skipTalk = false)
    {
        if (flowNextMethod == FlowMethodType.BANK && bank != null)
        {
            bank.onSubscribe(this, listenedCurrency);
        }
    }

    public void unsubscribeToBankChanges()
    {
        if (flowNextMethod == FlowMethodType.BANK && bank != null)
        {
            bank.onUnsubscribe(this, listenedCurrency);
        }
    }

    public void onListeningToBank(Currency currency, double delta, double total, string formattedTotal)
    {
        canMoveToNext = total >= targetValue;
        if (canMoveToNext) moveForward();
    }

#if UNITY_EDITOR

    public static string tutorialPreviewTag = "TutorialPreview";

    public void preview()
    {
        Canvas canvas = GameObject.FindGameObjectWithTag("Canvas")?.GetComponent<Canvas>();

        if (canvas != null)
        {
            //Remove previous
            parent.removePreview();
     
            //Overlay
            GameObject overlayItem = (GameObject)GameObject.Instantiate(Resources.Load("UI/Effects/blackOverlay"));
            overlayItem.tag = tutorialPreviewTag;
            overlayItem.transform.SetParent(canvas.transform, false);

            overlayItem.GetComponent<Image>().color = getOverlayColor();

            //Text
            GameObject textItem = (GameObject)GameObject.Instantiate(Resources.Load("UI/Tutorials/TalkItem"));
            textItem.tag = tutorialPreviewTag;
            textItem.transform.SetParent(canvas.transform, false);

            ((RectTransform)textItem.transform).localPosition = new Vector3(0, canvas.pixelRect.height * textPosY);
            textItem.transform.Find("Text").GetComponent<Text>().text = text.key;

            //Arrow
            if (usesArrow)
            {
                GameObject star = (GameObject)GameObject.Instantiate(Resources.Load("UI/Tutorials/CirclePreview"));
                star.transform.SetParent(canvas.transform, false);
                star.tag = tutorialPreviewTag;
                
                GameObject arrow = (GameObject)GameObject.Instantiate(Resources.Load("UI/Tutorials/Arrow"));
                arrow.tag = tutorialPreviewTag;
                arrow.transform.SetParent(star.transform, false);

                Vector2 origin = ((RectTransform)star.transform).rect.center;
                arrow.transform.localPosition = new Vector2(
                    origin.x + distanceFromParent * Mathf.Cos(angleFromParent * Mathf.Deg2Rad),
                    origin.y + distanceFromParent * Mathf.Sin(angleFromParent * Mathf.Deg2Rad));

                arrow.transform.eulerAngles = new Vector3(0, 0, angleFromParent + 180);
            }

        }
    }
#endif
}

public enum FlowMethodType
{
    TAP,
    BANK
}

