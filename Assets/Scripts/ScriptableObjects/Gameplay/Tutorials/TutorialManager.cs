﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[Serializable]
public class TutorialManager : IStorable {


    public Library tutorials;
    public Library tutorialQuests;
    public QuestsWeights questWeights;

    public TutorialQuest[] activeQuests;
    public int maxAmountOfAvailableQuests;
    public int activeTutorial = -1;
    [HideInInspector] public Queue<TutorialQuest> completedQuests;
    public TutorialQuest peekedQuest;

    [HideInInspector]
    public GameplayController parent;

    //Start from clean state
    public void init(GameplayController dad = null)
    {
        if (dad != null) parent = dad;

        activeTutorial = -1;
        activeQuests = new TutorialQuest[maxAmountOfAvailableQuests];

        for (int i = 0; i < tutorials.Count; i++)
        {
            ((Tutorial)tutorials[i]).init(this, (Tutorials)i);
        }

        foreach(TutorialQuest q in tutorialQuests)
        {
            q.init(this);
        }

        completedQuests = new Queue<TutorialQuest>();
    }

    public void postInit()
    {        
        getActiveTutQuests();
        peekedQuest = null;

        //Launch recruit tutorial if nothing has been launched up til now
        launchTutorial(Tutorials.RECRUIT_ANIMALS_TUTORIAL);
    }

    internal void postLoad()
    {
        //In case of tutorial quests
        foreach (TutorialQuest quest in tutorialQuests)
        {
            if (quest.isCompleted && quest.isActive) completedQuests.Enqueue(quest);
        }
    }


    public void launchTutorial(Tutorials tutIndex, bool forceLaunch = false, float delayInSeconds = 0.0f)
    {
        parent.getRoot().GetComponent<MonoBehaviour>().StartCoroutine(launchTutorialWithDelay(tutIndex, forceLaunch, delayInSeconds));
    }

    private IEnumerator launchTutorialWithDelay(Tutorials tutIndex, bool forceLaunch, float delay)
    {
        if (delay > 0.0f) yield return new WaitForSeconds(delay);

        Tutorial tutorial = (Tutorial)tutorials[(int)tutIndex];

        if (!tutorial.hasBeenDisplayed || forceLaunch)
        {
            tutorial.index = 0;
            onPreLaunchtutorial(tutIndex);

            activeTutorial = (int)tutIndex;

            parent.findController<UIController>().launchTutorial();

            parent.findController<ServerController>().sendAnalyticWithParams("TutorialStart", "TutorialID".pair(tutIndex.ToString()));
        }

    }

    //Method that launches a tutorial without any ui, it just set everything up and unlocks everything
    internal void launchHeadlessTutorial(Tutorials tuto)
    {
        Tutorial tutorial = (Tutorial)tutorials[(int)tuto];
        if (tutorial.hasBeenDisplayed) return;

        tutorial.hasBeenDisplayed = true;

        onPreLaunchtutorial(tuto);

        foreach (ConversationItem item in tutorial.conversation)
        {
            if(item.unlockableContent != UIUnlocks.NONE && item.unlockableContent != UIUnlocks.COUNT)
            {
                parent.findController<UIUnlocksController>().unlock(item.unlockableContent);
            }
        }

        tutorial.hasFinished = true;
        onPostFinishTutorial(tuto);
    }

    private bool onPreLaunchtutorial(Tutorials index)
    {
        bool canLaunch = false;
        switch (index)
        {
            case Tutorials.QUESTS_TUTORIAL:
                preLaunchQuestsTutorial();
                canLaunch = true;
                break;

            case Tutorials.SEND_TO_SLEEP_TUTORIAL:
                preLaunchSendToSleepTutorial();
                canLaunch = true;
                break;

            case Tutorials.BUILD_RESTAURANT_TUTORIAL:
                obtainGoldForTutorial(parent.gameplayManager.city[CityGroup.TAVERN].segments[0].price);
                canLaunch = parent.gameplayManager.buildingSlots.findSlotByBuilding(parent.gameplayManager.city[CityGroup.TAVERN]) == null;
                if(canLaunch) preLaunchSendToEatTutorial();
                break;

            case Tutorials.STOCK_RESTAURANT_TUTORIAL:
                preLaunchtockRestaurantTutorial();
                canLaunch = true;
                break;

            case Tutorials.SECOND_HOME_TUTORIAL:
                ((Excavator)parent.gameplayManager.city[CityGroup.EXCAVATOR]).neededBuildingFlag = parent.gameplayManager.city[CityGroup.HOME01];
                break;

            case Tutorials.SHOP_TUTORIAL:
                parent.findController<PurchaseController>().scrollFlag = true;
                ((Alpha)parent.gameplayManager.alphas[0]).pack.mission?.pauseMission();
                break;

            case Tutorials.BUILD_TOWN_HALL_TUTORIAL:
                obtainGoldForTutorial(parent.gameplayManager.city[CityGroup.TOWN_HALL].segments[0].price);
                break;

            case Tutorials.CRAFT_BUFF_TUTORIAL:
                obtainGoldForTutorial(parent.gameplayManager.buffs[Buffs.WARM_WELCOME].getUpgradePriceByLevel());
                break;
        }

        if (!canLaunch) return false;

        parent.findController<UIController>().resetAlphaListSelection();
        return canLaunch;
    }


    private void onPostFinishTutorial(Tutorials index)
    {
        activeTutorial = -1;
        switch (index)
        {
            case Tutorials.QUESTS_TUTORIAL:
                parent.hireAlpha(parent.gameplayManager.alphaSlots[0], true);
                break;

            case Tutorials.STOCK_RESTAURANT_TUTORIAL:
                ((Alpha)parent.gameplayManager.alphas[0]).requestRender(); //Render to show hunger

                parent.gameplayManager.city[CityGroup.MARKET].init(parent, CityGroup.MARKET);
                parent.gameplayManager.launchMarket();
                break;

            case Tutorials.CRAFT_BUFF_TUTORIAL:
                launchTutorial(Tutorials.SEND_TO_SLEEP_TUTORIAL);
                break;

            case Tutorials.SHOP_TUTORIAL:
                ((Balloon)parent.gameplayManager.city[CityGroup.BALLOON]).stopTutorialMode();
                ((Alpha)parent.gameplayManager.alphas[0]).pack.mission?.resumeMission();

                break;

            case Tutorials.CLAIM_QUEST_TUTORIAL:
                launchTutorial(Tutorials.SCOUTING_TUTORIAL, false, 0.5f);
                break;

            case Tutorials.UPGRADE_HOME_TUTORIAL:
                parent.findController<UIController>().displayWavesOnAlpha((Alpha)parent.gameplayManager.alphas[0]);
                break;

            case Tutorials.DISPLAY_INVENTORY_TUTORIAL:
                launchTutorial(Tutorials.SCOUTING_TUTORIAL_WITH_GIFTS, false, 1.5f);
                break;
        }
    }

    public bool hasAllBeenDisplayed(params Tutorials[] parameters)
    {
        bool displayed = true;
        foreach (Tutorials item in parameters)
        {
            displayed = displayed && ((Tutorial)tutorials[(int)item]).hasBeenDisplayed;
        }

        return displayed;
    }

    public bool hasAnyBeenDisplayed(params Tutorials[] parameters)
    {
        bool displayed = false;
        foreach (Tutorials item in parameters)
        {
            displayed = displayed || ((Tutorial)tutorials[(int)item]).hasBeenDisplayed;
        }

        return displayed;
    }

    public bool hasAllBeenCompleted(params Tutorials[] parameters)
    {
        bool displayed = true;
        foreach (Tutorials item in parameters)
        {
            displayed = displayed && ((Tutorial)tutorials[(int)item]).hasFinished;
        }

        return displayed;
    }

    public bool hasAllBeenClaimed(params TutQuests[] parameters)
    {
        bool claimed = true;
        foreach (TutQuests item in parameters)
        {
            claimed = claimed && completedQuests.Contains((TutorialQuest)tutorialQuests[(int)item]);
        }

        return claimed;
    }

    public bool hasAnyBeenClaimed(params TutQuests[] parameters)
    {
        bool claimed = false;
        foreach (TutQuests item in parameters)
        {
            claimed = claimed || completedQuests.Contains((TutorialQuest)tutorialQuests[(int)item]);
        }

        return claimed;
    }

    private void preLaunchQuestsTutorial()
    {
        //We need to do this fake tick because with the post init system, the tick has been done before initializing and activating the quest
        ((TutorialQuest)tutorialQuests[(int)TutQuests.HIRE_ALPHA_02]).tick(1);
        parent.findController<UIUnlocksController>().unlock(UIUnlocks.QUESTS_BUTTON);
        //parent.tryInvokeTutQuestDialog();
    }

    private void preLaunchSendToSleepTutorial()
    {
        parent.gameplayManager.alphaSlots[0].alpha.pack.mood = PackMood.SLEEPY;
        parent.gameplayManager.alphaSlots[0].alpha.requestRender();
    }

    private void preLaunchSendToEatTutorial()
    {
        parent.gameplayManager.alphaSlots[0].alpha.pack.mood = PackMood.HUNGRY;

        Excavator excavator = (Excavator)parent.gameplayManager.city[CityGroup.EXCAVATOR];
        excavator.neededBuildingFlag = parent.gameplayManager.city[CityGroup.TAVERN];
    }

    private void preLaunchtockRestaurantTutorial()
    {
        obtainGoldForTutorial(parent.gameplayManager.city[CityGroup.TAVERN].getActiveSegment().stockPrice);
        parent.gameplayManager.city[CityGroup.TAVERN].getActiveSegment().stockAmount = 0;
    }

    private void obtainGoldForTutorial(double price)
    {
        parent.gameplayManager.bank.update(Currency.GOLD, price - parent.gameplayManager.bank[Currency.GOLD].value, BankUpdateStrategy.ADD);
    }

    public void sendNextTutorial()
    {
        if (activeTutorial != -1)
        {
            Tutorial tutorial = (Tutorial)tutorials[activeTutorial];

            tutorial.hasBeenDisplayed = true;

            ConversationItem next = tutorial.next();
            if(next != null) next.onSent();
            parent.sendNextTutorial(next, tutorial.index-1);

            //The tutorial is finished
            if (next == null)
            {
                tutorial.hasFinished = true;
                parent.findController<ServerController>().sendAnalyticWithParams("TutorialEnd", "TutorialID".pair(((Tutorials)activeTutorial).ToString()));

                //Reset active tutorial
                onPostFinishTutorial((Tutorials)activeTutorial);
            }
        }
    }

    public TutorialQuest[] getActiveTutQuests()
    {
        for (int i = 0; i < maxAmountOfAvailableQuests; i++)
        {
            if (activeQuests[i] == null)
            {
                activeQuests[i] = popNextAvailableTutQuest();
                if(activeQuests[i] != null) activeQuests[i].isActive = true;
            }
        }
        return activeQuests;
    }

    private TutorialQuest popNextAvailableTutQuest()
    {
        foreach (TutorialQuest quest in tutorialQuests)
        {
            if(!quest.isCompleted && !quest.isActive)
            {
                quest.subscribeToBankChanges();
                return quest;
            }
        }
        return null;
    }


    public void claimTutorialQuest(TutorialQuest quest)
    {
        quest.claim();
        peekedQuest = null;

        for (int i = 0; i < maxAmountOfAvailableQuests; i++)
        {
            if (activeQuests[i] == quest) activeQuests[i] = null;
        }

        getActiveTutQuests(); //Force activate the new quests
    }

    public bool isAnyActiveQuestCompleted()
    {
        bool isCompleted = false;

        if (activeQuests.Length > 0)
        {
            for (int i = 0; i < maxAmountOfAvailableQuests; i++)
            {
                if(activeQuests[i] != null) isCompleted |= activeQuests[i].isCompleted;
            }
        }
        return isCompleted;
    }

    public int getNumberOfQuestsUpTo(TutorialQuest quest)
    {
        int max = int.MinValue;
        foreach (TutorialQuest item in activeQuests)
        {
            int index = tutorialQuests.IndexOf(item);
            if (index > max) max = index;
        }

        int target = tutorialQuests.IndexOf(quest);

        return target - max;
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        JSONObject quests = new JSONObject();

        JSONObject tutQuests = new JSONObject();
        foreach (TutorialQuest quest in tutorialQuests) tutQuests.Add(quest.name, quest.save());
        quests.Add("quests_list", tutQuests);

        JSONArray aQuests = new JSONArray();
        for (int i = 0; i < activeQuests.Length; i++) aQuests.Add(activeQuests[i] == null? "NULL" : activeQuests[i].name);
        quests.Add("active_quests", aQuests);

        if (peekedQuest != null) quests.Add("peeked", peekedQuest.name);

        obj.Add("quests", quests);

        JSONObject tuts = new JSONObject();
        foreach (Tutorial tutorial in tutorials) tuts.Add(tutorial.name, tutorial.save());
        obj.Add("tutorials", tuts);

        return obj;

    }

    public void load(JSONObject jsonFile)
    {
        JSONObject quests = jsonFile.GetObject("quests");
        JSONObject tutQuests = quests.GetObject("quests_list");
        JSONArray aQuests = quests.GetArray("active_quests");
        JSONObject tuts = jsonFile.GetObject("tutorials");

        foreach (TutorialQuest quest in tutorialQuests) quest.load(tutQuests.GetObject(quest.name));
        if (quests.ContainsKey("peeked")) peekedQuest = (TutorialQuest)tutorialQuests.items.Find(e => e.name == quests.GetString("peeked"));

        for (int i = 0; i < maxAmountOfAvailableQuests; i++)
        {
            TutorialQuest quest = (TutorialQuest)tutorialQuests.items.Find(e => e.name == aQuests[i].Str);
            
            if(quest != null) quest.subscribeToBankChanges(true);
            activeQuests[i] = quest;
        }

        foreach (Tutorial tutorial in tutorials) tutorial.load(tuts.GetObject(tutorial.name));
    }

    internal bool canShowTutQuestClaimDialog()
    {
        return hasAllBeenDisplayed(Tutorials.QUESTS_TUTORIAL) &&
            (activeTutorial == -1 || activeTutorial == (int)Tutorials.CLAIM_QUEST_TUTORIAL) &&
            parent.findController<UIController>().panels.Count <= 0 &&
            parent.gameplayManager.gameMode == GameMode.NORMAL &&
            !parent.prestigeManager.isOnPrestigeCutscene;
    }

#if UNITY_EDITOR
    public TutorialQuest createEmptyTutorialQuest()
    {
        TutorialQuest quest = ScriptableObject.CreateInstance<TutorialQuest>();
        return quest;
    }

    public TutorialQuest bakeRandomTutorialQuest()
    {
        TutorialQuest quest = createEmptyTutorialQuest();
        quest.type = questWeights.findRandomWeighted();

        quest.tickUntil = getTicksByQuestType(quest.type);
        quest.reward = questWeights.findRandomRewardAmountWeighted();
        quest.rewardAmount = getQuestRewardAmountByRewardType(quest.reward);
        getInventoryItemReward(quest);

        AssetDatabase.CreateAsset(
            quest, 
            Path.Combine(
                "Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "Baked", tutorialQuestAssetName(quest)));

        tutorialQuests.items.Add(quest);

        return quest;
    }

    public string tutorialQuestAssetName(TutorialQuest quest)
    {
        return tutorialQuests.IndexOf(quest).ToString();
    }

    private void getInventoryItemReward(TutorialQuest quest)
    {
        if (quest.reward != TutorialQuestRewards.INVENTORY_ITEM) return;

        quest.inventoryItemReward = (InventoryItem)parent.gameplayManager.inventory[UnityEngine.Random.Range(0, parent.gameplayManager.inventory.Count)];
    }

    private double getQuestRewardAmountByRewardType(TutorialQuestRewards reward)
    {
        double toBeReturned = 1;

        switch (reward)
        {
            case TutorialQuestRewards.GOLD:
                toBeReturned = 20 * Mathf.Log(tutorialQuests.Count);
                break;
            case TutorialQuestRewards.FEAT_INVENTORY_SLOT:
            case TutorialQuestRewards.FEAT_INVENTORY:
            case TutorialQuestRewards.ALPHA:
            case TutorialQuestRewards.FEAT_MARKET:
                toBeReturned = 1;
                break;
            case TutorialQuestRewards.INVENTORY_ITEM:
                toBeReturned = UnityEngine.Random.Range(1, 20);
                break;
            case TutorialQuestRewards.CREDIT:
                toBeReturned = UnityEngine.Random.Range(1, 20) + Mathf.Log(tutorialQuests.Count);
                break;
        }

        return toBeReturned;
    }

    private double getTicksByQuestType(QuestType type)
    {
        double toBeReturned = 1;
        switch (type)
        {
            case QuestType.RECRUIT_ANIMALS:
                toBeReturned = Math.Floor(50 * Mathf.Log(tutorialQuests.Count));
                break;
            case QuestType.BUILD_HOSPITAL:
            case QuestType.BUILD_BATHS:
            case QuestType.BUILD_PLAYGROUND:
            case QuestType.BUILD_RESTAURANT:
            case QuestType.HIRE_ALPHA:
            case QuestType.PERFORM_PRESTIGE:
                toBeReturned = 1;
                break;
            case QuestType.PICK_GIFTS:
            case QuestType.SEND_TO_SCOUT:
            case QuestType.TAP_BALLOONS:
            case QuestType.FILL_STOCK:
                toBeReturned = UnityEngine.Random.Range(1, 10);
                break;
            case QuestType.UPGRADE_BUFF:
            case QuestType.IMPROVE_BUILDING:
            case QuestType.TAP_VEHICLES:
                toBeReturned = UnityEngine.Random.Range(1, 20);
                break;
            case QuestType.SEND_TO_EAT:
            case QuestType.SEND_TO_BATH:
            case QuestType.SEND_TO_PLAY:
            case QuestType.SEND_TO_VET:
            case QuestType.SEND_TO_SLEEP:
            case QuestType.EVOLVE_BUILDING:
                toBeReturned = UnityEngine.Random.Range(1, 5);
                break;
            case QuestType.OBTAIN_INVENTORY_ITEMS:
                toBeReturned = UnityEngine.Random.Range(1, 50);
                break;
        }

        return toBeReturned;
    }
#endif


    public void forceDismissActiveTutorial()
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        parent.sendNextTutorial(null, -1);
        activeTutorial = -1;
#endif
    }

}

//Never erase any entry unless you reeeeally know what you're doing
public enum QuestType
{
    NONE,

    [Description("500")]
    RECRUIT_ANIMALS,

    [Description("1")]
    HIRE_ALPHA,

    [Description("5")]
    SEND_TO_SCOUT,  

    [Description("7")]
    PICK_GIFTS,    

    [Description("20")]
    IMPROVE_BUILDING,  

    [Description("1000")]
    UPGRADE_BUFF,   

    [Description("Deprecated")]
    SEND_TO_SLEEP,//Deprecated

    [Description("2")]
    TAP_BALLOONS,   

    [Description("20")]
    TAP_VEHICLES,      

    [Description("Deprecated")]
    BUILD_RESTAURANT,  

    [Description("30")]
    OBTAIN_INVENTORY_ITEMS,

    [Description("Deprecated")]
    BUILD_HOSPITAL,

    [Description("Deprecated")]
    BUILD_BATHS,

    [Description("Deprecated")]
    BUILD_PLAYGROUND,

    [Description("Deprecated")]
    SEND_TO_EAT,//Deprecated

    [Description("Deprecated")]
    SEND_TO_BATH,//Deprecated

    [Description("Deprecated")]
    SEND_TO_PLAY,//Deprecated

    [Description("Deprecated")]
    SEND_TO_VET,//Deprecated

    [Description("1")]
    PERFORM_PRESTIGE,

    [Description("5")]
    FILL_STOCK,

    [Description("1")]
    EVOLVE_BUILDING,

    [Description("2")]
    SEND_TO_MISSION,

    [Description("2")]
    BUILD_NEW_BUILDING,

    COUNT
}
