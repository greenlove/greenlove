﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialLauncher : MonoBehaviour
{
    public GameplayController controller;
    public Tutorials tutorial;
    public float delay;

    public void launchTutorial()
    {
        controller.tutorialManager.launchTutorial(tutorial, false, delay);
    }
}
