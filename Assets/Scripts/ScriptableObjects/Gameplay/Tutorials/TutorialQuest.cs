﻿using BuildingBlocks;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Boomlagoon.JSON;

[CreateAssetMenu(menuName = "Building Blocks/Tutorials/New Tutorial Quest")]
public class TutorialQuest : ScriptableObject, IBankListener, IStorable
{
    [HideInInspector]
    public bool isCompleted;

    [HideInInspector]
    public bool isActive;
    
    private Bank bank;

    [Space]
    public QuestType type;

    [HideInInspector]
    public Currency tickTo;

    [SerializeField]
    public double tickUntil;

    [ReadOnly]
    public double amount;

    [Space]
    public TutorialQuestRewards reward;

    [ShowIf("reward", TutorialQuestRewards.ALPHA)]
    public Alphas alphaReward;

    [ShowIf("reward", TutorialQuestRewards.INVENTORY_ITEM)]
    public InventoryItem inventoryItemReward;

    [ShowIf("canShowRewardSimpleAmount")]
    public double rewardAmount;

    [HideInInspector]
    public float normalized { get { return(float)(amount / tickUntil); } }

    [HideInInspector]
    public StringReference description;

    public bool triggersTutorialOnCompleted;
    [ShowIf("triggersTutorialOnCompleted")]
    public Tutorials onCompletedTutorial;

    [SerializeField, ShowIf("triggersTutorialOnCompleted")]
    private float onCompletedTutorialDelay;

    public bool triggersTutorialOnClaimed;
    [ShowIf("triggersTutorialOnClaimed")]
    public Tutorials onClaimedTutorial;

    [SerializeField, ShowIf("triggersTutorialOnClaimed")]
    private float onClaimedTutorialDelay;

    [HideInInspector]
    TutorialManager parent;

    public GameObject dialog;
    public bool isFtue;

    public void init(TutorialManager parent)
    {
        bank = parent.parent.gameplayManager.bank;

        description = new StringReference(StringReferenceValue.LOC);
        description.loc = parent.parent.findController<LocalizationController>();
        description.key = string.Format("QUEST_{0}", type.ToString());
        tickTo = getCurrencyByQuestType();

        amount = 0;
        isCompleted = false;
        isActive = false;

        dialog = null;

        this.parent = parent;
    }


    /// <summary>
    /// Increases the amount of the quest until reaching the goal
    /// </summary>
    /// <returns>true if completed, false otherwise</returns>
    public bool tick(double delta)
    {
        if (!isCompleted) amount += delta;
        isCompleted = amount >= tickUntil;

        if (isCompleted)
        {
            if(triggersTutorialOnCompleted) parent.launchTutorial(onCompletedTutorial, false, onCompletedTutorialDelay);
            if (!parent.completedQuests.Contains(this))
            {
                parent.completedQuests.Enqueue(this);
            }

            unsubscribeToBankChanges();
            parent.parent.findController<ServerController>().sendAnalyticWithParams("QuestComplete", "QuestID".pair(((TutQuests)parent.tutorialQuests.IndexOf(this)).ToString()));
        }
        return isCompleted;
    }

    /**Listen to changes on the bank**/
    public void subscribeToBankChanges(bool skipTalk = false)
    {
        bank.onSubscribe(this, tickTo, skipTalk);
    }

    public void unsubscribeToBankChanges()
    {
        bank.onUnsubscribe(this, tickTo);
    }

    public void onListeningToBank(Currency c, double delta, double total, string formattedTotal)
    {
        tick(delta);
    }

    internal void claim()
    {
        isActive = false;
        if (triggersTutorialOnClaimed) parent.launchTutorial(onClaimedTutorial, false, onClaimedTutorialDelay);
        if (parent.completedQuests.Contains(this)) parent.completedQuests.Dequeue();
    }

    public void load(JSONObject jsonFile)
    {
        if (jsonFile == null) return;

        isCompleted = jsonFile.GetBoolean("isCompleted");
        isActive = jsonFile.GetBoolean("isActive");
        amount = jsonFile.GetNumber("amount");

    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("isCompleted", isCompleted);
        obj.Add("isActive", isActive);
        obj.Add("amount", amount);

        return obj;
    }

    private Currency getCurrencyByQuestType()
    {
        Currency toBeReturned = Currency.GOLD;

        switch (type)
        {
            case QuestType.NONE:
                toBeReturned = Currency.NONE;
                break;
            case QuestType.RECRUIT_ANIMALS:
                toBeReturned = Currency.DOGS;
                break;
            case QuestType.HIRE_ALPHA:
                toBeReturned = Currency.HIRED_ALPHAS;
                break;
            case QuestType.SEND_TO_SCOUT:
                toBeReturned = Currency.TIMES_SENT_TO_SCOUT;
                break;
            case QuestType.PICK_GIFTS:
                toBeReturned = Currency.GIFTS_PICKED;
                break;
            case QuestType.IMPROVE_BUILDING:
                toBeReturned = Currency.HOME_ENLARGEMENTS;
                break;
            case QuestType.UPGRADE_BUFF:
                toBeReturned = Currency.UPGRADED_BUFFS;
                break;
            case QuestType.SEND_TO_SLEEP:
                toBeReturned = Currency.TIMES_SENT_TO_SLEEP;
                break;
            case QuestType.TAP_BALLOONS:
                toBeReturned = Currency.AD_BALLOONS_TOUCHED;
                break;
            case QuestType.TAP_VEHICLES:
                toBeReturned = Currency.AD_VEHICLES_TOUCHED;
                break;
            case QuestType.BUILD_RESTAURANT:
                toBeReturned = Currency.BUILT_TAVERNS;
                break;
            case QuestType.OBTAIN_INVENTORY_ITEMS:
                toBeReturned = Currency.INVENTORY_ITEMS_OBTAINED;
                break;
            case QuestType.BUILD_HOSPITAL:
                toBeReturned = Currency.BUILT_HOSPITALS;
                break;
            case QuestType.BUILD_BATHS:
                toBeReturned = Currency.BUILT_BATH_HOUSES;
                break;
            case QuestType.BUILD_PLAYGROUND:
                toBeReturned = Currency.BUILT_PLAYGROUNDS;
                break;
            case QuestType.SEND_TO_EAT:
                toBeReturned = Currency.TIMES_SENT_TO_EAT;
                break;
            case QuestType.SEND_TO_BATH:
                toBeReturned = Currency.TIMES_SENT_TO_BATH;
                break;
            case QuestType.SEND_TO_PLAY:
                toBeReturned = Currency.TIMES_SENT_TO_PLAY;
                break;
            case QuestType.SEND_TO_VET:
                toBeReturned = Currency.TIMES_SENT_TO_VET;
                break;
            case QuestType.PERFORM_PRESTIGE:
                toBeReturned = Currency.PRESTIGES;
                break;
            case QuestType.FILL_STOCK:
                toBeReturned = Currency.REPLENISHED_STOCKS;
                break;
            case QuestType.EVOLVE_BUILDING:
                toBeReturned = Currency.EVOLVED_BUILDINGS;
                break;
            case QuestType.BUILD_NEW_BUILDING:
                toBeReturned = Currency.BUILT_BUILDINGS;
                break;
            case QuestType.SEND_TO_MISSION:
                toBeReturned = Currency.TIMES_SENT_TO_MISSION;
                break;
        }

        return toBeReturned;
    }

    public double getRewardAmount()
    {
        double multipliedReward = rewardAmount;

        if(reward == TutorialQuestRewards.GOLD)
        {
            multipliedReward = parent.parent.gameplayManager.applyCurrencyMultipliers(multipliedReward);
        }
        else if(reward == TutorialQuestRewards.INVENTORY_ITEM)
        {
            multipliedReward = parent.parent.gameplayManager.applyCurrencyMultipliers(multipliedReward);
            multipliedReward *= parent.parent.gameplayManager.inventoryItemsMultiplier;
        }
        else if(reward == TutorialQuestRewards.CREDIT)
        {
            multipliedReward *= parent.parent.gameplayManager.premiumCurrencyMultiplier;
        }

        return multipliedReward;
    }

#if UNITY_EDITOR
    private bool canShowRewardSimpleAmount()
    {
        return reward == TutorialQuestRewards.GOLD || 
            reward == TutorialQuestRewards.CREDIT ||
            reward == TutorialQuestRewards.INVENTORY_ITEM;
    }
#endif
}

public enum TutorialQuestRewards
{
    NONE = -1,
    //Don't add things above here

    GOLD,
    ALPHA,
    FEAT_INVENTORY,
    FEAT_INVENTORY_SLOT,
    INVENTORY_ITEM,
    FEAT_MARKET,
    CREDIT,

    //Don't add things below here
    COUNT
}