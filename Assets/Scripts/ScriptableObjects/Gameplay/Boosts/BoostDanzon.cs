﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostDanzon : IBoost
{
    public void init() { }

    public void call(GameplayController controller, float dt)
    {
        foreach (Alpha alpha in controller.gameplayManager.alphas)
        {
            if (alpha.isHired)
            {
                alpha.pack.mood = PackMood.HAPPY;
            }
        }
    }
}
