﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Boosts/Boost Library")]
public class BoostLibrary : Library, IStorable
{
    private GameplayController context;
    public bool isLocked;
    public int selectedBoost;
    public Boost selected { get { return (Boost)(selectedBoost == -1 ? null : items[selectedBoost]); } }

    public Boost this[Boosts key]
    {
        get
        {
            return (Boost)items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }

    public Boost findByID(string id)
    {
        return (Boost)items.Find(e => e.name == id);
    }

    public void init(GameplayController controller)
    {
        isLocked = true;
        context = controller;
        selectedBoost = -1;

        foreach (Boost item in items)
        {
            item.init(context);
        }
    }

    internal void reset()
    {
        foreach (Boost item in items)
        {
            item.reset();
        }

    }

    public void update(float dt)
    {
        foreach(Boost boost in items)
        {
            boost.update(dt);
        }
    }

    public void load(JSONObject jsonFile)
    {
        isLocked = jsonFile.GetBoolean("locked");

        foreach (Boost boost in this)
        {
            boost.load(jsonFile.GetObject(boost.name));
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("locked", isLocked);
        
        foreach (Boost boost in this)
        {
            obj.Add(boost.name, boost.save());
        }

        return obj;
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(BoostLibrary)), CanEditMultipleObjects]
    public class BoostLibraryEditor : LibraryEditor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
#endif
}
