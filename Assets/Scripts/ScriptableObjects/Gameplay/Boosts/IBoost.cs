﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBoost
{
    void init();
    void call(GameplayController controller, float dt);
}
