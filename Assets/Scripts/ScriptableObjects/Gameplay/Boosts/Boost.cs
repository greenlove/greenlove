﻿using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Boosts/new boost")]
public class Boost : ScriptableObject, IStorable
{
    public int maxStock;
    public int remainingStock;
    public bool isActive;
    public float boostTimeInSeconds;

    [ReadOnly]
    public float boostRemainingTime;

    private string callerName;
    IBoost boostOrchestrator;

    private GameplayController context;

    public void init(GameplayController controller)
    {
        context = controller;

        reset();

        callerName = string.Format("Boost{0}", name);
        var objectType = Type.GetType(callerName);

        boostOrchestrator = Activator.CreateInstance(objectType) as IBoost;
        boostOrchestrator.init();
    }

    public void reset()
    {
        boostRemainingTime = 0;
        remainingStock = maxStock;
    }

    public void update(float dt)
    {
        if(isActive)
        {
            boostRemainingTime -= dt;
            isActive = boostRemainingTime > 0;

            boostOrchestrator.call(context, dt);
        }
    }

    public bool purchase()
    {
        bool purchaseable = canPurchase();
        if (purchaseable)
        {
            remainingStock--;
            isActive = true;
            boostRemainingTime += boostTimeInSeconds;
        }

        return purchaseable;
    }

    public bool canPurchase()
    {
        return remainingStock > 0;
    }

    public string getFormattedName(LocalizationController loc)
    {
        return loc.getTextByKey(string.Format("BOOST_{0}_NAME", name.ToUpper()));
    }

    public string getFormattedDescription(LocalizationController loc)
    {
        string localized = loc.getTextByKey(string.Format("BOOST_{0}_DESC", name.ToUpper()));

        return string.Format(localized, (boostTimeInSeconds / 60.0f).ToString("0.#"));
    }

    public void load(JSONObject jsonFile)
    {
        remainingStock = (int)jsonFile.GetNumber("stock");
        isActive = jsonFile.GetBoolean("active");
        boostRemainingTime = (float)jsonFile.GetNumber("time");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("stock", remainingStock);
        obj.Add("active", isActive);
        obj.Add("time", boostRemainingTime);

        return obj;
    }
}
