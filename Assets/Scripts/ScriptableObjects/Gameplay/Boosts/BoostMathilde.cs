﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostMathilde : IBoost
{
    private float tick = 0.1f;
    private float buffer;

    public void init()
    {
        buffer = 0;
    }

    public void call(GameplayController controller, float dt)
    {
        buffer += dt;
        if (buffer >= tick)
        {
            Home home = controller.gameplayManager.city.findRandomHomeWithSpace();

            if (home == null)
            {
                return;
            }

            controller.summonAnimal(true);
            buffer = 0;
        }
    }
}
