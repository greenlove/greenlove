﻿using BuildingBlocks;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[Serializable]
public class FlockManager
{
    private List<string> invocables;

    private Pool<FlockMember> pool;
    private List<GameObject> carPool;

    private System.Random rand;

    public float soundDelay;
    private float soundDelayAccumulator;

    public float maxPetsPerSecond;
    [ReadOnly] public float currentPetsPerSecond;

    [Range(0.1f, 1.0f)]
    public float flockAgentRadius = 0.2f;
    [Range(1.0f, 720.0f)]
    public float flockAgentAngularSpeed = 60f;
    [Range(0.1f, 10.0f)]
    public float flockAgentSpeed = 2f;
    public float flockSpeedMultiplier;
    [Range(0.1f, 10.0f)]
    public float flockAgentAcceleration = 5f;

    public float dismissDistance;
    public float maxFlockMemberAliveTime;

    private int bulkSpawnCounter;
    private Queue<AlphaMission> bulkSpawnQueue;

    private NavMeshSurface surface;
    private GameplayController controller;

    public float carBuffer;

    public GameObject[] animals;
    public GameObject[] cars;

    public void init(GameplayController controller = null)
    {
        currentPetsPerSecond = 0.0f;

        pool = new Pool<FlockMember>();
        carPool = new List<GameObject>();

        rand = new System.Random();
        //Debug.Log("rand is null? " + rand == null);

        this.controller = controller;

        soundDelayAccumulator = 0;
        flockSpeedMultiplier = 0;
        bulkSpawnCounter = 0;
        bulkSpawnQueue = new Queue<AlphaMission>();

        surface = GameObject.Find("Floor").GetComponent<NavMeshSurface>();
        buildSurface();

    }


    public void buildSurface()
    {
        if(surface != null) surface.BuildNavMesh();
    }

    public GameObject summonAnimal(Vector3 origin, AlphaMission mission)
    {
        GameObject summoned;
        FlockMember member;
        
        //If pool is empty
        if (pool.peek() == null)
        {   
            //Create a dog
            summoned = (GameObject)GameObject.Instantiate(animals[UnityEngine.Random.Range(0, animals.Length)]);

            //disable it
            summoned.SetActive(false);
            
            //Push it to the pool;
            pool.push(new FlockMember(summoned, mission)); 
        }
        
        member = pool.pop();
        member.gameObject.GetComponent<BoxCollider>().enabled = true;
        member.mission = mission;

        summoned = member.gameObject;
        summoned.SetActive(true);

        NavMeshAgent agent = summoned.GetComponent<NavMeshAgent>();
        if (agent == null)
        {
            agent = summoned.AddComponent<NavMeshAgent>();
            agent.agentTypeID = 0;

            agent.radius = flockAgentRadius;
            agent.angularSpeed = flockAgentAngularSpeed;
            agent.speed = flockAgentSpeed;
            agent.acceleration = flockAgentAcceleration;
            agent.height = .3f;
        }

        agent.Warp(new Vector3(origin.x, surface.transform.position.y, origin.z));
        agent.ResetPath();
        agent.enabled = false;

        return summoned;
    }

    public void bulkSummonAndSendToTarget(Vector3 summonPoint, Vector3 target, AlphaMission mission)
    {
        if (bulkSpawnCounter > 0)
        {
            bulkSpawnQueue.Enqueue(mission);
        }
        else
        {
            float bulkSpawnDelay = 0.005f;
            MonoBehaviour monoBehaviour = controller.getRoot().GetComponent<MonoBehaviour>();

            //Find if there are agents alive with this exact mission
            List<FlockMember> list = pool.actives.FindAll(element => element.mission.isSameMission(mission));

            foreach (FlockMember item in list) {
                item.gameObject.GetComponent<NavMeshAgent>().SetDestination(target);
            }

            for (int i = 0; i < Math.Max(0, mission.totalRepresented - list.Count); i++) {
                bulkSpawnCounter++;
                GameObject summoned = summonAnimal(summonPoint, mission);
                summoned.SetActive(false);
                monoBehaviour.StartCoroutine(CreateSummon(i * bulkSpawnDelay, summoned, target, mission));
            }
        }
    }

    private IEnumerator CreateSummon(float waitTime, GameObject summoned, Vector3 target, AlphaMission mission) {
        yield return new WaitForSeconds(waitTime);

        summoned.SetActive(true);
        NavMeshAgent agent = summoned.GetComponent<NavMeshAgent>();

        agent.enabled = true;
        agent.SetDestination(target);

        bulkSpawnCounter--;

        if (bulkSpawnCounter <= 0 && bulkSpawnQueue.Count > 0)
        {
            AlphaMission nextMission = bulkSpawnQueue.Dequeue();
            bulkSummonAndSendToTarget(nextMission.origin, nextMission.destination, nextMission);
        }
    }

    public void pauseAll()
    {
        for (int i = 0; i < pool.actives.Count && pool.actives[i] != null; i++)
        {
            if (pool.actives[i].mission.isPaused)
            {
                NavMeshAgent agent = pool.actives[i].gameObject.GetComponent<NavMeshAgent>();
                agent.Warp(agent.pathEndPosition);
            }
        }
    }

    public void checkRemoveFlockMember(float dt)
    {
        NavMeshAgent agent;

        for (int i = 0; i < pool.actives.Count && pool.actives[i] != null; i++)
        {
            if (!pool.actives[i].mission.isPaused)
            {
                FlockMember member = pool.actives[i];

                member.aliveTime += dt;
                agent = member.gameObject.GetComponent<NavMeshAgent>();
                float remainingDistance = member.mission.distanceToDestination(member.gameObject);

                if ((agent.hasPath && remainingDistance < dismissDistance) || pool.actives[i].aliveTime >= maxFlockMemberAliveTime)
                {
                    controller.notifyAnimalArrived(member.mission);
                    member.gameObject.SetActive(false);
                    member.aliveTime = 0;

                    pool.actives.Remove(member);
                    pool.push(member);
                    i--;
                }

                if (agent.hasPath && remainingDistance < dismissDistance * 1.5f)
                {
                    member.gameObject.GetComponent<BoxCollider>().enabled = false;
                }
            }
        }


        //Cars
        for (int i = 0; i < carPool.Count && carPool[i] != null; i++)
        {
            agent = carPool[i].gameObject.GetComponent<NavMeshAgent>();

            if ((agent.hasPath && agent.remainingDistance < dismissDistance))
            {
                GameObject.Destroy(carPool[i]);
                i--;
            }
        }
        carPool.RemoveAll(element => element == null);
    }


    public void playDogSounds(float dt)
    {
        if (pool == null) return;

        soundDelayAccumulator += dt;
        if(soundDelayAccumulator >= soundDelay && pool.actives.Count > 0)
        {
            controller.findController<AudioController>().play3DSound("pitch_bark", getFlockAveragePosition(pool.actives), 0.85f);
            soundDelayAccumulator = 0;
        }       
    }

    private Vector3 getFlockAveragePosition(List<FlockMember> flock)
    {
        float x = 0f, y = 0f, z = 0f;

        foreach (FlockMember member in flock)
        {
            x += member.gameObject.transform.position.x;
            y += member.gameObject.transform.position.x;
            z += member.gameObject.transform.position.x;
        }
        return new Vector3(x / flock.Count, y / flock.Count, z / flock.Count);
    }

    public void summonCar()
    {
        if (controller.gameplayManager.gameMode == GameMode.NORMAL)
        {
            GameObject car = (GameObject)GameObject.Instantiate(cars[UnityEngine.Random.Range(0, cars.Length)]);
            carPool.Add(car);

            float randValue = UnityEngine.Random.value;


            var agent = car.GetComponent<NavMeshAgent>();

            if (agent == null)
            {
                agent = car.AddComponent<NavMeshAgent>();
                agent.agentTypeID = 0; //alien hominid Humanoid
                agent.baseOffset = 0f;
                agent.speed = 0.75f;
                agent.radius = 0.19f;
                agent.height = .4f;
                BoxCollider boxCollider = car.AddComponent<BoxCollider>();
                MeshRenderer renderer = car.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>();
                boxCollider.center = renderer.bounds.center;
                boxCollider.size = renderer.bounds.size;
            }

            agent.Warp(controller.gameplayManager.city[randValue < 0.5f ? CityGroup.CAR_SPAWNER_1 : CityGroup.CAR_SPAWNER_0].position);

            agent.SetDestination(controller.gameplayManager.city[randValue < 0.5f ? CityGroup.CAR_SPAWNER_0 : CityGroup.CAR_SPAWNER_1].position);
        }
    }

    public void resetPrestige()
    {
        pool.actives.ForEach((FlockMember member) => { GameObject.Destroy(member.gameObject); });

        FlockMember inactive = null;

        while ((inactive = pool.pop()) != null)
        {
            GameObject.Destroy(inactive.gameObject);
        }

        pool.clear();
    }

    public void update(float dt)
    {
        currentPetsPerSecond = pool.actives.Count * dt * maxPetsPerSecond;
    }

    public bool arePetsPerSecondOverLimit()
    {
        return currentPetsPerSecond > maxPetsPerSecond;
    }
}


public enum FlockState
{
    ERROR = -1,
    NONE = 0,

    //Start adding below the last one here
    HOME_IDLING,
    HOME_WITH_GIFT,

    RECRUITED,
    SCOUTING,
    FEEDING,
    SLEEPING,
    SHOWER,
    PLAYING,
    HEALING,

    //Don't add anything from here
    COUNT
}