﻿using Boomlagoon.JSON;
using BuildingBlocks;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{
    [Serializable]
    public class GameplayManager : IStorable
    {
        public GameplayController parent;

        [Header("City")]
        [SerializeField]
        public City city;
        public BuildingSlotsLibrary buildingSlots;


        [Header("Animals")]
        public Library alphas;
        public Alpha selectedAlpha;
        public PackMood maxAvailableMood;

        public AlphaSlots alphaSlots;
        System.Random animalArrivedAssignatorRNG;

        private Pool<AlphaMission> missionPool;

        [Header("Cash")]
        public Bank bank;
        public float goldChanceOnRecruiting;
        public float baseGoldPerSecond;
        [ReadOnly] public double currentGoldPerSecond;
        private System.Random goldRecruitRNG;


        [Header("Inventory")]
        public Inventory inventory;
        public float premiumCurrencyChance;
        public float premiumCurrencyMultiplier;
        public float[] itemChances;
        private System.Random itemChanceRNG;
        private Dictionary<string, double> rewardsBuffer;


        [Header("Buffs")]
        public BuffsLibrary buffs;
        public float inventoryItemsMultiplier;
        public float goldOnRecruitMultiplier;
        public float offlineEarningsMultiplier;


        [Header("Boosts")]
        public BoostLibrary boosts;


        [Header("Breeds")] //This is quite legacy code, Candidate for deletion but I might as well keep it for now
        public List<Breed> unlockedBreeds;
        public BreedDoubleDictionary breedPrices;
        public Breed selectedBreed;


        [Header("Offers")]
        public int amountOfOfferedItems;
        public float offerMaxTimeInHours;
        public float bundleMaxTimeInHours;
        [HideInInspector] public InventoryItemOffer currentOffer;
        [HideInInspector] public BuffBundleOffer bundleOffer;

        [Header("Balloons")]
        public float balloonSummoningInterval;
        [ReadOnly]public float balloonSummonBuffer;
        public float balloonAdChance;
        public float balloonAdMultiplier;
        public float balloonCreditsChance;
        public float balloonItemChance;

        [Header("Game Mode")]
        public GameMode gameMode;


        private Pool<GameObject> currencyParticles;
        private Pool<GameObject> textParticles;
        private Stack<string> particlesDataStack;
        
        public void init(GameplayController controller = null)
        {
            if(controller != null) parent = controller;

            //Alphas
            for (int i = 0; i < alphas.Count; i++)((Alpha)alphas[i]).init(controller);
            selectedAlpha = null;
            maxAvailableMood = PackMood.SLEEPY; //For now alphas can only feel tired
            alphaSlots.init();

            //Bank
            bank.init();
            goldRecruitRNG = new System.Random();

            //Buffs
            buffs.init();

            //Boosts
            boosts.init(controller);

            //Inventory
            inventory.init(this);
            itemChanceRNG = new System.Random();

            //Init the city collection
            city.init(parent);

            //Init building slots
            buildingSlots.init(this);

            //Init the particles pool
            currencyParticles = new Pool<GameObject>();
            textParticles = new Pool<GameObject>();
            particlesDataStack = new Stack<string>();


            selectedBreed = Breed.NONE;

            rewardsBuffer = new Dictionary<string, double>();

            missionPool = new Pool<AlphaMission>();

            gameMode = GameMode.NORMAL;

            inventoryItemsMultiplier = 1;
            goldOnRecruitMultiplier = 1;
            premiumCurrencyMultiplier = 1;

            currentOffer = new InventoryItemOffer(this);
            currentOffer.init();

            bundleOffer = new BuffBundleOffer();
            bundleOffer.init(this);

            balloonSummonBuffer = 60; //Start with a headstart
        }


        public Vector3 getAnimalOrigin()
        {
            return city[(CityGroup)new System.Random().Next((int)CityGroup.SPAWNER_IN_0, (int)CityGroup.CAR_SPAWNER_0)].position;
        }

        public Vector3 getBalloonOrigin()
        {
            return city[(CityGroup)new System.Random().Next((int)CityGroup.SPAWNER_OUT_0, (int)CityGroup.BALLOON - 1)].position;
        }


        public void notifyAnimalRecruited(AlphaMission mission)
        {
            Alpha newLead = ((Alpha)alphas[(int)mission.lead]);

            if (mission.what == FlockState.RECRUITED && newLead.pack.amount >= city.findHomeByAlpha(newLead).getActiveSegment().capacity) return;
            
            //Update the global stat
            bank.update(Currency.DOGS, mission.amount, BankUpdateStrategy.ADD);

            //Add the alphas to any assigned slot
            newLead.pack.free += mission.amount;

            //Add recruiting gold if available
            double goldYay = goldRecruitRNG.NextDouble();
            if (goldYay <= goldChanceOnRecruiting)
            {
                double gold = applyCurrencyMultipliers(1 * goldOnRecruitMultiplier);

                bank.update(Currency.GOLD, gold, BankUpdateStrategy.ADD);

                Home home = city.findHomeByAlpha(newLead);
                stackParticles(home.position + Vector3.up * home.getActiveSegment().buildingEvo.GetComponent<MeshFilter>().sharedMesh.bounds.max.y, "gold_coin", gold);
            }
        }

        public void hireAlpha(AlphaSlot slot, bool forceHire = false)
        {
            slot.alpha.slotIndex = slot.id;
            slot.alpha.state = AlphaState.ASSIGNED;
            alphaSlots.assign(slot.id);
            slot.state = AlphaListViewState.ASSIGNED;

            AlphaSlot next = alphaSlots.getNextFrom(slot);
            if(next != null) next.state = AlphaListViewState.EMPTY;

            //You yourself
            slot.alpha.pack.free += 1;
            if (!forceHire) bank.update(Currency.DOGS, 1, BankUpdateStrategy.ADD);

            //Update the currency
            if(!forceHire) bank.update(Currency.HIRED_ALPHAS, 1, BankUpdateStrategy.ADD);

            slot.alpha.requestRender();
        }

        public string[] pickGift(int id)
        {
            return calculateGift(alphaSlots[id].alpha);
        }

        public string[] pickGift(Alpha a)
        {
            return calculateGift(a);
        }

        private string[] calculateGift(Alpha alpha)
        {
            int gifts = alpha.gifts;
            float alphaLevelMultiplier = 1 + Mathf.Log10(alpha.level);
            for (int j = 0; j < gifts; j++)
            {
                double packSize = alpha.giftsTracker[j];
                //Get the amount of gifts on this round(0 to 5)
                int attainedItems = getAmountOfUniqueGifts(bank[Currency.GIFTS_PICKED].value > 0);

                Dictionary<string, double> tempDict = inventory.stackItems(attainedItems, packSize);

                foreach (KeyValuePair<string, double> item in tempDict)
                {
                    double value = item.Value;
                    value = applyCurrencyMultipliers(item.Value) * inventoryItemsMultiplier;

                    if (rewardsBuffer.ContainsKey(item.Key))
                    {
                        rewardsBuffer[item.Key] += value;
                    }
                    else
                    {
                        rewardsBuffer.Add(item.Key, value);
                    }
                }
            }
            alpha.giftsTracker.Clear();

            //bank
            bank.update(Currency.GIFTS_PICKED, 1, BankUpdateStrategy.ADD);
            bank.update(Currency.INVENTORY_ITEMS_OBTAINED, rewardsBuffer.ContainsKey("total") && rewardsBuffer["total"] > 0 ? rewardsBuffer["total"] : 1, BankUpdateStrategy.ADD);

            //Premium currency, we give if there's at least one gift and the odds are good
            if (itemChanceRNG.NextDouble() > premiumCurrencyChance && rewardsBuffer.Count > 2)
            {
                double premium =  1 + (premiumCurrencyMultiplier * gifts * alphaLevelMultiplier);
                
                if (!rewardsBuffer.ContainsKey("premium"))
                {
                    rewardsBuffer.Add("premium", premium);
                }
                else
                {
                    rewardsBuffer["premium"] += premium;
                }
                parent.findController<ServerController>().sendAnalyticWithParams("GainHC", "Where".pair("AlphaGift"), "Amount".pair(rewardsBuffer["premium"].ToString()));
            }

            //parsing
            string[] returnable = new string[rewardsBuffer.Count];
            int i = 0;
            foreach (KeyValuePair<string, double> item in rewardsBuffer)
            {
                returnable[i] = string.Format("{0}#{1:0}", item.Key, bank.toFormattedCurrency(item.Value));
                i++;
            }

            return returnable;
        }


        private int getAmountOfUniqueGifts(bool hasGotGiftsYet)
        {
            //The first time we always deliver gift
            if (!hasGotGiftsYet) return 0;

            int uniqueGifts = 0;
            foreach (float chance in itemChances)
            {
                double itemChance = itemChanceRNG.NextDouble();
                if (itemChance > chance) break;

                uniqueGifts++;
            }

            return uniqueGifts;
        }


        public void claimDisplayedGifts()
        {
            UnityEngine.Object[] its = inventory.items.ToArray();
            foreach (KeyValuePair<string, double> item in rewardsBuffer)
            {
                InventoryItem found = (InventoryItem)Array.Find(its, element => element.name == item.Key);

                if(found != null)
                {
                    found.add(item.Value);
                }
                else
                {
                    if (item.Key == "gold")
                    {
                        bank.update(Currency.GOLD, item.Value, BankUpdateStrategy.ADD);
                    }
                    else if(item.Key == "premium")
                    {
                        bank.update(Currency.CREDIT, item.Value, BankUpdateStrategy.ADD);
                    }
                }
            }

            rewardsBuffer.Clear();
        }


        public void stackParticles(Vector3 position, string iconName, double value, float duration = 0.0f)
        {
            particlesDataStack.Push(string.Format("{0}#{1}#{2}#{3}", position.ToString(), iconName, value.ToString(), duration));
        }


        public void stackTextParticles(Vector3 position, string s)
        {
            particlesDataStack.Push(string.Format("{0}#{1}", position.ToString(), s));
        }


        public void launchAllParticles()
        {
            for (int i = 0; i < particlesDataStack.Count; i++)
            {
                string str = particlesDataStack.Pop();
                
                string[] s = str.Split('#');

                //Vector
                string[] v = s[0].Replace("(", "").Replace(")", "").Split(',');
                Vector3 vector = new Vector3(float.Parse(v[0]), float.Parse(v[1]), float.Parse(v[2]));

                if(s.Length > 2)
                {
                    //Value
                    double value = double.Parse(s[2]);

                    launchParticles(vector, s[1], value, float.Parse(s[3]));
                }
                else
                {
                    string msg = s[1];
                    launchParticles(vector, msg);
                }

            }
        }


        public void launchParticles(Vector3 position, string txt, float duration = 0.0f)
        {
            if (textParticles.peek() == null)
            {
                GameObject cp = (GameObject)GameObject.Instantiate(Resources.Load("Buildings/TextParticle"));
                textParticles.push(cp);
            }
            GameObject particle = textParticles.pop();
            particle.SetActive(true);
            

            particle.transform.position = new Vector3(position.x, position.y, position.z);

            TextMesh text = particle.transform.Find("text").GetComponent<TextMesh>();
            text.text = txt;
            text.color = Color.white;

            float particleLife = duration <= 0.0f? 5f : duration;

            Sequence sequence = DOTween.Sequence();
            sequence.Append(particle.transform.DOMoveY(position.y + 1.5f, particleLife));
            sequence.Insert(particleLife * 0.75f, DOTween.To(() => text.color, x => text.color = x, Color.clear, particleLife * 0.1f));

            sequence.SetEase(Ease.OutBounce);
            sequence.AppendCallback(() =>
            {
                textParticles.push(particle);
                particle.SetActive(false);
            });

        }


        public void launchParticles(Vector3 position, string iconName, double amount, float duration = 0.0f)
        {
            if(currencyParticles.peek() == null)
            {
                GameObject cp = (GameObject)GameObject.Instantiate(Resources.Load("Particles/CurrencyParticle"));
                currencyParticles.push(cp);
            }

            GameObject particle = currencyParticles.pop();
            particle.SetActive(true);

            float zOffset = UnityEngine.Random.Range(-0.5f, 0.5f);
            particle.transform.position = new Vector3(position.x, position.y, position.z + zOffset);

            TextMesh text = particle.transform.Find("text").GetComponent<TextMesh>();
            text.text = bank.toFormattedCurrency(amount);
            text.color = Color.black;

            SpriteRenderer icon = particle.transform.Find("icon").GetComponent<SpriteRenderer>();

            UIController ui = parent.findController<UIController>();
            icon.sprite = ui.atlases["others"].GetSprite(iconName);
            icon.color = Color.white;

            float particleLife = duration <= 0.0f ? 2.5f : duration;

            Sequence sequence = DOTween.Sequence();
            sequence.Append(particle.transform.DOMoveY(position.y + 1.5f, particleLife));
            sequence.Insert(particleLife * 0.75f, DOTween.To(() => text.color, x=> text.color = x, Color.clear, particleLife * 0.1f));
            sequence.Insert(particleLife * 0.75f, DOTween.To(() => icon.color, x => icon.color = x, new Color(1, 1, 1, 0), particleLife * 0.1f));

            sequence.SetEase(Ease.OutBounce);
            sequence.AppendCallback(() =>
            {
                currencyParticles.push(particle);
                particle.SetActive(false);
            });
        }

        public void displayWarningParticleOverBuilding(Building building)
        {
            if (building.warningParticle != null) return;

            GameObject particle = (GameObject)GameObject.Instantiate(Resources.Load("Particles/WarningParticle"));
            Vector3 maxBounds = building.getActiveSegment().buildingEvo.GetComponent<MeshFilter>().sharedMesh.bounds.max;

            particle.transform.position = new Vector3(building.position.x, building.position.y + maxBounds.y + 0.5f, building.position.z);

            Sequence sequence = DOTween.Sequence();
            sequence.Append(particle.transform.DOMoveY(particle.transform.position.y + 0.5f, 1f));
            sequence.SetLoops(-1, LoopType.Yoyo);
            sequence.Play();

            building.warningParticle = particle;
        }


        public void selectBreed(Breed breed)
        {
            selectedBreed = breed;
            parent.tutorialManager.launchTutorial(Tutorials.RECRUIT_ANIMALS_TUTORIAL);

            bank.update(Currency.PICKED_BREEDS, 1, BankUpdateStrategy.ADD);
            bank.update(breed == Breed.CATS ? Currency.PICKED_BREED_CAT : Currency.PICKED_BREED_DOG, 1, BankUpdateStrategy.ADD);
        }
        

        public AlphaMission createNewMission()
        {
            if(missionPool.peek() == null)
            {
                missionPool.push(new AlphaMission());
            }

            AlphaMission mission = missionPool.pop();
            return mission;
        }


        public void pushBackMission(AlphaMission mission)
        {
            missionPool.push(mission);
        }


        public void generateFlashOffer()
        {
            currentOffer.reset();
            List<InventoryItem> newOffer = inventory.createRandomOffer(amountOfOfferedItems);
            newOffer.ForEach((InventoryItem item) => { currentOffer.addItemToOffer(item); });

            currentOffer.amount = inventory.getAmountOfUniqueGift(Mathf.Max((float)bank[Currency.DOGS].value, 5000), newOffer[0], true);
            currentOffer.price = 60;

            currentOffer.remainingTime = offerMaxTimeInHours * 60 * 60;
        }

        public void generateBuffBundleOffer()
        {
            bundleOffer.offeredBuff = (Buff) buffs[UnityEngine.Random.Range(0, (int)(buffs.Count * 0.6f))];
            bundleOffer.bake();

            bundleOffer.remainingTime = bundleMaxTimeInHours * 60 * 60;
        }

        public bool purchaseShopOffer(InventoryItemOffer inventoryItemOffer, int index)
        {
            bool result = bank.canAfford(Currency.CREDIT, inventoryItemOffer.amount);
            if (result)
            {
                inventoryItemOffer.purchase(index);
                bank.update(Currency.CREDIT, inventoryItemOffer.price, BankUpdateStrategy.SUB);
            }

            return result;
        }

        public bool purchaseBundleOffer(BuffBundleOffer offer)
        {
            bool result = bank.canAfford(Currency.CREDIT, offer.price);
            if (result)
            {
                offer.isPurchased = true;
                bank.update(Currency.CREDIT, offer.price, BankUpdateStrategy.SUB);

                foreach(InventoryItem item in offer.offeredBuff.recipeComponents)
                {
                    item.add(offer.amount);
                }
            }

            return result;
        }

        internal void tapCar(GameObject obj)
        {
            if (obj == null) return;

            bank.update(Currency.AD_VEHICLES_TOUCHED, 1, BankUpdateStrategy.ADD);
            GameObject shadow = obj.transform.GetChild(0).transform.Find("Shadow").gameObject;
            Sequence sequence = DOTween.Sequence();

            sequence.Append(obj.transform.DOMoveY(obj.transform.position.y + 0.75f, 0.2f));
            if (shadow != null) sequence.Join(shadow.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.2f));
            sequence.SetLoops(2, LoopType.Yoyo);
            sequence.AppendCallback(() => 
            {
                if (parent.tutorialManager.hasAllBeenDisplayed(Tutorials.SCOUTING_TUTORIAL))
                {
                    double gold = currentGoldPerSecond * UnityEngine.Random.Range(1, 2f);
                    launchParticles(obj.transform.position, "gold_coin", gold);
                    bank.update(Currency.GOLD, gold, BankUpdateStrategy.ADD);
                }
            });

            sequence.OnComplete(() => { shadow.GetComponent<SpriteRenderer>().color = Color.white; });
            sequence.Play();

            parent.findController<AudioController>().play3DSound("car_claxon", obj.transform.position);
        }


        public double calculateOfflineEarningsGold(double secs)
        {
            //Limit is in one week
            float limit = Mathf.Min((float)secs, 24 * 7 * 60 * 60);
            limit = limit / 60.0f; //Let's use minutes for the calculation, using seconds is freaking big

            return applyCurrencyMultipliers(currentGoldPerSecond) * limit * offlineEarningsMultiplier;
        }

        public void load(JSONObject jsonFile)
        {
            gameMode = jsonFile.GetEnum<GameMode>("mode");

            city.load(jsonFile.GetObject("city"));
            buildingSlots.load(jsonFile.GetObject("buildingSlots"));
            loadAlphas(jsonFile.GetObject("alphas"));
            alphaSlots.load(jsonFile.GetObject("slots"));
            bank.load(jsonFile.GetObject("bank"));
            inventory.load(jsonFile.GetObject("inventory"));
            buffs.load(jsonFile.GetObject("buffs"));
            boosts.load(jsonFile.GetObject("boosts"));
            loadOffers(jsonFile.GetObject("offers"));
        }

        public JSONObject save()
        {
            JSONObject obj = new JSONObject();
            obj.Add("city", city.save());
            obj.Add("buildingSlots", buildingSlots.save());
            obj.Add("alphas", saveAlphas());
            obj.Add("slots", alphaSlots.save());
            obj.Add("bank", bank.save());
            obj.Add("inventory", inventory.save());
            obj.Add("buffs", buffs.save());
            obj.Add("boosts", boosts.save());
            obj.Add("offers", saveOffers());

            obj.Add("mode", gameMode.ToString());

            return obj;
        }

        private void loadAlphas(JSONObject jsonFile)
        {
            foreach (Alpha alpha in alphas)
            {
                alpha.load(jsonFile.GetObject(alpha.name));
            }
        }

        private JSONObject saveAlphas()
        {
            JSONObject obj = new JSONObject();
            foreach (Alpha alpha in alphas) obj.Add(alpha.name, alpha.save());

            return obj;
        }

        private void loadOffers(JSONObject jsonFile)
        {
            currentOffer = new InventoryItemOffer(this);
            currentOffer.init();
            currentOffer.load(jsonFile.GetObject("flash"));

            bundleOffer = new BuffBundleOffer();
            bundleOffer.init(this);
            bundleOffer.load(jsonFile.GetObject("bundle"));

        }

        private JSONObject saveOffers()
        {
            JSONObject obj = new JSONObject();

            obj.Add("flash", currentOffer.save());
            obj.Add("bundle", bundleOffer.save());

            return obj;
        }

        internal string getGoodOrBadMood(PackMood mood)
        {
            return mood == PackMood.HAPPY ? PackMood.HAPPY.ToString() : "SAD"; 
        }

        internal void launchBalloon()
        {
            Balloon model = (Balloon)city[CityGroup.BALLOON];
            if (model.obj != null) return;

            bank.update(Currency.AD_BALLOONS_DEPLOYED, 1, BankUpdateStrategy.ADD);

            GameObject balloon = GameObject.Instantiate(model.getActiveSegment().buildingEvo);
            balloon.name = model.name;

            Vector3 origin = getBalloonOrigin();
            balloon.transform.position = new Vector3(origin.x, balloon.transform.position.y, origin.z);

            model.startSteering(balloon);
        }

        public KeyValuePair<TutorialQuestRewards, DoubleVariable> getRewardBalloonByChance(float multiplier)
        {
            float rewardChance = UnityEngine.Random.Range(0.0f, 1.0f);
            KeyValuePair<TutorialQuestRewards, DoubleVariable> pair;
            DoubleVariable amount = DoubleVariable.CreateInstance<DoubleVariable>();
            if (bank[Currency.AD_BALLOONS_TOUCHED].value <= 1) rewardChance = 0; // Force credits


            if (rewardChance <= balloonCreditsChance)
            {
                amount.value = 3 * multiplier;
                amount.value *= premiumCurrencyMultiplier;
                amount.name = "credit";
                pair = new KeyValuePair<TutorialQuestRewards, DoubleVariable>(TutorialQuestRewards.CREDIT, amount);
            }
            else if (rewardChance <= balloonItemChance)
            {
                amount.value = applyCurrencyMultipliers(5 * multiplier) * inventoryItemsMultiplier;
                amount.name = inventory.getRandomItemByUnlockedSlots(new List<InventoryItem>()).name;
                pair = new KeyValuePair<TutorialQuestRewards, DoubleVariable>(TutorialQuestRewards.INVENTORY_ITEM, amount);
            }
            else
            {
                amount.value = applyCurrencyMultipliers(currentGoldPerSecond * UnityEngine.Random.Range(10, 15) * multiplier);
                amount.name = "gold";
                pair = new KeyValuePair<TutorialQuestRewards, DoubleVariable>(TutorialQuestRewards.GOLD, amount);
            }

            return pair;
        }

        public void claimBalloonReward(KeyValuePair<TutorialQuestRewards, DoubleVariable> pair)
        {
            switch (pair.Key)
            {
                case TutorialQuestRewards.GOLD:
                    bank.update(Currency.GOLD, pair.Value.value, BankUpdateStrategy.ADD);
                    break;
                case TutorialQuestRewards.INVENTORY_ITEM:
                    ((InventoryItem)inventory.items.Find(e => e.name == pair.Value.name)).add(pair.Value.value);
                    break;
                case TutorialQuestRewards.CREDIT:
                    bank.update(Currency.CREDIT, pair.Value.value, BankUpdateStrategy.ADD);
                    parent.findController<ServerController>().sendAnalyticWithParams("GainHC", "Where".pair("Balloon"), "Amount".pair(pair.Value.value.ToString()));
                    break;
            }
        }

        internal void sendBalloonAway()
        {
            Balloon balloon = (Balloon)city[CityGroup.BALLOON];
            balloon.hasClaimedGift = true;
            balloon.steer.maxVelocity = 0.05f;
            balloon.resumeSteering();

            parent.findController<AudioController>().playSound("balloon");
        }

        public void launchMarket()
        {
            Market market = (Market)city[CityGroup.MARKET];
            market.deploy();

            market.onBuilt(null, true);
        }

        public PackMood findMoodByBuilding(CityGroup building)
        {
            PackMood mood = PackMood.NONE;

            switch (building)
            {
                case CityGroup.HOME00:
                case CityGroup.HOME01:
                case CityGroup.HOME02:
                case CityGroup.HOME03:
                    mood = PackMood.SLEEPY;
                    break;
                case CityGroup.TAVERN:
                    mood = PackMood.HUNGRY;
                    break;
                case CityGroup.BATH_HOUSE:
                    mood = PackMood.DIRTY;
                    break;
                case CityGroup.PLAYGROUND:
                    mood = PackMood.BORED;
                    break;
                case CityGroup.HOSPITAL:
                    mood = PackMood.ILL;
                    break;
            }

            return mood;
        }

        internal void updateGold(float dt)
        {
            double dogs = bank[Currency.DOGS].value;
            if (dogs <= 0) return;

            currentGoldPerSecond = baseGoldPerSecond;
            currentGoldPerSecond *= (float) Math.Max(1.0f, Math.Log(dogs * dogs));

            currentGoldPerSecond = applyCurrencyMultipliers(currentGoldPerSecond);

            bank.update(Currency.GOLD, currentGoldPerSecond * dt, BankUpdateStrategy.ADD);
        }

        public double applyCurrencyMultipliers(double amount)
        {
            if (boosts[Boosts.ARCHANGEL].isActive) amount *= 2;
            amount *= parent.prestigeManager.earningsMultiplier;

            return amount;
        }

        internal Alpha findAlphaWithMood(PackMood mood)
        {
            Alpha chosen = null;

            foreach (Alpha alpha in alphas)
            {
                if(alpha.pack.isOnMission && alpha.pack.mood == mood)
                {
                    chosen = alpha;
                    break;
                }
            }

            return chosen;
        }
    }
}

public enum Breed
{
    NONE = -1,

    //Add more breeds form here
    DOGS,
    CATS,

    //Stop adding breeds from here
    LENGTH
}

public enum GameMode
{
    NONE = -1,

    //Add more modes form here
    NORMAL,
    BUILDING,

    //Stop adding modes from here
    LENGTH
}