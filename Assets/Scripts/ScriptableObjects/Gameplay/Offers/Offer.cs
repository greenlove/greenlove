﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

public class Offer : IStorable
{
    public bool isPurchased;
    public double amount;
    public double price;

    public float remainingTime;

    public virtual void init()
    {
        reset();
    }

    public virtual void reset()
    {
        isPurchased = false;
        amount = 0;
        price = 0;
        remainingTime = 0;
    }

    public virtual void load(JSONObject jsonFile)
    {
        isPurchased = jsonFile.GetBoolean("purchased", false);
        amount = jsonFile.GetNumber("amount", 0);
        remainingTime = (float)jsonFile.GetNumber("time", 0);
    }

    public virtual JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("purchased", isPurchased);
        obj.Add("amount", amount);
        obj.Add("time", remainingTime);
        return obj;
    }

    public virtual void purchase()
    {
        isPurchased = true;
    }
}
