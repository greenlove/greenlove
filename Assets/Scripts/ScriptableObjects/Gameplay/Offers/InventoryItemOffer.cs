﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using Gameplay;
using UnityEngine;

public class InventoryItemOffer : Offer
{
    public List<InventoryItem> subject;
    public List<bool> purchased;
    private GameplayManager parent;

    public InventoryItemOffer init(GameplayManager controller)
    {
        base.init();
        parent = controller;

        return this;
    }

    public override void reset()
    {
        base.reset();
        subject = new List<InventoryItem>();
        purchased = new List<bool>();
    }

    public InventoryItemOffer(GameplayManager manager)
    {
        parent = manager;
    }

    public override void load(JSONObject jsonFile)
    {
        if (jsonFile == null) return;
        base.load(jsonFile);

        reset();

        JSONArray array = jsonFile.GetArray("subject", null);
        if (array == null) return;

        for (int i = 0; i < array.Length; i++)
        {
            JSONObject itemObj = array[i].Obj;

            InventoryItem item = (InventoryItem)parent.inventory.items.Find(e => e.name == itemObj.GetString("item"));
            addItemToOffer(item, itemObj.GetBoolean("purchased", false));
        }
    }

    public override JSONObject save()
    {
        JSONObject obj = base.save();

        JSONArray array = new JSONArray();
        for (int i = 0; i < subject.Count; i++)
        {
            JSONObject itemObj = new JSONObject();
            itemObj.Add("item", subject[i].name);
            itemObj.Add("purchased", purchased[i]);
            array.Add(itemObj);
        }

        obj.Add("subject", array);

        return obj;
    }

    public void addItemToOffer(InventoryItem item, bool isPurchased = false)
    {
        subject.Add(item);
        purchased.Add(isPurchased);
    }

    public void purchase(int index)
    {
        purchased[index] = true;
    }

    internal new bool isPurchased(int index)
    {
        return purchased[index];
    }
}
