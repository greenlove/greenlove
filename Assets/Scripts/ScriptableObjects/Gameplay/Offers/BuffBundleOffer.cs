﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using Gameplay;
using UnityEngine;

public class BuffBundleOffer : Offer
{
    public Buff offeredBuff;
    private GameplayManager context;

    public void init(GameplayManager parent)
    {
        base.init();
        context = parent;
    }

    public override JSONObject save()
    {
        if (offeredBuff == null) return notAvailableSave();

        JSONObject obj = base.save();
        obj.Add("buff", offeredBuff.name);

        return obj;
    }

    private JSONObject notAvailableSave()
    {
        JSONObject na = new JSONObject();
        na.Add("buff", "NA");

        return na;
    }

    public override void load(JSONObject jsonFile)
    {
        if (jsonFile == null || jsonFile.GetString("buff") == "NA") return;

        base.load(jsonFile);
        offeredBuff = (Buff)context.buffs.items.Find(e => e.name == jsonFile.GetString("buff", ""));
    }

    internal void bake()
    {
        reset();

        price = 15;
        amount = (offeredBuff.level + 1) * offeredBuff.recipeMultiplier * UnityEngine.Random.Range(10, 20);
    }
}
