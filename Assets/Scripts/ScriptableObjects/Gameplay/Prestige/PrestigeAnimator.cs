﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrestigeAnimator : MonoBehaviour
{
    public UIController ui;
    public ColorizableController colors;
    public GameplayController gameplay;
    public AudioController audioController;

    public void hideHud()
    {
        ui.hideHud();
    }

    public void showHud()
    {
        ui.showHud();
        UIItem prestigeHud = ui.hudArea.transform.Find("PrestigeButton").GetComponent<UIItem>();
        prestigeHud.realTimeSubscribe(ui);
    }

    public void changeColorMode()
    {
        colors.changeColorMode();
    }

    public void cleanCity()
    {
        gameplay.prestigeManager.prestige();
    }

    public void startPrestigeTrack()
    {
        audioController.prestige();
    }

    public void startNewCityTrack()
    {
        audioController.changeTrack();
    }

    public void prestigeStart()
    {
        GameObject.Find("PrestigeAnimUI").transform.SetAsLastSibling();
        gameplay.prestigeManager.isOnPrestigeCutscene = true;
    }

    public void prestigeEnd()
    {
        gameplay.prestigeManager.isOnPrestigeCutscene = false;
    }
}
