﻿using BuildingBlocks;
using Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System.Text.RegularExpressions;
using Sirenix.OdinInspector;
using BuildingBlocks.ProductsArchive;
using Boomlagoon.JSON;
using BuildingBlocks.Backend;
using Ads;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Gameplay Controller")]
public class GameplayController : Controller {
    
    public GameplayManager gameplayManager;

    [Space(20)]
    public FlockManager flockManager;

    [Space(20)]
    public PrestigeManager prestigeManager;

    [Space(20)]
    public TutorialManager tutorialManager;

    private double timeBuffer = 0;
    private double carBuffer = 0;
    private double offlineEarningsBuffer = 60;
    private int accumulatedPetsPerMission = 0;

    public override void init(Main parent)
    {
        base.init(parent);
        InitGameplay();
        isStopped = false;
    }

    public void InitGameplay()
    {
        gameplayManager.init(this);
        tutorialManager.init(this);
        prestigeManager.init(this);
        flockManager.init(this);

        findController<TouchController>().onReleased.AddListener((Dictionary<string, object> parameters) =>
        {
            touchReleasedReceived((GameObject)parameters["gameobject"], (Vector3)parameters["hitpoint"], (bool)parameters["is_holding"]);
        });

        findController<TouchController>().onTouched.AddListener((Dictionary<string, object> parameters) =>
        {
            touchBeginReceived((GameObject)parameters["gameobject"], (Vector3)parameters["hitpoint"]);
        });

        findController<ServerController>().onElapsedTimeCalculated.AddListener((double timeDiff) =>
        {
            Debug.Log("On Elapsed Time calculated: " + timeDiff);
            ReduceTimes(timeDiff);
        });
    }

    private void initialBuild()
    {
        buildInSlot(4, gameplayManager.city[CityGroup.HOME00], true);
        buildInSlot(1, gameplayManager.city[CityGroup.EXCAVATOR], true);
    }


    //Method only for recruiting animals
    public void summonAnimal(bool isAutomated)
    {
        Home home = gameplayManager.city.findRandomHomeWithSpace();

        if (home != null)
        {
            int amount = 1;

            if (flockManager.arePetsPerSecondOverLimit())
            {
                accumulatePetsForMission();
                return;
            }
            else
            {
                if(accumulatedPetsPerMission > 10)//Unload at 10
                {
                    amount += accumulatedPetsPerMission;
                    accumulatedPetsPerMission = 0;
                }
            }

            gameplayManager.bank.update(Currency.CALLED_ANIMALS, amount, BankUpdateStrategy.ADD);

            AlphaMission mission = gameplayManager.createNewMission();
            mission.init(amount, FlockState.RECRUITED, home.alphaSlot.alpha.alphaID);
            mission.setTotalRepresented(1);
            mission.origin = gameplayManager.getAnimalOrigin();
            mission.destination = home.homeDestination;

            flockManager.bulkSummonAndSendToTarget(mission.origin, home.homeDestination, mission);
        }
        else
        {
            if(!isAutomated) findController<UIController>().showSimpleFeedback(findController<LocalizationController>().getTextByKey("HOUSE_FULL_CAPACITY"));
        }
    }

    private void accumulatePetsForMission()
    {
        accumulatedPetsPerMission++;
    }

    public void requestNextTutorial()
    {
        tutorialManager.sendNextTutorial();
    }


    public void sendNextTutorial(ConversationItem next, int index)
    {
        findController<UIController>().showNextTutorial(next, index);
    }
    

    public void claimTutorialQuest(TutorialQuest quest)
    {
        switch (quest.reward)
        {
            case TutorialQuestRewards.GOLD:
                gameplayManager.bank.update(Currency.GOLD, quest.getRewardAmount(), BankUpdateStrategy.ADD);
                break;
            case TutorialQuestRewards.ALPHA:
                Alpha alpha = (Alpha)gameplayManager.alphas[(int)quest.alphaReward];
                alpha.state = AlphaState.UNLOCKED;
                findController<ServerController>().sendAnalyticWithParams("NewAlpha", "AlphaID".pair(alpha.alphaID.ToString()));
                if (gameplayManager.city.findHomeByAlpha(alpha).isPurchased()) alpha.state = AlphaState.ASSIGNED;
                break;
            case TutorialQuestRewards.FEAT_INVENTORY:
                gameplayManager.inventory.isLocked = false;
                break;
            case TutorialQuestRewards.FEAT_INVENTORY_SLOT:
                gameplayManager.inventory.purchaseNextSlot();
                break;
            case TutorialQuestRewards.INVENTORY_ITEM:
                quest.inventoryItemReward.add(quest.getRewardAmount());
                break;
            case TutorialQuestRewards.FEAT_MARKET:
                gameplayManager.boosts.isLocked = false;
                break;
            case TutorialQuestRewards.CREDIT:
                gameplayManager.bank.update(Currency.CREDIT, quest.getRewardAmount(), BankUpdateStrategy.ADD);
                break;
            default:
                break;
        }

        tutorialManager.claimTutorialQuest(quest);
        gameplayManager.bank.update(Currency.CLAIMED_QUESTS, 1, BankUpdateStrategy.ADD);
    }

    public override void postinit()
    {
        if (findController<SavefileController>().saveLoadedCorrectly)
        {
            onPostLoad();
        }
        else if(!isStopped)
        {
            initialBuild();
            tutorialManager.postInit();
        }
    }

    public override void stop()
    {
        base.stop();

        tutorialManager.forceDismissActiveTutorial();
    }

    public override void update(float dt)
    {
        if (isStopped) return;

        timeBuffer += dt;

        //TODO Refactor, esto está un poco guarro
        flockManager.playDogSounds(dt);
        flockManager.update(dt);

        if (timeBuffer >= 1)
        {
            flockManager.checkRemoveFlockMember((float)timeBuffer);
            timeBuffer = 0;
        }

        //Update gold
        gameplayManager.updateGold(dt);

        //Check pack mission
        foreach (Alpha p in gameplayManager.alphas) p.update(dt);

        foreach (Building item in gameplayManager.city) item.update(dt);

        //Buffs
        foreach (Buff buff in gameplayManager.buffs) buff.update(dt, this);

        //Car
        carBuffer += dt;
        if(carBuffer >= 7.5f)
        {
            carBuffer = 0;
            flockManager.summonCar();
        }

        //Boosts
        gameplayManager.boosts.update(dt);

        //Offers
        gameplayManager.currentOffer.remainingTime -= dt;
        if(gameplayManager.currentOffer.remainingTime <= 0)
        {
            gameplayManager.generateFlashOffer();
        }

        gameplayManager.bundleOffer.remainingTime -= dt;
        if(gameplayManager.bundleOffer.remainingTime <= 0)
        {
            gameplayManager.generateBuffBundleOffer();
        }

        //Tutorial quests
        if(tutorialManager.peekedQuest == null && tutorialManager.completedQuests?.Count > 0)
        {
            tutorialManager.peekedQuest = tutorialManager.completedQuests.Dequeue();
        }
        else
        {
            if (tutorialManager.canShowTutQuestClaimDialog())
            {
                tryInvokeTutQuestDialog();
            }
            else if(tutorialManager.peekedQuest?.dialog != null)
            {
                tutorialManager.peekedQuest.dialog.SetActive(false);
            }
        }

        //Ad balloon
        if (tutorialManager.hasAllBeenCompleted(Tutorials.UPGRADE_HOME_TUTORIAL))
        {
            gameplayManager.balloonSummonBuffer += dt;
            if (gameplayManager.balloonSummonBuffer >= gameplayManager.balloonSummoningInterval)
            {
                gameplayManager.balloonSummonBuffer = 0;
                gameplayManager.launchBalloon();
            }
        }
    }

    public void onPostLoad()
    {
        //Rebuild the city
        foreach (BuildingSlot slot in gameplayManager.buildingSlots)
        {
            if (slot.building != null && !GameObject.Find(slot.building.name))
            {
                gameplayManager.buildingSlots.toBeBuilt = slot.building;
                buildInSlot(slot.number, true);
            }
        }

        //In case of buffs
        foreach (Buff buff in gameplayManager.buffs)
        {
            buff.levelUp(this, canAfford: true, wakeUpFromLoad: true);
            buff.makeVisible();
        }

        //In case of building mode
        if(gameplayManager.gameMode == GameMode.BUILDING)
        {
            findController<UIController>().displayBuildingMode(gameplayManager.buildingSlots.toBeBuilt);
            startBuildingMode(gameplayManager.buildingSlots.toBeBuilt, true);
        }

        tutorialManager.postLoad();

        //Market
        if (tutorialManager.hasAllBeenDisplayed(Tutorials.SHOP_TUTORIAL))
        {
            gameplayManager.launchMarket();
        }
    }


    public override void postupdate(float dt)
    {
        if (isStopped) return;

        //Launch all particles
        gameplayManager.launchAllParticles();
    }


    public void launchOfflineEarnings()
    {
        if (tutorialManager.hasAnyBeenDisplayed(Tutorials.BUILD_RESTAURANT_TUTORIAL))
        {
            double diffInSeconds = findController<SavefileController>().loadElapsedTime;

            if (diffInSeconds > offlineEarningsBuffer)
            {
                double offlineGold = gameplayManager.calculateOfflineEarningsGold(diffInSeconds);
                invokeOfflineEarningsPanel(offlineGold, diffInSeconds);
            }
        }
    }


    public void notifyAnimalArrived(AlphaMission mission)
    {
        if (mission.what == FlockState.RECRUITED || mission.what == FlockState.NONE)
        {
            gameplayManager.notifyAnimalRecruited(mission);
            prestigeManager.tryDisplayTutorial();
        }
        //Check if the mission is finished
        else if(mission.subtractOneMissionMember() && mission.lead != Alphas.NONE)
        {
            ((Alpha)gameplayManager.alphas[(int)mission.lead]).finishMission();
        }
    }

    private Vector3 findLocationByState(FlockState state, AlphaMission mission) {
        Vector3 origin = Vector3.zero;
        Building building;

        if (state == FlockState.SCOUTING)
        {
            if (mission.isPrologueFinished)
            {
                building = gameplayManager.city[(CityGroup)new System.Random().Next((int)CityGroup.SPAWNER_IN_0, (int)CityGroup.SPAWNER_IN_0 + 7)];
            }
            else
            {
                building = gameplayManager.city[(CityGroup)new System.Random().Next((int)CityGroup.SPAWNER_OUT_0, (int)CityGroup.SPAWNER_OUT_0 + 10)];
            }

            if (building != null) origin = building.position;
        }
        else
        {
            building = findBuildingByState(state, mission.lead);
            if (building != null) origin = building.position;
        }

        return origin;
    }

    public Building findBuildingByState(FlockState state, Alphas lead)
    {
        Building origin = null;

        switch (state)
        {
            case FlockState.FEEDING:
                origin = gameplayManager.city[CityGroup.TAVERN];
                break;
            case FlockState.SHOWER:
                origin = gameplayManager.city[CityGroup.BATH_HOUSE];
                break;
            case FlockState.HEALING:
                origin = gameplayManager.city[CityGroup.HOSPITAL];
                break;
            case FlockState.PLAYING:
                origin = gameplayManager.city[CityGroup.PLAYGROUND];
                break;
            case FlockState.SLEEPING:
                origin = gameplayManager.city[getOriginHome(lead)];
                break;
            case FlockState.SCOUTING:
                origin = gameplayManager.city[(CityGroup)new System.Random().Next((int)CityGroup.SPAWNER_IN_0, (int)CityGroup.SPAWNER_IN_0 + 7)];
                break;
        }

        return origin;
    }

    public bool canSendToMissionByStock(Alpha alpha)
    {
        double stock = 0;

        Building building = findBuildingByState(alpha.pack.getMissionType(), alpha.alphaID);
        if(building != null) stock = building.getActiveSegment().stockAmount;

        bool canSend = stock > 0;
        if(canSend)
        {
            building.getActiveSegment().stockAmount -= 1;
        }

        return canSend;
    }

    internal bool canSendToMissionByVacancy(Alpha alpha, FlockState missionType)
    {
        Alpha theOther = (Alpha)gameplayManager.alphas.items.Find(e => e != alpha && ((Alpha)e).pack.isOnMission && ((Alpha)e).pack.mission.what == missionType);
        return theOther == null;
    }

    internal bool canSendToMissionByCapacity(Alpha alpha, FlockState missionType)
    {
        double capacity = 0;

        Building building = findBuildingByState(alpha.pack.getMissionType(), alpha.alphaID);
        if (building != null) capacity = building.getActiveSegment().capacity;        

        return capacity > alpha.pack.amount;
    }


    public void sendToScout(AlphaMission mission)
    {
        mission.origin = gameplayManager.city[getOriginHome(mission.lead)].position;
        mission.destination = findLocationByState(mission.what, mission);

        if (mission.origin != mission.destination)
        {
            flockManager.bulkSummonAndSendToTarget(
                mission.origin,
                mission.destination,
                mission);
        }
        else
        {
            mission.isPrologueFinished = true; //No need to have a prologue if there's no trip
        }
    }

    public CityGroup getOriginHome(Alphas lead)
    {
        Alpha alphaOnAMission = (Alpha)gameplayManager.alphas[(int)lead];
        CityGroup home = CityGroup.HOME00;
        for (int i = 0; i < gameplayManager.city.Count; i++)
        {
            if (gameplayManager.city[i].GetType() == typeof(Home) && ((Home)gameplayManager.city[i]).alphaSlot.alpha == alphaOnAMission)
            {
                home = (CityGroup)i;
                break;
            }
        }

        return home;
    }


    public bool returnFromMission(AlphaMission mission)
    {
        Alpha alpha = (Alpha)gameplayManager.alphas[(int)mission.lead];
        Vector3 origin = findLocationByState(mission.what, mission);
        Vector3 destination = gameplayManager.city.findHomeByAlpha(alpha).homeDestination;

        bool willDoReturn = origin != destination;
        if (willDoReturn)
        {
            flockManager.bulkSummonAndSendToTarget(origin, destination, mission);
        }

        return willDoReturn;
    }

    public void touchReleasedReceived(GameObject obj, Vector3 coordinate, bool wasDragging, bool comesFromTutorial = false)
    {
        if (isStopped) return;

        touchReleasedReceived(obj.transform.name, coordinate, wasDragging, comesFromTutorial, obj);
    }

    public void touchReleasedReceived(string obj, Vector3 coordinate, bool wasDragging, bool comesFromTutorial = false, GameObject gameObject = null)
    {
        if (findController<UIController>().panels.Count > 0 || wasDragging || isStopped) return; //Avoid processing touches with open panels

        obj = obj.Replace("Collider", "");

        if (Regex.IsMatch(obj, @"Home\d+"))
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            CityGroup building = CityGroup.HOME00;
            string buildingName = obj.ToMayusUnderscored();
            Enum.TryParse(buildingName, out building);
            invokeHomeView(building);
        }

        else if (obj.Contains("Dog") || obj.Contains("Cat"))
        {
            //displayAnimalFeelings(obj);
        }

        else if (obj == gameplayManager.city[CityGroup.TAVERN].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokeTavernView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.TOWN_HALL].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokeTownHallView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.BATH_HOUSE].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokeBathHouseView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.HOSPITAL].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokeHospitalView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.PLAYGROUND].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokePlaygroundView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.MARKET].name)
        {
            Market market = (Market)gameplayManager.city[CityGroup.MARKET];
            if (gameplayManager.gameMode == GameMode.BUILDING || !market.arrived()) return;

            invokeMarketView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.EXCAVATOR].name)
        {
            if (gameplayManager.gameMode == GameMode.BUILDING) return;
            invokeBuilderView(null);
        }

        else if (obj == gameplayManager.city[CityGroup.BALLOON].name)
        {
            deployBalloonRewards();
        }

        else if (obj.Contains("slot_"))
        {
            int slotNumber = int.Parse(obj.Replace("slot_", ""));
            buildInSlot(slotNumber);
        }

        else if (obj.Contains("CAR_"))
        {
            gameplayManager.tapCar(gameObject);
        }
    }

    internal void touchBeginReceived(GameObject obj, Vector3 coordinate)
    {
        if (isStopped) return;
    }


    internal void holdReceived(GameObject obj, Vector3 coordinate)
    {
        if (isStopped) return;
    }

    public void hireAlpha(AlphaSlot slot, bool forceHire = false)
    {
        gameplayManager.hireAlpha(slot, forceHire);

        UIPanel panel = invokeNewAlphaPopup(slot);
        panel.onClosed.AddListener((bool parameter) => 
        {
            tutorialManager.launchTutorial(Tutorials.QUESTS_TUTORIAL2);
        });
    }

    public void selectAlpha(int id)
    {
        gameplayManager.alphaSlots.selectedSlot = id;
    }


    public string[] pickGift(int slotID)
    {
        //inventory solving
        return gameplayManager.pickGift(slotID);
    }


    public string[] pickGift(Alpha a)
    {
        //inventory solving
        return gameplayManager.pickGift(a);
    }


    public void claimGifts()
    {
        gameplayManager.claimDisplayedGifts();

        if(gameplayManager.bank[Currency.GIFTS_PICKED].value == 1)
        {
            tutorialManager.launchTutorial(Tutorials.DISPLAY_INVENTORY_TUTORIAL, false, 0.5f);
        }
        else
        {
            tutorialManager.launchTutorial(Tutorials.BUILD_TOWN_HALL_TUTORIAL, false, 0.5f);
        }

        if (tutorialManager.hasAllBeenDisplayed(Tutorials.SEND_TO_SLEEP_TUTORIAL))
        {
            tutorialManager.launchTutorial(Tutorials.BUILD_RESTAURANT_TUTORIAL);
        }

        findController<ServerController>().sendAnalyticWithParams("AlphaClaimGift");
    }

    public void obtainBoost(Boost boost)
    {
        if (boost != null && boost.canPurchase())
        {
            findController<AdsController>().playAdById(string.Format("PeddlersGuild{0}", gameplayManager.boosts.IndexOf(boost) + 1)).onSuccess.AddListener(() => 
            {
                boost.purchase();
                findController<UIController>().purchaseBoost(boost);
            });
        }
    }

    public void invokeInventoryView(params object[] parameters)
    {
        findController<UIController>().injectInventoryView(gameplayManager.inventory,
            findController<UIController>().invokeUIPanel("UI/Dump/Inventory/InventoryPopup"));
    }

    public void invokePrestigeView()
    {
        findController<UIController>().injectPrestigeView(findController<UIController>().invokeUIPanel(string.Format("UI/Dump/{0}", "PrestigePopup")));
    }

    public void invokeHomeView(params object[] parameters)
    {
        if (!tutorialManager.hasAnyBeenDisplayed(Tutorials.UPGRADE_HOME_TUTORIAL, Tutorials.FULL_HOME_TUTORIAL)) return;

        findController<UIController>().injectHomeView((Home)gameplayManager.city[(CityGroup)parameters[0]],
            findController<UIController>().invokeUIPanel("UI/Buildings/HomePopUp"));
    }

    internal void invokeUpgradeBuildingPanel(Building building)
    {
        findController<UIController>().injectBuildingUpgradePanel(building, findController<UIController>().invokeUIPanel("UI/Buildings/UpgradePanel/BuildingUpgradePopup"));
    }

    public void invokeMarketView(object[] parameters)
    {
        findController<UIController>().injectMarketView((Market)gameplayManager.city[CityGroup.MARKET],
            findController<UIController>().invokeUIPanel(string.Format("UI/Market/MarketPopUp")),
            gameplayManager.boosts);
    }

    private void invokeBuilderView(object[] p)
    {
        if (!tutorialManager.hasAllBeenDisplayed(Tutorials.BUILD_TOWN_HALL_TUTORIAL)) return;

        findController<UIController>().injectExcavatorView(findController<UIController>().invokeUIPanel("UI/Buildings/Excavator/ExcavatorPopUp"), gameplayManager.city);
    }

    public void invokeTutQuestView(object[] parameters)
    {
        findController<UIController>().injectTutQuestView(tutorialManager, findController<UIController>().invokeUIPanel(string.Format("UI/TutorialQuests/{0}", "QuestsPopup")));
    }

    public void tryInvokeTutQuestDialog()
    {
        invokeTutQuestDialog(tutorialManager.peekedQuest);
        if (tutorialManager.hasAllBeenCompleted(Tutorials.QUESTS_TUTORIAL))
        {
            tutorialManager.launchTutorial(Tutorials.CLAIM_QUEST_TUTORIAL, false, 0.05f);
        }
    }

    internal void invokeTutQuestDialog(TutorialQuest quest)
    {
        findController<UIController>().injectTutQuestDialog(quest);

    }

    public void invokeAlphasListView()
    {
        findController<UIController>().injectAlphasListView(gameplayManager.alphaSlots,
            findController<UIController>().invokeUI(string.Format("UI/Alphas/{0}", "AlphasList")));
    }

    private UIPanel invokeNewAlphaPopup(AlphaSlot slot)
    {
        findController<UIController>().closeAllOpenPanels();
        findController<AudioController>().playSound("new_alpha");
        return findController<UIController>().injectNewAlphaPanel(slot, findController<UIController>().invokeUIPanel("UI/Alphas/NewAlphaPopup"));
    }

    public void invokeDumpView(object[] parameters)
    {
        findController<UIController>().injectDumpPanel(findController<UIController>().invokeUIPanel(string.Format("UI/Dump/{0}", "MenuPopup")));
    }

    public void invokeCreditsView()
    {
        findController<UIController>().invokeUIPanel(string.Format("UI/Dump/{0}", "CreditsPopup"));
    }

    public void invokeCommunityView()
    {
        findController<UIController>().injectCommunityPanel(findController<UIController>().invokeUIPanel(string.Format("UI/Dump/{0}", "CommunityPopup")));
    }

    public void invokeSettingsView()
    {
        findController<UIController>().injectSettingsPanel(findController<UIController>().invokeUIPanel(string.Format("UI/Dump/{0}", "SettingsPopup")));
    }

    internal void invokePrivacyPanel()
    {
        findController<UIController>().injectPrivacyPanel(findController<UIController>().invokeUIPanel("UI/Dump/PrivacyPopup"));
    }

    private void invokeTavernView(object[] parameters)
    {
        findController<UIController>().injectTavernPanel((Tavern)gameplayManager.city[CityGroup.TAVERN],
            findController<UIController>().invokeUIPanel("UI/Buildings/BuildingPopUp"));
    }

    private void invokeBathHouseView(object[] parameters)
    {
        findController<UIController>().injectBathHousePanel((BathHouse)gameplayManager.city[CityGroup.BATH_HOUSE],
            findController<UIController>().invokeUIPanel("UI/Buildings/BuildingPopUp"));
    }

    private void invokePlaygroundView(object[] parameters)
    {
        findController<UIController>().injectPlaygroundPanel((Building)gameplayManager.city[CityGroup.PLAYGROUND],
            findController<UIController>().invokeUIPanel("UI/Buildings/BuildingPopUp"));
    }

    private void invokeHospitalView(object[] parameters)
    {
        findController<UIController>().injectHospitalPanel((Building)gameplayManager.city[CityGroup.HOSPITAL],
            findController<UIController>().invokeUIPanel("UI/Buildings/BuildingPopUp"));
    }

    private void invokeTownHallView(object[] parameters)
    {
        if (!tutorialManager.hasAllBeenDisplayed(Tutorials.CRAFT_BUFF_TUTORIAL)) return;

        findController<UIController>().injectTownHallPanel(gameplayManager.buffs, findController<UIController>().invokeUIPanel("UI/Buildings/TownHall/TownHallPopUp"), (TownHall)gameplayManager.city[CityGroup.TOWN_HALL]);
    }

    public UIShopPanel invokeShop()
    {
        return findController<UIController>().injectShopPanel(findController<PurchaseController>().products, findController<UIController>().invokeUIPanel("UI/Shop/ShopPopup"));
    }

    private void invokeOfflineEarningsPanel(double offlinegold, double diffInSeconds)
    {
        findController<UIController>().injectOfflineEarningsPanel(offlinegold, diffInSeconds,
            findController<UIController>().invokeUIPanel("UI/OfflineEarnings/OfflineEarningsPopup"));
    }

    public IEnumerator invokeRatingsPopup(RatingsController ratings, float seconds)
    {
        yield return new WaitForSeconds(seconds);

        UIController ui = findController<UIController>();

        if (tutorialManager.activeTutorial == -1 && ui.panels.Count <= 0)
        {
            ui.injectRatingsPopup(ratings, ui.invokeUIPanel("UI/Ratings/RatingPopup"));
        }
        else
        {
            yield return new WaitForSeconds(5.0f);
        }
    }

    private void invokeBalloonAdView(KeyValuePair<TutorialQuestRewards, DoubleVariable> pair)
    {
        findController<UIController>().injectAdBalloonPanel(pair, findController<UIController>().invokeUIPanel("UI/Balloon/BalloonPopup"));
    }

    public void startBuildingMode(Building building, bool canAfford)
    {
        if (!canAfford) return;

        gameplayManager.gameMode = GameMode.BUILDING;

        foreach (BuildingSlot item in gameplayManager.buildingSlots)
        {
            if (item.building != null) continue;
            
            GameObject slotSignal = (GameObject)Instantiate(Resources.Load("Buildings/Prefabs/BuildingModeTile"));
            slotSignal.transform.localPosition = new Vector3(item.position.x , item.position.y + 0.15f, item.position.z);
            slotSignal.name = item.name;

            gameplayManager.buildingSlots.AddDisplayed(item, slotSignal);
            gameplayManager.buildingSlots.toBeBuilt = building;

            Sequence sequence = DOTween.Sequence();
            sequence.Append(slotSignal.transform.DOScale(1.1f, 1.0f));
            sequence.SetLoops(-1, LoopType.Yoyo);
            sequence.Play();
        }

        findController<CameraController>().focusOn(gameplayManager.buildingSlots.freeSlotsAveragePoint());

        //Pause missions
        foreach (Alpha alpha in gameplayManager.alphas)
        {
            alpha?.pack?.mission?.pauseMission();   
        }

        //Remove dogs
        flockManager.pauseAll();
    }


    private void buildInSlot(int slotNumber, bool forceBuild = false)
    {
        if(forceBuild || (gameplayManager.gameMode == GameMode.BUILDING && gameplayManager.buildingSlots.toBeBuilt != null))
        {
            BuildingSlot slot = gameplayManager.buildingSlots[slotNumber-1];

            if (slot.buildingView != null) return;
            slot.building = gameplayManager.buildingSlots.toBeBuilt;

            GameObject newBuilding = Instantiate(slot.building.getActiveSegment().buildingEvo);
            newBuilding.transform.position = slot.position;
            newBuilding.name = slot.building.name;

            if (!forceBuild) findController<CameraController>().focusOn(newBuilding);

            slot.buildingView = newBuilding;
            slot.building.position = slot.position;
            gameplayManager.buildingSlots.toBeBuilt.slot = slot;
 
            slot.building.onBuilt(slot, forceBuild);//Building callback to setup whatever is required upon building

            flockManager.buildSurface();

            dispelBuildingMode();

            if (gameplayManager.city[CityGroup.EXCAVATOR].warningParticle != null) Destroy(gameplayManager.city[CityGroup.EXCAVATOR].warningParticle);

            if (!forceBuild)
            {
                displayBuildingCreationParticles(slot.position);

                gameplayManager.bank.update(Currency.GOLD, slot.building.segments[0].price, BankUpdateStrategy.SUB);
                gameplayManager.bank.update(Currency.BUILT_BUILDINGS, 1, BankUpdateStrategy.ADD);

                findController<ServerController>().sendAnalyticWithParams("SpendSC", "SCProduct".pair("AddBuilding"), "BuildingID".pair(slot.building.id.ToString()), "Amount".pair(slot.building.segments[0].price.ToString()));
                findController<ServerController>().sendAnalyticWithParams("AddBuilding", "BuildingID".pair(slot.building.id.ToString()));

                findController<AudioController>().playSound("new_building");
                findController<UIController>().dispelBuildingMode();
                findController<SavefileController>().save();
            }
        }
    }

    public void buildInSlot(int slotNumber, Building toBeBuilt, bool forceBuild = false)
    {
        gameplayManager.buildingSlots.toBeBuilt = toBeBuilt;
        buildInSlot(slotNumber, forceBuild);
    }

    public void evolveBuilding(Building building, BuildingSegment segment, bool canAfford, bool isManual = false)
    {
        if(!building.isPurchased()) return;

        string eventName = canAfford ? "SpendSC" : "LackSC";
        findController<ServerController>().sendAnalyticWithParams(eventName, "Amount".pair("ImproveBuilding"), "BuildingID".pair(building.id.ToString()), "Amount".pair(segment.price.ToString()));
        findController<ServerController>().sendAnalyticWithParams("ImproveBuilding", "BuildingID".pair(building.id.ToString()), "Amount".pair(segment.price.ToString()));

        if (!canAfford) return;

        gameplayManager.bank.update(Currency.GOLD, segment.price, BankUpdateStrategy.SUB);
        gameplayManager.bank.update(Currency.EVOLVED_BUILDINGS, 1, BankUpdateStrategy.ADD);

        building.activeSegment = segment.index;

        if (isManual)
        {
            findController<CameraController>().focusOn(building.slot.buildingView, null, () =>
            {
                findController<AudioController>().playSound("new_building");
                displayBuildingCreationParticles(building.slot.position);
                building.slot.buildingView = bringEvolvedBuilding(building, segment);
                building.onEvolved();
            });

        }
        else
        {
            building.slot.buildingView = bringEvolvedBuilding(building, segment);
            building.onEvolved();
        }

        if (building.warningParticle != null) Destroy(building.warningParticle);

        flockManager.buildSurface();

        findController<SavefileController>().preSave(); //Worthy enough to look for a save
    }

    private GameObject bringEvolvedBuilding(Building building, BuildingSegment segment)
    {
        Destroy(building.slot.buildingView);

        GameObject newBuilding = Instantiate(segment.buildingEvo);
        newBuilding.transform.position = building.slot.position;
        newBuilding.name = building.name;

        //Lock purchasing this segment and all previous ones, this is a one way road
        for (int i = 0; i <= segment.index; i++)
        {
            building.segments[i].isPurchased = true;
        }

        for (int i = 0; i < newBuilding.transform.childCount; i++)
        {
            gameplayManager.buffs.tryMakingVisible(newBuilding.transform.GetChild(i).name);
        }

        return newBuilding;
    }

    public void displayBuildingCreationParticles(Vector3 buildingPosition)
    {
        CameraController cameraController = findController<CameraController>();
        Vector3 particlesPos = Vector3.Lerp(cameraController.cam.transform.position, buildingPosition, 0.85f);
        
        GameObject particles = Instantiate(gameplayManager.buildingSlots.evolveParticles);
        particles.transform.position = particlesPos;

        GameObject confetti = Instantiate(gameplayManager.buildingSlots.confettiParticles);
        
        Sequence seq = DOTween.Sequence();
        seq.Append(particles.transform.DOMove(particlesPos, 3.0f));
        seq.OnUpdate(() => 
        {
            Vector3 confettiPos = Vector3.Lerp(cameraController.cam.transform.position, cameraController.rig.transform.position, 0.5f);
            confettiPos.y += 7.5f;

            confetti.transform.position = confettiPos;
        });
        seq.AppendCallback(() => 
        {
            for (int i = 0; i < particles.transform.childCount; i++)
            {
                particles.transform.GetChild(i).GetComponent<ParticleSystem>().Stop();
                confetti.GetComponent<ParticleSystem>().Stop();
            }
        });
        seq.Append(particles.transform.DOMove(particlesPos, 3.0f));
        seq.AppendCallback(() =>
        {
            Destroy(particles);
        });

        seq.Append(confetti.transform.DOMove(confetti.transform.position, 10.0f)); //Wait and do nothing
        seq.OnComplete(() => 
        {
            Destroy(confetti);
        });

        seq.Play();
    }


    public void dispelBuildingMode()
    {
        gameplayManager.buildingSlots.clearAllDisplayed();
        gameplayManager.buildingSlots.toBeBuilt = null;
        gameplayManager.gameMode = GameMode.NORMAL;


        //Resume missions
        foreach (Alpha alpha in gameplayManager.alphas)
        {
            alpha?.pack?.mission?.resumeMission();
        }
    }


    public void cleanCity()
    {
        //Reset all the buildings
        foreach (Building building in gameplayManager.city) building.reset();

        //Building slots
        gameplayManager.buildingSlots.prestige();
        foreach (BuildingSlot item in gameplayManager.buildingSlots.items)
        {
            Building result = item.resetPrestige();
            if (result != null) buildInSlot(item.number, result, true);
        }

        //Reset Alphas and slots (The views are reset on the uicontroller, not here)
        foreach (AlphaSlot slot in gameplayManager.alphaSlots)
        {
            if (slot.id > 0 && slot.alpha.state == AlphaState.ASSIGNED)
            {
                slot.state = AlphaListViewState.EMPTY;
                slot.alpha.state = AlphaState.UNLOCKED;
                findController<UIController>().displayWavesOnAlpha(slot.alpha);
            }

            Alpha a = slot.alpha;
            if (a != null)
            {
                a.reset(this);
                a.requestRender();
            }
            
        }

        gameplayManager.alphaSlots.reset();

        //Reset currencies
        gameplayManager.bank.prestigeReset();

        //Reset inventory
        //gameplayManager.inventory.reset(); Now we leep inventory items between prestiges

        //Reset all the doggies that might still be wandering
        flockManager.resetPrestige();

        //Reset balloon
        ((Balloon)gameplayManager.city[CityGroup.BALLOON]).destroyObject();

        //Market
        ((Market)gameplayManager.city[CityGroup.MARKET]).destroyObject();
        gameplayManager.launchMarket();

        //Boosts
        gameplayManager.boosts.reset();
    }


    public bool purchaseCats()
    {
        bool success = gameplayManager.bank.canAfford(Currency.CREDIT, gameplayManager.breedPrices[Breed.CATS]);
        if (success)
        {
            foreach (Alpha item in gameplayManager.alphas)
            {
                if (item.specie == Breed.CATS)
                {
                    item.state = AlphaState.UNLOCKED;
                }
            }
            if(!hasPurchaseBreed(Breed.CATS)) gameplayManager.unlockedBreeds.Add(Breed.CATS);
        }

        return success;
    }


    public bool hasPurchaseBreed(Breed breed)
    {
        return gameplayManager.unlockedBreeds.Contains(breed);
    }


    public void iapPurchaseSuccess(IAPItem product)
    {
        gameplayManager.bank.update(Currency.CREDIT, product.rewardAmount, BankUpdateStrategy.ADD);
        findController<UIController>().notifyPurchaseSuccess(product);
        findController<SavefileController>().preSave(); //Force saving
    }


    public bool purchaseShopOffer(InventoryItemOffer offer, int index)
    {
        bool result = gameplayManager.purchaseShopOffer(offer, index);

        if (result) gameplayManager.inventory.purchaseItemWithSlot(offer.subject[index], offer.amount);
        sendCreditsExchangeEvent(result, "Amount".pair(offer.price.ToString()), "HCProduct".pair(offer.subject[index].name));

        return result;
    }

    internal bool purchaseBundleOffer(BuffBundleOffer offer)
    {
        bool success = gameplayManager.purchaseBundleOffer(offer);
        sendCreditsExchangeEvent(success, "Amount".pair(offer.price.ToString()), "HCProduct".pair(string.Format("Buff offer: {0}", offer.offeredBuff.name)));

        return success;
    }

    private void sendCreditsExchangeEvent(bool success, params KeyValuePair<string, string>[] eventParams)
    {
        findController<ServerController>().sendAnalyticWithParams(success? "SpendHC" : "LackHC", eventParams);
    }

    private void deployBalloonRewards()
    {
        Balloon balloon = (Balloon)gameplayManager.city[CityGroup.BALLOON];

        if (balloon.hasDeployedGift) return;

        balloon.stopSteering();

        balloon.hasDeployedGift = true;
        gameplayManager.bank.update(Currency.AD_BALLOONS_TOUCHED, 1, BankUpdateStrategy.ADD);

        //Ad or normal gift
        float adOrGift = UnityEngine.Random.Range(0.0f, 1.0f);
        if (gameplayManager.bank[Currency.AD_BALLOONS_DEPLOYED].value <= 1) adOrGift = 1; //Force normal

        if(adOrGift > gameplayManager.balloonAdChance) //Normal
        {
            KeyValuePair<TutorialQuestRewards, DoubleVariable> pair = gameplayManager.getRewardBalloonByChance(1);

            UIController ui = findController<UIController>();
            Vector2 balloonInCanvas = ui.WorldToCanvasPosition(balloon.obj.transform.position);

            string iconName = pair.Key == TutorialQuestRewards.INVENTORY_ITEM ? pair.Value.name : string.Format("{0}_coin", pair.Value.name);
            gameplayManager.stackParticles(
                Vector3.Lerp(balloon.obj.transform.position, findController<CameraController>().cam.transform.position, 0.1f),
                iconName,
                pair.Value.value,
                10.0f);

            gameplayManager.claimBalloonReward(pair);
            gameplayManager.sendBalloonAway();
        }
        else //Ad
        {
            invokeBalloonAdView(gameplayManager.getRewardBalloonByChance(gameplayManager.balloonAdMultiplier));
        }
    }

    internal void ReduceTimes(double timeDiff)
    {
        Debug.Log("Entro a reduce Times: Time Diff = " + timeDiff);

        if (timeDiff < 10) return; //10 seconds threshold to avoid feeding this all the time

        foreach (Alpha alpha in gameplayManager.alphas)
        {
            if (!alpha.pack.isOnMission) continue;

            alpha.pack.mission.elapsedTime += (float)timeDiff;
        }
    }


    public override void load(JSONObject jsonFile)
    {
        gameplayManager.load(jsonFile.GetObject("gameplay"));
        tutorialManager.load(jsonFile.GetObject("tuts"));
        prestigeManager.load(jsonFile.GetObject("prestige"));
    }


    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("gameplay", gameplayManager.save());
        obj.Add("tuts", tutorialManager.save());
        obj.Add("prestige", prestigeManager.save());

        return obj;
    }
}