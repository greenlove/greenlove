﻿using Boomlagoon.JSON;
using BuildingBlocks.Backend;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PrestigeManager : IStorable
{
    [HideInInspector]
    public float earningsMultiplier;

    public int basePrice;
    public string currentTownName;

    private GameplayController context;

    private string[] townNames;

    [HideInInspector]
    public bool isOnPrestigeCutscene;

    public void init(GameplayController c)
    {
        context = c;
        earningsMultiplier = 1;
        isOnPrestigeCutscene = false;

        initTownNames();
        pickTownName();
    }

    public int getPriceByAmountOfPrestiges()
    {
        return Mathf.FloorToInt(basePrice * (float)(context.gameplayManager.bank[Currency.PRESTIGES].value + 1) * 0.75f);
    }

    public void prestige()
    {
        currentTownName = "";
        earningsMultiplier += getNextEarningsMultiplier();
        context.gameplayManager.bank.update(Currency.PRESTIGES, 1, BankUpdateStrategy.ADD);
        context.cleanCity();

        context.findController<ServerController>().sendAnalyticWithParams("NewPrestige", "Amount".pair(context.gameplayManager.bank[Currency.PRESTIGES].value.ToString()));
        context.findController<SavefileController>().preSave();
    }

    public bool canPrestige()
    {
        return getPriceByAmountOfPrestiges() <= Math.Ceiling(context.gameplayManager.bank[Currency.DOGS].value + 10);
    }

    public string getNextEarningsMultiplierFormatted()
    {
        float multiplier = getNextEarningsMultiplier();

        return string.Format("+{0}%", context.gameplayManager.bank.toFormattedCurrency(getNextEarningsMultiplier() * 100));
    }

    public string getCurrentEarningsMultiplierFormatted()
    {
        float multiplier = getNextEarningsMultiplier();

        return string.Format("+{0}%", context.gameplayManager.bank.toFormattedCurrency((earningsMultiplier * 100) - 100));
    }

    public float getNextEarningsMultiplier()
    {
        float price = (float)getPriceByAmountOfPrestiges();
        float petsBase = (Mathf.Max(price, (float)context.gameplayManager.bank[Currency.DOGS].value) / price) - 1;
        float multiplier = (float) Math.Max(1.15f, 1 + context.gameplayManager.bank[Currency.PRESTIGES].value * Math.Log(earningsMultiplier + 1));

        return petsBase * multiplier;
    }

    
    public string pickTownName()
    {
        //Pick a new name only if the town has no name
        if(currentTownName == "")
        {
            currentTownName = townNames[UnityEngine.Random.Range(0, townNames.Length - 1)];
        }

        return currentTownName;
    }


    private void initTownNames()
    {
        townNames = new string[]
        {
            "Pismeno",
            "Swindon",
            "Nerton",
            "Calchester",
            "Tardide",
            "Oakheart",
            "Nerton",
            "Troutberk",
            "Violl's Garden",
            "Westray",
            "Hammaslahti",
            "Hampstead",
            "Threlkeld",
            "Pavv",
            "Frostford",
            "Ballater",
            "Pontybridge",
            "Stratford",
            "Cardend",
            "Harnsey",
            "Pendle",
            "Drumnadrochit",
            "Keld",
            "Grasmere",
            "Coningsby",
            "Tarnstead",
            "Sharpton",
            "Drumnadrochit",
            "Galssop",
            "Graycott",
            "Ardglass",
            "Dewhurst",
            "Scrabster",
            "Archmouth",
            "Accreton",
            "Aeberuthey",
            "Davenport",
            "Pitmerden",
            "Lanercoast",
            "Forstford",
            "Perlshaw",
            "Aberuthven",
            "Macclesfield",
            "Blue Field",
            "Nerton",
            "Blackpool",
            "Appleby",
            "Everwinter",
            "Mensfield",
            "Limesvilles",
            "Caister",
            "Bardford",
            "Mensfield",
            "City of Fire",
            "Ormskirk",
            "Dewsbury",
            "Keld",
            "Lanercoast",
            "Portsmouth",
            "Orilon",
            "Runswick",
            "Holsworthy",
            "Brickelwhyte",
            "Addersfield",
            "Blencalgo",
            "South Warren",
            "Ormskirk",
            "Findochty",
            "Tardide",
            "Linemell",
            "Berkton",
            "Bardford",
            "Nantwich",
            "Aston",
            "Garrigill",
            "Snowmelt",
            "Dragontail",
            "Coalfell",
            "Bardford"
        };
    }

    public void load(JSONObject jsonFile)
    {
        if (jsonFile == null) return;

        earningsMultiplier = (float)jsonFile.GetNumber("multiplier");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("multiplier", earningsMultiplier);

        return obj;
    }

    internal void tryDisplayTutorial()
    {
        if (context.tutorialManager.hasAllBeenDisplayed(Tutorials.PRESTIGE_TUTORIAL)) return;

        if (context.gameplayManager.bank[Currency.DOGS].value >= getPriceByAmountOfPrestiges() * 0.4f)
        {
            context.tutorialManager.launchTutorial(Tutorials.PRESTIGE_TUTORIAL);
        }
    }
}
