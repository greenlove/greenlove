﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Bank/Currency Var")]
public class CurrencyVariable : DoubleVariable
{
    public bool resettableOnPrestige;
    public bool clampNegatives;

    public override double add(double v)
    {
        double value = base.add(v);

        return tryClampNegatives();
    }

    public override double sub(double v)
    {
        double value = base.sub(v);

        return tryClampNegatives();
    }

    public override double prod(double v)
    {
        double value = base.prod(v);

        return tryClampNegatives();
    }

    public override double div(double v)
    {
        double value = base.div(v);

        return tryClampNegatives();
    }

    public override double setMax(double v)
    {
        double value = base.setMax(v);

        return tryClampNegatives();
    }

    private double tryClampNegatives()
    {
        value = clampNegatives ? Math.Max(0, value) : value;

        return value;
    }
}
