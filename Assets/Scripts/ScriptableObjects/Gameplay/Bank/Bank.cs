﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Boomlagoon.JSON;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

//A bank is a library for currencies
[CreateAssetMenu(menuName ="Building Blocks/Gameplay/Bank")]
public class Bank : Library, IStorable
{
    public LocalizationController loc;

    private List<IBankListener>[] subscribers;

    private string[] magnitudes;
    private string[] shortMagnitudes;
    private double[] formatSteps;

#if GREENLOVE_DEBUG || UNITY_EDITOR
    public bool isAllFree = false;
#endif

    public CurrencyVariable this[Currency key]
    {
        get
        {
            return (CurrencyVariable)items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }


    public void init()
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        isAllFree = false;
#endif
        foreach (CurrencyVariable item in items)
        {
            item.value = 0;
        }


        if (subscribers != null) return;

        subscribers = new List<IBankListener>[(int)Currency.LENGTH];
        for (int i = 0; i < subscribers.Length; i++)
        {
            subscribers[i] = new List<IBankListener>();
        }

        magnitudes = new string[]
        {
            "Thousand",            "Million",              "Billion",              "Trillion",           "Quadrillion",         "Quintillion",
            "Sextillion",          "Septillion",           "Octillion",            "Nonillion",          "Decillion",           "Undecillion",
            "Duodecillion",        "Tredecillion",         "Quatuourdecillion",    "Quinquadecillion",   "Sedecillion",         "Septendecillion",
            "Octodecillion",       "Novendecillion",       "Vigintillion",         "Unvigintillion",     "Duovigintillion",     "Tresvigintillion",
            "Quatuorvigintillion", "Quinquavigintillion",  "Sesvigintillion",      "Septemvigintillion", "Octovigintillion",    "Novemvigintillion",
            "Trigintillion",       "Octovigintillion",     "Novemvigintillion",    "Trigintillion",      "Untrigintillion",     "Duotrigintillion",
            "Trestrigintillion",   "Quatuortrigintillion", "Quinquatrigintillion", "Sestrigintillion",   "Septentrigintillion", "Octotrigintillion",
            "Noventrigintillion",  "Quadragintillion"
        };

        shortMagnitudes = new string[]
        {
            "K",  "M",   "B",   "T",   "q",  "Q",
            "s",  "S",   "O",   "N",   "D",  "U",
            "Dd", "Td",  "qd",  "Qd",  "sd", "Sd",
            "Od", "Nd",  "V",   "Uv",  "Dv", "Tv",
            "qv", "Qv",  "sv",  "Sv",  "Ov", "Nv",
            "Tg", "Ovg", "Nvg", "Tgt", "Ug", "Dt",
            "Tt", "qt",  "Qt",  "st",  "St", "Ot",
            "Nt", "Qg"
        };

        formatSteps = new double[shortMagnitudes.Length];
        formatSteps[0] = 1000;
        for (int i = 1; i < formatSteps.Length; i++)
        {
            formatSteps[i] = formatSteps[i - 1] * 1000;
        }
    }


    public string getFormatted(Currency c)
    {
        return toFormattedCurrency(this[c].value);
    }

    public string toFormattedCurrency(double d, bool isCostOfSomething = false)
    {
        string s = "";

        if (d >= 1000)
        {
            int ciphers = (int)Math.Floor(Math.Log10(d));
            int index = (ciphers / 3) - 1;

            string format = string.Format("{{0:0.{0}}}", "".PadRight(2 - ciphers % 3, '0'));
            string dFormatted = string.Format(format, d / formatSteps[index]);

            StringBuilder builder = new StringBuilder();
            builder.Append(dFormatted);
            builder.Append(index+1 < shortMagnitudes.Length ? shortMagnitudes[index] : string.Format("^{0}", ciphers));

            s = builder.ToString();
        }
        else if (isCostOfSomething && d == 0)
        {
            s = loc.getTextByKey("CURRENCY_TAG_FREE");
        }
        else
        {
            s = string.Format("{0:0}", d);
        }
        
        return s;
    }

    public bool canAfford(Currency c, double opponent)
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        if (isAllFree) return true;
#endif
        return this[c].value >= opponent;
    }

    public void prestigeReset()
    {
        for (int i = 0; i < (int)Currency.LENGTH; i++)
        {
            Currency key = (Currency)i;
            if(this[key].resettableOnPrestige)
            {
                this[key].value = 0;
                talk(key, 0);
            }
        }

        update(Currency.DOGS, 1, BankUpdateStrategy.ADD);//Add the starter alpha
    }


    public void update(Currency key, double value, BankUpdateStrategy strat)
    {
        double d1 = this[key].value;
        double d2 = 0;

#if GREENLOVE_DEBUG || UNITY_EDITOR
        if (!isAllFree)
        {
#endif
            switch (strat)
            {
                case BankUpdateStrategy.ADD:
                    d2 = this[key].add(value);
                    break;
                case BankUpdateStrategy.SUB:
                    d2 = this[key].sub(value);
                    break;
                case BankUpdateStrategy.PROD:
                    d2 = this[key].prod(value);
                    break;
                case BankUpdateStrategy.DIV:
                    d2 = this[key].div(value);
                    break;
                case BankUpdateStrategy.MAX:
                    d2 = this[key].setMax(value);
                    break;
            }
#if GREENLOVE_DEBUG || UNITY_EDITOR
        }
#endif
        talk(key, d2 - d1);
    }

    public void onSubscribe(IBankListener listener, Currency currency, bool skipTalk = false)
    {
        if (subscribers == null) init();

        if (!subscribers[(int)currency].Contains(listener) && listener != null)
        {
            subscribers[(int)currency].Add(listener);

            //kickoff a first talk to the subscribers
            if(!skipTalk) update(currency, 0, BankUpdateStrategy.ADD);
        }
    }

    public void onUnsubscribe(IBankListener listener, Currency currency)
    {
        if (subscribers[(int)currency].Contains(listener)) subscribers[(int)currency].Remove(listener);

    }

    public void talk(Currency c, double delta)
    {
        if (delta == 0) return;

        for (int i = subscribers[(int)c].Count -1; i > -1; i--)
        {
            subscribers[(int)c][i].onListeningToBank(c, delta, this[c].value, getFormatted(c));
        }
    }

    public void talkAllCurrencies()
    {
        for (int i = 0; i < (int)Currency.LENGTH; i++)
        {
            talk((Currency)i, 0);
        }
    }

    public void load(JSONObject jsonFile)
    {
        foreach (CurrencyVariable item in this)
        {
            item.value = jsonFile.GetNumber(item.name);
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        foreach (CurrencyVariable item in this) obj.Add(item.name, item.value);

        return obj;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Bank)), CanEditMultipleObjects]
    public class BankEditor : LibraryEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
#endif
}

public enum BankUpdateStrategy
{
    ADD, SUB, PROD, DIV, MAX
}