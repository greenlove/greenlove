﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBankListener
{
    void subscribeToBankChanges(bool skipTalk = false);
    void unsubscribeToBankChanges();

    void onListeningToBank(Currency currency, double delta, double total, string formattedTotal);
}
