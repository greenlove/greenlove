﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Morning Coffee")]
public class BuffMorningCoffee : BuffBuildingDiscounts
{
}
