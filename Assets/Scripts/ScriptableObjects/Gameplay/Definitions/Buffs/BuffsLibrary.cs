﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Buffs Library")]
public class BuffsLibrary : Library, IStorable
{
    public Buff this[Buffs key]
    {
        get
        {
            return (Buff)items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }

    public double[] groupPrices;
    private bool[] purchasedGroups;

    public int currentPage;
    public int buffsPerPage;
    public Buff selectedBuff;

    public void init()
    {
        currentPage = 0;

        purchasedGroups = new bool[groupPrices.Length];
        purchasedGroups[0] = true;
        for (int i = 1; i < purchasedGroups.Length; i++)
        {
            purchasedGroups[i] = false;
        }

        selectedBuff = this[0];

        for (int i = 0; i < items.Count; i++)
        {
            //((Buff)items[i]).init(i/buffsPerPage < 1);
            ((Buff)items[i]).init(false, this);
        }
    }

    public double getCurrentGroupPrice()
    {
        return groupPrices[currentPage];
    }

    public bool isCurrentPagePurchased()
    {
        return isPagePurchased(currentPage);
    }

    public bool isPagePurchased(int page)
    {
        return purchasedGroups[page];
    }

    public void purchaseCurrentPage()
    {
        purchasedGroups[currentPage] = true;

        int index = currentPage * buffsPerPage;
        for (int i = index; i < index + buffsPerPage && i < items.Count; i++)
        {
            ((Buff)items[i]).isPurchased = true;
        }
    }

    public void paginate(bool left)
    {
        int maxPages = Mathf.FloorToInt(((float)Count / 3));
        if (left)
        {
            currentPage++;
            if (currentPage > maxPages) currentPage = 0;
        }
        else
        {
            currentPage--;
            if (currentPage < 0) currentPage = maxPages;
        }

        //Debug.Log(currentPage);
        selectedBuff = (Buff)items[currentPage * 3];
    }

    public void tryMakingVisible(string name)
    {
        ((Buff)items.Find(e => e.name == name.Replace("Socket", "")))?.makeVisible();
    }

    public bool shouldBeVisible(string name)
    {
        Buff target = ((Buff)items.Find(e => e.name == name));
        return target != null && target.isPurchased && target.level > 0;

    }

    public void load(JSONObject jsonFile)
    {
        foreach (Buff buff in this)
        {
            buff.load(jsonFile.GetObject(buff.name));
        }

        JSONArray groups = jsonFile.GetArray("groups");
        for (int i = 0; i < groups.Length; i++)
        {
            purchasedGroups[i] = groups[i].Boolean;
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        foreach (Buff buff in this)
        {
            obj.Add(buff.name, buff.save());
        }

        //WARNING: THIS MAY BE DEPRECATED IN THE FUTURE
        JSONArray groups = new JSONArray();
        for (int i = 0; i < purchasedGroups.Length; i++)
        {
            groups.Add(purchasedGroups[i]);
        }
        obj.Add("groups", groups);

        return obj;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(BuffsLibrary)), CanEditMultipleObjects]
    public class BuffsLibraryEditor : LibraryEditor
    {
        SerializedProperty groupPrices;

        protected void OnEnable()
        {
            groupPrices = serializedObject.FindProperty("groupPrices");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
#endif
}
