﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Sweet Dreams")]
public class BuffSweetDreams : Buff
{
    [Space]
    public float basePercentage;
    public float maxPercentage;

    private float percentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        percentage = basePercentage;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, percentage.ToPercentage());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);
        percentage = Mathf.Lerp(basePercentage, maxPercentage, evolution.Evaluate(normalizedLevel));

        controller.gameplayManager.inventoryItemsMultiplier = 1 + percentage;
    }
}
