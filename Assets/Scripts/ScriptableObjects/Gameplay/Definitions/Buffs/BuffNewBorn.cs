﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/New Born")]
public class BuffNewBorn : BuffBuildingDiscounts
{
}
