﻿using BuildingBlocks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Red String")]
public class BuffRedString : Buff
{
    [Space]
    public int tapAndHoldLevel;
    public float amountOfAnimals;
    public int maxAmountOfAnimals;

    public StringReference hold;
    public StringReference perSecond;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        amountOfAnimals = 1;
        base.init(purchased, lib);
    }

    public override string getFormattedDescription()
    {
        bool canTapNHold = canTapAndHold();

        return string.Format(
            localizedDescription.value,
            canTapNHold ? hold.value : "",
            string.Format("{0:0.0}", amountOfAnimals),
            canTapNHold ? perSecond.value : "");
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);

        amountOfAnimals = Mathf.Lerp(1, maxAmountOfAnimals, evolution.Evaluate(normalizedLevel));
    }

    public bool canTapAndHold()
    {
        return level >= tapAndHoldLevel;
    }
}
