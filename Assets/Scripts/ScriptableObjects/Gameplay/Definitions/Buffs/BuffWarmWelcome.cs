﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Warm Welcome")]
public class BuffWarmWelcome : Buff
{
    [Space]
    public float baseCalledPets;
    public float baseCalledInterval;
    public float maxCalledPets;
    public float maxCalledInterval;

    private float calledPets;
    private float calledInterval;


    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);

        calledPets = baseCalledPets;
        calledInterval = baseCalledInterval;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, calledPets, calledInterval);
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);

        calledPets = Mathf.Lerp(baseCalledPets, maxCalledPets, evolution.Evaluate(normalizedLevel));
        calledInterval = Mathf.Lerp(baseCalledInterval, maxCalledInterval, evolution.Evaluate(normalizedLevel));
    }

    public override void update(float dt, GameplayController controller)
    {
        base.update(dt, controller);

        if (updateBuffer * calledPets * dt >= calledInterval * dt && level > 0)
        {
            updateBuffer = 0;
            applyEffect(controller);
        }
    }

    public override void applyEffect(GameplayController controller)
    {
        base.applyEffect(controller);
        controller.summonAnimal(true);
    }
}
