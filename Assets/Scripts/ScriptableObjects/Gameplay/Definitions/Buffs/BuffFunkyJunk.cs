﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Funky Junk")]
public class BuffFunkyJunk : BuffBuildingDiscounts
{
}
