﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Starlight Parade")]
public class BuffStarlightParade : Buff
{
    [Space]
    public float baseSpeedPercentage;
    public float maxSpeedPercentage;

    private float speedPercentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        speedPercentage = baseSpeedPercentage;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, speedPercentage.ToPercentage());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);
        speedPercentage = Mathf.Lerp(baseSpeedPercentage, maxSpeedPercentage, evolution.Evaluate(normalizedLevel));

        controller.flockManager.flockSpeedMultiplier = speedPercentage;
    }

    public override void makeVisible()
    {
        base.makeVisible();
        
        if(isPurchased) instancedView.transform.Find("WaterfallTrees").gameObject.SetActive(!parent[Buffs.ONE_TIME_GAL].isPurchased);
    }
}
