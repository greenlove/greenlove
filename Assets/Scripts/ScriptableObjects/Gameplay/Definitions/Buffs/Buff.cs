﻿using Boomlagoon.JSON;
using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/New Buff")]
public class Buff : ScriptableObject, IStorable
{
    public StringReference localizedName;
    public StringReference localizedDescription;
    public int level;
    public int maxLevel;
    public bool isPurchased;

    public InventoryItem[] recipeComponents;

    public float[] recipeRequiredConst;
    [HideInInspector] public float[] recipeRequired;
    public float recipeMultiplier;

    public double baseUpgradePrice;
    public float upgradePriceMultiplier;

    public BuffEffectType effectType;

    public GameObject view;
    [HideInInspector] public GameObject instancedView;

    protected float updateBuffer;

    protected BuffsLibrary parent;

    [HideInInspector]
    public float normalizedLevel { get { return (float)level / (float)maxLevel; } }

    public AnimationCurve evolution;

    public virtual void init(bool purchased, BuffsLibrary lib)
    {
        level = 0;
        isPurchased = purchased;

        recipeRequired = new float[recipeRequiredConst.Length];
        for (int i = 0; i < recipeRequired.Length; i++)
        {
            recipeRequired[i] = recipeRequiredConst[i];
        }

        parent = lib;
        instancedView = null;
    }

    public bool canUpgradeRecipe()
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        if (GameObject.FindWithTag("Main").GetComponent<Main>().getController<CheatsController>().isAllFree) return true;
#endif
        bool canAfford = level < maxLevel;

        for (int i = 0; i < recipeComponents.Length; i++)
        {
            if (recipeComponents[i].amount.value < Mathf.FloorToInt(recipeRequired[i])) canAfford = false;
        }

        return canAfford;
    }

    public virtual string getFormattedDescription()
    {
        return "";
    }

    public virtual double getUpgradePriceByLevel()
    {
        return baseUpgradePrice * upgradePriceMultiplier * (level+1);
    }

    public virtual void levelUp(GameplayController controller, bool canAfford, bool wakeUpFromLoad = false)
    {
        if(!wakeUpFromLoad) sendAnalyticEvent(controller, canAfford);

        if (!canAfford) return;

        if (!wakeUpFromLoad)
        {
            isPurchased = true;
            level++;
            for (int i = 0; i < recipeComponents.Length; i++)
            {
                recipeComponents[i].amount.value -= recipeRequired[i];
                if (recipeComponents[i].amount.value < 0) recipeComponents[i].amount.value = 0; //Careful with negative values
            }

            if (controller.gameplayManager.city[CityGroup.TOWN_HALL].warningParticle != null)
                Destroy(controller.gameplayManager.city[CityGroup.TOWN_HALL].warningParticle);

            controller.gameplayManager.bank.update(Currency.UPGRADED_BUFFS, 1, BankUpdateStrategy.ADD);
            controller.gameplayManager.bank.update(Currency.GOLD, getUpgradePriceByLevel(), BankUpdateStrategy.SUB);
            
            //Only once
            if (level == 1)
            {
                makeVisible();
                controller.flockManager.buildSurface();
                controller.displayBuildingCreationParticles(instancedView.transform.position);
                controller.findController<CameraController>().focusOn(instancedView);
            }
        }

        //Modify recipe required
        for (int i = 0; i < recipeComponents.Length; i++)
        {
            float multiplier = level <= 0 ? 1 : level * recipeMultiplier;
            recipeRequired[i] = recipeRequiredConst[i] * multiplier;
        }
    }

    private void sendAnalyticEvent(GameplayController controller, bool canAfford)
    {
        string eventType = level <= 1? "AddBuff" : "ImproveBuff";
        string eventID = canAfford ? "SpendSC" : "LackSC";

        controller.findController<ServerController>().sendAnalyticWithParams(eventID, "SCProduct".pair(eventType), "BuffID".pair(name), "Amount".pair(getUpgradePriceByLevel().ToString()));
        controller.findController<ServerController>().sendAnalyticWithParams(eventType, "BuffID".pair(name));
    }

    public virtual void update(float dt, GameplayController controller)
    {
        if(effectType == BuffEffectType.TIME_LOOPABLE) updateBuffer += dt;
    }

    public virtual void applyEffect(GameplayController controller)
    {
        //Nothing here
    }


    public virtual void makeVisible()
    {
        if (isPurchased && level > 0)
        {
            GameObject parent = GameObject.Find(string.Format("{0}Socket", name));
            GameObject me = view != null ? Instantiate(view) : GameObject.Find(name);
            //Debug.Log(name + (me == null ? " IS NULL" : " IS NOT NULL"));
            me.name = name;
            instancedView = me;

            if(parent != null) me.transform.SetParent(parent.transform, false);
        }
    }

    public virtual void load(JSONObject jsonFile)
    {
        level = (int)jsonFile.GetNumber("level", 0);
        isPurchased = jsonFile.GetBoolean("purchased", false);
    }

    public virtual JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("level", level);
        obj.Add("purchased", isPurchased);

        return obj;
    }
}

public enum BuffEffectType
{
    NONE,
    //Add stuff from here

    TIME_LOOPABLE,
    SINGLE_HIT,

    //Don't add anything from here
    SIZE
}
