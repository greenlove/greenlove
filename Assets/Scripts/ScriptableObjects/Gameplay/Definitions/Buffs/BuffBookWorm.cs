﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Book Worm")]
public class BuffBookWorm : Buff
{
    [Space]
    public float baseGoldChancePercentage;
    public float maxGoldChancePercentage;

    private float goldChancePercentage;
    private float previousChancePercentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        goldChancePercentage = baseGoldChancePercentage;
        previousChancePercentage = 0;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, goldChancePercentage.ToPercentage());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);

        previousChancePercentage = goldChancePercentage;
        goldChancePercentage = Mathf.Lerp(baseGoldChancePercentage, maxGoldChancePercentage, evolution.Evaluate(normalizedLevel));

        controller.gameplayManager.goldChanceOnRecruiting *= 1 + Mathf.Abs(goldChancePercentage - previousChancePercentage);
    }
}
