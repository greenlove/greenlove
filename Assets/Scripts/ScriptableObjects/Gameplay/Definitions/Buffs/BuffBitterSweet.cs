﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//DEPRECATED
[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Bitter Sweet")]
public class BuffBitterSweet : Buff
{
    [Space]
    public float baseSendInterval;
    public float basePackPercentage;

    public float MaxSendInterval;
    public float MaxPackPercentage;

    private float sendInterval;
    private float packPercentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        sendInterval = baseSendInterval;
        packPercentage = basePackPercentage;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, sendInterval, packPercentage.ToPercentageString());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);

        sendInterval = Mathf.Lerp(baseSendInterval, MaxSendInterval, normalizedLevel);
        packPercentage = Mathf.Lerp(basePackPercentage, MaxPackPercentage, normalizedLevel);
    }

    public override void update(float dt, GameplayController controller)
    {
        base.update(dt, controller);

        if (updateBuffer >= sendInterval && level > 0)
        {
            updateBuffer = 0;
            applyEffect(controller);
        }
    }
}
