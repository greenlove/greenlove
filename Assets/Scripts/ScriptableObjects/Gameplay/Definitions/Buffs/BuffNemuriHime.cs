﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Buffs/Nemuri Hime")]
public class BuffNemuriHime : Buff
{
    [Space]
    public float baseGoldPercentage;
    public float maxGoldPercentage;

    private float goldPercentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        goldPercentage = baseGoldPercentage;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, goldPercentage.ToPercentage());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);
        goldPercentage = Mathf.Lerp(baseGoldPercentage, maxGoldPercentage, evolution.Evaluate(normalizedLevel));

        controller.gameplayManager.goldOnRecruitMultiplier = 1 + goldPercentage;
    }
}
