﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

public class BuffBuildingDiscounts : Buff
{
    [Space]
    public float baseDiscountPercentage;
    public float maxDiscountPercentage;

    private float discountPercentage;

    public override void init(bool purchased, BuffsLibrary lib)
    {
        base.init(purchased, lib);
        discountPercentage = baseDiscountPercentage;
    }

    public override string getFormattedDescription()
    {
        return string.Format(localizedDescription.value, discountPercentage.ToPercentage());
    }

    public override void levelUp(GameplayController controller, bool canAfford, bool wakeUpCall = false)
    {
        base.levelUp(controller, canAfford, wakeUpCall);
        setupDiscountPercentage();
    }

    public virtual void setupDiscountPercentage()
    {
        discountPercentage = Mathf.Lerp(baseDiscountPercentage, maxDiscountPercentage, evolution.Evaluate(normalizedLevel));
    }

    public override void load(JSONObject jsonFile)
    {
        base.load(jsonFile);
        setupDiscountPercentage();
    }

    public double getDiscount()
    {
        return isPurchased? 1 - discountPercentage : 1;
    }
}
