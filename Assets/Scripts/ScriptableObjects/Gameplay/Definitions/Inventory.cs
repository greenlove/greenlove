﻿using Boomlagoon.JSON;
using BuildingBlocks;
using Gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Inventory/New Inventory")]
public class Inventory : Library, IStorable
{
    public InventoryItem this[Stock key]
    {
        get
        {
            return (InventoryItem)items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }

    [Space(20)]
    [SerializeField]
    public int unlockedSlots;

    [SerializeField]
    public int occupiedSlots;

    [SerializeField]
    public int selectedSlot;

    public DoubleReference[] slotPrices;

    public float baseMultiplier;

    [MinMaxRange(0, 5.0f)]
    public RangedFloat critChance;

    private float totalCurrentAccumulatedWeight;

    public bool isLocked;

    private GameplayManager context;

    public void init(GameplayManager manager)
    {
        reset();
        unlockedSlots = Count; //Hack to avoid having locked slots for now
        isLocked = true;
        context = manager;
    }

    public void reset()
    {
        foreach (InventoryItem item in items)
        {
            item.init(this);
        }

        calculateAccumulatedWeight();

        occupiedSlots = 0;
        selectedSlot = 0;
    }

    private void calculateAccumulatedWeight()
    {
        totalCurrentAccumulatedWeight = 0;

        for (int i = 0; i < items.Count; i++)
        {
            InventoryItem item = (InventoryItem)items[i];
            totalCurrentAccumulatedWeight += item.weigth;
            item.accumulatedWeight = totalCurrentAccumulatedWeight;
        }
    }

    public Dictionary<string, double> stackItems(int attainedItems, double packSize)
    {
        double total = 0;
        List<InventoryItem> excluded = new List<InventoryItem>();

        Dictionary<string, double> returnable = new Dictionary<string, double>();

        int scouts = (int)context.bank[Currency.TIMES_SENT_TO_SCOUT].value;
        if (scouts >=1 && scouts < 2)
        {
            returnable.Add("AncientWaltz", 6);
            total = 6;
        }
        else if(scouts >= 2 && scouts < 3)
        {
            returnable.Add("SicilianVendetta", 5);
            total = 5;
        }
        else
        {
            for (int i = 0; i < attainedItems; i++)
            {
                //Pick one item according to the slots
                InventoryItem candidate = getRandomItemByUnlockedSlots(excluded);

                if (candidate == null) break;
                excluded.Add(candidate);

                //Resolve how much of each
                double amount = getAmountOfUniqueGift(packSize, candidate);
                
                total += amount;

                returnable.Add(candidate.name, amount);

            }
        }

        //Bank stat collection purposes
        returnable.Add("total", total);

        return returnable;
    }

    internal bool isEmpty()
    {
        return items.TrueForAll(element => ((InventoryItem)element).amount.value == 0);
    }

    public double getAmountOfUniqueGift(double packSize, InventoryItem candidate, bool forceCrit = false)
    {
        //Resolve how much of each
        return 1 + (packSize * baseMultiplier * (Math.Max(candidate.slot, 1) + 1) * (forceCrit? 1 : critChance.randomValue));
    }

    public InventoryItem getRandomItemByUnlockedSlots(List<InventoryItem> excluded)
    {
        List<InventoryItem> candidates = new List<InventoryItem>();
        foreach (InventoryItem item in items)
        {
            //If complies with the gating and not already on the list
            if(!excluded.Contains(item))
            {
                //Check if it exists in the unlocked ones
                if (item.slot != -1)
                {
                    candidates.Add(item);
                }
                else if(item.slot == -1 && occupiedSlots < unlockedSlots)
                {
                    candidates.Add(item);//Esto se puede programar mejor ^^u
                }
            }
        }
        //Find by weight
        float weight = UnityEngine.Random.Range(0, totalCurrentAccumulatedWeight);
        
        return candidates.Count == 0? null : findByAccumulatedWeight(weight, candidates);
    }

    private InventoryItem findByAccumulatedWeight(float weight, List<InventoryItem> candidates)
    {
        InventoryItem returnable = null;

        for (int i = 0; i < items.Count; i++)
        {
            InventoryItem item = (InventoryItem)items[i];
            
            if (weight <= item.accumulatedWeight && candidates.Contains(item))
            {
                returnable = item;
                break;
            }
        }
        return returnable;
    }

    private InventoryItem findClosestToUnlockedSlots()
    {
        InventoryItem returnable = null;
        for (int i = Count-1; i > 0; i--)
        {
            InventoryItem item = (InventoryItem)items[i];

            if(item.slot != -1)
            {
                if (item.slot > unlockedSlots) continue;
                else { returnable = item; break; }
            }
        }

        return returnable;
    }

    public InventoryItem findBySlot(int i)
    {
        return (InventoryItem)items.Find(e => ((InventoryItem)e).slot == i);
    }

    public InventoryItem findBySelectedSlot()
    {
        return selectedSlot >= 0 ? (InventoryItem)items.Find(e => ((InventoryItem)e).slot == selectedSlot) : null;
    }

    public bool hasRemainingLockedSlots()
    {
        return unlockedSlots < Count;
    }

    public double nextUnlockedSlotPrice()
    {
        return slotPrices[unlockedSlots - 5].value;
    }

    public int purchaseNextSlot()
    {
        if (unlockedSlots < items.Count)
        {
            unlockedSlots++;
            calculateAccumulatedWeight();
        }
        return unlockedSlots;
    }

    public List<InventoryItem> createRandomOffer(int amount)
    {
        return getNLessLikely(amount);
    }

    public bool hasFreeSlots()
    {
        return unlockedSlots - occupiedSlots > 0;
    }

    internal void purchaseItemWithSlot(InventoryItem subject, double amount)
    {
        if (subject.slot == -1)
        {
            if (hasFreeSlots())
            {
                subject.slot = occupiedSlots;
            }
            else
            {
                subject.slot = purchaseNextSlot();
            }
        }
        subject.amount.value += amount;
        occupiedSlots++;
    }

    public void load(JSONObject jsonFile)
    {
        foreach (InventoryItem item in this)
        {
            item.load(jsonFile.GetObject(item.name));
        }

        unlockedSlots = (int)jsonFile.GetNumber("unlockedSlots");
        occupiedSlots = (int)jsonFile.GetNumber("occupiedSlots");

        isLocked = jsonFile.GetBoolean("isLocked");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        foreach (InventoryItem item in this) obj.Add(item.name, item.save());

        obj.Add("unlockedSlots", unlockedSlots);
        obj.Add("occupiedSlots", occupiedSlots);
        obj.Add("isLocked", isLocked);

        return obj;
    }

    private List<InventoryItem> getNLessLikely(int amount)
    {
        List<InventoryItem> result = new List<InventoryItem>();

        UnityEngine.Object[] lessLikely = new UnityEngine.Object[items.Count];
        items.CopyTo(lessLikely);

        lessLikely.OrderByDescending(element => ((InventoryItem)element).weigth);
        int startIndex = (int)(lessLikely.Length * 0.67);

        for (int i = 0; i < amount; i++)
        {
            InventoryItem it = (InventoryItem) lessLikely[UnityEngine.Random.Range(startIndex, lessLikely.Length)];
            result.Add(it);
        }

        return result;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Inventory)), CanEditMultipleObjects]
    public class InventoryEditor : LibraryEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
#endif
}

