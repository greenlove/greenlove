﻿using Boomlagoon.JSON;
using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Inventory/New Item")]
public class InventoryItem : ScriptableObject, IStorable
{
    public StringReference itemName;
    public DoubleReference amount;

    [Space(20)]

    public float weigth;
    public float accumulatedWeight;

    public int slot;
    private Inventory parent;
    public Color bgColor;

    public void init(Inventory inventory)
    {
        //default value
        slot = -1;
        amount.value = 0;

        accumulatedWeight = 0;
        parent = inventory;
    }

    public void load(JSONObject jsonFile)
    {
        amount.value = jsonFile.GetNumber("amount");
        slot = (int)jsonFile.GetNumber("slot");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("amount", amount.value);
        obj.Add("slot", slot);

        return obj;
    }

    internal void add(double amount)
    {
        if (slot < 0)
        {
            slot = parent.occupiedSlots;
            parent.occupiedSlots++;
        }
        this.amount.value += amount;
    }
}
