﻿using Boomlagoon.JSON;
using BuildingBlocks;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/New Alpha")]
public class Alpha : ScriptableObject, IStorable
{
    public AlphaState state;
    public int level;
    public double xp;
    private double nextXp;
    public double normalizedXp
    {
        get
        {
            return xp / nextXp;
        }
    }

    private float[] xps;

    public bool isHired
    {
        get
        {
            return slotIndex != -1;
        }
    }

    public Alphas alphaID;
    public Breed specie;

    public StringReference alphaName;
    public StringReference description;

    [HideInInspector]
    public int slotIndex;

    [HideInInspector]
    public Pack pack;

    [HideInInspector]
    public List<double> giftsTracker;
    private bool renderRequested;
    private float renderRequestedTime = 0.5f;
    private float renderRequestedBuffer;

    public int gifts
    {
        get
        {
            return giftsTracker == null ? 0 : giftsTracker.Count;
        }
    }

    [HideInInspector]
    public bool hasLeveledUpFlag;

    [HideInInspector]
    public GameplayController context;

    public Sequence waveSequence;

    public void init(GameplayController parent)
    {
        slotIndex = -1;
        context = parent;
        state = AlphaState.LOCKED;
        reset(parent);
        renderRequested = false;
        renderRequestedBuffer = 0;
        waveSequence = null;
    }

    public void reset(GameplayController parent)
    {
        pack = new Pack(this);
        pack.free += 1; //Add myself to the pack
        giftsTracker = new List<double>();
    }

    public void update(float dt)
    {
        if(renderRequested)
        {
            renderRequestedBuffer += dt;
            renderSlot();

            if (renderRequestedBuffer >= renderRequestedTime)
            {
                renderRequestedBuffer = 0;
                renderRequested = false;
            }
        }

        try { pack.updatePack(dt); }
        catch (NullReferenceException e)
        {
            Debug.LogError(string.Format("EL ERROR DEL ALPHA PACK NULL!!!!! {0}: {1}", e.Message, e.Source));
        }
    }


    public void addXp(double x)
    {
        level++;
        hasLeveledUpFlag = true;
    }


    public void addGift()
    {
        giftsTracker.Add(pack.amount);
    }


    private void findNextXP()
    {
        nextXp += (level+1) * Mathf.Log(level+1);
    }


    private float[] initXps()
    {
        return new float[]
        {
            20,    50,    100,   210,   330,   450,   570,   700,   900,    1000,   1200,   1500,   2000,   2500,   3200,   4000,   5000,   6000,   7500,   9000,   11000,   13000,  17000,   20000,
            25000, 32000, 40000, 50000, 60000, 70000, 80000, 90000, 110000, 130000, 150000, 170000, 200000, 250000, 300000, 360000, 450000, 550000, 660000, 800000, 1000000, 1750000
        };
    }

    public void finishMission()
    {
        pack.finishMission();
    }

    public void findWhatToDoNext()
    {
        if (pack.mission != null)
        {
            bool willDoReturn = context.returnFromMission(pack.mission);
            if(pack.mission.what != FlockState.SCOUTING && pack.mission.what != FlockState.RECRUITED) context.findController<UIController>().displayWavesOnAlpha(this);
            finishMission();
        }
    }

    public float getNormalizedServiceRemainingTime(AlphaMission mission)
    {
        float result = 0;

        switch(pack?.mission.what)
        {
            case FlockState.FEEDING:
                result = context.gameplayManager.city[CityGroup.TAVERN].getNormalizedRemainingTIme(mission);
                break;
            case FlockState.SLEEPING:
                result = context.gameplayManager.city[context.getOriginHome(alphaID)].getNormalizedRemainingTIme(mission);
                break;
            case FlockState.SHOWER:
                result = context.gameplayManager.city[CityGroup.BATH_HOUSE].getNormalizedRemainingTIme(mission);
                break;
            case FlockState.HEALING:
                result = context.gameplayManager.city[CityGroup.HOSPITAL].getNormalizedRemainingTIme(mission);
                break;
            case FlockState.PLAYING:
                result = context.gameplayManager.city[CityGroup.PLAYGROUND].getNormalizedRemainingTIme(mission);
                break;
        }

        return result;
    }

    internal float getServiceRemainingTime(AlphaMission mission)
    {
        float result = 0;

        switch (pack?.mission.what)
        {
            case FlockState.FEEDING:
                result = context.gameplayManager.city[CityGroup.TAVERN].getRemainingTIme(mission);
                break;
            case FlockState.SLEEPING:
                result = context.gameplayManager.city[context.getOriginHome(alphaID)].getRemainingTIme(mission);
                break;
            case FlockState.SHOWER:
                result = context.gameplayManager.city[CityGroup.BATH_HOUSE].getRemainingTIme(mission);
                break;
            case FlockState.HEALING:
                result = context.gameplayManager.city[CityGroup.HOSPITAL].getRemainingTIme(mission);
                break;
            case FlockState.PLAYING:
                result = context.gameplayManager.city[CityGroup.PLAYGROUND].getRemainingTIme(mission);
                break;
        }

        return result;
    }

    internal bool isDoingSomething()
    {
        return
            pack.mission?.what == FlockState.SHOWER ||
            pack.mission?.what == FlockState.SLEEPING ||
            pack.mission?.what == FlockState.FEEDING ||
            pack.mission?.what == FlockState.PLAYING ||
            pack.mission?.what == FlockState.HEALING ||
            pack.mission?.what == FlockState.SCOUTING;
    }

    internal void requestRender()
    {
        renderRequested = true;
    }

    internal void renderSlot()
    {
        if (slotIndex != -1)
        {
            context.gameplayManager.alphaSlots[slotIndex].stageCam.Render();
        }
    }

    public void load(JSONObject jsonFile)
    {
        state = jsonFile.GetEnum<AlphaState>("state");

        level = (int)jsonFile.GetNumber("level");
        xp = jsonFile.GetNumber("xp");
        
        slotIndex = (int)jsonFile.GetNumber("slot");

        pack.load(jsonFile.GetObject("pack"));

        JSONArray gifts = jsonFile.GetArray("gifts");

        for (int i = 0; i < gifts.Length; i++)
        {
            giftsTracker.Add((int)gifts[i].Number);
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("state", state.ToString());
        obj.Add("level", level);
        obj.Add("xp", xp);
        obj.Add("slot", slotIndex);
        obj.Add("pack", pack.save());

        JSONArray gifts = new JSONArray();
        foreach (double i in giftsTracker) gifts.Add(i);
        obj.Add("gifts", gifts);

        return obj;
    }
}

public enum AlphaState
{
    NONE = -1,

    //Don't add states above here
    LOCKED,
    UNLOCKED,
    ASSIGNED,
    //Don't add states below this

    LENGTH
}