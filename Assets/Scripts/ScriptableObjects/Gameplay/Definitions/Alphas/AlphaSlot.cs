﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Alphas/New Alpha Slot")]
public class AlphaSlot : ScriptableObject, IStorable
{
    [HideInInspector]
    public int id;

    public Alpha alpha;

    public AlphaListViewState state;

    public RenderTexture renderTexture;
    public Camera stageCam;

    public Animator alphaAnimator;

    [HideInInspector]
    public UIAlphaListItem ui;

    [HideInInspector]
    public UIAlphaListItemActionButton uiAction;

    public void init(int ID)
    {
        id = ID;
        state = AlphaListViewState.LOCKED;
        uiAction = null;
        stageCam = Array.Find(GameObject.FindGameObjectsWithTag("PetCam"), element => element.name == string.Format("Camera0{0}", id)).GetComponent<Camera>();
        alphaAnimator = Array.Find(GameObject.FindGameObjectsWithTag("PetModel"), element => element.name == alpha.name).GetComponent<Animator>();
    }

    public void load(JSONObject jsonFile)
    {
        state = jsonFile.GetEnum<AlphaListViewState>("state");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        obj.Add("state", state.ToString());

        return obj;
    }
}