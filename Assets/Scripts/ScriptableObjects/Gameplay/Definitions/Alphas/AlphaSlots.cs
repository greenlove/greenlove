﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/Alphas/New Alpha Slots Library")]
public class AlphaSlots : Library, IStorable
{

    private int assignedTrackerIndex;
    private int[] assignedTracker;//An array showing in which positions are the alphas assigned
    private System.Random random;
    public int selectedSlot;
    
    public AlphaSlot current
    {
        get
        {
            return selectedSlot == -1 ? null : this[selectedSlot];
        }
    }

    public void init()
    {
        reset();
        for (int i = 0; i < Count; i++) this[i].init(i);
        random = new System.Random();
    }

    public void reset()
    {
        assignedTrackerIndex = -1;
        selectedSlot = -1;
        assignedTracker = new int[items.Count];
        for (int i = 0; i < assignedTracker.Length; i++) assignedTracker[i] = -1;
    }

    public new AlphaSlot this[int key]
    {
        get
        {
            return (AlphaSlot)items[key];
        }
        set
        {
            items[key] = value;
        }
    }

    public bool isAnyLocked()
    {
        return howManyLocked() > 0;
    }

    public bool isAnyUnassigned()
    {
        return howManyUnassigned() > 0;
    }

    public bool isAnyAssigned()
    {
        return howManyAssigned() > 0;
    }

    //Can be optimized
    public int howManyLocked()
    {
        return items.FindAll(element => ((AlphaSlot)element).state == AlphaListViewState.LOCKED).Count;
    }

    public int howManyUnassigned()
    {
        return items.FindAll(element => ((AlphaSlot)element).state == AlphaListViewState.EMPTY).Count;
    }

    public int howManyAssigned()
    {
        return items.FindAll(element => ((AlphaSlot)element).state == AlphaListViewState.ASSIGNED).Count;
    }


    public void assign(int slotID)
    {
        //If assigned, put into the tracker
        if(!Array.Exists(assignedTracker, element => element.Equals(slotID)))
        {
            assignedTrackerIndex++;
            assignedTracker[assignedTrackerIndex] = slotID;
        }
    }

    public int getRandomAssignedIndex()
    {
        return random.Next(0, assignedTrackerIndex + 1);
    }


    internal AlphaSlot getNextFrom(AlphaSlot slot)
    {
        int index = items.IndexOf(slot);
        if (index+1 >= Count) return null;

        return (AlphaSlot)items[index + 1];
    }


    public void selectNext()
    {
        bool found = false;
        int i = selectedSlot;

        while (!found)
        {
            i++;
            if (i >= Count) i = 0;

            if (this[i].alpha != null)
            {
                selectedSlot = i;
                found = true;
            }
        }
    }

    public void selectPrevious()
    {
        bool found = false;
        int i = selectedSlot;

        while (!found)
        {
            i--;
            if (i < 0) i = Count - 1;

            if (this[i].alpha != null)
            {
                selectedSlot = i;
                found = true;
            }
        }
    }

    public void load(JSONObject jsonFile)
    {
        assignedTrackerIndex = (int)jsonFile.GetNumber("index");
        JSONArray tracks = jsonFile.GetArray("tracker");
        for (int i = 0; i < tracks.Length; i++)
        {
            assignedTracker[i] = (int)tracks[i].Number;
        }

        foreach (AlphaSlot slot in this)
        {
            slot.load(jsonFile.GetObject(slot.name));
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("index", assignedTrackerIndex);
        JSONArray tracks = new JSONArray();
        for (int i = 0; i < assignedTracker.Length; i++)
        {
            tracks.Add(assignedTracker[i]);
        }
        obj.Add("tracker", tracks);

        foreach (AlphaSlot slot in this)
        {
            obj.Add(slot.name, slot.save());
        }

        return obj;
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(AlphaSlots)), CanEditMultipleObjects]
    public class AlphaSlotsEditor : LibraryEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }

#endif
}

