﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boomlagoon.JSON;
using UnityEngine;

public class Pack : IStorable
{

    public PackMood mood;
    public string moodKey
    {
        get
        {
            string returnable = "";

            if (amount <= 0)
            {
                returnable = "PACK_STATE_EMPTY";
            }
            else if (mission != null)
            {
                if (mission.what == FlockState.FEEDING)
                {
                    returnable = "PACK_STATE_EATING";
                }
                else if (mission.what == FlockState.SHOWER)
                {
                    returnable = "PACK_STATE_SHOWERING";
                }
                else if (mission.what == FlockState.SCOUTING)
                {
                    returnable = "PACK_STATE_SCOUTING";
                }
                else if (mission.what == FlockState.SLEEPING)
                {
                    returnable = "PACK_STATE_SLEEPING";
                }
                else if (mission.what == FlockState.HEALING)
                {
                    returnable = "PACK_STATE_HEALING";
                }
                else if (mission.what == FlockState.PLAYING)
                {
                    returnable = "PACK_STATE_PLAYING";
                }
            }
            else
            {
                returnable = "PACK_STATE_HOME";
                if (mood > PackMood.HAPPY) returnable = string.Format("PACK_STATE_{0}", mood.ToString());
            }

            return returnable;
        }
    }

    public double free;
    public AlphaMission mission;

    public float minMissionTime = 45;
    public float moodChangeTimeInSeconds = 120;
    private float moodChangeBuffer = 0;

    private Alpha lead;
    private bool lockedUntilReturnIsDone;

    public double amount
    {
        get
        {
            double returnable = free;

            if(mission != null)
            {
                returnable = free + mission.amount;
            }

            return returnable;
        }
    }

    public bool isOnMission
    {
        get
        {
            return mission != null;
        }
    }

    public Pack(Alpha a)
    {
        free = 0;
        lead = a;
        moodChangeBuffer = 0;
        lockedUntilReturnIsDone = false;
        mood = PackMood.HAPPY;
        mission = null;
    }

    public bool sendToMission()
    {
        bool canBeSentToMission = false;

        if (mission == null)
        {
            //Define the deployed amount of animals
            double animalsOnMission = free;

            //Create a new mission object
            mission = lead.context.gameplayManager.createNewMission();
            mission.init(animalsOnMission, getMissionType(), lead.alphaID);
            mission.setTotalRepresented(Math.Min(10, (int)animalsOnMission));

            free -= animalsOnMission;

            mission.elapsedTime = 0;

            canBeSentToMission = true;

            updateMissionStats();
        }

        return canBeSentToMission;
    }

    private void updateMissionStats()
    {
        lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_MISSION, 1, BankUpdateStrategy.ADD);

        switch (mission.what)
        {
            case FlockState.SCOUTING:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_SCOUT, 1, BankUpdateStrategy.ADD);
                break;
            case FlockState.FEEDING:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_EAT, 1, BankUpdateStrategy.ADD);
                break;
            case FlockState.SLEEPING:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_SLEEP, 1, BankUpdateStrategy.ADD);
                break;
            case FlockState.SHOWER:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_BATH, 1, BankUpdateStrategy.ADD);
                break;
            case FlockState.PLAYING:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_PLAY, 1, BankUpdateStrategy.ADD);
                break;
            case FlockState.HEALING:
                lead.context.gameplayManager.bank.update(Currency.TIMES_SENT_TO_VET, 1, BankUpdateStrategy.ADD);
                break;
        }
    }

    public FlockState getMissionType()
    {
        FlockState mission = FlockState.NONE;

        switch (mood)
        {
            case PackMood.HUNGRY:
                mission = FlockState.FEEDING;
                break;
            case PackMood.DIRTY:
                mission = FlockState.SHOWER;
                break;
            case PackMood.SLEEPY:
                mission = FlockState.SLEEPING;
                break;
            case PackMood.BORED:
                mission = FlockState.PLAYING;
                break;
            case PackMood.ILL:
                mission = FlockState.HEALING;
                break;
            default:
                mission = FlockState.SCOUTING;
                break;
        }

        return mission;
    }

    public void updatePack(float dt)
    {
        if (lead.isHired)
        {
            if (!lockedUntilReturnIsDone)
            {
                if(mission != null)
                {
                    updateScouting(dt);
                }
            }
            updateMood(dt);
        }
    }

    private void updateScouting(float dt)
    {
        if (mission.what != FlockState.RECRUITED)
        {
            updateMissionElapsedTime(mission, dt);
            if (getRemainingMissionTime() <= 0)
            {
                FlockState purpose = mission.what;

                lead.findWhatToDoNext();
                //lockedUntilReturnIsDone = true;

                if (purpose != FlockState.SCOUTING)
                {
                    moodChangeBuffer = 0;
                    mood = PackMood.HAPPY;
                    lead.context.tutorialManager.launchTutorial(Tutorials.UPGRADE_HOME_TUTORIAL);
                }
            }
        }
    }

    //Update dt with a check for ftue to make it faster
    public void updateMissionElapsedTime(AlphaMission mission, float dt)
    {
        if (mission.isPaused) return;

        float delta = dt;
        if (!lead.context.tutorialManager.hasAllBeenDisplayed(Tutorials.STOCK_RESTAURANT_TUTORIAL)) delta = delta * 4;
#if UNITY_EDITOR || GREENLOVE_DEBUG
        if (lead.context.findController<CheatsController>().shortMissionTimes) delta = delta * 20;
#endif
        mission.elapsedTime += delta;
    }

    private void updateMood(float dt)
    {
        if(!lead.context.tutorialManager.hasAllBeenDisplayed(Tutorials.BUILD_RESTAURANT_TUTORIAL)) return;

        moodChangeBuffer += dt;
        if(moodChangeBuffer >= moodChangeTimeInSeconds)
        {
            moodChangeBuffer = 0;
            if(mood == PackMood.HAPPY)
            {
                mood = getMoody();
                lead.requestRender();
            }
        }
    }

    //Method to generate a random mood different than happy
    private PackMood getMoody()
    {
        PackMood mood = PackMood.SLEEPY;

        if (lead.context.gameplayManager.bank[Currency.TIMES_SENT_TO_SCOUT].value > 2)
        {
            mood = (PackMood)UnityEngine.Random.Range(
                (int)PackMood.HAPPY + 1, 
                Math.Min((int)PackMood.LENGTH, (int)lead.context.gameplayManager.maxAvailableMood+2));

            lead.context.findController<ServerController>().sendAnalyticWithParams("AlphaStateChange", "AlphaID".pair(lead.alphaID.ToString()), "StateID".pair(mood.ToString()));
        }

        if (mission == null)
        {
            lead.context.findController<UIController>().displayWavesOnAlpha(lead);
            lead.context.findController<AudioController>().playSound("tired_dog");
        }
        return mood;
    }

    public float getNormalizedRemainingMissionTime()
    {
        float time = 0;

        if(mission != null)
        {
            if (mission.what == FlockState.SCOUTING)
            {
                float div = mission.elapsedTime / minMissionTime;
                time = div <= 0 ? 0 : 1 - div;
            }
            else
            {
                time = lead.getNormalizedServiceRemainingTime(mission);
            }
        }
        return time;
    }

    public float getRemainingMissionTime()
    {
        float time = 0;

        if (mission != null)
        {
            if (mission.what == FlockState.SCOUTING)
            {
                time = (minMissionTime * (1.0f - lead.context.flockManager.flockSpeedMultiplier)) - mission.elapsedTime;
            }
            else
            {
                time = lead.getServiceRemainingTime(mission);
            }
        }
        return time;
    }

    public string getFormattedRemainingMissionTime()
    {
        string result = "...";
        if(mission != null && getRemainingMissionTime() > 0)
        {
            result = TimeSpan.FromSeconds(getRemainingMissionTime()).ToString(@"mm\:ss");
        }
        return result;
    }


    public void finishMission()
    {
        if (mission != null)
        {
            if (!mission.isPrologueFinished)
            {
                mission.isPrologueFinished = true;
                return;
            }

            //Return the mission to the pool
            lead.context.gameplayManager.pushBackMission(mission);

            free += mission.amount;


            if (mission.what == FlockState.SCOUTING)
            {
                lead.addGift();
                lead.context.gameplayManager.bank.update(Currency.GIFTS_OFFERED, 1, BankUpdateStrategy.ADD);
                if (lead.context.gameplayManager.bank[Currency.GIFTS_OFFERED].value <= 1)
                {
                    lead.context.tutorialManager.launchTutorial(Tutorials.PICK_GIFT_TUTORIAL);
                }
                else
                {
                    lead.context.tutorialManager.launchTutorial(Tutorials.PICK_GIFT_WITH_ITEMS_TUTORIAL);
                }
            }
            else if(mission.what == FlockState.SLEEPING)
            {
                lead.context.tutorialManager.launchTutorial(Tutorials.UPGRADE_HOME_TUTORIAL);
            }

            //Set mission to null here
            mission = null;
            lockedUntilReturnIsDone = false;

            lead.requestRender();
        }
    }

    internal string getActionVerb()
    {
        string result = "";
        switch (mood)
        {
            case PackMood.HUNGRY:
                result = "TAVERN_VERB";
                break;
            case PackMood.DIRTY:
                result = "BATH_HOUSE_VERB";
                break;
            case PackMood.SLEEPY:
                result = "SLEEP_VERB";
                break;
            case PackMood.BORED:
                result = "PLAYGROUND_VERB";
                break;
            case PackMood.ILL:
                result = "HOSPITAL_VERB";
                break;
            default:
                result = "SCOUTING_VERB";
                break;
        }

        return result;
    }

    public void load(JSONObject jsonFile)
    {
        mood = jsonFile.GetEnum<PackMood>("mood");
        free = (int)jsonFile.GetNumber("amount");
        if(jsonFile.ContainsKey("mission"))
        {
            mission = new AlphaMission();
            mission.load(jsonFile.GetObject("mission"));
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("mood", mood.ToString());
        obj.Add("amount", free);
        if (mission != null) obj.Add("mission", mission.save());

        return obj;
    }
}

public enum PackMood
{
    NONE = 0,

    //Add states from here
    //Careful, order matters, don't change it lightly
    HAPPY,
    SLEEPY,
    HUNGRY,
    DIRTY,
    BORED,
    ILL,
    //Stop adding from here
    LENGTH
}