﻿using Boomlagoon.JSON;
using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaMission : IStorable
{
    private Guid missionID;
    public double amount;

    public int totalRepresented { get; private set; }
    private int totalRepresentedMirror;
    public Vector3 destination;
    public Vector3 origin;

    public double singleRepresented
    {
        get
        {
            return amount / totalRepresented;
        }
    }

    public FlockState what { get; private set; }

    public Alphas lead;

    public bool isPaused;

    public float elapsedTime;
    private float pausedTime;

    //Prologue is the first part of a mission which is going, the next part (epilogue) is returning
    public bool isPrologueFinished;

    public AlphaMission()
    {
        init();
    }

    public void init(double am, FlockState w, Alphas l)
    {
        init();

        amount = am;
        what = w;
        lead = l;
    }

    private void init()
    {
        amount = 0;
        what = FlockState.NONE;
        lead = Alphas.NONE;
        totalRepresented = 0;
        totalRepresentedMirror = 0;
        elapsedTime = 0;
        pausedTime = 0;
        isPaused = false;
        isPrologueFinished = false;
        missionID = Guid.NewGuid();
    }

    public void setTotalRepresented(int num)
    {
        totalRepresented = num;
        totalRepresentedMirror = num;
    }

    public void updateState(FlockState newState)
    {
        what = newState;
    }

    public void pauseMission()
    {
        isPaused = true;
        pausedTime = elapsedTime;
    }

    public void resumeMission()
    {
        elapsedTime = pausedTime;
        pausedTime = 0;
        isPaused = false;
    }

    public bool subtractOneMissionMember()
    {
        totalRepresentedMirror--;
        return totalRepresentedMirror <= 0; 
    }

    public bool isSameMission(AlphaMission theOtherMission)
    {
        return missionID.Equals(theOtherMission?.missionID);
    }

    public Vector3 getDestinationByContext()
    {
        return isPrologueFinished ? origin : destination;
    }

    public float distanceToDestination(GameObject gameObject)
    {
        return Vector3.Distance(gameObject.transform.position, getDestinationByContext());
    }

    public void load(JSONObject jsonFile)
    {
        missionID = Guid.Parse(jsonFile.GetString("id"));
        amount = jsonFile.GetNumber("amount");
        totalRepresented = (int)jsonFile.GetNumber("represented");
        origin.FromJSONObject(jsonFile.GetObject("origin"));
        destination.FromJSONObject(jsonFile.GetObject("destination"));
        what = jsonFile.GetEnum<FlockState>("mission_type");
        lead = jsonFile.GetEnum<Alphas>("lead");
        isPaused = jsonFile.GetBoolean("paused");
        elapsedTime = (float)jsonFile.GetNumber("elapsed_time");
        if (jsonFile.ContainsKey("paused_time")) pausedTime = (float)jsonFile.GetNumber("paused_time");
        isPrologueFinished = jsonFile.GetBoolean("prologue");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("id", missionID.ToString());
        obj.Add("amount", amount);
        obj.Add("represented", totalRepresented);
        obj.Add("origin", origin.ToJSONObject());
        obj.Add("destination", destination.ToJSONObject());
        obj.Add("mission_type", what.ToString());
        obj.Add("lead", lead.ToString());
        obj.Add("paused", isPaused);
        obj.Add("elapsed_time", elapsedTime);
        if (isPaused) obj.Add("paused_time", pausedTime);
        obj.Add("prologue", isPrologueFinished);

        return obj;
    }
}
