﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Market")]
public class Market : Vehicle
{
    private Vector3 desiredPosition;
    public Vector3 startPosition;
    private bool checkArrived;
    private bool isDeployed;

    public override void init(GameplayController g, CityGroup group)
    {
        base.init(g, CityGroup.MARKET);
        isSteering = false;
        hasArrived = false;
        checkArrived = false;
        isDeployed = false;
    }

    public override void update(float dt)
    {
        base.update(dt);

        if(checkArrived) hasArrived = arrived();

        if (hasArrived)
        {
            NavMeshObstacle obstacle = obj.AddComponent<NavMeshObstacle>();
            obstacle.carving = true;

            context.gameplayManager.bank.update(Currency.MARKET_ARRIVED_TIMES, 1, BankUpdateStrategy.ADD);

            context.findController<AudioController>().play3DSound("market", obj.transform.position);

            checkArrived = false;
            hasArrived = false; //Stop iterating here
        }
    }

    public void deploy()
    {
        if (isDeployed) return;
        isDeployed = true;

        obj = Instantiate(getActiveSegment().buildingEvo);
        obj.name = name;

        obj.GetComponent<Animator>().Play("MarketMovement");
        checkArrived = true;
    }

    public bool arrived()
    {
        Animator animator = obj.GetComponent<Animator>();
        return animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f;
    }

    public void launchMarketTutorial()
    {
        context.tutorialManager.launchTutorial(Tutorials.BOOSTS_TUTORIAL);
    }

    public override void destroyObject()
    {
        base.destroyObject();
        isDeployed = false;
    }
}
