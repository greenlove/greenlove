﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : Building
{
    public SteeringAgent steer;
    public float maxSpeed;
    public float arrivedDistance;
    protected bool hasArrived;
    protected bool isSteering;
    public GameObject obj;
    protected Vector3 target;

    public override void init(GameplayController g, CityGroup ID)
    {
        steer = new SteeringAgent();
        steer.maxVelocity = maxSpeed;
        isSteering = false;
        hasArrived = false;
        steer.init(maxSpeed, getActiveSegment().buildingEvo.GetComponent<MeshFilter>().sharedMesh.bounds.size.magnitude);
        base.init(g, ID);
    }

    public virtual void startSteering(GameObject b)
    {
        isSteering = true;
        obj = b;
    }

    public override void update(float dt)
    {
        base.update(dt);

        if (isSteering)
        {
            float distance = Vector3.Distance(obj.transform.position, target);
            hasArrived = distance <= arrivedDistance;
            if(hasArrived) isSteering = false;
        }
    }

    public void stopSteering()
    {
        steer.pause();
    }

    public void resumeSteering()
    {
        steer.play();
    }

    public virtual void destroyObject()
    {
        if(obj != null) Destroy(obj);
    }
}
