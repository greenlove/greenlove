﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Excavator")]
public class Excavator : Building
{
    public Building neededBuildingFlag;
    public Building lastUnlockedBuilding;

    public override void init(GameplayController g, CityGroup ID)
    {
        base.init(g, ID);
        neededBuildingFlag = null;
        lastUnlockedBuilding = context.gameplayManager.city[CityGroup.HOME00];
    }

    public override JSONObject save()
    {
        JSONObject obj = base.save();
        obj.Add("last_building", lastUnlockedBuilding.id.ToString());

        return obj;
    }

    public override void load(JSONObject jsonFile)
    {
        base.load(jsonFile);

        CityGroup lastBuilding = CityGroup.NONE;
        Enum.TryParse(jsonFile.GetString("last_building", "NONE"), out lastBuilding);

        if(lastBuilding == CityGroup.NONE)
        {
            for (CityGroup i = CityGroup.TAVERN; i < CityGroup.MARKET; i++)
            {
                if(context.gameplayManager.city[i].isPurchased()) lastBuilding = i;
            }
        }

        lastUnlockedBuilding = context.gameplayManager.city[lastBuilding];
    }
}
