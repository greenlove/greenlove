﻿using Boomlagoon.JSON;
using BuildingBlocks;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BuildingSegment : IStorable, ILocalizable
{
    [HideInInspector]
    public int index;

    public bool isPurchased;
    public double price;
    public GameObject buildingEvo;

    public double stockConstant;
    public double stockAmount;
    public double stockPrice;

    public double capacity;

    [HideInInspector] public int rangeSteps;
    public int currentStep;
    public Vector2Int range;

    [HideInInspector] public string[] upgradeTitles;
    [HideInInspector] public string[] upgradeDescriptions;

    private Building parent;
    private LocalizationController loc;

    public void init(Building parent)
    {
        index = Array.IndexOf(parent.segments, this);
        isPurchased = false;
        this.parent = parent;
        rangeSteps = 100;
        currentStep = 0;
        stockAmount = stockConstant;

        loc = parent.context.findController<LocalizationController>();
        subscribeToLangChanges(loc);
        setupTexts();
    }

    public string iconName { get { return string.Format("{0}_0{1}_0{2}", parent.name.ToUpper(), index + 1, parent.context.findController<ColorizableController>().currentColorMode.value); } } 

    private void setupTexts()
    {
        upgradeTitles = new string[]
        {
            loc.getTextByKey(string.Format("{0}_NAME_0{1}", parent.textsID, (index * 2))),
            loc.getTextByKey(string.Format("{0}_NAME_0{1}", parent.textsID, (index * 2) + 1))
        };

        upgradeDescriptions = new string[]
        {
            loc.getTextByKey(string.Format("{0}_DESC_0{1}", parent.textsID, (index * 2))),
            loc.getTextByKey(string.Format("{0}_DESC_0{1}", parent.textsID, (index * 2) + 1))
        };
    }

    public float getStepNormalized()
    {
        return (float)currentStep / rangeSteps;
    }

    public void refillStock(bool canAfford)
    {
        parent.context.findController<ServerController>().sendAnalyticWithParams(canAfford ? "SpendSC" : "LackSC", "SCProduct".pair("BuildingStock"), "BuildingID".pair(parent.id.ToString()), "Amount".pair(stockPrice.ToString()));

        if (!canAfford) return;

        parent.context.gameplayManager.bank.update(Currency.REPLENISHED_STOCKS, 1, BankUpdateStrategy.ADD);
        parent.context.gameplayManager.bank.update(Currency.GOLD, stockPrice, BankUpdateStrategy.SUB);
        stockAmount = stockConstant;
    }

    public bool isStockReplenished()
    {
        return stockAmount == stockConstant;
    }

    public void load(JSONObject jsonFile)
    {
        isPurchased = jsonFile.GetBoolean("isPurchased");
        stockAmount = Math.Min(stockConstant, jsonFile.GetNumber("stock"));
        currentStep = (int)jsonFile.GetNumber("step");
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        
        obj.Add("isPurchased", isPurchased);
        obj.Add("stock", stockAmount);
        obj.Add("step", currentStep);

        return obj;
    }

    public void onLangChanged(LocalizationController locController = null)
    {
        setupTexts();
    }

    public void subscribeToLangChanges(LocalizationController locController = null)
    {
        locController.subscribeToLocListening(this);
    }

    public void unsubscribeToLangChanges()
    {
        loc.unsubscribeToLocListening(this);
    }
}
