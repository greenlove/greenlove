﻿using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Home")]
public class Home : Building
{
    public AlphaSlot alphaSlot;
    public Vector3 homeDestination;
    public Material material;

    public override void update(float dt)
    {
        base.update(dt);

        if(!canHostMore(1) && warningParticle == null)
        {
            context.gameplayManager.displayWarningParticleOverBuilding(this);
            context.tutorialManager.launchTutorial(Tutorials.FULL_HOME_TUTORIAL);
        }
    }

    private bool hasSpace()
    {
        return alphaSlot.alpha.pack.amount < getActiveSegment().capacity;
    }

    public bool canHost(double amount)
    {
        return amount < getActiveSegment().capacity; 
    }

    public bool canHostMore(double amount)
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        if (context.findController<CheatsController>().limitlessCapacity) return true;
#endif
        return alphaSlot.alpha.pack.amount + amount < getActiveSegment().capacity;
    }

    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        homeDestination = slot.buildingView.transform.position;
        fixColliderName();

        base.onBuilt(slot, forceBuild);

        setViewMaterial();

        if (forceBuild) return;
        context.hireAlpha(alphaSlot);
    }

    public override void onEvolved()
    {
        fixColliderName();
        homeDestination = GameObject.Find(name).transform.position;
        base.onEvolved();
        setViewMaterial();
    }

    private void fixColliderName()
    {
        for (int i = 0; i < slot.buildingView.transform.childCount; i++)
        {
            GameObject item = slot.buildingView.transform.GetChild(i).gameObject;
            if (item.name.Contains("Collider"))
            {
                item.name = name + "Collider";
            }
        }
    }

    private void setViewMaterial()
    {
        slot.buildingView.GetComponent<MeshRenderer>().material = material;
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(Building)), CanEditMultipleObjects]
    public class HomeEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            Building home = target as Building;
            DrawDefaultInspector();
            
        }
    }
#endif
}
