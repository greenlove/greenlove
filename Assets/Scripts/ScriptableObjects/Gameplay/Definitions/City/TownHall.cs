﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/TownHall")]
public class TownHall : Building
{
    public override void init(GameplayController g, CityGroup ID)
    {
        base.init(g, ID);
        if (g.gameplayManager.bank[Currency.PRESTIGES].value > 0) isBuildable = false;
    }

    public override void update(float dt)
    {
        base.update(dt);

        if (context.tutorialManager.hasAllBeenDisplayed(Tutorials.CRAFT_BUFF_TUTORIAL) && canCraftBuff())
        {
            context.gameplayManager.displayWarningParticleOverBuilding(this);
        }
        else
        {
            Destroy(warningParticle);
        }
    }

    public bool canCraftBuff()
    {
        bool result = false;
        foreach(Buff buff in context.gameplayManager.buffs)
        {
            if(buff.canUpgradeRecipe() && context.gameplayManager.bank.canAfford(Currency.GOLD, buff.getUpgradePriceByLevel()))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        base.onBuilt(slot, forceBuild);

        if (forceBuild)
        {
            context.tutorialManager.launchHeadlessTutorial(Tutorials.CRAFT_BUFF_TUTORIAL);
        }
        else
        {
            context.tutorialManager.launchTutorial(Tutorials.CRAFT_BUFF_TUTORIAL, false, 0.5f);
        }
    }
}
