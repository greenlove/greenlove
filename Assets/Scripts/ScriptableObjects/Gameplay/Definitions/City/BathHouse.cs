﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Bath house")]
public class BathHouse : Building
{
    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        base.onBuilt(slot, forceBuild);
        context.gameplayManager.maxAvailableMood = PackMood.DIRTY;

        if (forceBuild) return;

        context.gameplayManager.bank.update(Currency.BUILT_BATH_HOUSES, 1, BankUpdateStrategy.ADD);
        context.gameplayManager.buffs[Buffs.MORNING_COFFEE].makeVisible();
    }

    internal override double getStockPrice()
    {
        return base.getStockPrice() * ((BuffMorningCoffee)context.gameplayManager.buffs[Buffs.MORNING_COFFEE]).getDiscount();
    }
}
