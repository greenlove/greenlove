﻿using Boomlagoon.JSON;
using BuildingBlocks;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Sirenix.OdinInspector;


[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/New Generic Building")]
public class Building : ScriptableObject, IStorable
{
    public string textsID;
    public bool isSpawner;
    public bool isBuildable;
    public bool isDestructibleOnPrestige;

    [HideInInspector]
    public Vector3 position;

    [SerializeField]
    protected double basePrice;

    [SerializeField]
    protected float priceMultiplier;

    [SerializeField]
    protected LocalizationController locController;

    [HideInInspector]
    public GameplayController context;

    [HideInInspector]
    public GameObject warningParticle;

    public int activeSegment;
    public BuildingSegment[] segments;
    internal CityGroup id;

    public BuildingSlot slot;

    [HideInInspector]
    public bool hasNoStockFlag;

    [HideInInspector]
    public bool hasNoCapacityFlag;

    public virtual void init(GameplayController g, CityGroup ID)
    {
        context = g;
        reset();
        warningParticle = null;
        this.id = ID;

        hasNoStockFlag = false;
        hasNoCapacityFlag = false;

        if (isSpawner)
        {
            segments = new BuildingSegment[] { new BuildingSegment() };
            segments[0].buildingEvo = GameObject.Find(name);
        }
    }


    public double getNextLevelPrice()
    {
        return basePrice * priceMultiplier * ((getActiveSegment().currentStep+1) + 100 * activeSegment);
    }

    protected virtual double getNextLevelPrice(int level)
    {
        return basePrice * Mathf.Pow(priceMultiplier, level);
    }

    public virtual void update(float dt)
    {
        if(isSpawner)
        {
            position = segments[0].buildingEvo.transform.position;
        }
    }

    public virtual bool upgradeServiceTime(Bank bank, bool canAfford, bool withEvent = false)
    {
        bool success = false;
        double nextPrice = getNextLevelPrice();

        context.findController<ServerController>().sendAnalyticWithParams(canAfford ? "SpendSC" : "LackSC", "SCProduct".pair("UpgradeBuilding"), "BuildingID".pair(id.ToString()), "Amount".pair(nextPrice.ToString()));
        context.findController<ServerController>().sendAnalyticWithParams("UpgradeBuilding", "BuildingID".pair(id.ToString()), "Amount".pair(nextPrice.ToString()));

        if (!canAfford) return success;

        success = true;

        bank.update(Currency.GOLD, nextPrice, BankUpdateStrategy.SUB);
        BuildingSegment segment = getActiveSegment();

        if (segment.currentStep < segment.rangeSteps)
        {
            segment.currentStep++;
        }
        
        context.gameplayManager.bank.update(Currency.HOME_ENLARGEMENTS, 1, BankUpdateStrategy.ADD);

        Alpha onBuildingAlpha = context.gameplayManager.findAlphaWithMood(context.gameplayManager.findMoodByBuilding(id));
        if(onBuildingAlpha != null) onBuildingAlpha.pack.updateMissionElapsedTime(onBuildingAlpha.pack.mission, 1.0f);
        
        return success;
    }

    internal float getRemainingTIme(AlphaMission mission)
    {
        return (float)currentServiceTime() - mission.elapsedTime;
    }

    internal float getNormalizedRemainingTIme(AlphaMission mission)
    {
        return getRemainingTIme(mission) / (float)currentServiceTime();
    }

    public virtual void reset()
    {
        if (isSpawner) return;

        activeSegment = 0;

        for (int i = 0; i < segments.Length; i++)
        {
            segments[i].init(this);
        }

        dispelWarningParticle();
    }


    public virtual void onBuilt(BuildingSlot slot,  bool forceBuild)
    {
        this.slot = slot;
        segments[0].isPurchased = true;
    }

    public virtual void onEvolved()
    {
    }

    public bool isPurchased()
    {
        bool isPurchased = false;

        if(segments != null)
        {
            if (isSpawner) isPurchased = true;
            else
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    isPurchased |= segments[i].isPurchased;
                }
            }
        }

        return isPurchased;
    }

    internal BuildingSegment getActiveSegment()
    {
        return segments[activeSegment];
    }

    internal double currentServiceTime()
    {
        return Mathf.Lerp(getActiveSegment().range.x, getActiveSegment().range.y, getActiveSegment().getStepNormalized());
    }

    public string getCleanIDName()
    {
        return Regex.Replace(id.ToString(), @"[\d-]", string.Empty);
    }

    public virtual void load(JSONObject jsonFile)
    {
        if (jsonFile.ContainsKey("position")) position = position.FromJSONObject(jsonFile.GetObject("position"));

        activeSegment = (int)jsonFile.GetNumber("activeSegment", 0);
        if(segments.Length > 0)
            for (int i = activeSegment; i >= 0; i--) segments[i].isPurchased = true;
        

        if(jsonFile.ContainsKey("segment")) getActiveSegment().load(jsonFile.GetObject("segment"));
    }

    public virtual JSONObject save()
    {
        JSONObject obj = new JSONObject();

        if(isBuildable && isPurchased()) obj.Add("position", position.ToJSONObject());
        obj.Add("activeSegment", activeSegment);
        if(segments.Length > 0) obj.Add("segment", getActiveSegment().save());

        return obj;
    }

    public void displayWarningParticle()
    {
        context.gameplayManager.displayWarningParticleOverBuilding(this);
    }

    public void dispelWarningParticle()
    {
        Destroy(warningParticle);
    }

    internal virtual double getStockPrice()
    {
        return getActiveSegment().stockPrice;
    }
}
