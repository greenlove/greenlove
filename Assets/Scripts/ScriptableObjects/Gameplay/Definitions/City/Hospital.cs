﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Hospital")]
public class Hospital : Building
{
    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        base.onBuilt(slot, forceBuild);
        context.gameplayManager.maxAvailableMood = PackMood.ILL;

        if (forceBuild) return;

        context.gameplayManager.bank.update(Currency.BUILT_HOSPITALS, 1, BankUpdateStrategy.ADD);
        context.gameplayManager.buffs[Buffs.FUNKY_JUNK].makeVisible();
    }

    internal override double getStockPrice()
    {
        return base.getStockPrice() * ((BuffFunkyJunk)context.gameplayManager.buffs[Buffs.FUNKY_JUNK]).getDiscount();
    }
}
