﻿using Boomlagoon.JSON;
using BuildingBlocks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/New Empty City")]
public class City : Library, IStorable {

    public Building this[CityGroup key]
    {
        get
        {
            return (Building)items[(int)key];
        }
        set
        {
            items[(int)key] = value;
        }
    }
    

    public void init(GameplayController context)
    {
        for (int i = 0; i < this.Count; i++)
        {
            ((Building)this[i]).init(context, (CityGroup)i);
        }

        this[CityGroup.HOME00].segments[0].isPurchased = true;
        //Add builder
    }

    public void load(JSONObject jsonFile)
    {
        for (int i = 0; i < this.Count; i++)
        {
            Building building = (Building)this[i];
            if (!building.isSpawner && building.id != CityGroup.EXCAVATOR) building.load(jsonFile.GetObject(this[i].name));
        }

        this[CityGroup.EXCAVATOR].load(jsonFile.GetObject("Excavator"));
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        for (int i = 0; i < Count; i++)
        {
            if (!this[0].isSpawner) obj.Add(this[i].name, ((Building)this[i]).save());
        }

        return obj;
    }

    internal bool areAllHomesPurchased()
    {
        bool isPurchased = false;
        for (int i = (int)CityGroup.HOME00; i <= (int)CityGroup.HOME03; i++)
        {
            isPurchased = isPurchased & this[(CityGroup)i].isPurchased();
        }

        return isPurchased;
    }

    internal Building getNextHomeToPurchase()
    {
        Building toBeReturned = null;

        for (int i = (int)CityGroup.HOME00; i <= (int)CityGroup.HOME03; i++)
        {
            if (!this[(CityGroup)i].isPurchased())
            {
                toBeReturned = this[(CityGroup)i];
                break;
            }
        }

        return toBeReturned;
    }

    internal Home findRandomHomeWithSpace()
    {
        Home theOne = null;

        CityGroup[] homes = { CityGroup.HOME00, CityGroup.HOME01, CityGroup.HOME02, CityGroup.HOME03 };
        System.Random random = new System.Random();
        random.Shuffle(homes);

        for (int i = 0; i < homes.Length; i++)
        {
            Home home = (Home)this[homes[i]];
            if (home.isPurchased() && home.canHostMore(1))
            {
                theOne = home;
                break;
            }
        }

        return theOne;
    }

    internal Home findHomeByAlpha(Alpha alpha)
    {
        Home building = null;

        for (CityGroup i = CityGroup.HOME00; i <= CityGroup.HOME03; i++)
        {
            Home home = (Home)this[i];
            if (home.alphaSlot.alpha == alpha)
            {
                building = home;
                break;
            }
        }

        return building;
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(City)), CanEditMultipleObjects]
    public class CityEditor : LibraryEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
#endif
}
