﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using Gameplay;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Building Slots Library")]
public class BuildingSlotsLibrary : Library, IStorable
{
    private List<GameObject> displayedViews;
    private List<BuildingSlot> displayedModels;
    public Building toBeBuilt;
    public GameObject evolveParticles;
    public GameObject confettiParticles;

    GameplayManager parent;

    public new BuildingSlot this[int key]
    {
        get
        {
            return (BuildingSlot)items[key];
        }
        set
        {
            items[key] = value;
        }
    }


    public void init(GameplayManager manager)
    {
        parent = manager;

        displayedViews = new List<GameObject>();
        displayedModels = new List<BuildingSlot>();

        foreach (BuildingSlot item in items)
        {
            item.init(this);
        }
    }

    public void AddDisplayed(BuildingSlot item, GameObject slotSignal)
    {
        displayedViews.Add(slotSignal);
        displayedModels.Add(item);
    }

    public void clearAllDisplayed()
    {
        displayedModels.Clear();

        foreach (GameObject item in displayedViews)
        {
            Destroy(item);
        }
        displayedViews.Clear();
        toBeBuilt = null;
    }

    public BuildingSlot findSlotByBuilding(Building building)
    {
        return (BuildingSlot)items.Find(e => ((BuildingSlot)e).building == building);
    }

    public void load(JSONObject jsonFile)
    {
        if (jsonFile.ContainsKey("toBuilt") && parent.gameMode == GameMode.BUILDING) toBeBuilt = getBuilding((int)jsonFile.GetEnum<CityGroup>("toBuilt"));

        for (int i = 0; i < Count; i++)
        {
            this[i].load(jsonFile.GetObject(this[i].name));
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();

        if (toBeBuilt != null) obj.Add("toBuilt", toBeBuilt.id.ToString());
        for (int i = 0; i < Count; i++)
        {
            obj.Add(this[i].name ,this[i].save());
        }

        return obj;
    }

    public Building getBuilding(int index)
    {
        return parent.city[(CityGroup)index];
    }

    internal void prestige()
    {
        displayedViews = new List<GameObject>();
        displayedModels = new List<BuildingSlot>();
    }

    internal BuildingSlot findAnyFreeSlot()
    {
        BuildingSlot emptySlot = null;

        foreach (BuildingSlot slot in this)
        {
            if(slot.building == null)
            {
                emptySlot = slot;
                break;
            }
        }

        return emptySlot;
    }

    internal Vector3 freeSlotsAveragePoint()
    {
        Vector3 result = Vector3.zero;
        int freeSlots = 0;

        foreach (BuildingSlot item in this)
        {
            if(item.building == null)
            {
                result += item.position;
                freeSlots++;
            }
        }

        return result / freeSlots;
    }
}
