﻿using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Building Slot")]
public class BuildingSlot : ScriptableObject, IStorable
{
    public Vector3 position;
    public Building building;
    public GameObject buildingView;
    public bool occupiedFromStart;
    public int number { get { return int.Parse(name.Replace("slot_", "")); } }

    BuildingSlotsLibrary parent;

    public void init(BuildingSlotsLibrary parent)
    {
        this.parent = parent;
        reset();
    }

    public void reset()
    {
        if (building != null)
        {
            building = null;
            Destroy(buildingView);
        }
    }

    public Building resetPrestige()
    {
        Building toBeReturned = null;
        if(building != null)
        {
            if(!building.isDestructibleOnPrestige) toBeReturned = building;
            
            building = null;
            Destroy(buildingView);
            buildingView = null;
        }

        return toBeReturned;
    }

    public void load(JSONObject jsonFile)
    {
        if (jsonFile.ContainsKey("building"))
        {
            building = parent.getBuilding((int)jsonFile.GetEnum<CityGroup>("building"));
        }
    }

    public JSONObject save()
    {
        JSONObject obj = new JSONObject();
        if (building != null) obj.Add("building", building.id.ToString());

        return obj;
    }

#if UNITY_EDITOR

    private GameObject previewGizmo;

    [Button("preview")]
    private void preview()
    {
        previewGizmo = new GameObject(string.Format("Preview_{0}", name));
        previewGizmo.GetComponent<Transform>().position = position;
    }

    [Button("Stop Preview")]
    private void stopPreview()
    {
        DestroyImmediate(previewGizmo);
    }
#endif
}
