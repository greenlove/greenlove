﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Tavern")]
public class Tavern : Building
{
    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        base.onBuilt(slot, forceBuild);
        context.gameplayManager.maxAvailableMood = PackMood.HUNGRY;

        if (forceBuild) return;

        context.gameplayManager.bank.update(Currency.BUILT_TAVERNS, 1, BankUpdateStrategy.ADD);

        //Force build restaurant tutorial to be finished
        Tutorial buildRestaurant = ((Tutorial)context.tutorialManager.tutorials[(int)Tutorials.BUILD_RESTAURANT_TUTORIAL]);
        buildRestaurant.hasBeenDisplayed = true;
        buildRestaurant.hasFinished = true;

        context.tutorialManager.launchTutorial(Tutorials.STOCK_RESTAURANT_TUTORIAL, false, 3.5f);
        context.gameplayManager.buffs[Buffs.HOLY_WATER].makeVisible();
    }

    internal override double getStockPrice()
    {
        return base.getStockPrice() * ((BuffHolyWater)context.gameplayManager.buffs[Buffs.HOLY_WATER]).getDiscount();
    }
}
