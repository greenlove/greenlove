﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Playground")]
public class Playground : Building
{
    public override void onBuilt(BuildingSlot slot, bool forceBuild)
    {
        base.onBuilt(slot, forceBuild);
        context.gameplayManager.maxAvailableMood = PackMood.BORED;

        if (forceBuild) return;

        context.gameplayManager.bank.update(Currency.BUILT_PLAYGROUNDS, 1, BankUpdateStrategy.ADD);
        context.gameplayManager.buffs[Buffs.NEW_BORN].makeVisible();
    }

    internal override double getStockPrice()
    {
        return base.getStockPrice() * ((BuffNewBorn)context.gameplayManager.buffs[Buffs.NEW_BORN]).getDiscount();
    }
}
