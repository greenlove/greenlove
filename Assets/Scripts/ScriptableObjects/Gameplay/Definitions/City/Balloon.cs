﻿using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Gameplay/City/Balloon")]
public class Balloon : Vehicle
{
    [ReadOnly]public bool hasDeployedGift;
    [ReadOnly]public bool hasClaimedGift;
    public float height;

    private CameraController cam;
    private bool isOnTutorial;

    public override void init(GameplayController g, CityGroup ID)
    {
        steer = new SteeringAgent();
        steer.init(maxSpeed, getActiveSegment().buildingEvo.GetComponent<MeshFilter>().sharedMesh.bounds.size.magnitude);

        cam = context.findController<CameraController>();

        base.init(g, ID);
        isOnTutorial = false;
    }

    public override void reset()
    {
        base.reset();

        if(isSteering)
        {
            Destroy(obj);
            isSteering = false;
            hasArrived = false;
            isOnTutorial = false;
        }
    }

    public override void startSteering(GameObject b)
    {
        base.startSteering(b);

        height = obj.transform.position.y;
        hasDeployedGift = false;
        hasClaimedGift = false;
        steer.maxVelocity = maxSpeed;
        position = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
    }

    public override void update(float dt)
    {
        base.update(dt);

        if (isSteering)
        {
            Vector3 rig = cam.rig.transform.position;
            Vector3 camPosition = cam.cam.transform.position;

            Vector3 destination = Vector3.zero;

            if (hasClaimedGift)
            {
                destination = Vector3.up * (camPosition.y + 3.0f);
                steer.maxVelocity = maxSpeed * 5.0f;
            }
            else
            {
                destination = new Vector3(rig.x, height, rig.z);
            }
            target = Vector3.LerpUnclamped(position, destination, 2.0f);

            obj.transform.position = steer.arrive(obj.transform.position, target, 2.0f);

            if (!context.tutorialManager.hasAllBeenDisplayed(Tutorials.SHOP_TUTORIAL))
            {
                if (cam.isInFrustum(obj.GetComponent<MeshFilter>()))
                {
                    startTutorialMode();
                }
            }
        }

        if(isOnTutorial)
        {
            context.tutorialManager.launchTutorial(Tutorials.SHOP_TUTORIAL);
        }

        //Arrived
        if (hasArrived) Destroy(obj);
    }
    
    public override void load(JSONObject jsonFile)
    {
        //Do nothing lol
    }

    public override JSONObject save()
    {
        //Do nothing lol
        return new JSONObject();
    }

    public void startTutorialMode()
    {
        isSteering = false;
        isOnTutorial = true;
    }

    public void stopTutorialMode()
    {
        if(obj != null) isSteering = true;
        isOnTutorial = false;
    }
}
