﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockMember
{
    public GameObject gameObject;
    public AlphaMission mission;
    public float aliveTime;

    public FlockMember(GameObject obj, AlphaMission m)
    {
        gameObject = obj;
        mission = m;
        aliveTime = 0;
    }

    public bool hasTarget()
    {
        return mission.what > FlockState.NONE && mission.what < FlockState.COUNT && mission.what != FlockState.NONE;
    }
}