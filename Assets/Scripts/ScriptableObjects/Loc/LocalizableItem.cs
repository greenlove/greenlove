﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This represents a row from the los table
/// </summary>
[Serializable]
public class LocalizableItem : ICSVRow {

    [HideInInspector]
    public int id;

    [CustomValueDrawer("DrawKeyProperty")]
    public string sKey;

    [HideInInspector]
    public string comment;

    [HideInInspector]
    public string group;

    [HideInInspector]
    public string[] values;

    public GreenLoveEvent<LocalizableItem> OnSelected;

#if UNITY_EDITOR
    [HideInInspector]
    public bool isSelected = false;

    private GUIStyle style;

    [HideInInspector]
    public bool isUsed = false;
#endif

    public LocalizableItem() { init(); }
    public LocalizableItem(string sK) { init(sK); }

    private void init(string k = "")
    {
        sKey = k;
        group = "";
        createEmptyValues();

        OnSelected = new GreenLoveEvent<LocalizableItem>();
    }

    private void createEmptyValues()
    {
        values = new string[(int)Lang.MAX_LANGS];
        for (int i = 0; i < values.Length; i++) values[i] = "";
    }

    public void save(Lang lang, string value)
    {
        values[(int)lang] = value;
    }

    public bool containsText(string searchString)
    {
        searchString = searchString.ToLower();

        bool returnable = false;

        try
        {
            returnable = sKey.ToLower().Contains(searchString) || comment.ToLower().Contains(searchString);

            foreach (string val in values)
            {
                if (returnable = returnable || val.ToLower().Contains(searchString)) break;
            }
        }
        catch(NullReferenceException e)
        {
            returnable = false;
        }

        return returnable;
    }

    public string toCSVString(string separator)
    {
        StringBuilder row = new StringBuilder();
        row.AppendFormat("\"{0}\"{1}", sKey, separator);
        row.AppendFormat("\"{0}\"{1}", comment, separator);

        for (int i = 0; i < (int)Lang.MAX_LANGS; i++)
        {
            row.AppendFormat("\"{0}\"{1}", values[i], i >= (int)Lang.MAX_LANGS -1 ? string.Empty : separator);
        }

        return row.ToString();
    }

    public static string getCSVHeaders(string separator = ";")
    {
        StringBuilder header = new StringBuilder();
        header.AppendFormat("Key" + separator);
        header.Append("Comments" + separator);

        for (int i = 0; i < (int)Lang.MAX_LANGS; i++)
        {
            header.AppendFormat("{0}{1}", ((Lang)i).ToString(), i >= (int)Lang.MAX_LANGS -1 ? string.Empty : separator);
        }

        return header.ToString();
    }

    string ICSVRow.getCSVHeaders()
    {
        return getCSVHeaders();
    }

    public void parse(Dictionary<string, string> row)
    {
        if(row["Key"] == "PRESTIGE_INFO_02")
        {
            Debug.Log("Suck my dick");
        }
        sKey = row["Key"];
        comment = row["Comments"];

        for (int i = 0; i < (int)Lang.MAX_LANGS; i++)
        {
            values[i] = row[((Lang)i).ToString()];
        }
    }

#if UNITY_EDITOR
    private string DrawKeyProperty(string key, GUIContent content)
    {
        Color textColor = isSelected ? Color.blue : isUsed? Color.black : Color.red;
        style = new GUIStyle(GUI.skin.button);
        style.normal.textColor = textColor;
        style.active.textColor = textColor;
        style.focused.textColor = textColor;
        style.hover.textColor = textColor;
        style.fontStyle = isSelected? FontStyle.Bold : FontStyle.Normal;
       

        if (GUILayout.Button(key, style))
        {
            OnSelected.Invoke(this);
            isSelected = true;
        }
        return key;
    }
#endif
}
