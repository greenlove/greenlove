﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class SystemLanguageExtensions
{
    /// <summary>
    /// This method will try to find a Lang matching the systemlang by its description, if none found it will return the first one which we assue its english
    /// </summary>
    /// <param name="sysLang">Myself</param>
    /// <returns>A Lang, if none founds, returns the first on the enum aka 'en'</returns>
    public static Lang toLang(this SystemLanguage sysLang)
    {
        return Enum.GetValues(typeof(Lang)).Cast<Lang>().FirstOrDefault(v => v.toFriendlyString() == sysLang.ToString());
    }
}
