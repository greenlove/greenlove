﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Boomlagoon.JSON;
using UnityEngine;


[CreateAssetMenu(menuName = "Building Blocks/Loc/Localization Controller")]
public class LocalizationController : Controller {

    private LocalizationManager locManager;

    public Lang currentLang = Lang.ERROR_LANG;

    private List<ILocalizable> localizables;

    public override void init(Main main)
    {
        base.init(main);

        CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

        locManager = new LocalizationManager();
        currentLang = Application.systemLanguage.toLang();
        locManager.loadLang(currentLang);

        localizables = new List<ILocalizable>();
    }


    public string getTextByKey(string stringValue)
    {
        return locManager == null? string.Empty : locManager?.getTextByKey(stringValue, currentLang);
    }

    public string getCurrentLangName()
    {
        return getTextByKey(string.Format("{0}_KEY", currentLang.toFriendlyString().ToUpper()));
    }

#if UNITY_EDITOR
    public void saveKey(string key)
    {
        locManager.saveKey(key);
    }


    public void saveValueByKey(Lang lang, string key, string value)
    {
        locManager.saveValue(lang, key, value);
    }
#endif


    public bool switchLanguage(Lang l)
    {
        bool ok = locManager.loadLang(l);

        if (ok)
        {
            currentLang = l;

            foreach (ILocalizable localizable in localizables)
            {
                localizable.onLangChanged(this);
            }
        }

        return ok;
    }

    public void subscribeToLocListening(ILocalizable localizable)
    {
        localizables.Add(localizable);
    }

    public void unsubscribeToLocListening(ILocalizable localizable)
    {
        localizables.Remove(localizable);
    }

    public override void load(JSONObject jsonFile)
    {
        Lang newLang = currentLang;
        Enum.TryParse(jsonFile.GetString("current_lang", "en"), out newLang);

        if (currentLang != newLang) switchLanguage(newLang);
    }

    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("current_lang", currentLang.ToString());

        return obj;
    }

    internal string[] findClosestKeys(string key, int amount = 1)
    {
        if (locManager == null) init(null);

        string[] keys = new string[amount];

        for (int i = 0; i < amount; i++)
        {
            keys[i] = locManager.findClosestKey(key, keys).Key;
        }

        return keys;
    }
}


