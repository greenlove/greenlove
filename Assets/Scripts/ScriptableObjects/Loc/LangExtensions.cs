﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using UnityEngine;

public static class LangExtensions
{
    public static SystemLanguage toSystemLang(this Lang lang)
    {
        SystemLanguage returnable = SystemLanguage.English;

        Enum.TryParse(lang.toFriendlyString(), out returnable);

        return returnable;
    }

    public static string toFriendlyString(this Lang value)
    {
        Type type = value.GetType();
        string name = Enum.GetName(type, value);
        if (name != null)
        {
            FieldInfo field = type.GetField(name);
            
            if (field != null)
            {
                DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attr != null) return attr.Description;
            }
        }
        return null;
    }
}
