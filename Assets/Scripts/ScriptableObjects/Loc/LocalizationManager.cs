﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class LocalizationManager {

    private string[] poData;
    private int version = 1;

    public static string ERROR_VALUE_NOT_FOUND = "Value not found";
    public static string TEMPLATE_PATH = "template.pot";
    public static string PO_FILE_EXT = ".po";

    //Header de keys y values
    const string sKeyHeader = "msgid \"{0}\"";
    const string sValueHeader = "msgstr \"{0}\"";

    private List<LocalizableItem> localTable;
    public HashSet<string> groups;
    private Dictionary<string, LocalizableItem> entryCache;

    public LocalizationManager()
    {
        entryCache = new Dictionary<string, LocalizableItem>();
        localTable = new List<LocalizableItem>();
        groups = new HashSet<string>();
    }

    private void initPOHeader(Lang eLang = Lang.ERROR_LANG)
    {
        poData = new string[]
        {
            "#, fuzzy",
            string.Format(sKeyHeader,""),
            string.Format(sValueHeader,""),
            string.Format("\"Project-Id-Version: {0:0.0}\"", version),
            string.Format("\"POT-Creation-Date: {0}\"", DateTime.UtcNow),
            string.Format("\"PO-Revision-Date: {0}\"", DateTime.UtcNow),
            "\"Last-Translator: GreenLove Dev Team\"",
            "\"Language-Team: GreenLove Dev Team\"",
            string.Format("\"Language: {0}\"", eLang == Lang.ERROR_LANG? "" : eLang.ToString()),
            "\"MIME-Version: 1.0\"",
            "\"Content-Type: text/plain; charset=UTF-8\\n\"",
            "\"Content-Transfer-Encoding: 8bit\\n\"",
            "\"X-Generator: Poedit 1.8.11\\n\"",
            "\"X-Poedit-Basepath: .\\n\"",
            "\"Plural-Forms: nplurals=2; plural=(n != 1);\\n\"",
            "\"X-Poedit-KeywordsList: --keyword[-]\\n\"",
            "\"X-Poedit-SearchPath-0: .\\n\"",
        };
    }

    //Returns a pair with the key and it's distance according to Demerau Lavenshtein algorythm
    internal KeyValuePair<string, int> findClosestKey(string key, params string[] excluded)
    {
        int bestDistance = int.MaxValue;
        string bestCandidate = string.Empty;

        foreach (LocalizableItem candidate in localTable)
        {
            if (key == candidate.sKey || Array.Exists(excluded, element => element == candidate.sKey)) continue;

            int candidateDistance = key.distanceTo(candidate.sKey);

            if(candidateDistance < bestDistance)
            {
                bestDistance = candidateDistance;
                bestCandidate = candidate.sKey;
            }
        }

        return new KeyValuePair<string, int>(bestCandidate, bestDistance);
    }



    /// <summary>
    /// Método para cargar todos los idiomas y ponerlos en una lista matricial reconocible por el editor de loc
    /// </summary>
    /// <returns>Lista con todo lo encontrado de momento</returns>
    public List<LocalizableItem> loadAllLangs()
    {
        localTable.Clear();

        for (int i = 0; i < (int)Lang.MAX_LANGS; i++)
        {
            loadLang((Lang)i);
        }
        return localTable;

    }


    public bool loadLang(Lang lang)
    {
        StreamReader langReader = getLangFile(Application.streamingAssetsPath + "/Loc/GetText/" + string.Format("{0}.po", lang.ToString()));
        
        //Cargo la plantilla
        StreamReader templateReader = getLangFile(Application.streamingAssetsPath + "/Loc/GetText/template.pot");
        
        //Sólo verifico con pLangAsset porque asumo que el template siempre está
        if (langReader == null) return false;
        
        if (localTable == null) localTable = new List<LocalizableItem>();

        string sKey = null;
        string sVal = "";
        string group = "";
        StringBuilder sComment = new StringBuilder();

        string sLangLine;
        string sTemplateLine;

        //Fuerzo una vez la lectura del template
        templateReader.ReadLine();

        while ((sTemplateLine = templateReader.ReadLine()) != null)
        {
            //Fuerzo el carro para que el langReader siga al template reader al mismo paso
            sLangLine = langReader.ReadLine();

            if (sTemplateLine.StartsWith("msgid \"")) sKey = sTemplateLine.Substring(7, sTemplateLine.Length - 8).ToUpper();

            else if (sLangLine.StartsWith("msgstr \"")) sVal = sLangLine.Substring(8, sLangLine.Length - 9).Replace("\\n", "\n");

            else if (sLangLine.StartsWith("#") && !sLangLine.StartsWith("#,") && !sLangLine.StartsWith("# ")) sComment.AppendLine(sLangLine.Remove(0, 3));

            else if (sLangLine.StartsWith("# Group: "))
            {
                group = sLangLine.Replace("# Group: ", "");
                groups.Add(group);
            }
            else if (sLangLine.Contains("Project-Id-Version:")) int.TryParse(sLangLine.Replace("\"Project-Id-Version:", "").Replace("\"", ""), out version);

            else
            {
                //Ya he capturado lo que buscaba, vuelvo a poner a null
                if (!string.IsNullOrEmpty(sKey))
                {
                    bool notOnTheList = true;
                    LocalizableItem item = findByKey(sKey);

                    if (notOnTheList = item == null) item = new LocalizableItem();

                    item.sKey = sKey;
                    item.comment = sComment.ToString();
                    item.values[(int)lang] = sVal != null ? sVal : "";
                    item.group = group;

                    if (notOnTheList) localTable.Add(item);
                    item.id = localTable.Count;

                    sKey = sVal = null;
                    sComment = new StringBuilder();
                }
            }
        }

        return true;
    }

    private StreamReader getLangFile(string path)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        UnityWebRequest www = UnityWebRequest.Get(path);
        www.SendWebRequest();

        while (!www.downloadHandler.isDone) { }
        Debug.Log(www.downloadHandler.text);
        return new StreamReader(www.downloadHandler.text.ToStream());
#else
        return new StreamReader(path);
#endif
    }


    /// <summary>
    /// Método para obtener el valor de la tabla según el key
    /// </summary>
    /// <param name="sKey">Key para buscar</param>
    /// <returns></returns>
    public string getTextByKey(string sKey, Lang lang)
    {
        string sRes = "";
        if (sKey != null && localTable != null)
        {
            LocalizableItem item = null;

            if (entryCache.ContainsKey(sKey))
            {
                item = entryCache[sKey];
            }
            else
            {
                item = findByKey(sKey);
                entryCache.Add(sKey, item);
            }

            if (item != null) sRes = item.values[(int)lang];
            else sRes = string.Format("[{0}: {1}]", sKey, ERROR_VALUE_NOT_FOUND);
        }

        return sRes;
    }

    public LocalizableItem findByKey(string k)
    {
        return localTable.Find(e => e.sKey == k);
    }

    public bool containsKey(string k)
    {
        return findByKey(k) != null;
    }

#if UNITY_EDITOR

    /// <summary>
    /// A partir de una lista editada, guarda todos los ficheros a imagen y semejanza
    /// 1. Todos los ficheros incluido el template tienen las mismas keys
    /// 2. Los values vacíos se guardan junto a su key
    /// </summary>
    /// <param name="locList">Lista con la info a guardar</param>
    /// <param name="createNonExistantFiles">Si se desea crear los ficheros que no existan y llenarlos con values vacíos, true por defecto</param>
    public void saveAllFiles(List<LocalizableItem> locList, bool createNonExistantFiles = true)
    {
        StringBuilder sBuilder;
        version++;

        //Por cada lang
        for (int i = -1; i < (int)Lang.MAX_LANGS; i++)
        {
            sBuilder = new StringBuilder();
            initPOHeader((Lang)i);

            //Añado el header
            for (int j = Math.Min(1 + i, 1); j < poData.Length; j++)
            {
                sBuilder.AppendLine(poData[j]);
            }

            //Añado una linea vacía
            sBuilder.AppendLine();

            //Añado las keys y los values
            for (int k = 0; k < locList.Count; k++)
            {
                //Group ("# Group: ")
                sBuilder.AppendLine(string.Format("# Group: {0}", locList[k].group));

                //Comments Multiline support
                if (!string.IsNullOrEmpty(locList[k].comment))
                {
                    string[] comments = locList[k].comment.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string comment in comments)
                    {
                        sBuilder.AppendLine(string.Format("#. {0}", comment));
                    }
                }

                //Keys
                sBuilder.AppendLine(string.Format(sKeyHeader, i < 0? locList[k].sKey : locList[k].values[0].Replace("\n", "\\n")));

                //Values
                string toAppend = (i >= 0 ? locList[k].values[i] : locList[k].values[0]).TrimEnd(new char[] { '\r', '\n' }).Replace("\n", "\\n");
                sBuilder.AppendLine(string.Format(sValueHeader, toAppend));
                sBuilder.AppendLine();
            }

            string sPath = Path.Combine(Application.streamingAssetsPath, "Loc", "GetText", (Lang)i == Lang.ERROR_LANG ? TEMPLATE_PATH : ((Lang)i).ToString() + PO_FILE_EXT);

            //Verifico si existe el text asset
            if (createNonExistantFiles)
            {
                if (!File.Exists(sPath)) File.Create(sPath).Close();
            }

            if (File.Exists(sPath))
            {
                Debug.Log(string.Format("EXISTE EL FICHERO {0}, procedo a escribir", ((Lang)i == Lang.ERROR_LANG ? TEMPLATE_PATH : ((Lang)i).ToString() + PO_FILE_EXT)));
                File.WriteAllText(sPath, sBuilder.ToString());
            }
            else
            {
                Debug.Log(string.Format("EL FICHERO {0} NO EXISTE", ((Lang)i == Lang.ERROR_LANG ? TEMPLATE_PATH : ((Lang)i).ToString() + PO_FILE_EXT)));
            }
        }
    }

    //Se puede mejorar
    public bool saveKey(string key)
    {
        bool bOk = false;

        if (!containsKey(key))
        {
            List<LocalizableItem> table = loadAllLangs();

            table.Add(new LocalizableItem(key));

            saveAllFiles(table);

            bOk = true;
        }
        return bOk;
    }

    //Se puede mejorar
    public bool saveValue(Lang lang, string key, string value)
    {
        bool bOk = false;
        List<LocalizableItem> table = loadAllLangs();

        if(containsKey(key))
        {
            foreach (LocalizableItem item in table)
            {
                if(item.sKey == key)
                {
                    item.save(lang, value);
                    break;
                }
            }

            saveAllFiles(table);

            bOk = true;
        }
        return bOk;
    }

    public void importFromCsv(string path)
    {
        localTable.Clear();

        foreach (LocalizableItem item in CSV.Parse<LocalizableItem>(path, separator: ";"))
        {
            localTable.Add(item);
        }

        saveAllFiles(localTable);
    }

    public string toCsv()
    {
        return CSV.create(LocalizableItem.getCSVHeaders(";").Split(';'), localTable.ToArray(), ";");
    }
#endif
}


/// <summary>
/// Enum de idiomas soportados, han de ir en minúsculas para ser compliant con el estandar ISO (y compatibles con editores PO)
/// Si se añade un lang, recuerda añadirle la etiqueta Description con el nombre del idioma de manera que sea igual al del enum SystemLanguage
/// </summary>
public enum Lang
{
    [Description("English")]
    en,

    [Description("Spanish")]
    es,

    //Don't add langs from here
    MAX_LANGS,
    ERROR_LANG = -1
}

