﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemSoundEmmitter : MonoBehaviour
{
    private ParticleSystem particles;
    public float rate;
    private float randRate;
    private float emissionBuffer = 0;

    [SerializeField] private AudioController audioController;

    [Stem.SoundID, SerializeField] private int emittedSound;

    [SerializeField] private bool playSoundOnSpace;

    void Start()
    {
        particles = GetComponent<ParticleSystem>();
        randRate = rate + UnityEngine.Random.value * 5.0f;
    }

    // Update is called once per frame
    void Update()
    {
        emissionBuffer += Time.deltaTime;
        if(emissionBuffer >= randRate)
        {
            playSound(emittedSound);
            emissionBuffer = 0;
            randRate = rate + UnityEngine.Random.value * 5.0f;
        }
    }

    private void playSound(int id)
    {
        if(playSoundOnSpace)
        {
            audioController.play3DSound(id, particles.transform.position);
        }
        else
        {
            audioController.playSound(id);
        }
    }
}
