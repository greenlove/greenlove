﻿using Boomlagoon.JSON;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Stem;

[CreateAssetMenu(menuName = "Building Blocks/Audio/Audio Controller")]
public class AudioController : Controller
{
    [SerializeField] private DoubleVariable currentTrack;

    [SerializeField] private MusicBank musicBank;
    [SerializeField] private SoundBank soundBank;

    private bool isMainMute;

    public override void init(Main parent)
    {
        base.init(parent);

        restoreFX();
        restoreMusic();

        MusicManager.RegisterBank(musicBank);
        SoundManager.RegisterBank(soundBank);
    }

    public override void postinit()
    {
        base.postinit();
        changeTrack();
    }
    
    private string getTrackName()
    {
        return string.Format("theme_{0}", (int)currentTrack.value + 1);
    }

    public void playSound(int soundId)
    {
        soundBank.GetSound(soundId).SpatialBlend = 0.0f;
        SoundManager.Play(soundId);
    }

    public void playSound(string soundName)
    {
        soundBank.GetSound(soundName).SpatialBlend = 0.0f;
        SoundManager.Play(soundName);
    }

    public void play3DSound(int soundId, Vector3 position, float spatialBlend = 1.0f)
    {
        setup3DSound(soundBank.GetSound(soundId), spatialBlend);
        SoundManager.Play3D(soundId, position);
    }

    public void play3DSound(string soundName, Vector3 position, float spatialBlend = 1.0f)
    {
        setup3DSound(soundBank.GetSound(soundName), spatialBlend);
        SoundManager.Play3D(soundName, position);
    }

    private void setup3DSound(Sound sound, float spatialBlend)
    {
        sound.SpatialBlend = spatialBlend;
        sound.AttenuationMode = AttenuationMode.Linear;
        sound.DopplerLevel = 0.0f;
        sound.MinDistance = 1;
        sound.MaxDistance = 7.5f;
    }

    internal void changeTrack()
    {
        MusicManager.Stop("Prestige", 1.5f);

        MusicManager.SetPlaylist("Master", getTrackName());
        MusicManager.Play("Master", 2.5f);
    }

    internal void prestige()
    {
        MusicManager.Stop("Master", 2.5f);
        MusicManager.GetMusicPlayer("Master").Playlist = null;

        MusicManager.SetPlaylist("Prestige", "prestige");
        MusicManager.Play("Prestige", 1.5f);
    }

    public void muteFX()
    {
        soundBank.DefaultBus.Muted = true;
    }

    public void restoreFX()
    {
        soundBank.DefaultBus.Muted = false;
    }

    internal void toggleFX()
    {
        soundBank.DefaultBus.Muted = !soundBank.DefaultBus.Muted;
    }

    public void muteMusic()
    {
        isMainMute = true;
        muteMusicPlayers(isMainMute);
    }

    public void restoreMusic()
    {
        isMainMute = false;
        muteMusicPlayers(isMainMute);
    }

    internal void toggleMusic()
    {
        isMainMute = !isMainMute;
        muteMusicPlayers(isMainMute);
    }

    private void muteMusicPlayers(bool mute)
    {
        foreach (MusicPlayer player in musicBank.Players)
        {
            player.Muted = mute;
        }
    }

    public bool isFXMuted()
    {
        return soundBank.DefaultBus.Muted;
    }

    public bool isMusicMuted()
    {
        return isMainMute;
    }

    public override void load(JSONObject jsonFile)
    {
        isMainMute = jsonFile.GetBoolean("main", true);
        soundBank.DefaultBus.Muted = jsonFile.GetBoolean("fx", true);

        if (isMainMute) muteMusic();
        if (soundBank.DefaultBus.Muted) muteFX();
    }

    public override JSONObject save()
    {
        JSONObject obj = new JSONObject();

        obj.Add("main", isMainMute);
        obj.Add("fx", soundBank.DefaultBus.Muted);

        return obj;
    }

    public override void stop()
    {
        base.stop();

        MusicManager.Shutdown();
        SoundManager.Shutdown();
    }
}
