﻿using Boomlagoon.JSON;
using Gameplay;
//using PlayFab;
//using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace BuildingBlocks
{
    namespace Backend
    {
        [CreateAssetMenu(menuName = "Building Blocks/Backend/Savefile Controller")]
        public class SavefileController : Controller
        {
            private const string SAVE_NAME = "save";

            private const string SAVE_EXT = "json";

            private string savePath;
            private string compressedSavePath;

            private double elapsedTime;
            private bool isPaused = false;

            [SerializeField]
            private bool skipSaving;

            [SerializeField]
            private bool skipLoading;

            [SerializeField]
            private double SAVE_TIME_FREQUENCY_IN_MINUTES;

            [SerializeField]
            private double CONFLICT_TRESHOLD_IN_MINUTES;

            [SerializeField]
            private StringReference cloudID;

            public bool saveLoadedCorrectly;
            private string corruptedSaveCode;

            private double timestamp;
            public double loadElapsedTime;

            //Create save path
            private string saveDir = "";

            [SerializeField]
            private GameObject corruptedSaveNotifier;

            public override void init(Main parent)
            {
                base.init(parent);
#if UNITY_EDITOR
                saveDir = Application.streamingAssetsPath;
#else
                saveDir = Application.persistentDataPath;
#endif
                savePath = Path.Combine(saveDir, string.Format("{0}.{1}", SAVE_NAME, SAVE_EXT));
                compressedSavePath = Path.Combine(saveDir, string.Format("{0}.hfm", SAVE_NAME, SAVE_EXT)); //hfm is for Huffman

                saveLoadedCorrectly = false;
                corruptedSaveCode = "";

                loadElapsedTime = 0;

                ServerController server = findController<ServerController>();
                if (server == null) return;

                server.onServerTime.AddListener((KeyValuePair<string, double> cipheredTime) => 
                {
                    if(cipheredTime.Key == "save")
                    {
                        timestamp = cipheredTime.Value;
                        save();
                    }
                    else if(cipheredTime.Key.Contains("load"))
                    {
                        if (!cipheredTime.Key.Contains("failure"))
                        {
                            loadElapsedTime = findController<ServerController>().timeDifference(cipheredTime.Value, getSingleEntry("timestamp").Number);
                            findController<GameplayController>().ReduceTimes(loadElapsedTime);
                            findController<GameplayController>().launchOfflineEarnings();
                        }
                        else
                        {
                            Debug.Log("Getting timestamp from server on load failed");
                        }
                    }
                });
            }

            public override void postinit()
            {
                if (isStopped)
                {
                    recoverFromSavefileCorruption();
                    return;
                }

                base.postinit();
            }


            public override JSONObject save()
            {
#if UNITY_EDITOR
                if (skipSaving) return null;
#endif
                JSONObject obj = new JSONObject();
                obj.Add("version", Application.version);
                obj.Add("hasLoadedCutscene", true);
                obj.Add("timestamp", timestamp);
                obj.Add("game", findController<GameplayController>().save());
                obj.Add("uiunlocks", findController<UIUnlocksController>().save());
                obj.Add("ratings", findController<RatingsController>().save());
                obj.Add("lang", findController<LocalizationController>().save());
                obj.Add("audio", findController<AudioController>().save());
                obj.Add("color", findController<ColorizableController>().save());

                saveToLocal(obj.ToString());

                //saveToCloud(save);
                //Debug.Log(string.Format("Saved Succesfully: {0}", obj.ToString()));

                return obj;
            }


            private void saveToLocal(string save)
            {
#if UNITY_EDITOR
                if(!Directory.Exists(saveDir))
                {
                    Directory.CreateDirectory(saveDir);
                }
#endif
                if (!File.Exists(savePath))
                {
                    File.Create(savePath).Close();
                }

                //save locally
                File.WriteAllText(savePath, save);

                //Huffman
                Huffman.HuffmanEncoder.Encode(savePath, compressedSavePath);
            }

            /*
            private void saveToCloud(string save)
            {
                //save to playfab
                PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
                {
                    Data = new Dictionary<string, string>() {
                        {SAVE_NAME, save}
                    }

                },
                onCloudSaveSuccess,
                onCloudSaveFailure
                );
            }
            */

            private string getSaveString()
            {
                if (!File.Exists(savePath)) return null;
                return File.ReadAllText(savePath);

                //if (!File.Exists(compressedSavePath)) return null;
                //string compressedSave = Huffman.HuffmanDecoder.Decode(compressedSavePath);
                //
                //Debug.Log(compressedSave);
                //return compressedSave;
            }
            

            public override void load(JSONObject obj)
            {
#if UNITY_EDITOR
                if (skipLoading) return;
#endif
                preLoad();
                saveLoadedCorrectly = loadFromLocal();
            }

            private void recoverFromSavefileCorruption()
            {
                UICorruptedSaveNotifier notifier = findController<UIController>().invokeUIPanel(corruptedSaveNotifier, false, false).GetComponent<UICorruptedSaveNotifier>();
                notifier.message.text = string.Format(findController<LocalizationController>().getTextByKey("UI_CORRUPTED_SAVE_MSG"), corruptedSaveCode);

                context.StartCoroutine(restartFromCleanState());
            }

            private IEnumerator restartFromCleanState()
            {
                yield return new WaitForSeconds(5.0f);

                File.Delete(savePath);
                SceneManager.LoadScene("Preloader");
            }

            public bool loadFromLocal()
            {
                bool hasLoadedCorrectly = true;

                string save = getSaveString();
                if (!string.IsNullOrEmpty(save))
                {
                    JSONObject obj = JSONObject.Parse(save);
                    
                    timestamp = obj.GetNumber("timestamp");

                    findController<GameplayController>().load(obj.GetObject("game"));
                    findController<UIUnlocksController>().load(obj.GetObject("uiunlocks"));

                    findController<RatingsController>().load(obj.GetObject("ratings"));
                    findController<LocalizationController>().load(obj.GetObject("lang"));
                    findController<AudioController>().load(obj.GetObject("audio"));
                    findController<ColorizableController>().load(obj.GetObject("color"));

                    hasLoadedCorrectly = true;
                }
                else
                {
                    hasLoadedCorrectly = false;
                }

                return hasLoadedCorrectly;
            }

            public JSONValue getSingleEntry(string key)
            {
                JSONObject obj = JSONObject.Parse(getSaveString());
                return obj != null && obj.ContainsKey(key) ? obj.GetValue(key) : null;
            }



            /// <summary>
            /// Method to stop the saveObject to create new commit ID's to be saved
            /// </summary>
            /// <param name="v">should we stop commits or not?</param>
            public void lockCommits(bool v)
            {
                /*
                if(saveObject != null)
                {
                    saveObject.lockID(v);
                }
                */
            }


            public override void update(float dt)
            {
                if (isStopped || isPaused) return;
                
                elapsedTime += dt;

                if (elapsedTime >= (SAVE_TIME_FREQUENCY_IN_MINUTES * 60))
                {
                    elapsedTime = 0;
                    preSave();
                }
            }

            public void chooseCloudSave()
            {
                //loadGameEvent.Raise(loadFromCloudFlag, cloudObject.gameState);

                //cloudObject = null;

                lockCommits(false);
                isPaused = false;
            }


            public void chooseLocalSave()
            {
                lockCommits(false);
                isPaused = false;
            }

            //First save and then try saving again with a more recent timestamp
            public void preSave()
            {
                save(); 
                findController<ServerController>().getTimestamp("save");
            }

            private void preLoad()
            {
                findController<ServerController>().getTimestamp("load");
            }


            private string generateRandomString(int length)
            {
                Guid g = Guid.NewGuid();
                string GuidString = Convert.ToBase64String(g.ToByteArray());
                GuidString = GuidString.Replace("=", "");
                GuidString = GuidString.Replace("+", "");

                return GuidString.Substring(0, length);
            }

#if GREENLOVE_DEBUG || UNITY_EDITOR
            //TODO: Mover esto al server controller
            internal void uploadSaveToDevs(string filename, string url)
            {
                // Read file data
                byte[] data = Encoding.ASCII.GetBytes(getSaveString());
                string filenameWithExtension = filename + ".json";

                // Generate post objects
                Dictionary<string, object> postParameters = new Dictionary<string, object>();
                postParameters.Add("filename", filenameWithExtension);
                postParameters.Add("fileformat", "json");
                postParameters.Add("file", new FormUpload.FileParameter(data, filenameWithExtension, "application/json"));

                // Create request and receive response
                string userAgent = "Client";
                HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(url, userAgent, postParameters);

                // Process response
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                string fullResponse = responseReader.ReadToEnd();
                Debug.Log(fullResponse);
                webResponse.Close();
            }


            internal void saveCorrupted()
            {
                TextAsset corrupted = (TextAsset)Resources.Load("Cheats/corrupted");
                saveToLocal(corrupted.text);
            }
#endif
        }
    }
}