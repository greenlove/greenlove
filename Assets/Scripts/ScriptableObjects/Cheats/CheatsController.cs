﻿using Ads;
using BuildingBlocks.Backend;
using BuildingBlocks.ProductsArchive;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Building Blocks/Cheats/Cheat Controller")]
public class CheatsController : Controller
{
    [SerializeField]
    private GameObject debugPrefab;
    private GameObject debugUI;

    [SerializeField]
    private GameObject saveNamePrompt;

    private GameObject canvas;

    private UICheatsPanel panel;

    [SerializeField]
    private int maxLogLines;
    private string log;
    private Queue logQueue = new Queue();

    private double fpsBuffer;
    public bool isAllFree = false;
    public bool limitlessCapacity = false;
    public bool shortMissionTimes = false;

#if UNITY_EDITOR || GREENLOVE_DEBUG
    public static string savesHook = "https://discordapp.com/api/webhooks/718429728326942754/BBFGapXXK2gWFGKcwwo4SUE93L2LERjD5PdllXiuN22P68gT6-k8qxubNAM9-0sr3pmP";
#endif

    public override void init(Main parent)
    {
        base.init(parent);

#if GREENLOVE_DEBUG || UNITY_EDITOR

        context = parent;
        fpsBuffer = 0;
        isAllFree = false;
        limitlessCapacity = false;
        shortMissionTimes = false;

        if (debugPrefab != null)
        {
            debugUI = Instantiate(debugPrefab);
            canvas = findController<UIController>().panelArea.gameObject;


            debugUI.transform.SetParent(canvas.transform, false);

            panel = debugUI.GetComponent<UICheatsPanel>();

            panel.panelToggle.addOnTouchedListener((bool isLocked) => { toggleView(); });

            Application.logMessageReceived += HandleLog;

            panel.skipTutsButton.addOnTouchedListener((bool isLocked) => { skipAllTutorials(); });
            
            panel.unlockAllButton.addOnTouchedListener((bool isLocked) => { unlockAll(); });

            panel.claimRandomGiftButton.addOnTouchedListener((bool isLocked) => { purchaseInventorySlot(); });

            panel.viewAdButton.addOnTouchedListener((bool isLocked) => { displayAd("Debug"); });

            panel.allFreeButton.addOnTouchedListener((bool isLocked) => { setAllFree(); });

            panel.limitlessRecruitButton.addOnTouchedListener((bool isLocked) => { limitlessCapacity = !limitlessCapacity; });
#if GREENLOVE_DEBUG
            panel.goToBuildingSceneButton.addOnTouchedListener((bool isLocked) => { goToBuildingsScene(); });
#endif
            panel.changeColorPaletteButton.addOnTouchedListener((bool isLocked) => { changeColorPalette(); });

            panel.shortMissionTimesButton.addOnTouchedListener((bool isLocked) => { shortMissionTimes = !shortMissionTimes; });

            panel.uploadSaveToDevsButton.addOnTouchedListener((bool isLocked) => { uploadSavefile(); });

            panel.reloadWithBorruptedSaveButton.addOnTouchedListener((bool isLocked) => { reloadWithCorruptedSave(); });

            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            for (int i = 0; i < (int)Currency.LENGTH; i++)
            {
                Dropdown.OptionData option = new Dropdown.OptionData(((Currency)i).ToString());
                options.Add(option);
            }

            panel.currenciesDropdown.ClearOptions();
            panel.currenciesDropdown.AddOptions(options);
            panel.addCurrencyButton.addOnTouchedListener((bool isLocked) => { addCurrency(); });

            List<Dropdown.OptionData> tutorialsList = new List<Dropdown.OptionData>();
            for (int i = 0; i < (int)Tutorials.LENGTH; i++)
            {
                Dropdown.OptionData option = new Dropdown.OptionData(((Tutorials)i).ToString());
                tutorialsList.Add(option);
            }

            panel.tutorialsDropdown.ClearOptions();
            panel.tutorialsDropdown.AddOptions(tutorialsList);
            panel.launchTutorialsButton.addOnTouchedListener((bool isLocked) => { launchTutorial(); });

            toggleView(); //Hide cheats
        }
#endif
    }

    public override void update(float dt)
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        if(panel != null)
        {
            fpsBuffer += 1.0 / dt;
            fpsBuffer /= 2.0;
            panel.fpsText.text = string.Format("{0:0.0} FPS", fpsBuffer);
        }

        if(Input.GetKeyUp(KeyCode.Alpha1)) findController<ColorizableController>().changeColorMode();
#endif
    }

#if GREENLOVE_DEBUG || UNITY_EDITOR
    private void addCurrency()
    {
        findController<GameplayController>().gameplayManager.bank.update((Currency)panel.currenciesDropdown.value, double.Parse(panel.currencyInput.text), BankUpdateStrategy.ADD);
    }

    private void launchTutorial()
    {
        findController<GameplayController>().tutorialManager.launchTutorial((Tutorials)panel.tutorialsDropdown.value, true);
    }

    private void displayAd(string placement)
    {
        findController<AdsController>().playAdById(placement);
    }

    private void unlockAll()
    {
        for (int i = 0; i < (int)UIUnlocks.COUNT; i++)
        {
            findController<UIUnlocksController>().unlock((UIUnlocks)i);
        }

        GameplayController controller = findController<GameplayController>();
        BuildingSlotsLibrary slots = controller.gameplayManager.buildingSlots;
        City city = controller.gameplayManager.city;

        foreach (Building building in city)
        {
            if (!building.isBuildable || building.isPurchased()) continue;

            BuildingSlot slot = slots.findAnyFreeSlot();
            if (slot == null) return;

            slots.toBeBuilt = building;

            controller.buildInSlot(slot.number, building, true);
        }

        foreach (Buff buff in controller.gameplayManager.buffs)
        {
            buff.levelUp(controller, true);
            buff.makeVisible();
        }

        foreach (AlphaSlot alphaSlot in controller.gameplayManager.alphaSlots)
        {
            controller.hireAlpha(alphaSlot, true);
        }
    }

    private void setAllFree()
    {
        isAllFree = !isAllFree;
        findController<GameplayController>().gameplayManager.bank.isAllFree = isAllFree;
    }

    private void purchaseInventorySlot()
    {
        findController<GameplayController>().gameplayManager.inventory.purchaseNextSlot();
        findController<GameplayController>().gameplayManager.inventory.stackItems(5, 1000);
    }

    private void toggleView()
    {
        panel.view.SetActive(!panel.view.activeInHierarchy);
        putOnTop();
    }

    private void skipAllTutorials()
    {
        foreach(Tutorial tut in findController<GameplayController>().tutorialManager.tutorials)
        {
            tut.hasBeenDisplayed = true;
        }

        if(findController<GameplayController>().tutorialManager.activeTutorial != -1)
        {
            findController<GameplayController>().tutorialManager.forceDismissActiveTutorial();
        }
    }

    private void changeColorPalette()
    {
        findController<ColorizableController>().changeColorMode();
    }


    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        string newString = "\n [" + type + "] : " + logString.Substring(0, Math.Min(150, logString.Length));
        
        if (type == LogType.Exception) newString = "\n" + stackTrace;
        if (logQueue.Count >= maxLogLines) logQueue.Dequeue();

        logQueue.Enqueue(newString);

        log = string.Empty;
        foreach (string mylog in logQueue)
        {
            log += mylog;
        }

        if (panel != null)
        {
            panel.logText.text = log;
        }
    }

    public void uploadSavefile()
    {
        string filename = "";

        UIFilePrompt prompt = findController<UIController>().invokeUIPanel(saveNamePrompt, false, true).GetComponent<UIFilePrompt>();
        prompt.transform.SetAsLastSibling();
        
        prompt.yesButton.addOnTouchedListener((bool isLocked) => 
        {
            filename = prompt.input.text;

            if (filename == string.Empty) return;

            Debug.Log(filename);
            findController<SavefileController>().uploadSaveToDevs(filename, savesHook);
            prompt.close();
        });


        prompt.noButton.addOnTouchedListener((bool isLocked) =>
        {
            prompt.close();
        });

        
    }

    private void reloadWithCorruptedSave()
    {
        findController<SavefileController>().saveCorrupted();
        context.stop();

        SceneManager.LoadScene("Preloader");
    }
#endif

#if GREENLOVE_DEBUG
    private void goToBuildingsScene()
    {
        AsyncOperation ops = SceneManager.LoadSceneAsync("Buildings");
        ops.allowSceneActivation = true;
    }
#endif

    public void putOnTop()
    {
#if GREENLOVE_DEBUG || UNITY_EDITOR
        debugUI.transform.SetAsLastSibling();
#endif
    }
}
