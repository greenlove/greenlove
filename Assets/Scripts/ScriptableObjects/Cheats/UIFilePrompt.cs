﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIFilePrompt : UIPanel
{
    public UIItem yesButton;
    public UIItem noButton;

    public InputField input;


#if UNITY_EDITOR
    [CustomEditor(typeof(UIFilePrompt)), CanEditMultipleObjects]
    public class UIFilePromptEditor : UIPanelEditor
    {
        protected override void selfModule<T>(T Item)
        {
            UIFilePrompt item = Item as UIFilePrompt;
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.LabelField("Self Module: ");
            
            item.yesButton = (UIItem)EditorGUILayout.ObjectField("Yes Button", item.yesButton, typeof(UIItem), true);
            item.noButton = (UIItem)EditorGUILayout.ObjectField("No Button", item.noButton, typeof(UIItem), true);

            item.input = (InputField)EditorGUILayout.ObjectField("Input", item.input, typeof(InputField), true);

            EditorGUILayout.EndVertical();

        }
    }
#endif
}
