﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Building Blocks/Window/Window Controller")]
public class WindowController : Controller
{
    [SerializeField] private ScreenSizeMode sizeMode;
    [SerializeField] private bool allowFullScreen;

    [ShowIf("sizeMode", ScreenSizeMode.ASPECT_RATIO), HorizontalGroup("Aspect Ratio"), SerializeField]
    private float vertical;

    [ShowIf("sizeMode", ScreenSizeMode.ASPECT_RATIO), HorizontalGroup("Aspect Ratio"), SerializeField]
    private float horizontal;

    [ShowIf("sizeMode", ScreenSizeMode.SIZE), HorizontalGroup("Screen Size"), SerializeField]
    private int width;

    [ShowIf("sizeMode", ScreenSizeMode.SIZE), HorizontalGroup("Screen Size"), SerializeField]
    private int height;

    public override void init(Main parent)
    {
        base.init(parent);

#if UNITY_STANDALONE_WIN
        setWindowSize();
#endif
    }

    private void setWindowSize()
    {
        int h, w = 0;

        if (sizeMode == ScreenSizeMode.ASPECT_RATIO)
        {
            Resolution res = Screen.currentResolution;

            float ratio = horizontal / vertical;

            h = (int)(res.height * 0.9f);
            w = (int)(h * ratio);
        }
        else
        {
            h = height;
            w = width;
        }

        Screen.SetResolution(w, h, allowFullScreen);
    }
}

public enum ScreenSizeMode
{
    ASPECT_RATIO,
    SIZE
}