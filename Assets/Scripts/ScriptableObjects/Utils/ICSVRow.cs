﻿using System.Collections.Generic;

public interface ICSVRow
{
    string toCSVString(string separator);
    string getCSVHeaders();
    void parse(Dictionary<string, string> row);
}
