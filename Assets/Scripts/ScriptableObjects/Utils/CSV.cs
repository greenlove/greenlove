﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Text;

public class CSV
{
    static string SPLIT_RE = @"(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    static char[] TRIM_CHARS = { '\"' };

    public static T[] Parse<T>(string path, string separator = ",") where T : new()
    {
        string splitter = SPLIT_RE.Insert(0, separator);

        string[] lines = File.ReadLines(path).Select(element => element).ToArray<string>();
        string[] header = Regex.Split(lines[0], splitter);

        T[] result = new T[lines.Length - 1];

        for (int i = 1; i < lines.Length; i++)
        {
            var values = Regex.Split(lines[i], splitter);
            if (values.Length == 0 || values[0] == "") continue;

            var entry = new Dictionary<string, string>();
            for (var j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\n", "\n").Replace("\\", "");
                
                entry[header[j].TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "")] = value;
            }

            T row = new T();
            ((ICSVRow)row).parse(entry);
            result[i-1] = row;
        }
        return result;
    }

    public static string create<T>(string[] headers, T[] values, string separator = ",")
    {
        if (headers == null || headers.Length <= 0 || values == null || values.Length <= 0) return null;

        StringBuilder output = new StringBuilder();
        output.AppendLine(string.Join(separator, headers));

        for (int i = 0; i < values.Length; i++)
        {
            output.AppendLine(((ICSVRow)values[i]).toCSVString(separator).Replace("\n", "\\n"));
        }

        return output.ToString();
    }
}
