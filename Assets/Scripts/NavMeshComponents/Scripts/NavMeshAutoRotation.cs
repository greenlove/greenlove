﻿using UnityEngine;

public class NavMeshAutoRotation : MonoBehaviour
{
	void Update()
	{
		UnityEngine.AI.NavMeshAgent agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		if (!agent.isStopped)
		{
			Vector3 targetPosition = agent.pathEndPosition;
            Vector3 targetPoint = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);
			Vector3 direction = (targetPoint - transform.position).normalized;

            if (direction != Vector3.zero) {
                Quaternion lookRotation = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, 360);
            }
		}
	}
}