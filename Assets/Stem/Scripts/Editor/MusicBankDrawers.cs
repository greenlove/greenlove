﻿using UnityEditor;
using UnityEngine;
using System.Collections.ObjectModel;

namespace Stem
{
	internal class MusicManagerViewModel
	{
		public MusicBank[] banks = null;
		public GUIContent[] bankNames = null;
		public GUIContent[] playlistNames = null;
		public GUIContent[] playerNames = null;
		public int bankIndex = 0;
		public int playlistIndex = 0;
		public int playerIndex = 0;

		public MusicManagerViewModel(int desiredId)
		{
			banks = BankDBPostprocessor.LoadBankAssets<MusicBank>();
			int numBanks = (banks != null) ? banks.Length : 0;

			bankNames = new GUIContent[numBanks + 1];
			bankNames[0] = new GUIContent("None");
			for (int i = 0; i < numBanks; i++)
			{
				MusicBank bank = banks[i];
				if (bank == null)
					continue;

				bankNames[i + 1] = new GUIContent(string.Format("[{0}]", bank.name));
			}

			for (int i = 0; i < numBanks; i++)
			{
				MusicBank possibleBank = banks[i];
				if (possibleBank == null)
					continue;

				Playlist possiblePlaylist = possibleBank.GetPlaylist(desiredId);
				MusicPlayer possiblePlayer = possibleBank.GetMusicPlayer(desiredId);

				if (possiblePlaylist != null)
					playlistIndex = possibleBank.Playlists.IndexOf(possiblePlaylist);

				if (possiblePlayer != null)
					playerIndex = possibleBank.Players.IndexOf(possiblePlayer);

				if (possiblePlaylist != null || possiblePlayer != null)
				{
					bankIndex = i + 1;
					break;
				}
			}
		}

		internal void FetchPlaylistNames(int index)
		{
			MusicBank bank = banks[index];
			if (bank == null || bank.Playlists.Count == 0)
			{
				playlistNames = null;
				return;
			}

			if (playlistNames == null || (playlistNames.Length != bank.Playlists.Count))
				playlistNames = new GUIContent[bank.Playlists.Count];

			NameDuplicatesManager duplicates = new NameDuplicatesManager(null);
			for (int i = 0; i < bank.Playlists.Count; i++)
			{
				if (bank.Playlists[i] == null)
					continue;

				string playlistName = duplicates.GrabName(bank.Playlists[i].Name);
				playlistNames[i] = new GUIContent(playlistName);
			}
		}

		internal void FetchMusicPlayerNames(int index)
		{
			MusicBank bank = banks[index];
			if (bank == null || bank.Players.Count == 0)
			{
				playerNames = null;
				return;
			}

			if (playerNames == null || (playerNames.Length != bank.Players.Count))
				playerNames = new GUIContent[bank.Players.Count];

			NameDuplicatesManager duplicates = new NameDuplicatesManager(null);
			for (int i = 0; i < bank.Players.Count; i++)
			{
				if (bank.Players[i] == null)
					continue;

				string playerName = duplicates.GrabName(bank.Players[i].Name);
				playerNames[i] = new GUIContent(playerName);
			}
		}
	}

	internal class MusicManagerView
	{
		private MusicManagerViewModel viewModel = null;

		public MusicManagerView(MusicManagerViewModel model)
		{
			viewModel = model;
		}

		internal static void Label(ref Rect position, GUIContent label)
		{
			EditorGUI.LabelField(position, label);

			position.x += EditorGUIUtility.labelWidth;
			position.width -= EditorGUIUtility.labelWidth;
		}

		internal static bool WrongTypeMessage(Rect position, SerializedProperty prop, string name)
		{
			if (prop.propertyType == SerializedPropertyType.Integer)
				return false;

			EditorGUI.LabelField(position, string.Format("Use {0} with int type.", name));
			return true;
		}

		internal bool NoBanksMessage(Rect position)
		{
			if (viewModel.banks != null && viewModel.banks.Length > 0)
				return false;

			EditorGUI.LabelField(position, "No music banks found, consider adding one.");
			return true;
		}

		internal void BanksPopup(ref Rect position)
		{
			position.width *= 0.5f;

			viewModel.bankIndex = EditorGUI.Popup(position, viewModel.bankIndex, viewModel.bankNames);

			position.x += position.width;

			int bankIndex = viewModel.bankIndex - 1;
			if (bankIndex < 0 || bankIndex >= viewModel.banks.Length)
				return;

			viewModel.FetchPlaylistNames(bankIndex);
			viewModel.FetchMusicPlayerNames(bankIndex);
		}

		internal int PlaylistsPopup(Rect position)
		{
			if (viewModel.bankIndex == 0)
			{
				EditorGUI.LabelField(position, "select a music bank");
				return RegistryID.none;
			}

			if (viewModel.playlistNames == null || viewModel.playlistNames.Length == 0)
			{
				EditorGUI.LabelField(position, "bank has no playlists.");
				return RegistryID.none;
			}

			viewModel.playlistIndex = EditorGUI.Popup(position, viewModel.playlistIndex, viewModel.playlistNames);
			if (viewModel.playlistIndex >= viewModel.playlistNames.Length)
				return RegistryID.none;

			MusicBank bank = viewModel.banks[viewModel.bankIndex - 1];
			return bank.Playlists[viewModel.playlistIndex].ID;
		}

		internal int PlayersPopup(Rect position)
		{
			if (viewModel.bankIndex == 0)
			{
				EditorGUI.LabelField(position, "select a music bank");
				return RegistryID.none;
			}

			if (viewModel.playerNames == null || viewModel.playerNames.Length == 0)
			{
				EditorGUI.LabelField(position, "bank has no music players.");
				return RegistryID.none;
			}

			viewModel.playerIndex = EditorGUI.Popup(position, viewModel.playerIndex, viewModel.playerNames);
			if (viewModel.playerIndex >= viewModel.playerNames.Length)
				return RegistryID.none;

			MusicBank bank = viewModel.banks[viewModel.bankIndex - 1];
			return bank.Players[viewModel.playerIndex].ID;
		}
	}

	internal abstract class MusicBankDrawerBase : PropertyDrawer
	{
		internal MusicManagerViewModel viewModel = null;
		internal MusicManagerView view = null;
		internal string attributeName = null;

		internal abstract int GetID(Rect position);

		public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
		{
			MusicManagerView.Label(ref position, label);
			if (MusicManagerView.WrongTypeMessage(position, prop, attributeName))
				return;

			viewModel = new MusicManagerViewModel(prop.intValue);
			view = new MusicManagerView(viewModel);

			if (view.NoBanksMessage(position))
				return;

			EditorGUI.BeginChangeCheck();

			view.BanksPopup(ref position);
			int newId = GetID(position);

			if (EditorGUI.EndChangeCheck())
				prop.intValue = newId;
		}
	}

	[CustomPropertyDrawer(typeof(PlaylistIDAttribute))]
	internal class PlaylistIDDrawer : MusicBankDrawerBase
	{
		public PlaylistIDDrawer()
		{
			attributeName = "PlaylistID";
		}

		internal override int GetID(Rect position)
		{
			return view.PlaylistsPopup(position);
		}
	}

	[CustomPropertyDrawer(typeof(MusicPlayerIDAttribute))]
	internal class MusicPlayerIDDrawer : MusicBankDrawerBase
	{
		public MusicPlayerIDDrawer()
		{
			attributeName = "MusicPlayerID";
		}

		internal override int GetID(Rect position)
		{
			return view.PlayersPopup(position);
		}
	}
}
