﻿using System.Collections.ObjectModel;
using UnityEngine;

namespace Stem
{
	/// <summary>
	/// The main class for sound playback and bank management.
	/// </summary>
	public static class SoundManager
	{
		private static BankManager<SoundBank, SoundManagerRuntime> bankManager = new BankManager<SoundBank, SoundManagerRuntime>();
		private static SoundInstanceManagerRuntime instanceManagerRuntime = null;
		private static GameObject instanceManagerGameObject = null;
		private static bool shutdown = false;

		/// <summary>
		/// The collection of all registered sound banks.
		/// </summary>
		/// <value>A reference to a read-only collection of sound banks.</value>
		public static ReadOnlyCollection<SoundBank> Banks
		{
			get { return bankManager.Banks; }
		}

		/// <summary>
		/// The primary sound bank that will be searched first in case of name collisions.
		/// </summary>
		/// <value>A reference to a primary sound bank.</value>
		public static SoundBank PrimaryBank
		{
			get { return bankManager.PrimaryBank; }
			set { bankManager.PrimaryBank = value; }
		}

		/// <summary>
		/// Registers new sound bank.
		/// </summary>
		/// <param name="bank">A reference to a sound bank to register.</param>
		/// <returns>
		/// True, if sound bank was succesfully registered. False otherwise.
		/// </returns>
		public static bool RegisterBank(SoundBank bank)
		{
			return bankManager.RegisterBank(bank);
		}

		/// <summary>
		/// Deregisters existing sound bank.
		/// </summary>
		/// <param name="bank">A reference to a sound bank to deregister.</param>
		/// <returns>
		/// True, if sound bank was succesfully deregistered. False otherwise.
		/// </returns>
		public static bool DeregisterBank(SoundBank bank)
		{
			bank.OnDeregister();
			return bankManager.DeregisterBank(bank);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		public static void Play3D(int id, Vector3 position)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, volume);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, volume, pitch);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, volume, pitch, delay);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, volume);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, volume, pitch);
		}

		/// <summary>
		/// Plays one-shot sound in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, volume, pitch, delay);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		public static void Play3D(int id, Vector3 position, int variationIndex)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, variationIndex);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, int variationIndex, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, variationIndex, volume);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, int variationIndex, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, variationIndex, volume, pitch);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play3D(int id, Vector3 position, int variationIndex, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play3D(position, variationIndex, volume, pitch, delay);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, int variationIndex)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, variationIndex);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, int variationIndex, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, variationIndex, volume);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, int variationIndex, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, variationIndex, volume, pitch);
		}

		/// <summary>
		/// Plays specific one-shot sound variation in 3D space.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play3D(string name, Vector3 position, int variationIndex, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play3D(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play3D(position, variationIndex, volume, pitch, delay);
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		public static void Play(int id)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play();
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play(int id, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(volume);
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play(int id, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(volume, pitch);
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play(int id, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(volume, pitch, delay);
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// </remarks>
		public static void Play(string name)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play();
		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play(string name, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(volume);

		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play(string name, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(volume, pitch);

		}

		/// <summary>
		/// Plays one-shot sound.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play(string name, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(volume, pitch, delay);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		public static void Play(int id, int variationIndex)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(variationIndex);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play(int id, int variationIndex, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(variationIndex, volume);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play(int id, int variationIndex, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(variationIndex, volume, pitch);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play(int id, int variationIndex, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(id);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find sound, ID: {0}", id);
				return;
			}

			instance.Play(variationIndex, volume, pitch, delay);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// </remarks>
		public static void Play(string name, int variationIndex)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(variationIndex);
		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// </remarks>
		public static void Play(string name, int variationIndex, float volume)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(variationIndex, volume);

		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// </remarks>
		public static void Play(string name, int variationIndex, float volume, float pitch)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(variationIndex, volume, pitch);

		}

		/// <summary>
		/// Plays specific one-shot sound variation.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <param name="variationIndex">Variation index. Must be within <see cref="Stem.Sound.Variations"/> bounds.</param>
		/// <param name="volume">Volume of the sound. Value must be in [0;1] range.</param>
		/// <param name="pitch">Pitch of the sound. Value must be in [-3;3] range.</param>
		/// <param name="delay">Delay of the sound. Value must be greater or equal to zero.</param>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>Volume parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Volume"/> value.</para>
		/// <para>Pitch parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Pitch"/> value.</para>
		/// <para>Delay parameter value will override <see cref="Stem.SoundVariation"/>.<see cref="Stem.SoundVariation.Delay"/> value.</para>
		/// </remarks>
		public static void Play(string name, int variationIndex, float volume, float pitch, float delay)
		{
			if (shutdown)
				return;

			SoundInstance instance = FetchSoundInstance(name);
			if (instance == null)
			{
				Debug.LogWarningFormat("SoundManager.Play(): can't find \"{0}\" sound", name);
				return;
			}

			instance.Play(variationIndex, volume, pitch, delay);
		}

		/// <summary>
		/// Searches for the specified sound with a matching unique ID.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <returns>
		/// A reference to a sound, if found. Null reference otherwise.
		/// </returns>
		public static Sound GetSound(int id)
		{
			if (shutdown)
				return null;

			Sound sound = FetchSound(id);
			if (sound == null)
			{
				Debug.LogWarningFormat("SoundManager.GetSound(): can't find sound, ID: {0}", id);
				return null;
			}

			return sound;
		}

		/// <summary>
		/// Searches for the specified sound with a matching name.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <returns>
		/// A reference to a sound, if found. Null reference otherwise.
		/// </returns>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// </remarks>
		public static Sound GetSound(string name)
		{
			if (shutdown)
				return null;

			Sound sound = FetchSound(name);
			if (sound == null)
			{
				Debug.LogWarningFormat("SoundManager.GetSound(): can't find \"{0}\" sound", name);
				return null;
			}

			return sound;
		}

		/// <summary>
		/// Searches for the specified sound bus with a matching unique ID.
		/// </summary>
		/// <param name="id">Unique ID of the sound bus.</param>
		/// <returns>
		/// A reference to a sound bus, if found. Null reference otherwise.
		/// </returns>
		public static SoundBus GetSoundBus(int id)
		{
			if (shutdown)
				return null;

			SoundBus bus = FetchSoundBus(id);
			if (bus == null)
			{
				Debug.LogWarningFormat("SoundManager.GetSoundBus(): can't find sound bus, ID: {0}", id);
				return null;
			}

			return bus;
		}

		/// <summary>
		/// Searches for the specified sound bus with a matching name.
		/// </summary>
		/// <param name="name">Name of the sound bus.</param>
		/// <returns>
		/// A reference to a sound bus, if found. Null reference otherwise.
		/// </returns>
		/// <remarks>
		/// <para>If multiple banks have sound buses with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound bus will be used.</para>
		/// </remarks>
		public static SoundBus GetSoundBus(string name)
		{
			if (shutdown)
				return null;

			SoundBus bus = FetchSoundBus(name);
			if (bus == null)
			{
				Debug.LogWarningFormat("SoundManager.GetSoundBus(): can't find \"{0}\" sound bus", name);
				return null;
			}

			return bus;
		}

		/// <summary>
		/// Grabs an empty sound instance from the sound pool. Used for manual playback and custom mixing logic.
		/// </summary>
		/// <returns>
		/// A reference to an empty sound instance.
		/// </returns>
		/// <remarks>
		/// <para>This method may increase the size of the sound pool causing additional memory allocations.</para>
		/// <para>When a sound instance is not needed anymore, use <see cref="ReleaseSound(SoundInstance)"/> to return it back to the sound pool.</para>
		/// </remarks>
		public static SoundInstance GrabSound()
		{
			if (shutdown)
				return null;

			SoundInstanceManagerRuntime runtime = FetchSoundPool();
			if (runtime == null)
			{
				Debug.LogWarningFormat("SoundManager.GrabSound(): can't create sound pool");
				return null;
			}

			return runtime.GrabSound();
		}

		/// <summary>
		/// Grabs a sound instance from the sound pool. Used for manual playback and custom mixing logic.
		/// </summary>
		/// <param name="id">Unique ID of the sound.</param>
		/// <returns>
		/// A reference to a sound instance.
		/// </returns>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>This method may increase the size of the sound pool causing additional memory allocations.</para>
		/// <para>When a sound instance is not needed anymore, use <see cref="ReleaseSound(SoundInstance)"/> to return it back to the sound pool.</para>
		/// </remarks>
		public static SoundInstance GrabSound(int id)
		{
			if (shutdown)
				return null;

			SoundInstanceManagerRuntime runtime = FetchSoundPool();
			if (runtime == null)
			{
				Debug.LogWarningFormat("SoundManager.GrabSound(): can't create sound pool");
				return null;
			}

			Sound sound = FetchSound(id);
			if (sound == null)
			{
				Debug.LogWarningFormat("SoundManager.GrabSound(): can't find sound, ID: {0}", id);
				return null;
			}

			return runtime.GrabSound(sound);
		}

		/// <summary>
		/// Grabs a sound instance from the sound pool. Used for manual playback and custom mixing logic.
		/// </summary>
		/// <param name="name">Name of the sound.</param>
		/// <returns>
		/// A reference to a sound instance.
		/// </returns>
		/// <remarks>
		/// <para>If multiple banks have sounds with a matching name, the primary sound bank will be checked first.
		/// Within a bank, the first occurrence of found sound will be used.</para>
		/// <para>This method may increase the size of the sound pool causing additional memory allocations.</para>
		/// <para>When a sound instance is not needed anymore, use <see cref="ReleaseSound(SoundInstance)"/> to return it back to the sound pool.</para>
		/// </remarks>
		public static SoundInstance GrabSound(string name)
		{
			if (shutdown)
				return null;

			SoundInstanceManagerRuntime runtime = FetchSoundPool();
			if (runtime == null)
			{
				Debug.LogWarningFormat("SoundManager.GrabSound(): can't create sound pool");
				return null;
			}

			Sound sound = FetchSound(name);
			if (sound == null)
			{
				Debug.LogWarningFormat("SoundManager.GrabSound(): can't find \"{0}\" sound", name);
				return null;
			}

			return runtime.GrabSound(sound);
		}

		/// <summary>
		/// Releases sound instance and return it back to the sound pool.
		/// </summary>
		/// <param name="instance">A reference to a sound instance.</param>
		/// <returns>
		/// True, if the sound instance was successfully returned to sound pool. False otherwise.
		/// </returns>
		/// <remarks>
		/// <para>Once the sound instance is returned back to a sound pool, it's possible to reuse it again by calling <see cref="GrabSound()"/> or <see cref="GrabSound(string)"/>.</para>
		/// </remarks>
		public static bool ReleaseSound(SoundInstance instance)
		{
			if (shutdown)
				return false;

			SoundInstanceManagerRuntime runtime = FetchSoundPool();
			if (runtime == null)
			{
				Debug.LogWarningFormat("SoundManager.ReleaseSound(): can't create sound pool");
				return false;
			}

			return runtime.ReleaseSound(instance);
		}

		/// <summary>
		/// Stops all playing sounds.
		/// </summary>
		/// <remarks>
		/// <para>This method will also stop all sounds instances returned from <see cref="GrabSound()"/> or <see cref="GrabSound(string)"/>.</para>
		/// </remarks>
		public static void Stop()
		{
			if (shutdown)
				return;

			ReadOnlyCollection<SoundManagerRuntime> runtimes = bankManager.Runtimes;
			for (int i = 0; i < runtimes.Count; i++)
				runtimes[i].Stop();

			if (instanceManagerRuntime != null)
				instanceManagerRuntime.Stop();
		}

		/// <summary>
		/// Pauses all playing sounds.
		/// </summary>
		/// <remarks>
		/// <para>This method will also stop all sounds instances returned from <see cref="Stem.SoundManager.GrabSound()"/> or <see cref="Stem.SoundManager.GrabSound(string)"/>.</para>
		/// </remarks>
		public static void Pause()
		{
			if (shutdown)
				return;

			ReadOnlyCollection<SoundManagerRuntime> runtimes = bankManager.Runtimes;
			for (int i = 0; i < runtimes.Count; i++)
				runtimes[i].Pause();

			if (instanceManagerRuntime != null)
				instanceManagerRuntime.Pause();
		}

		/// <summary>
		/// Resumes all paused sounds.
		/// </summary>
		/// <remarks>
		/// <para>This method will also resume all sounds instances returned from <see cref="Stem.SoundManager.GrabSound()"/> or <see cref="Stem.SoundManager.GrabSound(string)"/>.</para>
		/// </remarks>
		public static void UnPause()
		{
			if (shutdown)
				return;

			ReadOnlyCollection<SoundManagerRuntime> runtimes = bankManager.Runtimes;
			for (int i = 0; i < runtimes.Count; i++)
				runtimes[i].UnPause();

			if (instanceManagerRuntime != null)
				instanceManagerRuntime.UnPause();
		}

		internal static void Init()
		{
			for (int i = 0; i < bankManager.Banks.Count; i++)
				bankManager.FetchRuntime(i);

			shutdown = false;
		}

		internal static void Shutdown()
		{
			shutdown = true;
		}

		internal static SoundManagerRuntime FetchSoundManagerRuntime(int id)
		{
			// Check all banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSound(id))
					return bankManager.FetchRuntime(i);
			}

			return null;
		}

		internal static SoundManagerRuntime FetchSoundManagerRuntime(string name)
		{
			// Check primary bank first
			int primaryIndex = bankManager.PrimaryBankIndex;
			if (primaryIndex != -1)
			{
				SoundBank bank = bankManager.Banks[primaryIndex];

				if (bank.Runtime.ContainsSound(name))
					return bankManager.FetchRuntime(primaryIndex);
			}

			// Check other banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				// Skip primary bank
				if (i == primaryIndex)
					continue;

				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSound(name))
					return bankManager.FetchRuntime(i);
			}

			return null;
		}

		internal static SoundInstance FetchSoundInstance(int id)
		{
			SoundManagerRuntime runtime = FetchSoundManagerRuntime(id);
			if (runtime == null)
				return null;

			Sound sound = runtime.Bank.GetSound(id);
			return runtime.FetchSoundInstance(sound);
		}

		internal static SoundInstance FetchSoundInstance(string name)
		{
			SoundManagerRuntime runtime = FetchSoundManagerRuntime(name);
			if (runtime == null)
				return null;

			Sound sound = runtime.Bank.GetSound(name);
			return runtime.FetchSoundInstance(sound);
		}

		internal static Sound FetchSound(int id)
		{
			// Check all banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSound(id))
					return bank.Runtime.GetSound(id);
			}

			return null;
		}

		internal static Sound FetchSound(string name)
		{
			// Check primary bank first
			int primaryIndex = bankManager.PrimaryBankIndex;
			if (primaryIndex != -1)
			{
				SoundBank bank = bankManager.Banks[primaryIndex];

				if (bank.Runtime.ContainsSound(name))
					return bank.Runtime.GetSound(name);
			}

			// Check other banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				// Skip primary bank
				if (i == primaryIndex)
					continue;

				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSound(name))
					return bank.Runtime.GetSound(name);
			}

			return null;
		}

		internal static SoundBus FetchSoundBus(int id)
		{
			// Check all banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSoundBus(id))
					return bank.Runtime.GetSoundBus(id);
			}

			return null;
		}

		internal static SoundBus FetchSoundBus(string name)
		{
			// Check primary bank first
			int primaryIndex = bankManager.PrimaryBankIndex;
			if (primaryIndex != -1)
			{
				SoundBank bank = bankManager.Banks[primaryIndex];

				if (bank.Runtime.ContainsSoundBus(name))
					return bank.Runtime.GetSoundBus(name);
			}

			// Check other banks
			for (int i = 0; i < bankManager.Banks.Count; i++)
			{
				// Skip primary bank
				if (i == primaryIndex)
					continue;

				SoundBank bank = bankManager.Banks[i];

				if (bank.Runtime.ContainsSoundBus(name))
					return bank.Runtime.GetSoundBus(name);
			}

			return null;
		}

		private static SoundInstanceManagerRuntime FetchSoundPool()
		{
			if (instanceManagerRuntime != null)
				return instanceManagerRuntime;

			instanceManagerGameObject = new GameObject();
			instanceManagerGameObject.name = "Sound Instance Pool";
			GameObject.DontDestroyOnLoad(instanceManagerGameObject);

			instanceManagerRuntime = instanceManagerGameObject.AddComponent<SoundInstanceManagerRuntime>();
			return instanceManagerRuntime;
		}
	}
}
