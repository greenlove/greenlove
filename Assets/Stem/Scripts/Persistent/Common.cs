﻿using UnityEngine;

namespace Stem
{
	/// <summary>
	/// Defines how audio clips will be managed in memory.
	/// </summary>
	public enum AudioClipManagementMode
	{
		/// <summary>Preload audio clip data during startup and keep it in memory.</summary>
		PreloadAndKeepInMemory,
		/// <summary>Unload audio clip data if it was not used for some time.</summary>
		UnloadUnused,
		/// <summary>Do not manage audio clip data and instead allow the developer to take control.</summary>
		Manual,
	}

	/// <summary>
	/// Defines how new audio content will be created after the drag-drop event. Provided audio clips will be used as sound variations or playlist tracks.
	/// </summary>
	public enum AudioClipImportMode
	{
		/// <summary>Create a single item and put all audio clips to it.</summary>
		SingleItemWithAllClips,
		/// <summary>Create multiple items with a single audio clips.</summary>
		MultipleItemsWithSingleClip,
	}

	/// <summary>
	/// The common bank interface for runtime state management.
	/// </summary>
	public interface IBank
	{
		/// <summary>
		/// Removes runtime state from the bank.
		/// </summary>
		/// <remarks>
		/// <para>This method is automatically called during the build.</para>
		/// </remarks>
		void ClearRuntime();
	}

	/// <summary>
	/// The container interface used by memory manager for audio clip management.
	/// </summary>
	public interface IAudioClipContainer
	{
		/// <summary>
		/// Gets the number of audio clips in the container.
		/// </summary>
		/// <returns>
		/// The number of audio clips.
		/// </returns>
		int GetNumAudioClips();

		/// <summary>
		/// Gets the audio clip at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the audio clip to get.</param>
		/// <returns>
		/// A reference to an audio clip.
		/// </returns>
		AudioClip GetAudioClip(int index);

		/// <summary>
		/// Gets the audio clip unload interval of the container.
		/// </summary>
		/// <remarks>
		/// <para>This value is only used if <see cref="IAudioClipContainer.GetAudioClipManagementMode"/> return value is <see cref="AudioClipManagementMode.UnloadUnused"/></para>
		/// </remarks>
		/// <returns>
		/// The time interval in seconds.
		/// </returns>
		float GetAudioClipUnloadInterval();

		/// <summary>
		/// Gets the audio clip management mode of the container.
		/// </summary>
		/// <returns>
		/// An enum value.
		/// </returns>
		AudioClipManagementMode GetAudioClipManagementMode();
	}
}
