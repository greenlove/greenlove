﻿using System;
using System.Collections.Generic;

namespace Stem
{
	public static class RegistryID
	{
		public const int none = 0;
	}

	internal static class RegistryDBRuntime
	{
		private static HashSet<int> usedIds = new HashSet<int>();
		private static Random random = new Random();

		internal static int GenerateID(bool register = false)
		{
			int newId = random.Next();

			while(usedIds.Contains(newId))
				newId = random.Next();

			if (register)
				usedIds.Add(newId);

			return newId;
		}

		internal static bool RegisterID(int id)
		{
			if (usedIds.Contains(id))
				return false;

			usedIds.Add(id);
			return true;
		}

		internal static bool ReleaseID(int id)
		{
			if (!usedIds.Contains(id))
				return false;

			usedIds.Remove(id);
			return true;
		}
	}
}
