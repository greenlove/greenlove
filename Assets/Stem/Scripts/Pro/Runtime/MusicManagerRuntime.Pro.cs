﻿using System.Collections.Generic;
using UnityEngine;

#if !STEM_DEBUG_SKIP_PRO

namespace Stem
{
	internal partial class MusicManagerRuntime : MonoBehaviour, IManagerRuntime<MusicBank>
	{
		private Dictionary<byte, SyncGroupRuntime> syncGroupRuntimes = new Dictionary<byte, SyncGroupRuntime>();

		internal void InitSyncGroups(MusicBank bank)
		{
			foreach (MusicPlayer player in bank.Players)
			{
				MusicPlayerRuntime runtime = new MusicPlayerRuntime(transform, player);
				playerRuntimes.Add(player, runtime);

				if (player.PlaybackMode != MusicPlayerPlaybackMode.Synced)
					continue;

				if (syncGroupRuntimes.ContainsKey(player.SyncGroup))
					continue;

				syncGroupRuntimes.Add(player.SyncGroup, new SyncGroupRuntime(player.SyncGroup, this));
			}
		}

		private void OnDestroy()
		{
			Dictionary<byte, SyncGroupRuntime>.Enumerator enumerator = syncGroupRuntimes.GetEnumerator();
			while(enumerator.MoveNext())
			{
				SyncGroupRuntime runtime = enumerator.Current.Value;
				runtime.Shutdown();
			}
		}
	}
}

#endif
