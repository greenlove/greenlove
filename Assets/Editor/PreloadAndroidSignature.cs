﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public static class PreloadAndroidSignature
{
    static PreloadAndroidSignature()
    {
        call();
    }

    public static void call()
    {
        PlayerSettings.Android.keystoreName = Path.Combine(new DirectoryInfo(Application.dataPath).Parent.FullName, "Keys", "greenlove.keystore");
        PlayerSettings.Android.keystorePass = "lovegreen";
        PlayerSettings.Android.keyaliasName = "greenlove";
        PlayerSettings.Android.keyaliasPass = "lovegreen";
    }
}
