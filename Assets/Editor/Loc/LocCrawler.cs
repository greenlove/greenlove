﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

public class LocCrawler
{
    private static string scriptsPath = Path.Combine(Application.dataPath, "Scripts", "ScriptableObjects");
    private static string codePattern = "(getTextByKey\\(\")(.+)(\"\\))";

    private static string prefabsPath = Path.Combine(Application.dataPath, "Resources", "UI");
    private static string modelsPath = Path.Combine(Application.dataPath, "Scripts", "Gameplay");
    private static string prefabsPattern = "(locKey: )(.+)";

    public static void call(LocalizationManager locManager)
    {
        Task.Run(async () => { crawl(locManager); }).GetAwaiter().GetResult();
    }

    private static async Task<bool> crawl(LocalizationManager manager)
    {
        findEntries(scriptsPath, ".cs", codePattern, manager);
        findEntries(prefabsPath, ".prefab", prefabsPattern, manager);
        findEntries(modelsPath, ".asset", prefabsPattern, manager);

        return true;
    }

    private static void findEntries(string dir, string ext, string pattern, LocalizationManager manager)
    {
        foreach (string path in GetFiles(dir, ext))
        {
            StreamReader reader = new StreamReader(path);
            string line = null;

            while ((line = reader.ReadLine()) != null)
            {
                if (!Regex.IsMatch(line, pattern)) continue;

                Match match = Regex.Match(line, pattern);
                for (int i = 0; i < match.Groups.Count; i++)
                {
                    if (!manager.containsKey(match.Groups[i].Value)) continue;

                    LocalizableItem locItem = manager.findByKey(match.Groups[i].Value);
                    locItem.isUsed = true;
                }
            }
        }
    }

    static IEnumerable<string> GetFiles(string path, string ext = "")
    {
        Queue<string> queue = new Queue<string>();
        queue.Enqueue(path);
        while (queue.Count > 0)
        {
            path = queue.Dequeue();
            try
            {
                foreach (string subDir in Directory.GetDirectories(path))
                {
                    queue.Enqueue(subDir);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            string[] files = null;
            try
            {
                files = Directory.GetFiles(path);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
            if (files != null)
            {
                for (int i = 0; i < files.Length; i++)
                {
                    if(Path.GetExtension(files[i]) == ext || string.IsNullOrEmpty(ext)) yield return files[i];
                }
            }
        }
    }
}
