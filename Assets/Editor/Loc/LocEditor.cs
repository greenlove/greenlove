﻿using Boomlagoon.JSON;
using BuildingBlocks;
using SFB;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LocEditor : OdinEditorWindow
{
    private string groupsPath;

    [HideInInspector]
    public LocalizationManager locManager;

    [VerticalGroup("Loc", Order = 0), SerializeField, LabelWidth(120), OnValueChanged("Search")]
    private string searchString;

    [HorizontalGroup("Loc/Data", Width = 0.3f, Order = 2), SerializeField, CustomValueDrawer("GroupsDrawer"), LabelWidth(0), 
        ListDrawerSettings(CustomAddFunction = "AddNewGroup", CustomRemoveElementFunction = "RemoveGroup", Expanded = true, ShowPaging = false)]
    private List<GroupSelector> groups;
    private List<string> groupNames;
    private int groupsSelectedEachFrame = 0;

    [HorizontalGroup("Loc/Data", Width = 0.3f, Order = 2), SerializeField, ListDrawerSettings(Expanded = true, NumberOfItemsPerPage = 25, CustomAddFunction = "AddNewEntry", CustomRemoveElementFunction = "RemoveEntry")]
    private List<LocalizableItem> displayed;
    private List<LocalizableItem> hidden;

    [HorizontalGroup("Loc/Data", Width = 0.4f, Order = 2), SerializeField, LabelWidth(100), CustomValueDrawer("DrawEditableZone")]
    private string keyArea;

    private LocalizableItem focused;

    [MenuItem("Window/Localization Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(LocEditor));
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        groupsPath = Path.Combine(Application.streamingAssetsPath, "Loc", "groups.json");

        locManager = new LocalizationManager();

        displayed = locManager.loadAllLangs();
        hidden = new List<LocalizableItem>();

        displayed.ForEach((LocalizableItem item) =>
        {
            item.OnSelected.AddListener((LocalizableItem locItem) => 
            {
                focused = locItem;
                fullList().ForEach((LocalizableItem others) => { others.isSelected = false; });
            });
        });

        displayed.Sort(compareLocEntries);

        JSONObject groupsObject = JSONObject.Parse(File.ReadAllText(groupsPath));

        groups = new List<GroupSelector>();

        foreach (KeyValuePair<string, JSONValue> item in groupsObject)
        {
            GroupSelector selector = new GroupSelector(item.Key, item.Value.Str, false);
            groups.Add(selector);
        }

        searchString = "";

        //Find the leys that are being used
        LocCrawler.call(locManager);
    }

    private List<LocalizableItem> fullList()
    {
        List<LocalizableItem> full = new List<LocalizableItem>();
        full.AddRange(displayed);
        full.AddRange(hidden);

        return full;
    }

    private int compareLocEntries(LocalizableItem a, LocalizableItem b)
    {
        return string.Compare(a.sKey, b.sKey, StringComparison.InvariantCulture);
    }

    [HorizontalGroup("Loc/Buttons"), Button("Save all")]
    private void saveAll()
    {
        if (!reviewKeys()) return;

        List<LocalizableItem> full = fullList();
        full.Sort(compareLocEntries);

        locManager.saveAllFiles(full);

        JSONObject obj = new JSONObject();

        for (int i = 0; i < groups.Count; i++)
        {
            obj.Add(groups[i].id, groups[i].group);
        }

        File.WriteAllText(groupsPath, obj.ToString());
        AssetDatabase.Refresh();
    }

    [HorizontalGroup("Loc/Buttons"), Button("Import Csv")]
    private void importCsv()
    {
        string[] path = StandaloneFileBrowser.OpenFilePanel("Open csv", Application.dataPath, "csv", false);
        if (path == null || path.Length <= 0) return;

        locManager.importFromCsv(path[0]);

        this.Close();
        ShowWindow();
    }

    [HorizontalGroup("Loc/Buttons"), Button("Export Csv")]
    private void exportCsv()
    {
        string csv = locManager.toCsv();
        
        StandaloneFileBrowser.SaveFilePanelAsync("Save Loc", "", "greenloc", new ExtensionFilter[] { new ExtensionFilter("Csv", "csv") }, (string path) =>
        {
            File.WriteAllText(path, csv);
        });
    }

    /// <summary>
    /// Método para determinar si las keys son todas distintas
    /// 1. No valen keys repetidas
    /// 2. No valen keys vacías
    /// </summary>
    private bool reviewKeys()
    {
        bool bOk = true;

        HashSet<string> pHash = new HashSet<string>();
        List<LocalizableItem> full = fullList();
        int total = 0;
        for (int i = 0; i < full.Count; i++)
        {
            total = i;

            bOk = !string.IsNullOrEmpty(full[i].sKey) && !string.IsNullOrEmpty(full[i].values[0]) && pHash.Add(full[i].sKey);

            if (!bOk)
            {
                Debug.Log("------------------------------------");
                Debug.Log(string.Format("[{2}] {0}: {1}", full[i].sKey, full[i].values[0], i));
                Debug.Log("ERROR! Esta última entrada tiene la key repetida o la key o el valor en 'en' no están");
                Debug.Log("Tus cambios no se han guardado");
                break;
            }
        }

        if (bOk)
        {
            Debug.Log("");
            Debug.Log(string.Format("{0} Entradas", total));
            Debug.Log("¡Todas correctas!");
        }

        return bOk;
    }

    private GroupSelector GroupsDrawer(GroupSelector group, GUIContent content)
    {
        EditorWindow window = GetWindow(typeof(LocEditor));
        EditorGUILayout.BeginHorizontal();

        EditorGUIUtility.fieldWidth = window.position.width * 0.28f;

        group.group = EditorGUILayout.TextField(group.group);

        group.selected = EditorGUILayout.Toggle(group.selected);

        EditorGUILayout.EndHorizontal();

        return group;
    }

    protected override void OnBeginDrawEditors()
    {
        base.OnBeginDrawEditors();

        groupsSelectedEachFrame = getSelectedGroups().Count;
    }

    protected override void OnEndDrawEditors()
    {
        base.OnEndDrawEditors();

        if (getSelectedGroups().Count != groupsSelectedEachFrame)
        {
            filterEntries();
            displayed.Sort(compareLocEntries);
        }
    }

    private List<GroupSelector> getSelectedGroups()
    {
        return groups.FindAll(e => e.selected);
    }

    private void Search()
    {
        filterEntries();
    }

    private void filterEntries()
    {
        List<string> selectedGroups = groups.FindAll(e => e.selected).Select(x => x.id).ToList();

        List<LocalizableItem> full = fullList();
        hidden.Clear();
        displayed.Clear();

        for (int i = 0; i < full.Count; i++)
        {
            LocalizableItem entry = full[i];
            bool canAccessByGroup = selectedGroups.Count <= 0 ? true : selectedGroups.Contains(entry.group);
            bool canAccessBySearch = string.IsNullOrEmpty(searchString) ? true : canAccessByGroup && entry.containsText(searchString);

            if(canAccessByGroup && canAccessBySearch)
            {
                displayed.Add(entry);
            }
            else
            {
                hidden.Add(entry);
            }
        }
    }

    private void AddNewGroup()
    {
        GroupSelector newSelector = new GroupSelector(string.Format("New Group {0}", groups.Count), false);
        groups.Add(newSelector);
    }

    private void RemoveGroup(GroupSelector selector)
    {
        groups.Remove(selector);
    }

    private void AddNewEntry()
    {
        LocalizableItem item = new LocalizableItem();
        item.OnSelected.AddListener((LocalizableItem locItem) => 
        {
            focused = locItem;
        });

        displayed.Insert(0, item);
        item.id = 0;
    }

    private void RemoveEntry(LocalizableItem item)
    {
        displayed.Remove(item);
        item.OnSelected.RemoveAllListeners();
    }

    private string DrawEditableZone(string key, GUIContent content)
    {
        Rect windowRect = GetWindow(typeof(LocEditor)).position;
        float colWidth = windowRect.width * 0.4f;

        EditorStyles.textField.wordWrap = true;

        int normalFontSize = EditorStyles.textField.fontSize;

        if (focused != null)
        {
            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(colWidth));

            focused.sKey = EditorGUILayout.TextField("Key", focused.sKey, GUILayout.MaxWidth(colWidth));

            EditorStyles.textField.fontSize = 14;

            for (int i = 0; i < (int)Lang.MAX_LANGS; i++)
            {
                EditorGUILayout.LabelField(((Lang)i).toFriendlyString());
                focused.values[i] = EditorGUILayout.TextArea(focused.values[i], GUILayout.Height(120), GUILayout.MaxWidth(colWidth));
            }

            EditorGUILayout.LabelField("Comments");
            focused.comment = EditorGUILayout.TextArea(focused.comment, GUILayout.Height(120), GUILayout.MaxWidth(colWidth));

            EditorStyles.textField.fontSize = normalFontSize;

            EditorGUILayout.LabelField("Group");
            
            groupNames = new List<string>();
            groups.ForEach((GroupSelector selector) => 
            {
                groupNames.Add(selector.group);
            });
            
            int index = EditorGUILayout.Popup(groups.IndexOf(groups.Find(element => element.id == focused.group)), groupNames.ToArray(), GUILayout.MaxWidth(colWidth));
            if(index > -1) focused.group = groups[index].id;

            EditorGUILayout.EndVertical();
        }
    
        return key;
    }
}

[Serializable]
public struct GroupSelector
{
    public GroupSelector(string group, bool selected)
    {
        id = Guid.NewGuid().ToString();
        this.group = group;
        this.selected = selected;
    }

    public GroupSelector(string id, string group, bool selected)
    {
        this.id = id;
        this.group = group;
        this.selected = selected;
    }

    public string id;
    public string group;
    public bool selected;
}
