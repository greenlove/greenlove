﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Clase padre para las ventanas de Editor
/// </summary>
public class InkyEditor : EditorWindow {

    protected EditorWindow window;

    protected const string CREATE_NEW_SCENE_BUTTON_LABEL = "New scene";
    protected const string BROWSE_BUTTON_LABEL = "Browse";
    protected const string LOAD_BUTTON_LABEL = "Load";
    protected const string SAVE_BUTTON_LABEL = "Save";
    protected const string SAVE_RELOAD_BUTTON_LABEL = "Save & Reload";
    protected const string SAVE_AS_BUTTON_LABEL = "Save As";
    protected const string CLEAR_SCENE_BUTTON_LABEL = "Clear scene";
    

    protected string sFeedback = "";

    
    protected string sFeedbackContainer;


    //Vector para el scroll del feedback text editor
    protected Vector2 v2fScrollPos = new Vector2();



    /// <summary>
    /// Método para dar feedback en las vistas de editor
    /// </summary>
    /// <param name="sFeed">String de feedback, si no se pasa ningún string en la llamada se hace un clear de la caja de feed</param>
    protected void showFeedback(string sFeed = "")
    {
        //Clear
        if (sFeed == "") sFeedback = "";

        //Mensaje añadido + salto de linea
        else sFeedback += sFeed + "\n";

        Repaint();
    }


    protected virtual void OnGUI()
    {
        drawFeedbackBox();
    }

    protected virtual void drawFeedbackBox()
    {
        //Caja de feedback
        v2fScrollPos = GUILayout.BeginScrollView(v2fScrollPos);
        sFeedbackContainer = GUILayout.TextArea(sFeedback, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        GUILayout.EndScrollView();
    }
}
