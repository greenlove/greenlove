﻿using Boomlagoon.JSON;
using SFB;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ScreenshotsEditor
{
    public class ScreenshotsEditor : OdinEditorWindow
    {
        public Camera camera;
        public Transform stage;
        public ColorizableController colors;

        public GameObject[] targets;

        [TabGroup("Dimensions")]
        public int width;

        [TabGroup("Dimensions")]
        public int height;

        public bool isTransparent;

        [HorizontalGroup("Path")]
        public string savePath;


        [TextArea]
        public string feedback;

        private bool isInRightScene;

        private string configPath = Path.Combine(Directory.GetCurrentDirectory(), "Assets", "Editor", "Screenshots", "screenshots_cofig.json");

        [HorizontalGroup("Configs"), Button("Save Config")]
        private void saveConfig()
        {
            JSONObject obj = new JSONObject();

            JSONArray targ = new JSONArray();
            foreach (GameObject gameObject in targets)
            {
                targ.Add(new JSONValue(gameObject.name));
            }

            obj.Add("target", targ);

            obj.Add("width", width);
            obj.Add("height", height);

            obj.Add("transparent", isTransparent);
            obj.Add("save", savePath);

            obj.Add("rig", camera.name);
            obj.Add("stage", stage.name);

            File.WriteAllText(configPath, obj.ToString());
        }


        [HorizontalGroup("Configs"), Button("Load Config")]
        private void loadConfig()
        {
            string config = File.ReadAllText(configPath);
            JSONObject obj = JSONObject.Parse(config);

            JSONArray targ = obj.GetArray("target");
            targets = new GameObject[targ.Length];
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i] = GameObject.Find(targ[i].Str);
            }

            width = (int)obj.GetNumber("width");
            height = (int)obj.GetNumber("height");

            isTransparent = obj.GetBoolean("transparent");
            savePath = obj.GetString("save");

            camera = GameObject.Find(obj.GetString("rig")).GetComponent<Camera>();
            stage = GameObject.Find(obj.GetString("stage")).transform;
        }

        [Button("Take all screenshots")]
        private void takeScreenshots()
        {
            if (!validateForScreenshots()) return;

            camera.enabled = true;

            foreach (GameObject target in targets)
            {
                ScreenshotPrepData startData = prepareForScreenshot(target);
                saveScreenshot(takeScreenshot(camera), savePath, target.name);
                endScreenshot(target, startData);
            }

            camera.enabled = false;
        }

        [Button("Take one screenshots")]
        private void takeSingleScreenshot()
        {
            saveScreenshot(takeScreenshot(camera), savePath);
        }

        [Button("Take Game Ready Screenshots")]
        private void takeGameReadyScreenshots()
        {
            if (!validateForScreenshots()) return;

            if(colors == null)
            {
                showFeedback("THe Color controller is null, please instance one to proceed");
                return;
            }

            camera.enabled = true;

            foreach (GameObject target in targets)
            {
                ScreenshotPrepData startData = prepareForScreenshot(target);

                for (int i = 0; i < colors.maxColorModes.value; i++)
                {
                    colors.changeColorMode();

                    string name = string.Format("{0}_0{1}.png", target.name.ToMayusUnderscored(), colors.currentColorMode.value);
                    string path = Path.Combine(Application.dataPath, "Sprites", "Buildings");

                    saveScreenshot(takeScreenshot(camera), path, name);
                }

                endScreenshot(target, startData);
            }

            camera.enabled = false;
            AssetDatabase.Refresh();
        }

        private void endScreenshot(GameObject target, ScreenshotPrepData startData)
        {
            target.transform.position = startData.pos;
            target.transform.rotation = startData.rot;
            target.transform.localScale = startData.scale;
            if (startData.buffSocket != null) startData.buffSocket.SetActive(true);
            if(startData.shadows.Count > 0)
            {
                foreach (GameObject shadow in startData.shadows)
                {
                    shadow.SetActive(true);
                }
            }
            camera.backgroundColor = Color.white;
        }

        private ScreenshotPrepData prepareForScreenshot(GameObject target)
        {
            ScreenshotPrepData socket = new ScreenshotPrepData();
            socket.pos = target.transform.position;
            socket.rot = target.transform.rotation;
            socket.scale = target.transform.localScale;

            target.transform.position = stage.position;
            target.transform.rotation = stage.rotation;
            target.transform.localScale = Vector3.one;

            for (int i = 0; i < target.transform.childCount; i++)
            {
                GameObject child = target.transform.GetChild(i).gameObject;
                if (child.name.Contains("Socket"))
                {
                    socket.buffSocket = child;
                    child.SetActive(false);
                }

                else if (child.name.Contains("Shadow") || child.name.Contains("Tile"))
                {
                    socket.shadows.Add(child);
                    child.SetActive(false);
                }
            }

            return socket;
        }

        [HorizontalGroup("Path"), Button("Browse")]
        public void searchPath()
        {
            StandaloneFileBrowser.OpenFolderPanelAsync("Save your screenshots", Directory.GetCurrentDirectory(), false, (string[] paths) =>
            {
                if (paths != null && paths.Length > 0) savePath = paths[0];
            });
        }


        private string showFeedback(string feed = "")
        {
            if (string.IsNullOrEmpty(feed)) feedback = "";
            else
            {
                if (!string.IsNullOrEmpty(feed)) feedback += "\n";
                feedback += feed;
            }

            return feedback;
        }

        private Texture2D takeScreenshot(Camera camera)
        {
            camera.backgroundColor = isTransparent ? Color.clear : Color.white;

            RenderTexture rt = new RenderTexture(width, height, 24);
            camera.targetTexture = rt;

            TextureFormat tFormat = isTransparent? TextureFormat.ARGB32 : TextureFormat.RGB24;

            Texture2D screenShot = new Texture2D(width, height, tFormat, false);
            camera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, camera.pixelWidth, camera.pixelHeight), 0, 0);
            camera.targetTexture = null;
            RenderTexture.active = null;

            return screenShot;
        }

        private void saveScreenshot(Texture2D texture, string path, string name = null)
        {
            byte[] bytes = texture.EncodeToPNG();
            string savedPath = Path.Combine(path, name != null? name : generateScrenshotName() + ".png");

            File.WriteAllBytes(savedPath, bytes);
            showFeedback(string.Format("Took screenshot to: {0}", savedPath));
        }

        private string generateScrenshotName()
        {
            return string.Format("Screenshot_{0}", DateTime.Now.ToString("yyMMddHHmmss"));
        }

        private bool validateForScreenshots()
        {
            showFeedback();

            isInRightScene = SceneManager.GetActiveScene().name == "Buildings";
            if (!isInRightScene)
            {
                showFeedback("The editor won't work since it's not on the right Scene");
                return false;
            }

            if (camera == null)
            {
                showFeedback("The rig is not assigned");
                return false;
            }

            if (targets == null || targets.Length <= 0)
            {
                showFeedback("The targets are null");
                return false;
            }

            return true;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected override void OnEnable()
        {
            loadConfig();
        }

        [MenuItem("Tools/Laru3D/Screenshots Tool")]
        public static void ShowWindow()
        {
            //Show existing window instance. If one doesn't exist, make one.
            EditorWindow editorWindow = EditorWindow.GetWindow(typeof(ScreenshotsEditor));
            editorWindow.autoRepaintOnSceneChange = true;
            editorWindow.Show();
            editorWindow.titleContent.text = "Flocking Pets Screenshots";
        }

    }

    class ScreenshotPrepData
    {
        public Vector3 pos;
        public Quaternion rot;
        public Vector3 scale;
        public GameObject buffSocket;
        public List<GameObject> shadows;

        public ScreenshotPrepData()
        {
            shadows = new List<GameObject>();
        }
    }
}