﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public static class BuildTargetExtensions
{
    public static BuildTargetGroup ToBuildTargetGroup(this BuildTarget target)
    {
        return mapTarget(target);
    }

    private static BuildTargetGroup mapTarget(BuildTarget target)
    {
        BuildTargetGroup group = BuildTargetGroup.Unknown;
        switch (target)
        {
            case BuildTarget.Android:
                group = BuildTargetGroup.Android;
                break;
            case BuildTarget.iOS:
                group = BuildTargetGroup.iOS;
                break;
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                group = BuildTargetGroup.Standalone;
                break;
        }
        return group;
    }
}
