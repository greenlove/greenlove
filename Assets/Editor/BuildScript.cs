﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class BuildScript
{
    static void PerformBuild()
    {
        string[] args = Environment.GetCommandLineArgs();

        Debug.Log("================== All arguments passed to buildscript: ===================");
        for (int i = 0; i < args.Length; i++)
        {
            Debug.Log(string.Format("Argument {1} in buildscript: {0}", args[i], i));
        }
        Debug.Log("================== End of arguments ===================");

        string[] platforms = args[14].Split(',');
        string version = args[15];
        Dictionary<string, bool> opts = new Dictionary<string, bool>();
        opts.Add("debug", bool.Parse(args[16]));
        opts.Add("development", bool.Parse(args[17]));
        opts.Add("fake_ads", bool.Parse(args[18]));


        prepareForBuild();

        foreach (string platform in platforms)
        {
            if (platform.ToLower() == "android") PerformAndroidBuild(version, opts);
            if (platform.ToLower() == "ios") PerformIOSBuild(version, opts);
            if (platform.ToLower() == "win64") PerformWindowsBuild(version, opts);
        }
    }

    private static void prepareForBuild()
    {
        //Delete everything under streaming assets
        if (!Directory.Exists(Application.streamingAssetsPath)) return;

        DirectoryInfo di = new DirectoryInfo(Application.streamingAssetsPath);

        foreach (FileInfo file in di.GetFiles())
        {
            if (file.FullName.Contains("save")) file.Delete();
        }
    }

    static void PerformIOSBuild(string version, Dictionary<string, bool> options)
    {
        BuildPlayerOptions opts = generateOptions(BuildTarget.iOS, generatePath("Builds", "iOS", "Automated", version), version, options);
        Build(opts);
    }

    static void PerformAndroidBuild(string version, Dictionary<string, bool> options)
    {
        PreloadAndroidSignature.call();
        increaseBundleVersionCode();
        string packageName = "flockingpets" + (PlayerSettings.Android.buildApkPerCpuArchitecture ? "" : ".aab");
        BuildPlayerOptions opts = generateOptions(BuildTarget.Android, generatePath("Builds", "Android", "Automated", version, packageName), version, options);

        if (PlayerSettings.Android.buildApkPerCpuArchitecture)
        {
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARM64;
            Build(opts);

            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7;
            Build(opts);
        }
        else
        {
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.All;
            Build(opts);
        }
    }

    private static void PerformWindowsBuild(string version, Dictionary<string, bool> options)
    {
        BuildPlayerOptions opts = generateOptions(BuildTarget.StandaloneWindows64, generatePath("Builds", "Windows", "Automated", version, "flockingpets.exe"), version, options);
        Build(opts);
    }

    static void Build(BuildPlayerOptions opts)
    {
        var buildReport = BuildPipeline.BuildPlayer(opts);
        if (buildReport.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
        {
            throw new Exception(string.Format("Build failed with status {0}", buildReport.summary.result.ToString()));
        }
    }

    static BuildPlayerOptions generateOptions(BuildTarget target, string path, string version, Dictionary<string, bool> opts)
    {
        List<string> scenes = new List<string>()
        {
            "Assets/Scenes/Preloader.unity",
            "Assets/Scenes/Cutscene.unity",
            "Assets/Scenes/CityScene.unity"
        };

        if (opts["debug"]) scenes.Add("Assets/Scenes/Buildings.unity");

        BuildPlayerOptions options = new BuildPlayerOptions();
        options.target = target;
        options.scenes = scenes.ToArray();
        options.locationPathName = path;
        options.options = opts["development"] ? BuildOptions.Development : BuildOptions.None;

        PlayerSettings.bundleVersion = version;

        BuildTargetGroup res = target.ToBuildTargetGroup();

        if (target == BuildTarget.StandaloneWindows64)
        {
            PlayerSettings.SetApiCompatibilityLevel(res, ApiCompatibilityLevel.NET_4_6);
            PlayerSettings.fullScreenMode = FullScreenMode.Windowed;
            PlayerSettings.resizableWindow = false;
        }
        else
        {
            PlayerSettings.SetApiCompatibilityLevel(res, ApiCompatibilityLevel.NET_Standard_2_0);
        }

        generateEnvVars(target, opts);

        return options;
    }

    static void generateEnvVars(BuildTarget target, Dictionary<string, bool> opts)
    {
        BuildTargetGroup res = target.ToBuildTargetGroup();

        Dictionary<string, bool> definesLib = new Dictionary<string, bool>()
        {
            { "GREENLOVE_DEBUG", opts["debug"] },
            { "ODIN_INSPECTOR", true },
            { "NO_GPGS", target == BuildTarget.iOS },
        };

        HashSet<string> defines = new HashSet<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(res).Split(';'));

        foreach (KeyValuePair<string, bool> define in definesLib)
        {
            defines.Remove(define.Key);
        }

        foreach (KeyValuePair<string, bool> define in definesLib)
        {
            if (define.Value) defines.Add(define.Key);
        }

        PlayerSettings.SetScriptingDefineSymbolsForGroup(res, string.Join(";", defines));
    }

    static string generatePath(params string[] parameters)
    {
        string leftPath = Path.Combine(Application.dataPath.Replace("/Assets", ""));
        string rightPath = Path.Combine(parameters);

        string path = Path.Combine(leftPath, rightPath);

        Debug.Log("================== OUTPUT PATH: ===================");
        Debug.Log(leftPath);
        Debug.Log(rightPath);
        Debug.Log(path);
        Debug.Log("================== End of OUTPUT PATH ===================");

        return path;
    }

    private static void increaseBundleVersionCode()
    {
        string path = Path.Combine(Application.dataPath.Replace("/Assets", ""), "Builds", "Android", "bundle_version_code.txt");
        string code = File.ReadAllText(path);
        code = Regex.Replace(code, @"\t|\n|\r", "");

        int newCode = int.Parse(code) + 1;

        PlayerSettings.Android.bundleVersionCode = newCode;

        File.WriteAllText(path, newCode.ToString());
    }
}


//Argument in buildscript: /Applications/2018.4.11f1/Unity.app/Contents/MacOS/Unity
//Argument in buildscript: -projectPath
//Argument in buildscript: /Users/usuario/.jenkins/workspace/Flocking Pets
//Argument in buildscript: -quit
//Argument in buildscript: -username
//Argument in buildscript: UNITY_USER
//Argument in buildscript: -password
//Argument in buildscript: UNITY_PASS
//Argument in buildscript: -batchmode
//Argument in buildscript: -buildTarget
//Argument in buildscript: iOS
//Argument in buildscript: -nographics
//Argument in buildscript: -executeMethod
//Argument in buildscript: BuildScript.PerformBuild
//Argument in buildscript: iOS
//Argument in buildscript: 0.9.91
//Argument in buildscript: false
