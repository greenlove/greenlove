﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

public class CopyXcodeStoryboardAssets
{
    const string XCODE_IMAGES_FOLDER = "Unity-iPhone/Images.xcassets";
    const string SOURCE_FOLDER_LOGO = "LOGO_2K_PNG.imageset";
    const string SOURCE_FOLDER_BG = "pattern.imageset";
    const string SOURCE_FOLDER_ROOT = "Storyboard/storyboard/storyboard/Assets.xcassets";

    [PostProcessBuildAttribute(1)]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string sourceLogo = $"{SOURCE_FOLDER_ROOT}/{SOURCE_FOLDER_LOGO}";
            string sourceBg = $"{SOURCE_FOLDER_ROOT}/{SOURCE_FOLDER_BG}";
            string targetPathLogo = $"{path}/{XCODE_IMAGES_FOLDER}/{SOURCE_FOLDER_LOGO}";
            string targetPathBg = $"{path}/{XCODE_IMAGES_FOLDER}/{SOURCE_FOLDER_BG}";

            FileUtil.DeleteFileOrDirectory(targetPathLogo);
            FileUtil.DeleteFileOrDirectory(targetPathBg);
            FileUtil.CopyFileOrDirectory(sourceLogo, targetPathLogo);
            FileUtil.CopyFileOrDirectory(sourceBg, targetPathBg);
        }
    }
}