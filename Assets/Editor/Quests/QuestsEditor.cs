﻿using SFB;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class QuestsEditor : OdinEditorWindow
{
    public GameplayController controller;

    [SerializeField, TableList, ListDrawerSettings(CustomAddFunction = "AddNewQuest"), PropertyOrder(int.MaxValue)]
    private List<QuestsEditorListItem> questsList;

    [SerializeField, TabGroup("Tabs", "Migrate Quest")]
    private List<QuestType> migratableTypes;

    [SerializeField, TabGroup("Tabs", "Migrate Quest")]
    private QuestType toMigrateType;

    [SerializeField, TabGroup("Tabs", "Migrate Quest")]
    private int migratedTicks;

    private float orderListHandleOffset = 20;

    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/New Quests"), SerializeField]
    private int newQuestsAmount = 0;

    private void AddNewQuest()
    {
        questsList.Add(new QuestsEditorListItem(this, questsList.Count));
    }


    [MenuItem("Window/Quests Editor")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow editorWindow = EditorWindow.GetWindow(typeof(QuestsEditor));
        editorWindow.autoRepaintOnSceneChange = true;
        editorWindow.Show();
        editorWindow.titleContent.text = "Flocking Pets Quests";
    }

    protected override void OnEnable()
    {
        populateQuestsList();
    }

    private void populateQuestsList()
    {
        GameObject director = GameObject.Find("Main");
        Main main = director.GetComponent<Main>();
        main.initHash();
        controller = main.GetComponent<Main>().getController<GameplayController>();

        refreshQuestsList();
    }

    private void refreshQuestsList()
    {
        if (questsList == null) questsList = new List<QuestsEditorListItem>();
        else questsList.Clear();

        foreach (TutorialQuest quest in controller.tutorialManager.tutorialQuests)
        {
            addNewTutorialQuest(quest);
        }
    }

    private void addNewTutorialQuest(TutorialQuest quest)
    {
        QuestsEditorListItem item = new QuestsEditorListItem(this, questsList.Count);
        item.questType = quest.type;
        item.ticks = quest.tickUntil;
        item.rewardType = quest.reward;
        item.rewardAmount = quest.rewardAmount;

        questsList.Add(item);
    }

    
    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/New Quests"), Button("Create New Quests")]
    private void drawQuestsGenerator()
    {
        for (int i = 0; i < newQuestsAmount; i++)
        {
            TutorialQuest quest = controller.tutorialManager.bakeRandomTutorialQuest();
            addNewTutorialQuest(quest);
        }
    }

    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/Csv Endpoints"), Button("Import CSV")]
    private void importQuestsFromCsv()
    {
        StandaloneFileBrowser.OpenFilePanelAsync("Open Csv", "", new ExtensionFilter[] { new ExtensionFilter("Csv", "csv") }, false, (string[] paths) => 
        {
            questsList.Clear();

            foreach (QuestsEditorListItem item in CSV.Parse<QuestsEditorListItem>(paths[0]))
            {
                questsList.Add(item);
            }

            Repaint();
        });
    }

    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/Csv Endpoints"), Button("Export CSV")]
    private void exportCsv()
    {
        string csv = CSV.create(QuestsEditorListItem.getCSVHeaders(",").Split(','), questsList.ToArray());
        StandaloneFileBrowser.SaveFilePanelAsync("Save quests file", "", string.Format("quests_{0}", DateTime.Now.ToString("yyMMddHHmmss")), new ExtensionFilter[] { new ExtensionFilter("Csv", "csv") }, (string path) =>
        {
            File.WriteAllText(path, csv);
        });
    }

    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/Redo"), Button("Redo FTUE Quests List")]
    private void redoFTUEQuestsList()
    {
        //Remove all quests
        controller.tutorialManager.tutorialQuests.items.Clear();
        string bakedPath = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "Baked");
        string[] bakedQuests = Directory.GetFiles(bakedPath);
        foreach (string bakedQuest in bakedQuests)
        {
            AssetDatabase.DeleteAsset(bakedQuest);
        }

        //Redo the list
        string ftuePath = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "FTUE");
        string[] ftueQuests = Directory.GetFiles(ftuePath);
        foreach (string ftueQuest in ftueQuests)
        {
            TutorialQuest quest = AssetDatabase.LoadAssetAtPath<TutorialQuest>(ftueQuest);
            if(quest != null) controller.tutorialManager.tutorialQuests.items.Add(quest);
        }

        refreshQuestsList();
        Repaint();
    }

    [TabGroup("Tabs", "Create & Import"), HorizontalGroup("Tabs/Create & Import/Redo"), Button("Redo FTUE & Baked Quests List")]
    private void redoFTUEAndBakedQuests() 
    {
        controller.tutorialManager.tutorialQuests.items.Clear();

        string ftuePath = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "FTUE");
        string[] ftueQuests = Directory.GetFiles(ftuePath);
        foreach (string ftueQuest in ftueQuests)
        {
            TutorialQuest quest = AssetDatabase.LoadAssetAtPath<TutorialQuest>(ftueQuest);
            if (quest != null) controller.tutorialManager.tutorialQuests.items.Add(quest);
        }

        string bakedPath = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "Baked");
        string[] bakedQuests = Directory.GetFiles(ftuePath);
        foreach (string bakedQuest in bakedQuests)
        {
            TutorialQuest quest = AssetDatabase.LoadAssetAtPath<TutorialQuest>(bakedQuest);
            if (quest != null) controller.tutorialManager.tutorialQuests.items.Add(quest);
        }

        refreshQuestsList();
        Repaint();
    }

    internal void saveQuest(QuestsEditorListItem item)
    {
        TutorialQuest quest = loadQuest(questsList.IndexOf(item).ToString());

        if (quest == null)
        {
            return;
            //quest = controller.tutorialManager.createEmptyTutorialQuest();
        }

        saveQuest(quest, item);
    }

    private TutorialQuest loadQuest(string questName)
    {
        TutorialQuest quest = null;
        string path = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "FTUE", questName);

        quest = AssetDatabase.LoadAssetAtPath<TutorialQuest>(path);

        if (quest == null)
        {
            path = Path.Combine("Assets", "Scripts", "Gameplay", "Tutorials", "Quests", "Baked", questName);
            quest = AssetDatabase.LoadAssetAtPath<TutorialQuest>(path);
        }

        return quest;
    }

    private void saveQuest(TutorialQuest quest, QuestsEditorListItem item = null)
    {
        if (item != null)
        {
            quest.type = item.questType;
            quest.tickUntil = item.ticks;
            quest.reward = item.rewardType;
            quest.rewardAmount = item.rewardAmount;
        }

        quest.name = controller.tutorialManager.tutorialQuestAssetName(quest);

        EditorUtility.SetDirty(quest);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [Button("Migrate Types"), TabGroup("Tabs", "Migrate Quest")]
    private void migrateQuestTypes()
    {
        if (!canMigrate()) return;

        foreach (TutorialQuest quest in controller.tutorialManager.tutorialQuests)
        {
            if(migratableTypes.Contains(quest.type))
            {
                TutorialQuest asset = loadQuest(controller.tutorialManager.tutorialQuestAssetName(quest));
                if (asset != null)
                {
                    Debug.Log(string.Format("Quest with name {0} is NOT NULL", quest.name));
                    asset.type = toMigrateType;
                    quest.type = toMigrateType;
                    saveQuest(asset);
                }
                else
                {
                    Debug.Log(string.Format("Quest with name {0} is NULL", quest.name));
                }
            }
        }
    }

    [Button("Migrate Ticks"), TabGroup("Tabs", "Migrate Quest")]
    private void migrateQuestTicks()
    {
        if (!canMigrate()) return;

        foreach (TutorialQuest quest in controller.tutorialManager.tutorialQuests)
        {
            if (migratableTypes.Contains(quest.type))
            {
                TutorialQuest asset = loadQuest(controller.tutorialManager.tutorialQuestAssetName(quest));
                if (asset != null)
                {
                    asset.tickUntil = migratedTicks;
                    quest.tickUntil = migratedTicks;
                    saveQuest(asset);
                }
            }
        }
    }

    private bool canMigrate()
    {
        if (migratableTypes == null || migratableTypes.Count == 0) return false;

        if (toMigrateType == QuestType.NONE || toMigrateType == QuestType.COUNT) return false;

        return true;
    }
}
