﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestsEditorListItem : ICSVRow
{
    [SerializeField, ReadOnly, TableColumnWidth(30, false)]
    private int i;

    public QuestType questType;
    public double ticks;
    public TutorialQuestRewards rewardType;
    public double rewardAmount;

    private QuestsEditor editor;

    [Button("Save If existed previously"), ShowIf("editor", null)]
    public void saveQuest()
    {
        editor.saveQuest(this);
    }


    public QuestsEditorListItem(QuestsEditor editor, int index)
    {
        this.editor = editor;
        i = index;
    }

    public QuestsEditorListItem()
    {
    }


    public static string getCSVHeaders(string separator = ";")
    {
        return "questType" + separator + "ticks" + separator + "rewardType" + separator + "rewardAmount";
    }

    string ICSVRow.getCSVHeaders()
    {
        return getCSVHeaders();
    }

    public void parse(Dictionary<string, string> row)
    {
        Enum.TryParse<QuestType>(row["questType"], out questType);
        double.TryParse(row["ticks"], out ticks);
        Enum.TryParse<TutorialQuestRewards>(row["rewardType"], out rewardType);
        double.TryParse(row["rewardAmount"], out rewardAmount);
    }

    public string toCSVString(string separator = ";")
    {
        return string.Format("{0}" + separator + "{1}" + separator + "{2}" + separator + "{3}", questType.ToString(), ticks, rewardType.ToString(), rewardAmount.ToString(System.Globalization.CultureInfo.InvariantCulture));
    }
}
