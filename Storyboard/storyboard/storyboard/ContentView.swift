//
//  ContentView.swift
//  storyboard
//
//  Created by usuario on 05/04/2020.
//  Copyright © 2020 com. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
